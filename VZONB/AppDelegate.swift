//
//  AppDelegate.swift
//  VZONB
//
//  Created by Phuc on 5/10/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import AWSCore
import AWSCognito
import STPopup
import Firebase
import UserNotifications
import FirebaseMessaging
import Fabric
import Crashlytics

func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
#if DEBUG
    Swift.print(items, separator: separator, terminator: terminator)
#endif
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //Facebook SDK
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        ///
        IQKeyboardManager.shared.enable = true

        /// Firebase
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                Setting.shared.setFirebase(token: result.token)
            }
        }
        ///
        self.configNavigation()
        // Check login
        if !Setting.shared.getToken().isEmpty && Setting.shared.getSave() {
            self.showHomePage()
        }
        
        //Config AWS server
        let credentialProvider = AWSStaticCredentialsProvider(accessKey: "AKIAI6ZVX3GUX5SCUT4Q", secretKey: "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")
        let configuration = AWSServiceConfiguration(region: .USEast2, credentialsProvider: credentialProvider)
        configuration?.timeoutIntervalForResource = 600
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        if #available(iOS 11, *) {
            if self.window?.safeAreaInsets.top ?? 0 > 0 {
                Utility.shared.isPhoneX = true
            }
        }
        
        return true
    }
    
    private func configNavigation() {
        let navi = UINavigationBar.appearance()
        ///
        navi.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: Font.header]
        navi.tintColor = UIColor.black
        navi.barTintColor = UIColor(rgb: 0xF9F9F9) //UIColor.white
        ///
        navi.isTranslucent = false
        //St popup config
        let popup = STPopupNavigationBar.appearance()
        popup.barTintColor = Color.default
        popup.tintColor    = UIColor.white
        popup.barStyle     = .default
        popup.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: Font.header]
        
    }
    
    //MARK: - Universal Link
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let url = userActivity.webpageURL,
            let components = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
                return false
        }
        
        if components.path.contains("record") && !Setting.shared.getToken().isEmpty && Setting.shared.getSave() {

            DeepLinkParser.shared.parse(link: url.absoluteString)
            
            return true
        }
        
        return false
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        Messaging.messaging().shouldEstablishDirectChannel = false
        if !Setting.shared.getToken().isEmpty {
            VZOSocketManager.shared.socket.disconnect()
            VZOSocketManager.shared.isAuthened = false
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        if !Setting.shared.getToken().isEmpty {
            VZOSocketManager.shared.connect()
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    func applicationWillTerminate(_ application: UIApplication) { }
    
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        Setting.shared.setFirebase(token: fcmToken)
        UserService.shared.setFirebase(token: fcmToken, callback: { status in
            switch status {
            case "200":
                break
            default:
//                MessageManager.shared.show(title: "Sorry", body: "Something went wrong! Please try again later", style: MessageManager.Style.error)
                break
            }
        })
    }
}
extension AppDelegate {
    func showHomePage() {
        if let homeNavigation = StoryBoard.Home.viewController("HomeTabBar") as? HomeTabBarViewController {
            self.window?.rootViewController = homeNavigation
            self.window?.makeKeyAndVisible()
        }
    }
    
    @objc func showLoginPage() {
        if let loginPage = StoryBoard.User.viewController("LandingNavigation") as? UINavigationController {
            self.window?.rootViewController = loginPage
            self.window?.makeKeyAndVisible()
        }
    }
    func knockBackBanedUser(with message: String) {
        let topVc = UIApplication.topViewController()
        topVc?.showMessageOkWithCompletion(with: "Notice", message: message, completion: { [weak self] in
            topVc?.dismiss(animated: false, completion: nil)
            guard let `self` = self else { return }
            self.showLoginPage()
        })
    }
}

extension AppDelegate: MessagingDelegate {
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    public func application(received remoteMessage: MessagingRemoteMessage) {
        print("Func 1: ", remoteMessage.appData)
    }
    
    func tokenRefreshNotification(_ notification: Notification) {
        InstanceID.instanceID().instanceID { (result, error) in
            guard let refreshedToken: String = result?.token else {
                print("error: \(error?.localizedDescription ?? "can't get error!")")
                return
            }
            print(refreshedToken)
            Setting.shared.setFirebase(token: refreshedToken)
            UserService.shared.setFirebase(token: refreshedToken, callback: { status in
                switch status {
                case "200":
                    break
                default:
                    break
                }
            })
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    func connectToFcm() {
        Messaging.messaging().shouldEstablishDirectChannel = false
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        if let chatKey = userInfo["chatGroupKey"] as? String, let detailChat = StoryBoard.Message.viewController("MessengerDetailViewController") as? MessengerDetailViewController {
            let data = chatKey.components(separatedBy: "_")
            if data.count > 1 {
                detailChat.idUserChatting = data[0] == Setting.shared.getUserId() ? data[1] : data[0]
            }
            detailChat.chatGroupId = chatKey
            UIApplication.topViewController()?.navigationController?.pushViewController(detailChat, animated: true)
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
}
