//
//  CircleView.swift
//  VZONB
//
//  Created by Phuc on 6/3/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class CircleImage: UIImageView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.layer.bounds.width / 2
        self.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.layer.bounds.width / 2
        self.clipsToBounds = true
    }
}

class CircleButton: UIButton {
    
    @IBInspectable var isShadow: Bool = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    
    private func setup () {
        self.layoutIfNeeded()
        //Corner Radius
        let radiusSize: CGFloat = min(self.layer.bounds.height, self.layer.bounds.width) / 2
        self.layer.cornerRadius = radiusSize
        if self.isShadow {
            ///Shadow
            self.layer.shadowColor = UIColor.black.withAlphaComponent(0.7).cgColor
            self.layer.shadowOffset = CGSize(width: 0, height: 0)
            self.layer.shadowRadius = 5
            self.layer.shadowOpacity = 1
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if !self.isShadow {
            self.layer.shadowOpacity = 0
        }
    }
}

class CircleView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.layer.bounds.height / 2
//        self.clipsToBounds = true
        ///Shadow
        self.layer.shadowColor = UIColor.black.withAlphaComponent(0.7).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 1
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.layer.bounds.height / 2
//        self.clipsToBounds = true
        
        ///Shadow
        self.layer.shadowColor = UIColor.black.withAlphaComponent(0.7).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 1
    }
}

class ThinRadiusView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 3
        self.clipsToBounds = true
    }
}


//MARK: - GradientView
class GradientView: UIView {
    override class var layerClass: AnyClass { return CAGradientLayer.self }
    var gradient: CAGradientLayer { return layer as! CAGradientLayer }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    private func setup() {
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        gradient.colors = [UIColor.black.withAlphaComponent(0.1).cgColor, UIColor.black.withAlphaComponent(0.01).cgColor]
        gradient.locations = [0.4 , 1.0]
        gradient.startPoint = CGPoint(x: 0.5, y: 1)
        gradient.endPoint = CGPoint(x: 0.5, y: 0)
//        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
    }
}












