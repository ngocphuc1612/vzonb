//
//  IndentTextField.swift
//  OnBussinessProject
//
//  Created by Phuc on 1/5/17.
//  Copyright © 2017 Nguyen Xuan Thai. All rights reserved.
//

import UIKit

class IndentTextField: UITextField {
    
    @IBInspectable var indentY: CGFloat = 4
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 15, dy: indentY)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return self.textRect(forBounds: bounds)
    }
}
