//
//  LyricHandler.swift
//  VZONB
//
//  Created by PT on 3/26/18.
//  Copyright © 2018 Phuc. All rights reserved.
//

import Foundation

class LyricHandler {
    class func parseLyric(from strings: String) -> (sentences: Array<String>, timings: Array<Array<Double>>) {
        //
        if strings.isEmpty { return ([], []) }
        //
        var sentences: Array<String> = []
        var sentence: String = ""
        var timings: Array<Array<Double>> = []
        var timing: Array<Double> = []
        var startTime: Double = 0
        let arrStrings = strings.components(separatedBy: "\r\n").map({ $0.trimmingCharacters(in: CharacterSet.whitespaces) })
        for str in arrStrings {
            if str.contains("\\n") {
                self.addWordAndTime(word: str.replacingOccurrences(of: "\\n", with: ""), startTime: &startTime, sentence: &sentence, timing: &timing, timings: &timings)
                sentences.append(sentence)
                sentence = ""
                timings.append(timing)
                timing = []
            } else {
                self.addWordAndTime(word: str, startTime: &startTime, sentence: &sentence, timing: &timing, timings: &timings)
            }
        }
        if !sentence.isEmpty{
            sentences.append(sentence)
            timings.append(timing)
        }
        timings[timings.count - 1].append(timings[timings.count - 1].last ?? 0)
        sentences.insert(" ", at: 0)
        timings.insert([timings[0][0]], at: 0)
        return (sentences, timings)
    }
    class func addWordAndTime(word: String, startTime: inout Double, sentence: inout String, timing: inout Array<Double>, timings: inout Array<Array<Double>>) {
        // Get array values
        let timeLyricStringArray: Array<String> = word.components(separatedBy: " ")
        if timeLyricStringArray.count < 2 { return }
        
        // Get time
        var tempTime: Double = 0
        if timeLyricStringArray[0].contains(":") {
            let arrs: Array<String> = timeLyricStringArray[0].components(separatedBy: ":")
            if arrs.count > 1 {
                tempTime = ((Double(arrs[0]) ?? 0) * 60) + (Double(arrs[1]) ?? 0)
            }
        } else {
            tempTime = Double(timeLyricStringArray[0]) ?? 0
        }
        
        // Add string
        if sentence.isEmpty {
            sentence = timeLyricStringArray[1]
            timing.append(tempTime)
            if timings.count > 0 {
                timings[timings.count - 1].append(tempTime - startTime)
            }
        } else {
            sentence += " \(timeLyricStringArray[1])"
            timing.append(tempTime - startTime)
        }
        startTime = tempTime
    }
}
