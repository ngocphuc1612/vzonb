//
//  MediaPlayView.swift
//  VZONB
//
//  Created by Phuc on 7/8/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import AVFoundation

class MediaPlayView: UIView {

    @IBOutlet weak var buttonPlay: UIButton!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelCurrentTime:    UILabel?
    @IBOutlet weak var viewCamera   : UIView!
    @IBOutlet weak var viewAudio    : UIView?
    @IBOutlet weak var imageCover: UIImageView!
    @IBOutlet weak var sliderCurrentTime : UISlider?
    @IBOutlet weak var constraintBottomSlider: NSLayoutConstraint?
    @IBOutlet weak var viewSlider   : UIView?
    ///
    let activity = UIActivityIndicatorView()
    ///
//    fileprivate var audioPLayer: AVAudioPlayer?
    private var avplayer: AVPlayer?
    private var avplayerLayer: AVPlayerLayer?
//    var canPlay: Bool = false
    var isPlayed: Bool = false
//    private var observerContext = 0
    fileprivate var isLoaded: Bool = false
    fileprivate var isVideo : Bool = false
    fileprivate var isInteruption: Bool = false
    
    fileprivate var duration: Int = 0
    fileprivate var secs    : Int = 0
    fileprivate var minutes : Int = 0
    
    fileprivate var timer: Timer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.buttonPlay.isHidden = true
        self.viewAudio?.isUserInteractionEnabled = false
        self.viewAudio?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onPlayPause(_:))))
        self.sliderCurrentTime?.setThumbImage(UIImage(named: "thumb"), for: UIControlState.normal)
        NotificationCenter.default.addObserver(self, selector: #selector(self.interruptionNotification(_:)), name: NSNotification.Name.AVAudioSessionInterruption, object: nil)
        self.activity.center = self.viewAudio?.center ?? CGPoint.zero
        self.viewAudio?.addSubview(self.activity)
        self.activity.startAnimating()
        ///
        self.avplayerLayer = AVPlayerLayer()
        self.avplayerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.viewCamera.layer.addSublayer(self.avplayerLayer!)
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        
    }
    
    @objc func interruptionNotification(_ notification: Notification) {
        guard let type = notification.userInfo?[AVAudioSessionInterruptionTypeKey] as? UInt,
            let interruption = AVAudioSessionInterruptionType(rawValue: type) else {
                return
        }
//        if interruption == .ended && playerWasPlayingBeforeInterruption {
//            player.replaceCurrentItem(with: AVPlayerItem(url: radioStation.url))
//            play()
//        }
        
        if interruption == .began {
            self.timer?.invalidate()
            self.timer = nil
            self.sliderCurrentTime?.isUserInteractionEnabled = false
            self.isInteruption = true
            self.activity.startAnimating()
        } else {
            self.activity.stopAnimating()
            self.isInteruption = false
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.runTime), userInfo: nil, repeats: true)
            self.sliderCurrentTime?.isUserInteractionEnabled = true
            self.avplayer?.play()
        }
        
    }
    
    func preparePlayWith(link: String, type: String = "audio", imageLink: String = "") {
        self.imageCover.kf_setImage(url: imageLink)
        let isVideo = type == "audio" ? false : true
        guard let url: URL = URL(string: link) else { return }
        if isVideo {
            self.imageCover.isHidden = true
        } else {
            self.viewCamera.isHidden = true
        }
        
        DispatchQueue.global().async { [weak self] in
            guard let `self` = self else { return }
            let item = AVPlayerItem(url: url)
            self.avplayer = AVPlayer(playerItem: item)
            if #available(iOS 10.0, *) {
                self.avplayer?.automaticallyWaitsToMinimizeStalling = false
            } else {
                // Fallback on earlier versions
            }
            
            self.avplayer?.isMuted = false
            self.avplayerLayer?.player = self.avplayer
            ///
            self.duration = Int((self.avplayer?.currentItem?.asset.duration ?? kCMTimeZero).seconds)
            DispatchQueue.main.async {
                self.avplayer?.addObserver(self, forKeyPath: "status", options: [.new, .old, .initial], context: nil)
                self.isLoaded = true
                self.labelTime.time(totalSecs: self.duration, isPositive: false)  //strTime
                self.sliderCurrentTime?.maximumValue = Float(self.duration)
                
            }
        }
//        }
        
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        guard context == &observerContext else {
//            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
//            return
//        }
        // look at `change![.newKey]` to see what the status is, e.g.
        if keyPath == "status" {

            guard let status = self.avplayer?.status else { return }

            if status == .readyToPlay {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: { [weak self] in
                    guard let `self` = self else { return }
                    self.buttonPlay.isHidden = false
                    self.viewAudio?.isUserInteractionEnabled = true
                    self.activity.stopAnimating()
                })
            } else if status == .unknown {
                print("Unknow status")
            } else if status == .failed {
                print("Failed")
            }
        }
    }
    
    func destroySelf() {
        self.stop()
        self.avplayer?.replaceCurrentItem(with: nil)
        if self.isLoaded {
            self.avplayer?.removeObserver(self, forKeyPath: "status")
        }
        NotificationCenter.default.removeObserver(self)
        self.avplayer = nil
        self.avplayerLayer = nil
    }
    
    @IBAction func onChangeTime(_ sender: UISlider) {
        // Droped
        NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.visibleControl), object: false)
        NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.visibleControl), object: true)
        
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
        
        if !sender.isTracking {
            let currentSecs: Int = Int(sender.value)
            self.secs       = currentSecs % 60
            self.minutes    = currentSecs / 60
            self.labelTime.time(totalSecs: currentSecs, isPositive: false)
            self.labelCurrentTime?.time(totalSecs: self.duration - currentSecs)
            self.avplayer?.seek(to: CMTime(seconds: Double(currentSecs), preferredTimescale: 1))
            self.avplayer?.play()
            //Continue play
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.runTime), userInfo: nil, repeats: true)
            self.buttonPlay.isHidden = true
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if self.avplayerLayer != nil {
            self.avplayerLayer?.frame = self.bounds
            self.activity.center = self.viewAudio?.center ?? CGPoint.zero
        }
    }
    @IBAction func onPlayPause(_ sender: Any?) {
        //Check interuption
        guard self.isInteruption == false else { return }
        if (self.viewSlider?.isHidden ?? false) && self.isPlayed {
            NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.visibleControl), object: false)
            return
        }
        if self.isPlayed {
            self.isPlayed = false
            NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.visibleControl), object: false)
            self.buttonPlay.isHidden = false
            self.timer?.invalidate()
            self.timer = nil
            self.avplayer?.pause()
        } else {
            self.isPlayed = true
            NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.visibleControl), object: true)
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.runTime), userInfo: nil, repeats: true)
            self.buttonPlay.isHidden = true
            self.avplayer?.play()
        }
    }
    ///
    func stop () {
        self.timer?.invalidate()
        self.timer = nil
        self.buttonPlay.isHidden = false
        self.avplayer?.seek(to: CMTime(seconds: 0, preferredTimescale: 1))
        self.secs = 0
        self.minutes = 0
        self.labelCurrentTime?.time(totalSecs: 0)
        self.sliderCurrentTime?.value = 0
        self.avplayer?.pause()

    }
    ///
    @objc func runTime () {
        
        if self.isVideo && !(self.avplayerLayer?.isReadyForDisplay ?? false) {
            return
        }
        
        self.secs += 1
        if self.secs > 59 {
            self.secs = 0
            self.minutes += 1
        }
        self.sliderCurrentTime?.value = Float(self.minutes * 60 + self.secs)
        self.labelCurrentTime?.time(totalSecs: Int(self.sliderCurrentTime?.value ?? 0))
        
        let estimate = self.duration - (self.minutes * 60 + self.secs)
        if estimate >= 0 {
            self.labelTime.time(totalSecs: estimate, isPositive: false)
        } else {
            self.timer?.invalidate()
            self.timer = nil
            self.isPlayed = false
            NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.visibleControl), object: false)
            self.stop()
        }
        
    }
}
