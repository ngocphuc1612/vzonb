//
//  TablePopup.swift
//  VZONB
//
//  Created by Phuc on 6/25/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

protocol DismissPopover: class {
    func dismiss(reportType: ReportType)
}

final class TablePopup: UIView {
    
    fileprivate let tableView: UITableView = UITableView()
    fileprivate var reports: Array<Report> = Array<Report>()
    //Delegate will be assigned at HomePageCell.swift
    weak var delegate: DismissPopover? = nil
    ///
    init(frame: CGRect, data: Array<Report>) {
        self.reports = data
        super.init(frame: frame)
        self.backgroundColor = UIColor(rgb: 0xe8e8e8)
        self.setupTableView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor(rgb: 0xe8e8e8)
        self.setupTableView()
    }
    
    private func setupTableView() {
        self.tableView.register(UINib(nibName: "TablePopupCell", bundle: nil), forCellReuseIdentifier: "tablePopupCell")
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.separatorStyle = .none
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(self.tableView)
        //Constraints
        let top: NSLayoutConstraint = NSLayoutConstraint(item: self.tableView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0)
        let left: NSLayoutConstraint = NSLayoutConstraint(item: self.tableView, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.left, multiplier: 1.0, constant: 0)
        let right: NSLayoutConstraint = NSLayoutConstraint(item: self.tableView, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: 0)
        let bottom: NSLayoutConstraint = NSLayoutConstraint(item: self.tableView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0)
        self.addConstraints([top, left, right, bottom])
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isScrollEnabled = false
    }
}

extension TablePopup: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TablePopupCell = tableView.dequeueReusableCell(withIdentifier: "tablePopupCell", for: indexPath) as! TablePopupCell
        cell.bindingUIs(imageName: self.reports[indexPath.row].imageName, title: self.reports[indexPath.row].title, description: self.reports[indexPath.row].description)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Post Noti to HomeViewController.swift
        NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.report), object: self.reports[indexPath.row])
        // Call dismiss Popover from HomePageCell.swift
        self.delegate?.dismiss(reportType: self.reports[indexPath.row].type)
        self.delegate = nil
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 51
    }
}
