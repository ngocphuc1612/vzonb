//
//  TablePopupCell.swift
//  VZONB
//
//  Created by Phuc on 6/25/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class TablePopupCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var imageIcon: UIImageView!
    
    override func prepareForReuse() {
        self.labelDescription.text = ""
        super.prepareForReuse()
    }
    
    func bindingUIs(imageName: String, title: String, description: String) {
        self.imageIcon.image = UIImage(named: imageName)
        self.labelTitle.text = title
        self.labelDescription.text = description
    }
}
