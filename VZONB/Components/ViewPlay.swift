//
//  ButtonPlay.swift
//  VZONB
//
//  Created by Phuc on 7/12/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ViewPlay: UIView {
    // Stroke circle white
    private var maskShape: CAShapeLayer = CAShapeLayer()
    private var wrapLayer: CALayer = CALayer()
    private var layerBehind: CAShapeLayer = CAShapeLayer()
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // The circle wrap all content
        let path: UIBezierPath = UIBezierPath(arcCenter: CGPoint(x: rect.midX, y: rect.midY), radius: (rect.width - 8) / 2, startAngle: CGFloat(Double.pi / 2), endAngle: CGFloat(5 * Double.pi / 2), clockwise: true)
        self.maskShape.frame = self.bounds
        self.maskShape.path = path.cgPath
        self.maskShape.fillColor = UIColor.clear.cgColor
        self.maskShape.strokeColor = UIColor.blue.cgColor
        self.maskShape.lineWidth = 5
        // Wrap layer
        self.wrapLayer.frame = self.bounds
        self.wrapLayer.backgroundColor = UIColor.white.cgColor //UIColor(rgb: 0x555555).cgColor
        self.wrapLayer.addSublayer(self.maskShape)
        self.layer.mask = self.maskShape
        // Add ->
        self.layer.addSublayer(self.wrapLayer)
        /// This layer will run belong to beat duration
        self.layerBehind.frame = self.bounds
        let otherPath: UIBezierPath = UIBezierPath(arcCenter: CGPoint(x: rect.midX, y: rect.midY), radius: (rect.width - 8) / 2, startAngle: CGFloat(-Double.pi / 2), endAngle: CGFloat(3 * Double.pi / 2), clockwise: true)
        self.layerBehind.path = otherPath.cgPath
        self.layerBehind.fillColor = UIColor.clear.cgColor
        self.layerBehind.strokeColor = Color.default.cgColor
        self.layerBehind.lineWidth = 5
        self.layer.insertSublayer(self.layerBehind, below: self.maskShape)
        self.layerBehind.strokeEnd = 0
        
    }
    // // //
    func downloaded (percent: CGFloat) {
        self.layerBehind.strokeEnd = percent
    }
    
    func downloadCompleted() {
        self.layerBehind.strokeEnd = 0
    }
    
    // // //
    func startRecord(duration: Float) {
        
        let animation: CABasicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        
        animation.fromValue = 0.0001
        animation.toValue = 0.9999
        animation.duration = CFTimeInterval(duration)
        
        self.layerBehind.add(animation, forKey: "stokeEnd")
    }
    
    func resumeAnimation() {
        let pauseTime = self.layerBehind.timeOffset
        self.layerBehind.speed = 1.0
        self.layerBehind.timeOffset = 0.0
        self.layerBehind.beginTime = 0.0
        self.layerBehind.beginTime = self.layerBehind.convertTime(CACurrentMediaTime(), from: nil) - pauseTime
    }
    
    func pauseAnimation() {
        let pauseTime = self.layerBehind.convertTime(CACurrentMediaTime(), from: nil)
        self.layerBehind.speed = 0.0
        self.layerBehind.timeOffset = pauseTime
    }
    
    func stopAnimation() {
        self.layerBehind.removeAllAnimations()
    }
    
}
