//
//  Constant.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

struct Color {
    static let `default`: UIColor = UIColor(rgb: 0x017DBE)  //.//067DC3
    static let inactive : UIColor = UIColor(rgb: 0xC1C1C1)
    static let notVote  : UIColor = UIColor(rgb: 0x565656)
    static let emptyData: UIColor = UIColor(rgb: 0x888888)
    static let thumb_not_vote: UIColor = UIColor(rgb: 0x929292)
}

struct Link {
    static let TermOfServiceUrl: String = "https://schwaye.com/termofservice"
    static let PrivacyUrl      : String = "https://schwaye.com/policy"
}


struct Font {
    static let emptyData: UIFont = UIFont(name: "SanFranciscoDisplay-Semibold", size: 17)!
    static let header   : UIFont = UIFont(name: "SanFranciscoDisplay-Semibold", size: 17)!
}

struct NotiName {
    static let login        : String = "showLogin"
    static let report       : String = "listenReport"
    static let search       : String = "searchString"
    static let seemore      : String = "seemore"
    static let homeUpdate   : String = "homeUpdate"
    static let uploadResult : String = "uploadResult"
    static let doneRecord   : String = "doneRecord"
    static let posted       : String = "posted"
    static let updateProfile: String = "updateProfile"
    static let updateNoti   : String = "updateNoti"
    static let updateMessage: String = "updateMessage"
    static let updateBadge : String = "updateBadge"
    static let visibleControl : String = "visibleControl"
    static let posting      : String = "recordPosting"
    struct Noti {
        static let seen : String = "message_seen"
    }
    
    //MARK: - Chat
    static let chatSendResult: String = "chatSendResult"
    static let receiveMessage: String = "receiveMessage"
//    static let chatReceiveMessage: String = "chatReceivedMessage"
}

enum MediaType: String {
    case audio
    case video
    
    // Init with extension string
    init(ext: String) {
        if ext == "m4a" {
            self = MediaType.audio
        } else {
            self = MediaType.video
        }
    }
}
