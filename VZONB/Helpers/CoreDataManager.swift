//
//  CoreDataManager.swift
//  VZONB
//
//  Created by PT on 8/11/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager: NSObject {
    
    static let shared = CoreDataManager()
    var documentURL : URL { return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! }
    
    override init() {}
    
    func delAfterDay() {
//        var records = Array<Record>()
        DispatchQueue.global().async {
            let req:NSFetchRequest<NSManagedObject> = NSFetchRequest(entityName: "Recorded")
            do
            {
                let context = DataManager().managedObjectContext
                let contactsRusult = try context.fetch(req)
                for sp in contactsRusult
                {
                    let record = Record(perStore: sp)
                    if Utility.shared.needDelete(record.createdAt) {
                        DispatchQueue.main.async {
                            do {
                                try FileManager.default.removeItem(at: self.documentURL.appendingPathComponent(record.beatPath))
                                try FileManager.default.removeItem(at: self.documentURL.appendingPathComponent(record.voicePath))
                                if record.type == "video" {
                                    if let path = record.videoPath {
                                        try FileManager.default.removeItem(at: self.documentURL.appendingPathComponent(path))
                                    }
                                }
                            } catch let err {
                                print(err.localizedDescription)
                            }
                        }
                        context.delete(sp)
                    }
                }
                try context.save()
            }
            catch let err
            {
                print("Loi \(err.localizedDescription)")
            }
        }
        
        
    }
    
}
