//
//  Extension.swift
//  Play mp3
//
//  Created by Phuc on 5/2/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import Kingfisher
import Foundation

enum StoryBoard: String {
    
    case Sing = "Sing"
    case Home = "Home"
    case User = "User"
    case Search = "Search"
    case Message = "Message"
    func viewController(_ name: String) -> UIViewController {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main).instantiateViewController(withIdentifier: name)
    }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

//MARK: - UIViewController
extension UIViewController {
    func showPopup(title: String, description: String = "", imageName: String = "", okTitle: String = "Ok", cancelTitle: String = "", _ completion: (() -> Void)? = nil) {
        if let popup: AlertViewController = StoryBoard.User.viewController("AlertViewController") as? AlertViewController {
            popup.setContents(title: title, description: description, imageName: imageName, okTitle: okTitle, cancelTitle: cancelTitle)
            popup.completion = completion
            
            popup.modalPresentationStyle = .overFullScreen
            self.present(popup, animated: false, completion: nil)
        }
    }
    
    func showMessageForUser(with message: String, title: String = "Notice", okTitle: String = "Ok") {
        
        let alert:UIAlertController = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.alert)
        let alertActionOk:UIAlertAction = UIAlertAction(title: okTitle, style: UIAlertActionStyle.default, handler: nil)
        ///
        let titleFont = [NSAttributedStringKey.font: Font.header /*UIFont(name: "SanFranciscoDisplay-Semibold", size: 17.0)!*/]
        let messageFont = [NSAttributedStringKey.font: UIFont(name: "SanFranciscoDisplay-Regular", size: 14.0)!, NSAttributedStringKey.foregroundColor: UIColor(rgb: 0x4f4f4f)]
        let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
        let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
        alert.setValue(titleAttrString, forKey: "attributedTitle")
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        
        alert.addAction(alertActionOk)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //Mesage with action back to previous view
    func showMessageOkWithCompletion(with headerString: String = "Notice", message: String, completion: (() -> Void)? = nil) {
        let alert:UIAlertController = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let alertActionOk:UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (_) in
            completion?()
        }
        ///
        let titleFont = [NSAttributedStringKey.font: UIFont(name: "SanFranciscoDisplay-Semibold", size: 17.0)!]
        let messageFont = [NSAttributedStringKey.font: UIFont(name: "SanFranciscoDisplay-Regular", size: 14.0)!, NSAttributedStringKey.foregroundColor: UIColor(rgb: 0x4f4f4f)]
        let titleAttrString = NSMutableAttributedString(string: headerString, attributes: titleFont)
        let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
        alert.setValue(titleAttrString, forKey: "attributedTitle")
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        
        alert.addAction(alertActionOk)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //showMessage with an completion handler
    func showMessageWithAction(with headerString: String = "Notice", message: String, title: String = "Ok", cancelTitle: String = "Cancel", okStyle: UIAlertActionStyle = .destructive, completion: (() -> Void)? = nil) {
        let alert:UIAlertController = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.alert)
        let alertActionOk:UIAlertAction = UIAlertAction(title: title, style: okStyle) { (_) in
            completion?()
        }
        let alertActionCancel:UIAlertAction = UIAlertAction(title: cancelTitle, style: UIAlertActionStyle.default, handler: nil)
        ///
        let titleFont = [NSAttributedStringKey.font: UIFont(name: "SanFranciscoDisplay-Semibold", size: 17.0)!]
        let messageFont = [NSAttributedStringKey.font: UIFont(name: "SanFranciscoDisplay-Regular", size: 14.0)!, NSAttributedStringKey.foregroundColor: UIColor(rgb: 0x4f4f4f)]
        let titleAttrString = NSMutableAttributedString(string: headerString, attributes: titleFont)
        let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
        alert.setValue(titleAttrString, forKey: "attributedTitle")
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        ///
        alert.addAction(alertActionOk)
        alert.addAction(alertActionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // <---End
    
    //MARK: Progress HUD
    var currentHud: MBProgressHUD? {
        return UIApplication.shared.windows.first!.viewWithTag(9999) as? MBProgressHUD
    }
    
    func showLoading() {
        if let loader = UIApplication.shared.windows.first!.viewWithTag(9999) as? MBProgressHUD {
            loader.show(animated: true)
        } else {
            
            let loader = MBProgressHUD.showAdded(to: UIApplication.shared.windows.first!, animated: true)
            loader.label.font = UIFont.systemFont(ofSize: 14)
            loader.tag = 9999
            UIApplication.shared.windows.first!.bringSubview(toFront: loader)
        }
        
    }
    func hideLoading() {
        (UIApplication.shared.windows.first!.viewWithTag(9999) as? MBProgressHUD)?.hide(animated: true)
        
    }
    //<<<--End progress HUB
    
}

//MARK: - UITableView
extension UITableView {
    
    func showLoading() {
//        let framE = CGRect(x: (self.bounds.width - 50) / 2, y: (self.bounds.height - 50) / 2, width: 50, height: 50)
        let activityView: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect.zero)
        activityView.tag = 997
        activityView.activityIndicatorViewStyle = .whiteLarge
        activityView.color = Color.default
        activityView.startAnimating()
        self.addSubview(activityView)
        
        activityView.snp.makeConstraints { (make) in
            make.center.equalTo(self)
        }
        
    }
    
    func hideLoading(completionHandler: (() -> Void)? = nil) {
        if let activity: UIActivityIndicatorView = self.viewWithTag(997) as? UIActivityIndicatorView {
            activity.stopAnimating()
            activity.removeFromSuperview()
            completionHandler?()
        }
    }
}

//MARK: - Download image from Internet through link
extension UIImageView {
    
    func kf_setImage(url:String, placeholder: UIImage? = UIImage(named: "image_placeholder"), completion: ((_ image: UIImage?) -> Void)? = nil) {
        if let correctUrl:URL = URL(string: url) {
            self.kf.setImage(with: correctUrl, placeholder: placeholder, options: nil, progressBlock: nil, completionHandler: { (image, error, _, _) in
                if completion != nil && error == nil {
                    completion!(image)
                }
            })
        } else {
            self.image = placeholder
        }
    }
}

extension UITextField {
    func trimText() -> String {
        return (self.text ?? "").trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}

//MARK: - UILabel
extension UILabel {
    func time(totalSecs: Int, isPositive: Bool = true) {
        let min = totalSecs / 60
        let sec = totalSecs % 60
        self.text = (isPositive ? "" : "-") + (min < 10 ? "0\(min)" : "\(min)") + (sec < 10 ? ":0\(sec)" : ":\(sec)")
//        self.text = (isPositive ? "" : "-") + (minutes < 10 ? "0\(minutes)" : "\(minutes)") + (secs < 10 ? ":0\(secs)" : ":\(secs)")
    }
    
    var trimmedString: String {
        return self.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}
//MARK: - UISearchBar
extension UISearchBar
{
    func setPlaceholder(color: UIColor, imageSearch: String = "icon_search_blue", borderColor: UIColor? = nil)
    {
        let textFieldInsideSearchBar = self.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = color
        textFieldInsideSearchBar?.font = UIFont(name: "SanFranciscoDisplay-Regular", size: 15)
        if let color = borderColor {
            textFieldInsideSearchBar?.layer.borderColor = color.cgColor
            textFieldInsideSearchBar?.layer.borderWidth = 1
            textFieldInsideSearchBar?.layer.cornerRadius = 5
            textFieldInsideSearchBar?.clipsToBounds = true
            textFieldInsideSearchBar?.backgroundColor = UIColor.white
        }else {
            textFieldInsideSearchBar?.backgroundColor = UIColor(rgb: 0x005079)
        }
        
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = color
        textFieldInsideSearchBarLabel?.font = UIFont(name: "SanFranciscoDisplay-Regular", size: 15)
        
        self.setImage(UIImage(named: imageSearch), for: UISearchBarIcon.search, state: UIControlState.normal)
        
        
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }
    
    convenience init(rgb: Int, a: CGFloat = 1.0) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF,
            a: a
        )
    }
}

extension CALayer {
    func rotateInfinity(totalDuration: Float = 1, duration: CFTimeInterval = 10.0) {
        
        let ca = CABasicAnimation(keyPath: "transform.rotation.z")
        ca.duration = duration
        ca.repeatCount = Float.infinity
        
        ca.fromValue = NSNumber(value: 0)
        ca.toValue = NSNumber(value: 2 * Double.pi)
        
        self.add(ca, forKey: "transform.rotation.z")
        
    }
    
    func shadow(color: UIColor = UIColor.black.withAlphaComponent(0.5), radius: CGFloat = 0.5, width: CGFloat = 0.5, height: CGFloat = 0.5, opacity: Float = 0.5) {
        self.shadowColor = color.cgColor
        self.shadowRadius = radius
        self.shadowOffset = CGSize(width: width, height: height)
        self.shadowOpacity = opacity
    }
    
    
}

//MARK: - Rotate image handler
extension UIImageView {
    func rotate() {
        if self.layer.animation(forKey: "cdImage") == nil {
            let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
//            rotateAnimation.fromValue = 0
            rotateAnimation.toValue = Double.pi * 2
            rotateAnimation.duration = 12
            rotateAnimation.repeatCount = HUGE
            rotateAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
            self.layer.speed = 1
            self.layer.add(rotateAnimation, forKey: "cdImage")
        }
    }
    
    func resumeRotate() {
        let pauseTime = self.layer.timeOffset
        self.layer.speed = 1.0
        self.layer.timeOffset = 0.0
        self.layer.beginTime = 0.0
        self.layer.beginTime = layer.convertTime(CACurrentMediaTime(), from: nil) - pauseTime
    }
    
    func pauseRotate() {
        let pauseTime = self.layer.convertTime(CACurrentMediaTime(), from: nil)
        self.layer.speed = 0.0
        self.layer.timeOffset = pauseTime
    }
    
    func stopRotate() {
        self.layer.removeAllAnimations()
    }
}

extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

extension String {
    func md5() -> String {
        guard let str = self.cString(using: String.Encoding.utf8) else { return "" }
        let strLen = CUnsignedInt(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        CC_MD5(str, strLen, result)
        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        result.deinitialize(count: 1)
        return String(format: hash as String)
    }
    
    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }
    
    func isEmail() -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]+$", options: NSRegularExpression.Options.caseInsensitive)
            return regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil
        } catch { return false }
    }
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

extension UILabel{
    func boldPartString(boldString: [String], size:CGFloat){
        let string = self.text! as NSString
        let font = UIFont(name: "SanFranciscoDisplay-Semibold", size: size)!
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = self.textAlignment
        paragraph.lineBreakMode = self.lineBreakMode
        let attributedString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedStringKey.font:self.font, NSAttributedStringKey.paragraphStyle:paragraph])
        
        let boldFontAttribute = [NSAttributedStringKey.font: font, NSAttributedStringKey.paragraphStyle:paragraph,  NSAttributedStringKey.init("Tagged"): "YES"] as [NSAttributedStringKey : Any]
        
        // Part of string to be bold
        for s in boldString
        {
            attributedString.addAttributes(boldFontAttribute, range:string.range(of: s))
        }
        self.attributedText = attributedString
    }
}

extension UITextView{
    func trimValue() -> String {
        if let text = self.text {
            return text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        return ""
    }
    
    func boldPartString(boldString: [String], size:CGFloat){
        let string = self.text! as NSString
        let font = UIFont(name: "SanFranciscoDisplay-Semibold", size: size)!
        let attributedString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedStringKey.font: self.font ?? UIFont.systemFont(ofSize: 17)])
        
        let boldFontAttribute = [NSAttributedStringKey.font: font]
        
        // Part of string to be bold
        for s in boldString
        {
            attributedString.addAttributes(boldFontAttribute, range:string.range(of: s))
        }
        self.attributedText = attributedString
    }
    
    func boldPartString(boldString: [String], boldFont: UIFont, normalFont: UIFont){
        let string = self.text! as NSString
        let attributedString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedStringKey.font:normalFont])
        
        let boldFontAttribute = [NSAttributedStringKey.font: boldFont]
        
        // Part of string to be bold
        for s in boldString
        {
            let idx = string.range(of: s)
            attributedString.addAttributes(boldFontAttribute, range: idx)
        }
        self.attributedText = attributedString
    }
    
    func boldPartString(boldTag: [TagComment], boldFont: UIFont, normalFont: UIFont){
        let string = self.text! as NSString
        let attributedString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedStringKey.font:normalFont])
        
        let boldFontAttribute = [NSAttributedStringKey.font: boldFont]
        
        // Part of string to be bold
        for tag in boldTag
        {
            let idx = NSMakeRange(tag.startIndex, tag.name.count)
            attributedString.addAttributes(boldFontAttribute, range: idx)
        }
        self.attributedText = attributedString
    }
}

extension FileManager {
    var documentUrl: URL {
        return self.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
}
