//
//  MediaProcess.swift
//  VZONB
//
//  Created by Phuc on 8/1/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import AVFoundation

class MediaProcess: NSObject {
    
    static let shared = MediaProcess()
    
    private override init() {}
    
    let documentURL         : URL = FileManager.default.documentUrl
    
    // Pass the shortest media file first
    func mix(audios: Array<String>, volumes: Array<Float>, callback: @escaping (_ success: Bool, _ audioLink: URL?) -> Void) {
        ///
        let audioLink = self.documentURL.appendingPathComponent("merge.m4a")
        ///
        try? FileManager.default.removeItem(at: audioLink)
        if audios.count != volumes.count { return }
        ///
        let composition = AVMutableComposition()
        let exportAudioMix = AVMutableAudioMix()
        let inputParameters = NSMutableArray()
        
        var duration: CMTime = kCMTimeZero
        
        for iter in 0..<audios.count {
            let compositionMusic = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)
            guard let url = URL(string: audios[iter]) else { continue }
            let musicAsset = AVAsset(url: url)
            let musicAssetTrack = musicAsset.tracks(withMediaType: AVMediaType.audio).first
            ///
            let exportParam = AVMutableAudioMixInputParameters(track: musicAssetTrack)
            exportParam.setVolume(volumes[iter], at: kCMTimeZero)
            exportParam.trackID = (compositionMusic?.trackID)!
            inputParameters.add(exportParam)
            
            if duration == kCMTimeZero || (duration.seconds > 0 && duration > musicAsset.duration) {
                duration = musicAsset.duration
            }
            
            do {
                try compositionMusic?.insertTimeRange(CMTimeRange(start: kCMTimeZero, duration: duration), of: musicAssetTrack!, at: kCMTimeZero)
            } catch (let err) {
                print(err.localizedDescription)
            }
        }
        
        if let inputParams = inputParameters as? [AVAudioMixInputParameters] {
            exportAudioMix.inputParameters = inputParams
        }
        let exporter: AVAssetExportSession = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetAppleM4A)!
        exporter.outputURL = audioLink
        exporter.outputFileType = AVFileType.m4a
        exporter.audioMix = exportAudioMix
        ////
        exporter.exportAsynchronously {
            
            if exporter.status == .completed {
                //TODO: - Play
                for item in audios {
                    try? FileManager.default.removeItem(atPath: item)
                }
                
                DispatchQueue.main.async {
                    callback(true, audioLink)
                }
            } else {
                print("Error ", exporter.status)
                callback(false, nil)
            }
        }
    }
    
    //
    func mix(audio: String, video: String, callback: @escaping (_ success: Bool, _ url: URL?) -> Void) {
        //Del file
        let completionVideoUrl = self.documentURL.appendingPathComponent("completion.mp4")
//        if FileManager.default.fileExists(atPath: completionVideoUrl.absoluteString) {
//            Utility.shared.deleteFiles(filesUrl: completionVideoUrl)
//        }
        
        try? FileManager.default.removeItem(at: completionVideoUrl)
        
        guard let mergeUrl = URL(string: audio), let mergeVideo = URL(string: video) else { return }
        
        let composition = AVMutableComposition()
        let compositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        let compositionVideo = composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        // Audio First
        let audioAsset = AVAsset(url: mergeUrl)
        let videoAsset = AVAsset(url: mergeVideo)
        
        guard let audioTrack = audioAsset.tracks(withMediaType: AVMediaType.audio).first, let videoTrack = videoAsset.tracks(withMediaType: AVMediaType.video).first else {
            callback(false, nil)
            return
        }
        if videoTrack.isPlayable && (compositionVideo?.isPlayable)! {
            compositionVideo?.preferredTransform = videoTrack.preferredTransform
        }
        
        let videoComp = AVMutableVideoComposition()
        videoComp.renderSize = CGSize(width: 640, height: 480)
        videoComp.frameDuration = CMTimeMake(1, 30)
        
        do {
            
            try compositionTrack?.insertTimeRange(CMTimeRange(start: kCMTimeZero, duration: videoAsset.duration), of: audioTrack, at: kCMTimeZero)
            try compositionVideo?.insertTimeRange(CMTimeRange(start: kCMTimeZero, duration: videoAsset.duration), of: videoTrack, at: kCMTimeZero)
            
            ///
            let exporter: AVAssetExportSession = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)!
            exporter.outputURL = completionVideoUrl
            exporter.outputFileType = AVFileType.mp4
            exporter.exportAsynchronously {
                if exporter.status == .completed {
                    DispatchQueue.main.async {
                        callback(true, completionVideoUrl)
                    }
                } else {
                    DispatchQueue.main.async {
                        callback(false, nil)
                    }
                }
            }
        } catch (let err) {
            print(err.localizedDescription)
            callback(false, nil)
        }
    }
    
    //Format movie:number:.mov
    func mergeVideo (numberOfVideos: Int, callback: @escaping (_ success: Bool, _ url: URL?) -> Void) {
        
        let mergeVideo = self.documentURL.appendingPathComponent("mergeVideo.mp4")
        try? FileManager.default.removeItem(at: mergeVideo)
        let composition = AVMutableComposition()
        let compositionVideo = composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        
        for index in 1...numberOfVideos {
            let fileUrl = self.documentURL.appendingPathComponent("movie\(index).mov")
            
            let videoAsset = AVAsset(url: fileUrl)
            let videoTrack = videoAsset.tracks(withMediaType: AVMediaType.video).first!
            if videoTrack.isPlayable && (compositionVideo?.isPlayable)! {
                compositionVideo?.preferredTransform = videoTrack.preferredTransform
            }
            do {
                try compositionVideo?.insertTimeRange(CMTimeRange(start: kCMTimeZero, duration: videoAsset.duration), of: videoTrack, at: composition.duration)
            } catch {
                callback(false, nil)
            }
        }
        
        let exporter: AVAssetExportSession = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)!
        exporter.outputURL = mergeVideo
        exporter.outputFileType = AVFileType.mp4
        
        exporter.exportAsynchronously {
//            [weak self] in
//            guard let `self` = self else { return }
            
            if exporter.status == .completed {
                //TODO: - Play
                DispatchQueue.main.async {
//                    for iter in 1...numberOfVideos {
//                        let url = self.documentURL.appendingPathComponent("movie\(iter).mov")
//                        if FileManager.default.fileExists(atPath: url.absoluteString) {
//                            try? FileManager.default.removeItem(at: url)
//                        }
//                    }
                    callback(true, mergeVideo)
                }
            } else {
                callback(false, nil)
            }
        }
    }
    
}
