//
//  MessageManager.swift
//  VZONB
//
//  Created by PT on 8/8/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation
import SwiftMessages

class MessageManager: NSObject {
    
    static let shared = MessageManager()
    
    enum Style {
        case success, error, info
    }
    
    override init() {}
    
    func show(title: String, body: String, style: Style = .success/*, theme: Theme, iconStyle: IconStyle = .default*/) {
        
//        let messageView = MessageView.viewFromNib(layout: MessageView.Layout.CardView)
        let messageView: MessageView = try! SwiftMessages.viewFromNib()
        //
        var image: UIImage?
        var tintColor: UIColor
        //
        switch style {
        case .success:
            image = UIImage(named: "icon_success")
            tintColor = UIColor.white
        default:
            image = UIImage(named: "icon_error")
            tintColor = UIColor.red
        }
        messageView.configureTheme(backgroundColor: Color.default, foregroundColor: UIColor.white, iconImage: image, iconText: nil)
        messageView.configureDropShadow()
        messageView.configureContent(title: title, body: body)
        messageView.button?.isHidden = true
        messageView.iconImageView?.tintColor = tintColor
        
        messageView.titleLabel?.font = UIFont(name: "SanFranciscoDisplay-Bold", size: 14)!
        messageView.bodyLabel?.font  = UIFont(name: "SanFranciscoDisplay-Regular", size: 14)!
        
        var config = SwiftMessages.defaultConfig
        config.presentationStyle = .top
        config.duration = .seconds(seconds: 3)
        
//        SwiftMessages.hideAll()
//        SwiftMessages.show(config: config, view: messageView)
        SwiftMessages.show(view: messageView)
    }
    
    func notification (noti: NotiModel) {
        let messageView: MessageView = try! SwiftMessages.viewFromNib()
        
        messageView.configureTheme(backgroundColor: UIColor.white, foregroundColor: UIColor.black, iconImage: nil, iconText: "😅")
        messageView.configureDropShadow()
        messageView.button?.isHidden = true
        messageView.bodyLabel?.isHidden = true
        messageView.iconImageView?.isHidden = true
        
        let attrBold: Dictionary<NSAttributedStringKey, Any> = [NSAttributedStringKey.font: UIFont(name: "SanFranciscoDisplay-Bold", size: 14)!, NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor.black]
        let attributeStr = NSMutableAttributedString(string: noti.actor?.name ?? "", attributes: attrBold)
        
        if noti.action == "followed" {
            attributeStr.append(NSAttributedString(string: " follow you"))
        } else {
            attributeStr.append(NSAttributedString(string: " \(noti.action) "))
            attributeStr.append(NSAttributedString(string: noti.record?.name ?? "", attributes: attrBold))
        }
        
        messageView.titleLabel?.attributedText = attributeStr
        
        messageView.tapHandler = { (_) in
            DispatchQueue.main.async {
                let topVc = UIApplication.topViewController()
                if let navi = topVc?.navigationController {
//                    if noti.action != "followed" && noti.action != "commented" && noti.record != nil {
//                        if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
//                            playVc.record = noti.record!
//                            navi.pushViewController(playVc, animated: true)
//                        }
//                    } else
                    if noti.action == "voted" || noti.action == "tagged" {
                        DispatchQueue.main.async {
                            if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                                playVc.record = noti.record
                                //
                                navi.pushViewController(playVc, animated: true)
                            }
                        }
                    } else if noti.action == "followed" {
                        DispatchQueue.main.async {
                            if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
                                profileVc.isMe = false
                                profileVc.id   = noti.actor?.id ?? ""
                                navi.pushViewController(profileVc, animated: true)
                            }
                        }
                    } else if noti.action == "commented" || noti.action == "mentioned" {
//                        if let commentVc = StoryBoard.Home.viewController("CommentsViewController") as? CommentsViewController {
//                            if let record = noti.record {
//                                commentVc.recordId = record.id
//                                commentVc.record = record
//                                navi.pushViewController(commentVc, animated: true)
//                            }
//
//                        }
                        if let detailVC = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                            detailVC.record = noti.record
                            detailVC.commentParrent = noti.comment
                            detailVC.isTransferToComment = true
                            navi.pushViewController(detailVC, animated: true)
                        }
                    } else if noti.action == "replied" {
                        if let detailVC = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                            detailVC.record = noti.record
                            detailVC.isTransferToComment = true
                            detailVC.commentParrent = noti.comment
                            navi.pushViewController(detailVC, animated: true)
                        }
                    }
                }
            }
            
        }
        messageView.titleLabel?.font = UIFont(name: "SanFranciscoDisplay-Bold", size: 14)!
        messageView.bodyLabel?.font  = UIFont(name: "SanFranciscoDisplay-Regular", size: 14)!
        
        var config = SwiftMessages.defaultConfig
//        config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        config.presentationStyle = .top
        config.duration = .seconds(seconds: 3)
//        SwiftMessages.hideAll()
//        SwiftMessages.show(config: config, view: messageView)
        SwiftMessages.show(view: messageView)
    }
    
    func chat (message: Message, chatGroup: String = "", id: String) {
        let messageView = MessageView.viewFromNib(layout: MessageView.Layout.cardView)
        
        messageView.configureTheme(backgroundColor: Color.default, foregroundColor: UIColor.white, iconImage: nil, iconText: "😃")
        var name: String
        if message.sender.nickName.isEmpty {
            name = message.sender.firstname + " " + message.sender.lastname
        } else {
            name = message.sender.nickName
        }
        messageView.configureContent(title: name, body: message.content)
        messageView.configureDropShadow()
        messageView.button?.isHidden = true
//        messageView.bodyLabel?.isHidden = true
        messageView.iconImageView?.isHidden = true
        
        messageView.tapHandler = { (_) in
            DispatchQueue.main.async {
                let topVc = UIApplication.topViewController()
                if let navi = topVc?.navigationController {
                    if let detailChat = StoryBoard.Message.viewController("MessengerDetailViewController") as? MessengerDetailViewController {
                        MessageService.shared.markRead(id: id)
                        detailChat.idUserChatting = id
                        detailChat.chatGroupId = chatGroup
//                        detailChat.name         = message.sender.name
                        detailChat.avatarUrl    = message.sender.avatar
                        navi.pushViewController(detailChat, animated: true)
                        SwiftMessages.hideAll()
                    }
                }
            }
        }
        //        messageView.iconImageView?.image = UIImage(named: "avatar")
        messageView.titleLabel?.font = UIFont(name: "SanFranciscoDisplay-Bold", size: 14)!
        messageView.bodyLabel?.font  = UIFont(name: "SanFranciscoDisplay-Regular", size: 14)!
        
        var config = SwiftMessages.defaultConfig
//        config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        config.presentationStyle = .top
        config.duration = .seconds(seconds: 3)
//        SwiftMessages.hideAll()
//        SwiftMessages.show(config: config, view: messageView)
        SwiftMessages.show(view: messageView)
    }
}
