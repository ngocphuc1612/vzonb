//
//  DataOffline.swift
//  VZONB
//
//  Created by Phuc on 7/8/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation

struct Shared {
    static var searchingString: String = ""
    static var feedReports: Array<FeedReport> = Array<FeedReport>()
}
