//
//  UploadThread.swift
//  VZONB
//
//  Created by Phuc on 6/27/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation
import AWSS3
import AWSCore
import AWSCognito

class UploadThread {
    
    static let shared: UploadThread = UploadThread()
    private let transferManager = AWSS3TransferManager.default()
    
    private init () {}
    
    func upload (tags: Array<String> = [], mediaUrl: URL, name: String, resourceId: String, isVideo: Bool = false, description: String = "", ext: String = "m4a", record: Record? = nil, coverPath: URL, isContestGroup: Bool = false) {
        
        guard let uploadRequest = AWSS3TransferManagerUploadRequest() else { return }
        
        uploadRequest.bucket = "schwaye"
        let encodeName = name.md5()
        uploadRequest.key = "recorded/\(encodeName)_\(Setting.shared.getUserId())\(Date().timeIntervalSince1970).\(ext)"
        uploadRequest.body = mediaUrl
        uploadRequest.acl = .publicRead
        
        self.transferManager.upload(uploadRequest).continueWith(executor: AWSExecutor.immediate(), block: { (task:AWSTask<AnyObject>) -> Any? in
            if let error = task.error as NSError? {
                if error.domain == AWSS3TransferManagerErrorDomain/*, let code = AWSS3TransferManagerErrorType(rawValue: error.code)*/ {
//                    NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.uploadResult), object: false)
                    self.onShowUploadResult(isSuccess: false)
                } else {
                    print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error)")
                }
                return nil
            }
            
            if task.result != nil {
//                let url = AWSS3.default().configuration.endpoint.url
                guard /*let bucketName: String = uploadRequest.bucket, */let key: String = uploadRequest.key else { return nil }
//                guard let publicURL = url?.appendingPathComponent(bucketName).appendingPathComponent(key) else { return nil }
                
                // Upload to server
                let type: MediaType = MediaType(ext: ext)
                
                if type == .audio {
                    RecordService.shared.addNew(tags: tags, resourceId: resourceId, recordUrl: key, description: description, type: type, isContest: isContestGroup, callback: { (result) in
                        if result.status == 200 {
                            do {
                                self.onShowUploadResult(isSuccess: true)
                                // Post to SingRecordedViewController.swift
                                NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.posted), object: nil)
                                try FileManager.default.removeItem(at: mediaUrl)
                                
                            } catch (let err) {
                                print(err.localizedDescription)
                            }
                        } else {                            self.onShowUploadResult(isSuccess: false)
                        }
                    })
                } else {
                    AmazoneService.shared.upload(image: coverPath, name: record?.resource?.name ?? "", callback: { (success, imageLink) in
                        guard let link = imageLink else { return }
                        RecordService.shared.addNew(tags: tags, resourceId: resourceId, recordUrl: key, description: description, type: type, thumbnail: link, isContest: isContestGroup, callback: { (result) in
                            if result.status == 200 {
                                do {
                                    self.onShowUploadResult(isSuccess: true)
                                    // Post to SingRecordedViewController.swift
                                    NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.posted), object: nil)
                                    try FileManager.default.removeItem(at: mediaUrl)
                                    
                                } catch (let err) {
                                    print(err.localizedDescription)
                                }
                            } else {
                                self.onShowUploadResult(isSuccess: false)
                            }
                        })
                    })
                }
                DispatchQueue.main.async {
                    guard let id = record?.localId else { return }
                    _ = record?.delete(id: id)
                }
                
            }
            return nil
        })
    }
    
    func uploadResource() {
        
        guard let uploadRequest = AWSS3TransferManagerUploadRequest() else { return }
        
        uploadRequest.bucket = "schwaye"
        let encodeName = "Dung ai nhac ve anh ay"
        uploadRequest.key = "resource/\(encodeName)_\(Date().timeIntervalSince1970).mp3"
        uploadRequest.body = URL.init(fileURLWithPath: "/Users/phuc/Downloads/dung_ai_nhac_ve_anh_ay.mp3")
        uploadRequest.acl = .publicRead
        
        self.transferManager.upload(uploadRequest).continueWith(executor: AWSExecutor.default(), block: { (task:AWSTask<AnyObject>) -> Any? in
            if let error = task.error as NSError? {
                if error.domain == AWSS3TransferManagerErrorDomain {
                    self.onShowUploadResult(isSuccess: false)
                } else {
                    print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error)")
                }
                return nil
            }
            
            
            
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                guard let bucketName: String = uploadRequest.bucket, let key: String = uploadRequest.key else { return nil }
                guard let publicURL = url?.appendingPathComponent(bucketName).appendingPathComponent(key) else { return nil }
            }
            return nil
        })
    }
    
    private func onShowUploadResult(isSuccess: Bool) {
        MessageManager.shared.show(title: isSuccess ? "Congratulation" : "Sorry", body: isSuccess ? "You have posted your song. Your song will be viewed by other member." : "Some problem happended when uploding. Please try again", style: isSuccess ? .success : .error)
    }
    
}
