//
//  Utility.swift
//  VZONB
//
//  Created by Phuc on 6/9/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import AVFoundation

class Utility {
    
    static let shared: Utility = Utility()
    
    var isPhoneX: Bool = false
    
    init() {}
    
    func getMonthYearFrom(_ dateString: String) -> String {
        let dateFormatter = DateFormatter()
        ///
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        
        let date: Date = dateFormatter.date(from: dateString) ?? Date()
        dateFormatter.dateFormat = "MMM yyyy"
        
        return dateFormatter.string(from: date)
    }
    
    func convertDateTimeToString(_ created_at:String) -> String {
        
        let dateFormatter = DateFormatter()
        ///
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        if let date = dateFormatter.date(from: created_at) {
            //Compare created date with current date
            let today = Date()
            
            let days = Calendar.current.dateComponents([.day], from: date, to: today).day ?? 0
            //Today
            if days == 0 {
                let hours = Calendar.current.dateComponents([.hour], from: date, to: today).hour ?? 0
                if hours == 0 {
                    let minutes = Calendar.current.dateComponents([.minute], from: date, to: today).minute ?? 0
                    if minutes == 0 {
                        return "Just now"
                    }
                    return "\(minutes) mins"
                }
                return "\(hours) hours"
            } else if days == 1 {
                dateFormatter.dateFormat = "h:mm a"
                return "Yesterday at " + dateFormatter.string(from: date)
            } else if days <= 7 {
                dateFormatter.dateFormat = "E h:mm a"
                return dateFormatter.string(from: date) //"\(days) days ago"
            } else {
                dateFormatter.dateFormat = "dd MMM, yyyy"
                return dateFormatter.string(from: date)
            }
        }
        return ""
    }
    
    func createFormatDate() -> String {
        let dateFormatter = DateFormatter()
        ///
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        
        return dateFormatter.string(from: Date())
    }
    
    func needDelete(_ createdAt: String) -> Bool {
        let dateFormatter = DateFormatter()
        ///
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        if let date = dateFormatter.date(from: createdAt) {
            //Compare created date with current date
            let today = Date()

            let days = Calendar.current.dateComponents([.day], from: date, to: today).day ?? 0
            if days >= 1 { return true }
            
            return false
        }
        return false
    }
    
    func convertTimestampToString(date: Date = Date()) -> String {
        let dateFormatter = DateFormatter()
        ///
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX" 
        return dateFormatter.string(from: date)
    }
    
//    func stringDateNow() -> String {
//        
//        return ""
//    }
    
    func deleteFiles(filesUrl: URL...) {
        do {
            for file in filesUrl {
                try FileManager.default.removeItem(at: file)
            }
        } catch (let err) {
            print(err.localizedDescription)
        }
    }

    func intToMonth(intMonth: Int) -> String {
        switch intMonth {
        case 1: return "January"
        case 2: return "February"
        case 3: return "March"
        case 4: return "April"
        case 5: return "May"
        case 6: return "June"
        case 7: return "July"
        case 8: return "August"
        case 9: return "September"
        case 10: return "October"
        case 11: return "November"
        case 12: return "December"
        default: return "- / "
        }
    }
    
    func getThumbnailFrom(path: URL) -> UIImage? {
        do {
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(1, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
}
