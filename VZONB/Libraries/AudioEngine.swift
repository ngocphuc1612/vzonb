//
//  AudioEngine.swift
//  testAVAE
//
//  Created by Phuc on 6/12/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import AVFoundation

class AudioEngine {
    
    private var audioEngine = AVAudioEngine()
    private var playerNode = AVAudioPlayerNode()
    private var buffer = AVAudioPCMBuffer()
    private var reverb = AVAudioUnitReverb()
    private var input: AVAudioInputNode!
    private let mixer = AVAudioMixerNode()
    private let format = AVAudioFormat(standardFormatWithSampleRate: 44100, channels: 1)
    private var fileToSave: AVAudioFile!
    
    var isRunning: Bool = false
    var isStarted: Bool = false
    
    init(beatUrl: URL) {
        
        /// Remove existing Files
//        if FileManager.default.fileExists(atPath: self.voiceUrl.path) {
//            do {
//                try FileManager.default.removeItem(atPath: self.voiceUrl.path)
//                print("Success")
//            } catch {
//                print("Error")
//            }
//        }
        ////
        self.attachAllNode(beatUrl: beatUrl)
    }
    
    private func attachAllNode(beatUrl: URL) {
        //
//        assert(audioEngine.inputNode != nil)
        self.input = audioEngine.inputNode
        self.input.volume = 1
        self.reverb.loadFactoryPreset(AVAudioUnitReverbPreset.cathedral)
        ////
//        let output = audioEngine.outputNode
        let mainMixer = audioEngine.mainMixerNode
        ////
        self.audioEngine.attach(mixer)
        self.audioEngine.attach(self.reverb)
        ////
        
        self.audioEngine.connect(self.reverb, to: mixer, format: self.format)
        self.audioEngine.connect(mixer, to: mainMixer, format: self.format)
//        self.audioEngine.connect(mainMixer, to: output, format: self.format)
        ////
        do {
            let file = try AVAudioFile(forReading: beatUrl)
            self.buffer = AVAudioPCMBuffer(pcmFormat: file.processingFormat, frameCapacity: AVAudioFrameCount(file.length))!
            
            try file.read(into: self.buffer)
            self.audioEngine.attach(self.playerNode)
            self.playerNode.volume = 0.5
            self.audioEngine.connect(self.playerNode, to: mainMixer, format: self.buffer.format)
            
//            print(self.voiceUrl.absoluteString)
            
            
            
        } catch (let err) {
            print("Error ", err)
        }
    }
    
    //Start the engine
    func start(voiceUrl: URL) {
        do {
            self.audioEngine.connect(self.input, to: self.mixer, format: self.input.inputFormat(forBus: 0))
            self.audioEngine.connect(self.audioEngine.mainMixerNode, to: self.audioEngine.outputNode, format: self.format)
            ///
            self.isStarted = true
            self.isRunning = true
            self.fileToSave = try AVAudioFile(forWriting: voiceUrl, settings: self.mixer.outputFormat(forBus: 0).settings)
            
            let bufferSize:AVAudioFrameCount = 4096
            
            self.mixer.installTap(onBus: 0, bufferSize: bufferSize, format: self.mixer.outputFormat(forBus: 0), block: { (buff, when) in
                do {
                    try self.fileToSave.write(from: buff)
                } catch (let error) {
                    print("Error", error)
                }
            })
            
            try audioEngine.start()
            self.playerNode.play()
            self.playerNode.scheduleBuffer(buffer, at: nil, options: AVAudioPlayerNodeBufferOptions.loops, completionHandler: nil)
        } catch {
            self.isRunning = false
            print("Can't play")
        }
    }
    
    //Pause the engine
    func pause() {
        self.isRunning = false
        self.audioEngine.pause()
        self.playerNode.pause()
    }
    
    //Resume
    func resume() {
        do {
            self.isRunning = true
            try self.audioEngine.start()
            self.playerNode.play()
        } catch {
            self.isRunning = false
            NSLog("Fail to start")
        }
    }
    
    ////
    func stop() {
        self.isRunning = false
        self.audioEngine.disconnectNodeInput(self.input)
        self.audioEngine.disconnectNodeOutput(self.audioEngine.outputNode)
        self.mixer.removeTap(onBus: 0)
        self.audioEngine.stop()
        self.playerNode.stop()
    }
    
    ////
    func reset() {
        self.audioEngine.reset()
    }
    
    ///Setup mic volume
    func setMicVolume(_ volume: Float) {
        if self.input != nil {
            self.input.volume = volume
        }
    }
    
    ///Setup beat volume
    func setBeatVolume(_ volume: Float) {
        self.playerNode.volume = volume
    }
    
    /// getCurrent Volume
    func getMicVolume() -> Float {
        if self.input != nil {
            return self.input.volume
        }
        return 0
    }
    
    func getBeatVolume() -> Float {
        return self.playerNode.volume
    }
    
}
