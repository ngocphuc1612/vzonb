//
//  BaseModel.swift
//  SaiGonCA
//
//  Created by Phuc on 2/27/17.
//  Copyright © 2017 Tech For Life. All rights reserved.
//

import Foundation

protocol BaseModel {
    init(data: JSON)
}
