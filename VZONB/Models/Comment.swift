//
//  Comment.swift
//  VZONB
//
//  Created by Phuc on 7/16/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation

struct Comment: BaseModel {
//    "from": {
//    "id": "591f10a316d4a82e2418c941",
//    "name": "Rexviet",
//    "avatar": "https://camo.githubusercontent.com/341831200626efe3e0cf83317801fcac2200fbe2/68747470733a2f2f662e636c6f75642e6769746875622e636f6d2f6173736574732f323639323831302f323130343036312f34643839316563302d386637362d313165332d393230322d6637333934306431306632302e706e67"
//    },
//    "record": "591f135516d4a82e2418c943",
//    "content": "hay",
//    "createdAt": "2017-05-19T15:59:48.957Z",
//    "updatedAt": "2017-05-19T15:59:48.957Z",
//    "id": "591f167416d4a82e2418c945"
    
    var from: Person
    var recordId: String
    var parentId: String
    var content : String
    var createdAt: String
//    var updatedAt: String
    var id: String
    var liked   : Bool = false
    var commentCount: Int
    var lastUserReplied: Person?
    var expanded : Bool = false
    var mentions : Array<TagComment> = []
    //
    var childComment: Array<Comment> = Array()
    var lastPage: UInt8 = 1
    var current: UInt8 = 1
    
    init(data: JSON) {
        self.from = Person(data: data["from"])
        self.recordId = data["record"].stringValue
        self.content  = data["content"].stringValue
        self.commentCount = data["commentCount"].intValue
        self.createdAt = Utility.shared.convertDateTimeToString(data["createdAt"].stringValue)
//        self.updatedAt = Utility.shared.convertDateTimeToString(data["updatedAt"].stringValue)
        self.id        = data["id"].stringValue
        self.liked      = data["liked"].boolValue
        self.parentId   = data["parent"].stringValue
        self.lastUserReplied = Person(data: data["lastUserReplied"])
        self.mentions = data["mentions"].arrayValue.map({ TagComment(data: $0) })
    }
    
}
