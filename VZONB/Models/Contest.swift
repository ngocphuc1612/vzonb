//
//  Contest.swift
//  VZONB
//
//  Created by Gà Nguy Hiểm on 9/28/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

struct Contest: BaseModel {
    init() {
        
    }
    init(data: JSON) {
        self.users = data["users"].arrayValue.map({ Person(data: $0) })
        self.records = data["records"].arrayValue.map({ Record(data: $0) })
        self.winners = data["winners"].arrayValue.map({ Person(data: $0) })
        self.contest = data["contest"].stringValue
        self.round = data["round"].intValue
        self.isReleased = data["isReleased"].boolValue
        self.closeDate = data["closedDate"].stringValue
        self.releaseDate = Utility.shared.getMonthYearFrom(data["dueDate"].stringValue)
        self.isClosed = data["isClosed"].boolValue
        if self.round == 1 {
            self.contestDescription = "First Round Ranking"
        } else if self.round == 2 {
            self.contestDescription = "Second Round Ranking"
        } else if self.round == 3 {
            self.contestDescription = "Final Round Ranking"
        }
//        else {
//            self.contestDescription = "Final Round Ranking"
//        }
    }
    
    var users:     Array<Person> = Array()
    var records:   Array<Record> = Array()
    var winners:   Array<Person> = Array()
    var contest:     String = ""
    var round:    Int = 0
    var isReleased:      Bool = false
    var isClosed:   Bool = false
    var contestDescription  : String = ""
    var closeDate: String = ""
    var releaseDate: String = ""
}
