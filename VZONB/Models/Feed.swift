//
//  Feed.swift
//  VZONB
//
//  Created by Phuc on 6/26/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation

struct Person {
    var name: String
    var firstname   : String
    var lastname    : String
    var avatar      : String
    var id          : String
    var nickName    : String
    var isFollowed  : Bool
    init(data: JSON) {
        self.firstname  = data["firstName"].stringValue
        self.lastname   = data["lastName"].stringValue
        self.name = self.firstname.appending(" \(self.lastname)")
        //
        let avt: String = data["avatar"].stringValue
        self.avatar = (avt.contains("http") || avt.isEmpty) ? avt : (VZONBDomain.AWS + avt)
        
        self.id = data["id"].stringValue
        self.nickName = data["nickName"].stringValue
        self.isFollowed     = data["followed"].bool ?? true
    }
    init(name: String, id: String) {
        self.name = name
        self.id = id
        self.firstname = ""
        self.lastname = ""
        self.avatar = ""
        self.nickName = ""
        self.isFollowed = false
    }
}



struct Feed: BaseModel {
    
//    "actor": {
//    "name": "doan 123",
//    "avatar": "https://camo.githubusercontent.com/341831200626efe3e0cf83317801fcac2200fbe2/68747470733a2f2f662e636c6f75642e6769746875622e636f6d2f6173736574732f323639323831302f323130343036312f34643839316563302d386637362d313165332d393230322d6637333934306431306632302e706e67",
//    "id": "594c5c633cb1f28075293306"
//    },
//    "record": {
//        "votes": [],
//        "comments": 0,
//        "shares": 0,
//        "circles": 0,
//        "url": "https://s3.us-east-2.amazonaws.com/vzonb/recorded/0fec8c9f69413014b15173d5bf285e64_59270a9e91fd55e724d8cae3.mp3",
//        "owner": {
//        "avatar": "https://camo.githubusercontent.com/3418312",
//        "id": "59270a9e91fd55e724d8cae3"
//        },
//        "resource": {
//        "name": "Nơi này có anh",
//        "id": "593a5d56125a5d5c35fe3b35"
//        },
//        "isPublished": true,
//        "createdAt": "2017-06-23T13:51:09.659Z",
//        "updatedAt": "2017-06-23T13:51:09.659Z",
//        "id": "594d1ccd3cb1f28075293307",
//        "likes": 0,
//        "dislikes": 0,
//        "voted": false
//    },
//    "action": "voted",
//    "owner": {
//    "name": "Mr. Phuc",
//    "avatar": "https://camo.githubusercontent.com7",
//    "id": "59270a9e91fd55e724d8cae3"
//    },
//    "createdAt": "2017-06-18T06:56:04.972Z",
//    "updatedAt": "2017-06-18T06:56:04.972Z",
//    "id": "594f51f998db0392ccd7f81d"
    
    var id: String
//    var updatedAt: String
    var createdAt: String
    var owner: Person
    var action: String
    var actor: Person
    var record: Record
    
    init(data: JSON) {
        self.id = data["id"].stringValue
//        self.updatedAt = data["updatedAt"].stringValue
        self.createdAt = Utility.shared.convertDateTimeToString(data["createdAt"].stringValue)
        self.owner = Person(data: data["owner"])
        self.actor = Person(data: data["actor"])
        self.action = data["action"].stringValue
        self.record = Record(data: data["record"])
//        self.isPublished = data["isPublished"].boolValue
        ///
//        let records: JSON = data["record"]
//        self.recordId = records["id"].stringValue
//        self.votes = records["votes"].intValue
//        self.comments = records["comments"].intValue
//        self.shares = records["shares"].intValue
//        self.circles = records["circles"].intValue
//        self.url = records["url"].stringValue
//        ///
//        let resource: JSON = records["resource"]
//        self.resourceId = resource["id"].stringValue
//        self.resourceName = resource["name"].stringValue
        
    }
}
