//
//  MessageModel.swift
//  VZONB
//
//  Created by PT on 8/22/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation



struct Message: BaseModel {
//    "chatGroup": "599afa5545232fba091a92a5",
//    "sender": {
//    "firstName": "Rexviet",
//    "lastName": "VN",
//    "email": "rexvietvn@gmail.com",
//    "avatar": "google.com",
//    "nickName": "rexvietvn",
//    "id": "5919743c3377401e14e6f37c"
//    },
//    "type": "text",
//    "content": "yeppp",
//    "createdAt": "2017-08-21T15:52:53.883Z",
//    "updatedAt": "2017-08-21T15:52:53.883Z",
//    "id": "599b01d59aedae4b0a3bf2d7"
    
    enum State {
        case delivering
        case delivered
        case seen
        func value () -> String {
            switch self {
            case .delivering: return "Delivering"
            case .delivered : return "Delivered"
            case .seen      : return "Seen"
            }
        }
    }
    
    var chatGroup   : String = ""
    var sender      : Person
    var type        : String = ""
    var content     : String
    var createdAt   : String
    var id          : String = ""
    var isSeen      : Bool = false
    var state       : State = .delivered
    
    init(data: JSON) {
        self.chatGroup  = data["chatGroup"].stringValue
        self.sender     = Person(data: data["sender"])
        self.type       = data["type"].stringValue
        self.content    = data["content"].stringValue
        self.createdAt  = data["createdAt"].stringValue
        self.id         = data["id"].stringValue
//        self.isSeen     = data["isSeen"].boolValue
        self.isSeen     = data["seen"].arrayValue.count > 0
        if isSeen {
            self.state = .seen
        }
    }
    
    init(sender: Person, content: String, createdAt: String) {
        self.sender = sender
        self.content = content
        self.createdAt = Utility.shared.createFormatDate() // createdAt
    }
}

struct MessageNotiModel: BaseModel {
//    "owner": "59842a1b790e60940846242c",
//    "chatGroup": "59842a1b790e60940846242c_5919743c3377401e14e6f37c",
//    "lastMessage": {},
//    "members": [
//        {}, {}
//    ],
//    "unreadMessage": 2,
//    "createdAt": "2017-08-21T15:20:53.934Z",
//    "updatedAt": "2017-08-21T15:52:53.967Z",
//    "id": "599afa5545232fba091a92a6"
    
    var owner   : String
    var chatGroup: String
    var lastMessage : Message
    var members     : Array<ShortUser> = Array<ShortUser>()
    var unreadMessage   : Int
    var createdAt   : String
    var id          : String
    
    init(data: JSON) {
        
        self.owner      = data["owner"].stringValue
        self.chatGroup  = data["chatGroup"].stringValue
        self.lastMessage    = Message(data: data["lastMessage"])
        self.unreadMessage  = data["unreadMessage"].intValue
        self.createdAt      = Utility.shared.convertDateTimeToString(data["createdAt"].stringValue)
        self.id             = data["id"].stringValue
        self.members    = data["members"].arrayValue.map({ ShortUser(data: $0) })
    }
    
}
