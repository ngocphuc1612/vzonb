//
//  MyInfo.swift
//  VZONB
//
//  Created by Phuc on 7/4/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation

class GlobalInfo {
    
    static let shared = GlobalInfo()
    
    init() {}
    
    var info: User?
    var userStatus: UserStatus = UserStatus()
    var idChatting: String = ""
//    var numberOfNoti: Int = 0
//    var numberOfMessage: Int = 0
    var imageAvatar: UIImage? = nil
}
