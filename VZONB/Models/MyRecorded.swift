//
//  MyRecorded.swift
//  VZONB
//
//  Created by Phuc on 7/26/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation
import CoreData

struct MyRecorded {
    
    var id          : Int    = 0
    var beatPath    : String = ""
    var voicePath   : String = ""
    var resourceId  : String = ""
    var resourceName: String = ""
    var videoPath   : String = ""
    var type        : String = ""
    
    
    init(data: NSPersistentStore) {
        self.id = data.value(forKey: "id") as? Int ?? 0
        self.beatPath       = data.value(forKey: "beatPath") as? String ?? ""
        self.voicePath      = data.value(forKey: "voicePath") as? String ?? ""
        self.resourceId     = data.value(forKey: "resourceId") as? String ?? ""
        self.resourceName   = data.value(forKey: "resourceName") as? String ?? ""
        self.videoPath      = data.value(forKey: "videoPath") as? String ?? ""
        self.type           = data.value(forKey: "type") as? String ?? ""
    }
    
}
