//
//  NotiModel.swift
//  VZONB
//
//  Created by PT on 8/17/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation

struct NotiModel: BaseModel {
    
    //        - actor
    //            - id: string value,
    //            - firstName: string value,
    //            - lastName: string value,
    //            - avatar: string value
    //        - action: string value,
    //        - record: // if action is "followed", this field will not exist.
    //            - id: string value,
    //            - name: string value,
    //        - isViewed: bool value
    
    var actor: Person?
    var action: String = ""
    var record: Record?
    var isViewed: Bool = false
    var createdAt   : String = ""
    var id      : String = ""
    var comment : String = ""
    
    init(data: JSON) {
        self.actor = Person(data: data["actor"])
        self.action = data["action"].stringValue
        self.record = Record(data: data["record"])
        self.isViewed = data["isViewed"].boolValue
        self.comment = data["comment"].stringValue
        self.id     = data["id"].stringValue
        self.createdAt = Utility.shared.convertDateTimeToString(data["createdAt"].stringValue)
    }
}
