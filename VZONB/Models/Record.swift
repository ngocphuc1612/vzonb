//
//  Record.swift
//  VZONB
//
//  Created by Phuc on 6/9/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import CoreData

//struct Record1: BaseModel {
//    //    "url"         : "www.google.com",
//    //    "owner"       : "591f10a316d4a82e2418c941",
//    //    "resource"    : "591f119016d4a82e2418c942",
//    //    "isPublished" : false,
//    //    "createdAt"   : "2017-05-19T15:46:29.834Z",
//    //    "updatedAt"   : "2017-05-19T15:46:29.834Z",
//    //    "id"          : "591f135516d4a82e2418c943"
//    
//    var url: String = ""
//    var ownerId: String = ""
//    var resourceId: String = ""
//    var isPublished: Bool = false
//    var createAt: String = ""
//    var updateAt: String = ""
//    var id: String = ""
//    
//    init(data: JSON) {
//        self.url = data["url"].stringValue
//        self.ownerId = data["owner"].stringValue
//        self.resourceId = data["resource"].stringValue
//        self.isPublished = data["isPublished"].boolValue
//        self.createAt = Utility.shared.convertDateTimeToString(data["createdAt"].stringValue)
//        self.updateAt = data["updatedAt"].stringValue
//        self.id = data["id"].stringValue
//    }
//}

struct Record: BaseModel {
    
    init(data: JSON) {
        self.comments       = data["comments"].intValue
        self.shares         = data["shares"].intValue
        self.circles        = data["circles"].intValue
        self.likes          = data["likes"].intValue
        self.dislikes       = data["dislikes"].intValue
        self.views          = data["views"].intValue
        
        let url    : String = data["url"].stringValue
        self.url            = (url.contains("http") || url.isEmpty) ? url : (VZONBDomain.AWS + url)
        
        self.owner          = Person(data: data["owner"])
        self.resourceId     = data["resource"]["id"].stringValue
        self.resourceName   = data["resource"]["name"].stringValue
        self.resource       = Resource(data: data["resource"])
        self.type           = data["type"].stringValue
        self.caption        = data["caption"].stringValue
        self.isPublished    = data["isPublished"].boolValue
        self.createdAt      = Utility.shared.convertDateTimeToString(data["createdAt"].stringValue)
        self.id             = data["id"].stringValue
        self.voted          = data["voted"].stringValue
        self.name           = data["name"].stringValue
        
        let thumb: String = data["thumbnail"].stringValue
        self.coverPath      = (thumb.contains("http") || thumb.isEmpty) ? thumb : (VZONBDomain.AWS + thumb)
        
        self.subscribed     = data["subscribed"].boolValue
        self.current_contest = data["current_contest"].stringValue
        self.tags           = data["tags"].arrayValue.map({ ShortUser(data: $0) })
    }
    
    var comments:   Int = 0
    var shares:     Int = 0
    var circles:    Int = 0
    var likes:      Int = 0
    var dislikes:   Int = 0
    var views     : Int = 0
    var url:        String = ""
    var owner:      Person = Person(data: JSON.null)
    var resourceId: String = ""
    var resourceName: String = ""
    var type:       String = ""
    var caption:    String = ""
    var isPublished:Bool = false
    var voted:      String = ""
    var createdAt:  String = ""
    var id:         String = ""
    var beatPath  : String = ""
    var videoPath : String? = ""
    var voicePath : String = ""
    var coverPath : String = ""
    var localId   : Int    = 0
    var beatVolume: Float  = 0.0
    var voiceVolume: Float = 0.0
    var resource    : Resource?
    var name      : String = ""
    var subscribed: Bool = false
    var current_contest : String = ""
    var tags : Array<ShortUser> = []
    
    init(perStore: NSManagedObject) {
        self.localId        = perStore.value(forKey: "id")           as? Int    ?? 0
        self.beatPath       = perStore.value(forKey: "beatPath")     as? String ?? ""
        self.voicePath      = perStore.value(forKey: "voicePath")    as? String ?? ""
        self.resourceId     = perStore.value(forKey: "resourceId")   as? String ?? ""
        self.resourceName   = perStore.value(forKey: "resourceName") as? String ?? ""
        self.videoPath      = perStore.value(forKey: "videoPath")    as? String
        self.coverPath      = perStore.value(forKey: "coverPath")    as? String ?? ""
        self.type           = perStore.value(forKey: "type")         as? String ?? ""
        self.createdAt      = perStore.value(forKey: "createdAt")    as? String ?? "" //Utility.shared.convertDateTimeToString(perStore.value(forKey: "createdAt")    as? String ?? "")
        self.beatVolume     = perStore.value(forKey: "beatVolume")   as? Float  ?? 0
        self.voiceVolume    = perStore.value(forKey: "voiceVolume")  as? Float  ?? 0
    }
    
    init(beatPath: String, voicePath: String, videoPath: String?, cover: String, resourceId: String, resourceName: String, type: String) {
        self.beatPath       = beatPath
        self.voicePath      = voicePath
        self.videoPath      = videoPath
        self.resourceId     = resourceId
        self.resourceName   = resourceName
        self.type           = type
        self.coverPath      = cover
    }
    
    func save(beatVolume: Float = 0.5, voiceVolume: Float = 1.0) -> Bool {
        
        let context = DataManager().managedObjectContext
        let newRecord = NSEntityDescription.insertNewObject(forEntityName: "Recorded", into: context)
        
        //set value for each record
        newRecord.setValue(Setting.shared.getLastId(), forKey: "id")
        newRecord.setValue(self.type, forKey: "type")
        newRecord.setValue(self.resourceName, forKey: "resourceName")
        newRecord.setValue(self.resourceId, forKey: "resourceId")
        newRecord.setValue(self.beatPath, forKey: "beatPath")
        newRecord.setValue(self.voicePath, forKey: "voicePath")
        newRecord.setValue(self.coverPath, forKey: "coverPath")
        if self.videoPath != nil {
            newRecord.setValue(self.videoPath!, forKey: "videoPath")
        }
        newRecord.setValue(beatVolume, forKey: "beatVolume")
        newRecord.setValue(voiceVolume, forKey: "voiceVolume")
        let created = Utility.shared.convertTimestampToString(date: Date())
        newRecord.setValue(created, forKey: "createdAt")
        
        do
        {
            try context.save()
            Setting.shared.setLastId()
            return true
        }
        catch
        {
            print("Khong the save \(error)")
        }
        
        return false
    }
    
    func update(localId: Int, beatVol: Float, voiceVol: Float) {
        let context = DataManager().managedObjectContext
        let req:NSFetchRequest<NSManagedObject> = NSFetchRequest(entityName: "Recorded")
        req.predicate = NSPredicate(format: "id = %d", localId)
        do {
            let result = try context.fetch(req)
            if result.count > 0 {
                result[0].setValue(beatVol, forKey: "beatVolume")
                result[0].setValue(voiceVol, forKey: "voiceVolume")
            }
            try context.save()
        } catch (let err) {
            print(err.localizedDescription, " Update fail")
        }
    }
    
    func delete(id: Int) {
        let context = DataManager().managedObjectContext
        let req:NSFetchRequest<NSManagedObject> = NSFetchRequest(entityName: "Recorded")
        req.predicate = NSPredicate(format: "id = %d", id)
        do {
            let result = try context.fetch(req)
            for item in result {
                context.delete(item)
            }
            try context.save()
        } catch (let err) {
            print(err.localizedDescription, " Delete fail")
        }
    }
}
