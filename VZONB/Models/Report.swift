//
//  Report.swift
//  VZONB
//
//  Created by Phuc on 6/25/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation

internal enum ReportType {
    case report, record, unfollow, follow, turnOnNoti, turnOffNoti, saveSong, delete
    case removeTag
}

struct Report {
    
    var imageName: String = ""
    var title: String = ""
    var description: String = ""
    var type: ReportType = ReportType.record
    
    init(imageName: String, title: String, type: ReportType, description: String = "") {
        self.imageName = imageName
        self.title = title
        self.description = description
        self.type = type
    }
}
