//
//  Resource.swift
//  VZONB
//
//  Created by Phuc on 6/13/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation

struct Resource: BaseModel {
    
    ///
//    "beat": "https://drive.google.com/uc?id=0B67lPFsFyXNTbFFtQU9heXZTaVU&authuser=0",
//    "lyric": "https://drive.google.com/uc?export=download&id=0B67lPFsFyXNTRDZXN3pLZkpvMzQ",
//    "name": "Let Her Go",
//    "thumbnail": "google.com",
//    "singer": "Passenger",
//    "createdAt": "2017-05-19T15:38:56.007Z",
//    "updatedAt": "2017-05-19T15:38:56.007Z",
//    "id": "591f119016d4a82e2418c942"
    ///
    var beatUrl     : String = ""
    var lyricUrl    : String = ""
    var name        : String = ""
    var createdAt   : String = ""
    var singer      : String = ""
    var id          : String = ""
    var thumbnail   : String = ""
    var favorited   : Bool   = false
//    var updatedAt   : String = ""
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    init(data: JSON) {
        
        let beat = data["beat"].stringValue
        self.beatUrl    = (beat.contains("http") || beat.isEmpty) ? beat : (VZONBDomain.AWS + beat)
        
        let lyric       = data["lyric"].stringValue
        self.lyricUrl   = (lyric.contains("http") || lyric.isEmpty) ? lyric : (VZONBDomain.AWS + lyric)
        
        self.name       = data["name"].stringValue
        self.singer     = data["singer"].stringValue
        self.createdAt  = data["createdAt"].stringValue
        self.id         = data["id"].stringValue
        
        let thumb: String = data["thumbnail"].stringValue
        self.thumbnail  = (thumb.contains("http") || thumb.isEmpty) ? thumb : (VZONBDomain.AWS + thumb)
        
        self.favorited  = data["favorited"].boolValue
//        self.updatedAt = data["updatedAt"].stringValue
    }
}
//
//struct Favorited: BaseModel {
//    var resource: Resource
//    var id      : String
//    var createdAt: String
//    
//    init(data: JSON) {
//        self.resource   = Resource(data: data["resource"])
//        self.id         = data["id"].stringValue
//        self.createdAt  = Utility.shared.convertDateTimeToString(data["createdAt"].stringValue)
//    }
//    
//}
