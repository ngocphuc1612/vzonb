//
//  SearchAll.swift
//  VZONB
//
//  Created by Phuc on 7/8/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation

struct SearchAll: BaseModel {
    
    var users: Array<ShortUser>
    var records: Array<Record>
    
    init(data: JSON) {
        self.users = data["users"].arrayValue.map({ ShortUser(data: $0) })
        self.records = data["songs"].arrayValue.map({ Record(data: $0) })
    }
}
