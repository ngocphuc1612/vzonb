//
//  User.swift
//  VZONB
//
//  Created by Phuc on 5/25/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation

struct VZAddress: BaseModel {
//    var state   : Country
//    var country : Country
    var state   : String = ""
    var country : String = ""
    var city    : String = ""
    
    init(state: String, country: String, city: String) {
//        self.state   = Country(key: state, name: "")
        self.state   = state
        self.country = country
//        self.country = Country(key: country, name: "")
        self.city    = city
    }
    init(data: JSON) {
        self.city   = data["city"].stringValue
        self.state  = data["state"].stringValue
        self.country = data["country"].stringValue
//        self.state  = Country(data: data["state"])
//        self.country = Country(data: data["country"])
    }
    func json() -> Dictionary<String, String> {
        return [
            "country": self.country,
            "state": self.state,
            "city": self.city
        ]
    }
}

struct VZDOB: BaseModel {
    var month: Int
    var day  : Int
    init(month: Int, day: Int) {
        self.month = month
        self.day   = day
    }
    init(data: JSON) {
        self.month = data["month"].intValue
        self.day   = data["day"].intValue
    }
    func json() -> Dictionary<String, Int> {
        return [
            "month": self.month,
            "day": self.day
        ]
    }
}

struct Country: BaseModel {
    var key: String
    var name: String
    init(data: JSON) {
        self.key   = data["key"].stringValue
        self.name  = data["name"].stringValue
    }
    init(key: String, name: String) {
        self.key = key
        self.name = name
    }
}

struct User: BaseModel {
    // //
    var firstname: String = ""
    var lastname: String = ""
    var fullname: String = ""
    var nickname: String = ""
    var description: String = ""
    var email: String = ""
    var password: String = ""
    var avatarUrl: String = ""
    var id: String = ""
    var createdAt: String = ""
    var followed: Bool = false
    var address : VZAddress?
    var dob     : VZDOB?
    var following: Int = 0
    var followers: Int = 0
    var top5    : Array<Record>?
    var newRecord: Array<Record> = Array<Record>()
    
    init(data: JSON) {
        guard data != JSON.null else { return }
        self.fullname   = "\(data["firstName"].stringValue) \(data["lastName"].stringValue)"
        self.firstname  = data["firstName"].stringValue
        self.lastname   = data["lastName"].stringValue
        self.nickname   = data["nickName"].stringValue
        self.email      = data["email"].stringValue
        
        let avt: String = data["avatar"].stringValue
        self.avatarUrl  = avt.contains("http") ? avt : (VZONBDomain.AWS + avt)
        
        self.id         = data["id"].stringValue
        self.createdAt      = data["createdAt"].stringValue
        self.followed       = data["followed"].boolValue
        self.description    = data["description"].stringValue
        self.address    =   VZAddress(data: data["address"])
        self.dob        =   VZDOB(data: data["dob"])
        self.following  = data["following"].intValue
        self.followers  = data["followers"].intValue
        self.top5       = data["top5"].arrayValue.map({ Record(data: $0) })
        self.newRecord  = data["newRecord"].arrayValue.map({ Record(data: $0) })
    }
    
    init(firstname: String, lastname: String, email: String) {
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
    }
    
    func convertToJson() -> Dictionary<String, Any> {
        return [
            "firstName": self.firstname,
            "lastName": self.lastname,
            "email": self.email,
            "password": self.password,
            "confirmPassword": self.password
        ]
    }
}

//MARK: - Short User
struct ShortUser: BaseModel {
//    "firstName": "Rexviet",
//    "lastName": "VN",
//    "email": "rexvietvn@gmail.com",
//    "avatar": "google.com",
//    "nickName": "rexvietvn",
//    "id": "5919743c3377401e14e6f37c"
    
    var firstname   : String
    var lastname    : String
    var fullname    : String
    var email       : String
    var avatar      : String
    var nickname    : String
    var id          : String
    var publishSong : Int
    var followed    : Bool
    
    var isTagged    : Bool = false
    
    init(data: JSON) {
        self.firstname  = data["firstName"].stringValue
        self.lastname   = data["lastName"].stringValue
        self.fullname   = self.firstname.appending(" \(self.lastname)")
        self.email      = data["email"].stringValue
        
        let avt: String = data["avatar"].stringValue
        self.avatar     = (avt.contains("http") || avt.isEmpty) ? avt : (VZONBDomain.AWS + avt)
        
        self.nickname   = data["nickName"].stringValue
        self.id         = data["id"].stringValue
        self.publishSong = data["publishedCount"].intValue
        self.followed   = data["followed"].boolValue
    }
    
    init(id: String, fullname: String) {
        self.fullname = fullname
        self.id = id
        self.firstname  = ""
        self.lastname   = ""
        self.email      = ""
        self.avatar     = ""
        self.nickname   = ""
        self.publishSong = 0
        self.followed   = true
    }
}

struct InfoLogin: BaseModel {
    var user: User = User(data: JSON.null)
    var token: String = ""
    
    init(data: JSON) {
        guard data != JSON.null else { return }
        self.user = User(data: data["user"])
        self.token = data["token"].stringValue
    }
}

//MARK: - TagComment
struct TagComment: BaseModel {
    var id   : String
    var name    : String
    var startIndex    : Int
    
    init(data: JSON) {
        self.id  = data["id"].stringValue
        self.name   = data["name"].stringValue
        self.startIndex  = data["startIndex"].intValue
    }
    
    init(id: String, name: String, startIndex: Int){
        self.id = id
        self.name = name
        self.startIndex = startIndex
    }
    
    func jsonValue () -> Dictionary<String, Any> {
        let json : Dictionary<String, Any> = [
            "id": self.id,
            "name": self.name,
            "startIndex": self.startIndex
        ]
        return json
    }
}
