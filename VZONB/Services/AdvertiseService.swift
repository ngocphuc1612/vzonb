//
//  AdvertiseService.swift
//  VZONB
//
//  Created by PT on 2/8/18.
//  Copyright © 2018 Phuc. All rights reserved.
//

import Foundation
import Alamofire

struct AdvertiseModel: BaseModel {
//    "key": "ads_schway",
//    "value": "http://docs.google.com/uc?export=open&id=1p7fP7BAt92kxKSCkzT4FgwBUyJe4F0eY",
//    "createdAt": "2018-02-02T06:57:22.578Z",
//    "updatedAt": "2018-02-02T06:57:22.578Z",
//    "id": "5a740bd298325c700e9062f9"
    var key : String = ""
    var link : String = ""
    var id  : String = ""
    init(data: JSON) {
        self.id     = data["id"].stringValue
        self.key    = data["key"].stringValue
        self.link   = data["value"].stringValue
    }
    init() {}
}

class AdvertiseService: BaseService {
    static let shared = AdvertiseService()
    override var subUrl: String {
        return ""
    }
    func get(_ callback: @escaping (_ r: AdvertiseModel, _ error: String?) -> Void) {
        let url: String = self.baseUrl + "serversettings/ads_schway"
        sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            let status: Int = response.response?.statusCode ?? 0
            switch response.result {
            case .success(let response):
                if 200..<300 ~= status {
                    let advertise = AdvertiseModel(data: JSON(response))
                    callback(advertise, nil)
                } else {
                    callback(AdvertiseModel(), JSON(response)["err"].stringValue)
                }
            case .failure(let error):
                callback(AdvertiseModel(), error.localizedDescription)
            }
        }
    }
}
