//
//  AWSService.swift
//  VZONB
//
//  Created by PT on 8/16/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation
import AWSS3

class AmazoneService: NSObject {
    
    static let shared = AmazoneService()
    private let transferManager = AWSS3TransferManager.default()
    var documentURL : URL { return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! }
    var imageUrl    : URL { return self.documentURL.appendingPathComponent("imageUploading.jpg") }
    
    
    override init() {}
    
    func upload(image: URL, name: String, callback: @escaping (_ success: Bool, _ link: String?) -> Void) {
        guard let uploadRequest = AWSS3TransferManagerUploadRequest() else { return }
        uploadRequest.bucket = "schwaye"
        uploadRequest.key = "images/recorded_\(name)_\(Date().timeIntervalSince1970).jpg"
        uploadRequest.body = image
        uploadRequest.acl = .publicRead
        
        self.transferManager.upload(uploadRequest).continueWith(executor: AWSExecutor.default(), block: { (task:AWSTask<AnyObject>) -> Any? in
            if let error = task.error as NSError? {
                if error.domain == AWSS3TransferManagerErrorDomain {
                    print("Fail upload because domain")
                } else {
                    print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error)")
                }
                return nil
            }
            if task.result != nil {
                print("Success")
            }
            return nil
        })
        
        let url = AWSS3.default().configuration.endpoint.url
        guard let bucketName: String = uploadRequest.bucket, let key: String = uploadRequest.key else { return }
//        guard let publicURL = url?.appendingPathComponent(bucketName).appendingPathComponent(key) else { return }
        callback(true, key)
    }
    
    func uploadAvatar(image: UIImage, callback: @escaping (_ success: Bool, _ link: String?) -> Void) {
        guard let uploadRequest = AWSS3TransferManagerUploadRequest() else { return }
        guard let data = UIImageJPEGRepresentation(image, 0.8) else { return }
        do {
            try data.write(to: self.imageUrl)
            uploadRequest.bucket = "schwaye"
            uploadRequest.key = "images/avatar\(Setting.shared.getUserId()).jpg"
            uploadRequest.body = self.imageUrl
            uploadRequest.acl = .publicRead
            
            self.transferManager.upload(uploadRequest).continueWith(executor: AWSExecutor.default(), block: { (task:AWSTask<AnyObject>) -> Any? in
                if let error = task.error as NSError? {
                    if error.domain == AWSS3TransferManagerErrorDomain {
                        print("Fail upload because domain")
                    } else {
                        print("Error uploading: \(String(describing: uploadRequest.key)) Error: \(error)")
                    }
                    return nil
                }
                if task.result != nil {
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.updateProfile), object: true)
                    }
                }
                return nil
            })
            
            let url = AWSS3.default().configuration.endpoint.url
            guard let bucketName: String = uploadRequest.bucket, let key: String = uploadRequest.key else { return }
//            guard let _ = url?.appendingPathComponent(bucketName).appendingPathComponent(key) else { return }
            callback(true, key)
            
        } catch let err {
            print(err.localizedDescription)
            callback(false, nil)
        }
    }
    
    func delete (_ links: Array<String>) {
        var keys: Array<String> = []
        for link in links {
            if link.contains("/schwaye/") {
                let subPaths = link.components(separatedBy: "/schwaye/")
                if subPaths.count > 1 {
                    keys.append(subPaths[1])
                }
            }
        }
        let manager = AWSS3.default()
        guard let deleteObjectRq = AWSS3DeleteObjectRequest() else { return }
        deleteObjectRq.bucket = "schwaye"
        for key in keys {
            deleteObjectRq.key = key
            print(key)
            manager.deleteObject(deleteObjectRq)
        }
    }
    
}
