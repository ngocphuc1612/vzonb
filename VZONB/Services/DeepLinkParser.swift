//
//  DeepLinkParser.swift
//  VZONB
//
//  Created by PT on 9/23/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation

class DeepLinkParser {
    
    static let shared = DeepLinkParser()
    
    init() {
        
    }
    
    func parse(link: String) {
        let arr = link.components(separatedBy: "/record/")
        if arr.count > 1 {
            RecordService.shared.getBy(id: arr[1], callback: { (result) in
                if result.status == 200 {
                    DispatchQueue.main.async {
                        if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                            playVc.record = result.item
                            UIApplication.topViewController()?.navigationController?.pushViewController(playVc, animated: true)
                        }
                    }
                }
            })
        }
    }
}
