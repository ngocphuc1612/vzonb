//
//  NotificationService.swift
//  VZONB
//
//  Created by PT on 8/22/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation
import Alamofire

class NotificationService: BaseService {
    static let shared  = NotificationService()
    override var subUrl: String { return "notifications" }
    
    func gets(page: UInt8 = 1, _ callback: @escaping (_ result: ListResult<NotiModel>) -> Void) {
        let url = path + "?page=\(page)"
        self.sessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ListResult<NotiModel>(response))
        }
    }
    
    func markRead (id: String) {
        let url = path.appending("/\(id)/read")
        sessionManager.request(url, method: .put, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.response?.statusCode ?? "Mark read Fail")
        }
    }
    
//    func getBadge (_ callback: @escaping (_ result: DoResult<UserStatus>) -> Void) {
//        let url = path.appending("/badges")
//        self.sessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
//            callback(DoResult<BaUserStatusdge>(response))
//        }
//    }
    
    func reset(type: String = "noti") {
        let url = path.appending("/reset")
        self.sessionManager.request(url, method: .put, parameters: ["type": type], encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.response?.statusCode ?? "000")
        }
    }
//    /notifications/badges
}

class MessageService: BaseService {
    static let shared  = MessageService()
    override var subUrl: String { return "messages" }
    
    func gets(_ callback: @escaping (_ result: ListResult<MessageNotiModel>) -> Void) {
        self.sessionManager.request(self.baseUrl.appending("message-notifications"), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ListResult<MessageNotiModel>(response))
        }
    }
    
    func markRead(id: String) {
        let url = baseUrl.appending("message-notifications/\(id)/read")
        VZOSocketManager.shared.seen(id)
        sessionManager.request(url, method: .put, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.response?.statusCode ?? "000")
        }
    }
    
    func search (q: String, callback: @escaping (_ result: ServiceResult<MessageNotiModel>) -> Void) {
        let endcodeString: String = q.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) ?? ""
        let url = baseUrl.appending("message-notifications/search?q=\(endcodeString)")
        sessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            callback(ServiceResult<MessageNotiModel>(response))
        }
    }
    
    func getMessageWith(id: String, page: UInt8 = 1, callback: @escaping (_ result: ListResult<Message>) -> Void) {
        let url = self.path.appending("/\(id)?page=\(page)")
        self.sessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            print(response.response?.statusCode ?? "000 Messages GET")
            callback(ListResult<Message>(response))
        }
    }

    func createConversationTo(id: String, callback: @escaping (_ chatGroupKey: String?) -> Void) {
        let url = self.baseUrl.appending("chat-group/create")
        let param = [
            "to": id,
            "content": "aaa"
        ]
        self.sessionManager.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            let status: Int = response.response?.statusCode ?? 0
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if status == 200 // Success
                {
                    callback(json["chatGroupKey"].stringValue)
                } else {
                    callback(nil)
                }
                
            case .failure(let error):
                print(error.localizedDescription)
                callback(nil)
            }
        }
    }
    func delete(ids: Array<String>, callback: @escaping (_ result: DoResult<InfoLogin>) -> Void) {
        let param = ["ids": ids]
        sessionManager.request(baseUrl.appending("chat-group/del"), method: HTTPMethod.delete, parameters: param, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            callback(DoResult<InfoLogin>(response))
        }
    }
}
