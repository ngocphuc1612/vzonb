//
//  RecordService.swift
//  VZONB
//
//  Created by Phuc on 6/9/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation
import Alamofire

class RecordService: BaseService {
    
    internal static let shared: RecordService = RecordService()
    
    override var subUrl: String {
        return "records"
    }
    
    // MARK: - Add record
    ///**********************************************************************************
    //    Field           Type                Description
    //***********************************************************************************
    //    resource        String              Resource's Id.
    //    url             String              Record's URL after upload to Google Drive.
    ///**********************************************************************************
    func addNew(tags: Array<String> = [], resourceId: String, recordUrl: String, description: String = "", type: MediaType = .audio, thumbnail: String = "", isContest: Bool = false, callback: @escaping ((_ result: DoResult<Record>) -> Void)) {
        let url: String = self.path.appending("/add")
        ///
        var param: Dictionary<String, Any> = [
            "resource": resourceId,
            "url": recordUrl,
            "type": type.rawValue,
            "caption": description,
            "publish": true,
            "thumbnail": thumbnail,
            "tags"   : tags
        ]
        if isContest {
            param["current_contest"] = GlobalInfo.shared.userStatus.group.id
        }
        
        ///
        self.sessionManager.request(url, method: HTTPMethod.post, parameters: param, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<Record>(response))
        }
    }
    
    func removeTag(id: String, callback: @escaping (_ r: DoResult<ValidModel>) -> Void) {
        let url: String = self.path + "/\(id)/remove-tag"
        self.sessionManager.request(url, method: HTTPMethod.post, parameters: ["id": id], encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<ValidModel>(response))
        }
    }
    
    func deleteBy(id: String, link: Array<String>) {
        let url = self.path.appending("/\(id)")
        self.sessionManager.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            if response.response?.statusCode ?? 0 == 200 {
                AmazoneService.shared.delete(link)
            }
        }
    }
    
    ///
    func vote(recordId: String, vote: String = "likes", _ callback: @escaping (_ result: DoResult<Record>) -> Void) {
        let url: String = self.path.appending("/\(recordId)/vote")
        let param: Dictionary<String, String> = [
            "id"    : recordId,
            "type"  : vote
        ]
        self.sessionManager.request(url, method: HTTPMethod.post, parameters: param, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<Record>(response))
        }
    }
    
    // Mark view
    func view(id: String) {
        let url = self.path.appending("/\(id)/view")
        sessionManager.request(url, method: .post, parameters: ["id" : id], encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.response?.statusCode ?? "Mark View Fail")
        }
    }
    
    func getBy(id: String, callback: @escaping (_ result: DoResult<Record>) -> Void) {
        let url: String = self.path.appending("/\(id)")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<Record>(response))
        }
    }
    
    func reportTypes(_ callback: @escaping (_ result: ServiceResult<FeedReport>) -> Void) {
        let url: String = self.baseUrl.appending("report-types/")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ServiceResult<FeedReport>(response))
        }
    }
    
    func report(recordId: String, report: FeedReport, callback: @escaping (_ result: DoResult<InfoLogin>) -> Void) {
        let url: String = self.path.appending("/\(recordId)/report")
        let param: Dictionary<String, String> = [
            "id": recordId,
            "reportType" : report.id,
            "message": report.name
        ]
        
        self.sessionManager.request(url, method: HTTPMethod.post, parameters: param, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<InfoLogin>(response))
        }
        
    }
    
}

// MARK: - Comment
extension RecordService {
    func addComment(recordId: String, content: String, mentions: Array<TagComment>, callback: @escaping (_ result: DoResult<Comment>) -> Void) {
        let url: String = self.path.appending("/\(recordId)/comment")
        let param: Dictionary<String, Any> = [
            "id": recordId,
            "content": content,
            "mentions": mentions.map({ $0.jsonValue() })
        ]
        
        self.sessionManager.request(url, method: HTTPMethod.post, parameters: param, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<Comment>(response))
        }
        
    }
    
    func getComments(id: String, page: UInt8 = 1, callback: @escaping (_ result: ListResult<Comment>) -> Void) {
        let url: String = self.path.appending("/\(id)/comment?page=\(page)")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ListResult<Comment>(response))
        }
    }
    
    func getCommentReplies(id: String, page: UInt8 = 1, callback: @escaping (_ result: ListResult<Comment>) -> Void) {
        let url: String = baseUrl.appending("comments/\(id)/replies?page=\(page)")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ListResult<Comment>(response))
        }
    }
    
    func likeComment(id: String, callback: @escaping (_ status: String) -> Void) {
        let url = baseUrl.appending("comments/\(id)/like")
        sessionManager.request(url, method: .post, parameters: ["id": id], encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            var code = "0000"
            if let res = response.response {
                code = "\(res.statusCode)"
            }
            callback(code)
        }
    }
    
    func replyComment(id: String, content: String, mentions: [TagComment], callback: @escaping (_ result: DoResult<Comment>	) -> Void) {
        let url = baseUrl.appending("comments/\(id)/reply")
        let param: Dictionary<String, Any> = [
            "content": content,
            "mentions": mentions.map({ $0.jsonValue() })
        ]
        self.sessionManager.request(url, method: HTTPMethod.post, parameters: param, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<Comment>(response))
        }
    }
    
    func deleteComment(id: String, callback: @escaping (_ status: String) -> Void) {
        let url = baseUrl.appending("comments/\(id)/delete")
        sessionManager.request(url, method: .delete, parameters: ["id": id], encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            var code = "0000"
            if let res = response.response {
                code = "\(res.statusCode)"
            }
            callback(code)
        }
    }
    
    func editComment(id: String, content: String, mentions: Array<TagComment>, callback: @escaping (_ result: DoResult<Comment>    ) -> Void) {
        let url = baseUrl.appending("comments/\(id)/edit")
        let param: Dictionary<String, Any> = [
            "content": content,
            "mentions": mentions.map({ $0.jsonValue() })
        ]
        self.sessionManager.request(url, method: .put, parameters: param, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<Comment>(response))
        }
    }
}

extension RecordService {
    func subscribe(id: String, value: Bool = true, _ callback: @escaping (_ result: DoResult<InfoLogin>) -> Void) {
        var url: String
        if value {
            url = self.path.appending("/\(id)/subscribe")
        } else {
            url = self.path.appending("/\(id)/unsubscribe")
        }
        let param: Dictionary<String, Any> = [
            "id" : id,
            "subscribed": value
        ]
        self.sessionManager.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<InfoLogin>(response))
        }
        
    }
}

