//
//  ResourceService.swift
//  VZONB
//
//  Created by Phuc on 6/10/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation
import Alamofire

class ResourceService: BaseService {
    
    internal static let shared: ResourceService = ResourceService()
    
    override var subUrl: String {
        return "resources"
    }
    
    //MARK: - Get all resource
    func getBy(page: UInt8 = 1, callback: @escaping (_ result: ListResult<Resource>) -> Void) {
        let url: String = self.path.appending("?page=\(page)")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ListResult<Resource>(response))
        }
    }
    
    //MARK: - Get by Id
    func getBy(id: String, callback: @escaping (_ result: DoResult<Resource>) -> Void) {
        let url: String = self.path.appending("/\(id)")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<Resource>(response))
        }
    }
    
    func favorite(id: String, favorite: Bool, callback: @escaping (_ result: DoResult<Resource>) -> Void) {
        let url: String = self.path.appending("/\(id)/favorite")
        let param: Dictionary<String, Any> = [
            "id": id,
            "favorite": favorite.description
        ]
        self.sessionManager.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<Resource>(response))
        }
    }
    
//    func favorited(page: Int = 1, _ callback: @escaping (_ result: ListResult<Favorited>) -> Void) {
//        let url: String = self.baseUrl.appending("/me/favorites?page=\(page)")
//        self.sessionManager.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
//            callback(ListResult<Favorited>(response: response))
//        }
//    }
    
}
