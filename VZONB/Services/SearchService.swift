//
//  SearchService.swift
//  VZONB
//
//  Created by Phuc on 7/4/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation
import Alamofire

class SearchService: BaseService {
    
    static let shared: SearchService = SearchService()
    
    override var subUrl: String {
        return "search"
    }
    
    /// Resources
    func resources(query: String, page: UInt8 = 1, callback: @escaping (_ result: ListResult<Resource>) -> Void) {
        let endcodeString: String = query.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) ?? ""
        let url: String = self.path.appending("?from=resources&q=\(endcodeString)&page=\(page)")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ListResult<Resource>(response))
        }
    }
    
    /// Users
    func users(query: String, page: UInt8 = 1, callback: @escaping (_ result: ListResult<ShortUser>) -> Void) {
        let endcodeString: String = query.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) ?? ""
        let url: String = self.path.appending("?from=users&q=\(endcodeString)&page=\(page)")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ListResult<ShortUser>(response))
        }
    }
    
    // Records
    func records(query: String, page: UInt8 = 1, callback: @escaping (_ result: ListResult<Record>) -> Void) {
        let endcodeString: String = query.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) ?? ""
        let url: String = self.path.appending("?from=records&q=\(endcodeString)&page=\(page)")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ListResult<Record>(response))
        }
    }
    
    func all(query: String, page: Int = 1, callback: @escaping (_ result: DoResult<SearchAll>) -> Void) {
        let endcodeString: String = query.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) ?? ""
        let url: String = self.path.appending("?from=all&q=\(endcodeString)&page=\(page)")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<SearchAll>(response))
        }
    }
    
    func favorite(query: String, page: UInt8 = 1, callback: @escaping (_ result: ListResult<Resource>) -> Void) {
        let endcodeString: String = query.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) ?? ""
        let url: String = self.path.appending("?from=favorited&q=\(endcodeString)&page=\(page)")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ListResult<Resource>(response))
        }
    }
}
