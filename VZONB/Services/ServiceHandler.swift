//
//  ServiceHandler.swift
//  T04
//
//  Created by Dang Nguyen on 11/21/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

import Foundation
import Alamofire

struct VZONBDomain {
    static let api      : String = "https://api.schwaye.com/"
    
    static let shareApi : String = "https://schwaye.com/"
    
    static let AWS      : String = "http://cdn.schwaye.com/"
    
    static let termOfServiceUrl : String = "https://schwaye.com/termofservice"
    static let privacy          : String = "https://schwaye.com/policy"
}

class BaseService {
    
    let baseUrl: String = VZONBDomain.api
    
    var headers : [String : String] {
        get {
            let token: String = Setting.shared.getToken()
            if token.isEmpty {
                return ["Content-Type": "application/json"]
            } else {
                return ["Content-Type": "application/json", "Authorization": token]
            }
        }
    }
    
    var subUrl : String {
        get {
            return ""
        }
    }
    var path : String {
        get {
            return baseUrl.appending(subUrl)
        }
    }
    
    fileprivate var _sessionManager: SessionManager!
    internal var sessionManager : SessionManager {
        if _sessionManager == nil {
            let requestTimeout: TimeInterval = 25
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = requestTimeout
            
            _sessionManager = SessionManager(configuration: configuration)
            _sessionManager.delegate.sessionDidReceiveChallenge = { session, challenge in
                var disposition: URLSession.AuthChallengeDisposition = .performDefaultHandling
                var credential: URLCredential?
                
                if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
                    disposition = .useCredential
                    credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
                } else {
                    if challenge.previousFailureCount > 0 {
                        disposition = .cancelAuthenticationChallenge
                    } else {
                        credential = self._sessionManager.session.configuration.urlCredentialStorage?.defaultCredential(for: challenge.protectionSpace)
                        
                        if credential != nil {
                            disposition = .useCredential
                        }
                    }
                }
                
                return (disposition, credential)
            }
        }
        return _sessionManager
    }
}

struct ServiceResult<T : BaseModel>{
    var status = -1
    var message:String = ""
    var items = Array<T>()
    init() {}
    
    init(_ response: DataResponse<Any>){
        self.status = response.response?.statusCode ?? 0
        switch response.result {
        case .success(let value):
            let json = JSON(value)
            self.message = json["message"].stringValue
            if self.status == 200 {
                self.items.append(contentsOf: json["data"].arrayValue.map({T(data: $0)}))
            } else if self.status == 403 {
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                appDelegate?.knockBackBanedUser(with: json["err"].stringValue)
            }
        default: return
        }
    }
}

struct DoResult<T : BaseModel> {
    var status = -1
    var item = T(data: JSON.null)
    var message = ""
    init(_ response: DataResponse<Any>){
        self.status = response.response?.statusCode ?? 0
        switch response.result {
        case .success(let value):
            let json = JSON(value)
            self.message = json["message"].stringValue
            if self.status == 200 // Success
            {
                self.item = T(data: json["data"])
            } else if self.status == 403 {
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                appDelegate?.knockBackBanedUser(with: json["err"].stringValue)
            } else {
                self.message = json["err"].stringValue
            }
            
        case .failure(let error):
            self.message = error.localizedDescription
        }
    }
}

struct ListResult<T: BaseModel> {
    var status = -1
    var current: UInt8 = 0
    var totalItems: UInt8 = 0
    var lastPage: UInt8 = 0
    var items = Array<T>()
    
    var message = ""
    init(_ response: DataResponse<Any>){
        self.status = response.response?.statusCode ?? 0
        switch response.result {
        case .success(let value):
            let json = JSON(value)
            self.message = json["message"].stringValue
            if self.status == 200 // Success
            {
                let data = json //["data"]
                self.items = data["data"].arrayValue.map({ T(data: $0) })
                self.lastPage = data["last_page"].uInt8Value
                self.current = data["current_page"].uInt8Value
                self.totalItems = data["total_items"].uInt8Value
            } else if self.status == 403 {
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                appDelegate?.knockBackBanedUser(with: json["err"].stringValue)
            }
            
        case .failure(let error):
            self.message = error.localizedDescription
        }
    }
}

struct ContestResult<T : BaseModel>{
    var status = -1
    var item = T(data: JSON.null)
    var message = ""
    init(_ response: DataResponse<Any>){
        self.status = response.response?.statusCode ?? 0
        switch response.result {
        case .success(let value):
            let json = JSON(value)
            self.message = json["message"].stringValue
            if self.status == 200 // Success
            {
                self.item = T(data: json["data"])
            } else {
                self.message = json["err"].stringValue
            }
            
        case .failure(let error):
            self.message = error.localizedDescription
        }
    }
}
