//
//  SocketManager.swift
//  VZONB
//
//  Created by PT on 8/17/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation
import SocketIO

class VZOSocketManager: NSObject {
    
    static let shared: VZOSocketManager = VZOSocketManager()
    
    let manager = SocketManager(socketURL: URL(string: "https://socket.schwaye.com")!, config: [.log(false), .forceWebsockets(true), .compress])

    var socket: SocketIOClient {
        return manager.defaultSocket
    }

    var isAuthened      : Bool = false
    
    override init() {
        super.init()
        self.auth()
        self.onListenSocket()
    }
    
    func connect () {
        self.socket.connect()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
            self.socket.emit("socketAuth", ["token": Setting.shared.getToken().replacingOccurrences(of: "Bearer ", with: "")])
        }
    }
    
    func send(message: String, to id: String) {
        let data: Dictionary<String, Any> = [
            "chatGroup": id,
            "content": message
        ]
        self.socket.emit("addMessage", data)
    }
    
    func seen(_ groupId: String) {
        let data = ["chatGroup" : groupId]
        self.socket.emit("seenMessage", data)
    }
    
    func auth() {
        self.socket.on("socketAuth_rs") { (data, ack) in
            print(data)
            if let data = data.first as? Dictionary<String, Any> {
                if let success: Bool = data["success"] as? Bool {
                    if success {
                        self.isAuthened = success
                    } else {
                        if !Setting.shared.getToken().isEmpty {
                            self.connect()
                        }
                    }
                } else if let err: String = data["err"] as? String {
                    debugPrint(err)
                }
            }
        }
        
//        action = commented;
//        actor =     {
//            avatar = "https://graph.facebook.com/1146660668772298/picture?type=large";
//            firstName = "Ph\U00fac";
//            id = 598994abecbf72f737b8d8b7;
//            lastName = "Ng\U1ecdc";
//        };
//        createdAt = "2017-08-17T08:10:01.499Z";
//        id = 59954f59c2ad05910cc2ab6c;
//        isViewed = 0;
//        owner = 59891201ecbf72f737b8d8b6;
//        record =     {
//            id = 598ae55f3662a36d7cdbc111;
//            name = Rolex;
//            thumbnail = "https://vzonb.s3.us-east-2.amazonaws.com/resource/44d62005815d9b6a50749ca6d6912a25.jpg";
//        };
    }
    
    private func onListenSocket () {
        self.socket.on("noti") { (data, ack) in
            print(data)
            if let data = data.first as? Dictionary<String, Any> {
                let noti = NotiModel(data: JSON(data))
                let topVc = UIApplication.topViewController()
                if !(topVc is RecordViewController) {
                    if !(topVc is NotificationsViewController) {
                        MessageManager.shared.notification(noti: noti)
                        NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.updateBadge), object: 2)
                    } else {
                        NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.updateNoti), object: nil)
                    }
                }
                
            }
        }
        
        //MARK: - Send message result
        self.socket.on("addMessage_rs") { (data, ack) in
            print(data)
            if data.count < 1 { return }
            if let response: Dictionary<String, Any> = data[0] as? Dictionary<String, Any> {
                let success = (response["success"] as? Bool) ?? false
                NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.chatSendResult), object: success)
            }
        }
        
        //MARK: - Mark seen message
//        self.socket.on("seenMessage_rs") { (data, ack) in
////            print(data)
//            if data.count < 1 { return }
//            if let response: Dictionary<String, Any> = data[0] as? Dictionary<String, Any> {
//                let success = (response["success"] as? Bool) ?? false
//                print(success)
////                NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.Noti.seen), object: success)
//            }
//        }
        self.socket.on("seenMessage") { (data, ack) in
            if data.count < 1 { return }
            if let response: Dictionary<String, Any> = data[0] as? Dictionary<String, Any> {
                let success = (response["chatGroup"] as? String) ?? ""
                print(success)
//                if  {
                NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.Noti.seen), object: success.isEmpty ? false : true)
//                }
            }
        }
        
        
        self.onListenMessage()
    }
    
    private func onListenMessage () {
        socket.on("message") { (data, ack) in
            if let data = data.first as? Dictionary<String, Any> {
                print(data)
                let messageModel = MessageNotiModel(data: JSON(data))
                let topVc = UIApplication.topViewController()
                let chatGroup: String = messageModel.chatGroup
                let id: String = messageModel.id
                if topVc is MessengerDetailViewController && chatGroup == GlobalInfo.shared.idChatting {
                    NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.receiveMessage), object: messageModel)
                } else if topVc is MessengersViewController || topVc is RecordViewController {
                    NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.updateMessage), object: messageModel)
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.updateBadge), object: 3)
                    MessageManager.shared.chat(message: messageModel.lastMessage, chatGroup: chatGroup, id: id)
                }
                
            }
        }
    }
    
    
    
}
