//
//  UserService.swift
//  VZONB
//
//  Created by Phuc on 5/25/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation
import Alamofire

struct ContestGroup: BaseModel {
    
    let id       : String
    var canSubmit: Bool = false
    
    init(data: JSON) {
        self.id     = data["id"].stringValue
        self.canSubmit = data["canSubmit"].boolValue
    }
}

struct UserStatus: BaseModel {
    
    var badgeNoti: Int = 0
    var badgeMessage: Int = 0
    let group   : ContestGroup
    
    init() {
        self.group = ContestGroup(data: JSON.null)
    }
    
    init(data: JSON) {
        self.badgeMessage = data["badgeMessage"].intValue
        self.badgeNoti    = data["badgeNoti"].intValue
        self.group        = ContestGroup(data: data["contestGroup"])
    }
}

struct ValidModel : BaseModel {
    
    var success : Bool
    
    init(data: JSON) {
        self.success    = data["valid"].boolValue
    }
}

final class UserService: BaseService {
    
    static let shared = UserService()
    
    private let limit: Int = 3
    
    private override init() {}
    
    override var subUrl: String {
        return "user"
    }
    
    func checkEmail (_ email: String, callback: @escaping ((_ result: DoResult<ValidModel>) -> Void)) {
        let url = baseUrl.appending("check-mail?email=\(email)")
        sessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            callback(DoResult<ValidModel>(response))
        }
    }
    
    ///********************************************************
    //    Field             Type            Description
    //---------------------------------------------------------
    //    email             String          User's email.
    //    name              String          User's name.
    //    password          String          User's password.
    //    confirmPassword	String          Repeat password.
    ///********************************************************
    func createNew(user: User, callback: @escaping (_ result: DoResult<InfoLogin>) -> Void) {
        let url:String = self.path.appending("/create")
        self.sessionManager.request(url, method: HTTPMethod.post, parameters: user.convertToJson(), encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<InfoLogin>(response))
        }
    }
    /**
     Get record of users by ID
     */
    func getRecords(id: String, page: UInt8 = 1/*, publishType: PublishType = PublishType.all*/, callback: @escaping (_ result: ListResult<Record>) -> Void) {
        let url: String = self.path.appending("/\(id)/recorded?page=\(page)&published=all")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ListResult<Record>(response))
        }
    }
    
    // Feeds
    func getFeeds(page: UInt8 = 1, _ callback: @escaping (_ result: ListResult<Feed>) -> Void) {
        let url: String = self.path.appending("/me/feeds?page=\(page)&limit=\(limit)")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ListResult<Feed>(response))
        }
    }
    
    // Contest
    func getContest(id: String, _ callback: @escaping (_ result: ContestResult<Contest>) -> Void) {
        let url: String = self.baseUrl.appending("contests/\(id)/info")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ContestResult<Contest>(response))
        }
    }
    
    func getInfo(id: String = Setting.shared.getUserId(), _ callback: @escaping (_ result: DoResult<User>) -> Void) {
        let url: String = self.path.appending("/\(id)")
        self.sessionManager.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<User>(response))
        }
    }
    
    func followUserBy(id: String, follow: Bool = true, callback: @escaping (_ result: DoResult<ShortUser>) -> Void) {
        let url: String = self.path.appending("/\(id)/follow")
        let param = [
            "id":   id,
            "follow": follow.description
        ]
        self.sessionManager.request(url, method: HTTPMethod.post, parameters: param, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<ShortUser>(response))
        }
    }
    
    func update(info: Dictionary<String, Any>, callback: @escaping (_ result: DoResult<User>) -> Void) {
        let url: String = self.baseUrl.appending("me/update")
        self.sessionManager.request(url, method: .put, parameters: info, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<User>(response))
        }
        
    }
    
    func changePass(oldPass: String, newPass: String, callback: @escaping (_ result: DoResult<InfoLogin>) -> Void) {
        let url = self.baseUrl.appending("me/change-password")
        let param = [
            "currentPassword": oldPass,
            "newPassword"    : newPass,
            "confirmPassword": newPass
        ]
        self.sessionManager.request(url, method: .put, parameters: param, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<InfoLogin>(response))
        }
    }
}

//Badge
extension UserService {
    func status (_ callback: @escaping (_ result: DoResult<UserStatus>) -> Void) {
        let url = self.path.appending("/me/status")
        sessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<UserStatus>(response))
        }
    }
}

extension UserService {
    
    func setFirebase(token: String, callback: @escaping (_ status: String) -> Void) {
        let url = path.appending("/me/update-device-token")
        sessionManager.request(url, method: .put, parameters: ["deviceToken": token], encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            var code = "0000"
            if let res = response.response {
                code = "\(res.statusCode)"
            }
            callback(code)
        }
    }
    
    func countries(_ callback: @escaping (_ result: ServiceResult<Country>) -> Void) {
        let url: String = self.baseUrl.appending("countries")
        self.sessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ServiceResult<Country>(response))
        }
    }
    
    func stateOf(country: String, callback: @escaping (_ result: ServiceResult<Country>) -> Void) {
        let url: String = self.baseUrl.appending("countries/\(country)/states")
        self.sessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ServiceResult<Country>(response))
        }
    }
}

//MARK: - Follower - Blocked
extension UserService {
    
    func followers (id: String = Setting.shared.getUserId(), isFollower: Bool = true, callback: @escaping (_ result: ServiceResult<ShortUser>) -> Void) {
        let url: String = self.path.appending("/\(id)/\((isFollower ? "followers" : "following"))")
        self.sessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(ServiceResult<ShortUser>(response))
        }
    }
    
    func blockUser(id: String, callback: @escaping (_ status: String) -> Void) {
        let url = self.baseUrl.appending("block/\(id)/")

        sessionManager.request(url, method: .post, parameters: ["userId": id], encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            var code = "0000"
            if let res = response.response {
                code = "\(res.statusCode)"
            }
            callback(code)
        }
    }
    
    /**
     List of blocked users
     */
    func blockedUsers(_ page: UInt8 = 1, _ callback: @escaping (_ result: ListResult<ShortUser>) -> Void) {
        let url = self.baseUrl + "blocked/?page=\(page)"
        self.sessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON{ (response) in
            callback(ListResult<ShortUser>(response))
        }
    }
    
    /**
     Unblock by ID
     */
    func unblock(userId id: String) {
        let url = VZONBDomain.api + "unblock/\(id)"
        self.sessionManager.request(url, method: .post, parameters: ["userId": id], encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print("Unblock status ", response.response?.statusCode ?? 0)
        }
    }
}

extension AuthService {
    /**
     Enter user's email push to server
     */
    func forgotSend(email: String, callback: @escaping (_ success: Bool) -> Void) {
        let url = self.path + "/forget-password"
        let param = ["email": email]
        self.sessionManager.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            guard let status = response.response?.statusCode, status == 200 else { callback(false); return }
            callback(true)
        }
    }
    /**
     Enter validate server token
     */
    func forgotValidToken(_ token: String, callback: @escaping (_ success: Bool) -> Void) {
        let endcodeString: String = token.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) ?? ""
        let url = self.path + "/valid-token?token=\(endcodeString)"
        sessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            guard let status = response.response?.statusCode, status == 200 else { callback(false); return }
            callback(true)
        }
    }
    
    /**
     Change current password
      */
    func forgotChangePassTo(_ password: String, token: String, callback: @escaping (_ success: Bool) -> Void) {
        let url = self.path + "/reset-password?token=\(token)"
        let param = [
//            "token": token,
            "password": password,
            "confirmPassword": password
        ]
        sessionManager.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            guard let status = response.response?.statusCode, status == 200 else { callback(false); return }
            callback(true)
        }
    }
}

class AuthService: BaseService {
    
    static let shared = AuthService()
    
    override var subUrl: String {
        return "auth"
    }
    
    ///********************************************************
    //    Field             Type            Description
    //---------------------------------------------------------
    //    email             String          User's email.
    //    password          String          User's password.
    ///********************************************************
    
    func login(email: String, password: String, callback: @escaping (_ result: DoResult<InfoLogin>) -> ()) {
        let url: String = self.path.appending("/login")
        let param = [
            "email": email,
            "password": password
        ]
        self.sessionManager.request(url, method: HTTPMethod.post, parameters: param, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            callback(DoResult<InfoLogin>(response))
        }
    }
    
    func facebook(facebookId: String, userId: String, _ callback: @escaping (_ result: DoResult<InfoLogin>) -> Void) {
        
        let url: String = self.path.appending("/facebook")
        let param: Dictionary<String, Any> = [
            "facebookId": facebookId,
            "userId": userId
        ]
        
        self.sessionManager.request(url, method: HTTPMethod.post, parameters: param, encoding: JSONEncoding.default, headers: self.headers).responseJSON { (response) in
            callback(DoResult<InfoLogin>(response))
        }
    }
}
