//
//  Setting.swift
//  SaiGonCA
//
//  Created by Phuc on 3/1/17.
//  Copyright © 2017 Tech For Life. All rights reserved.
//

import Foundation

class Setting {
    
    static let shared: Setting = Setting()
    
    open func setFirebase(token: String) {
        if !token.isEmpty {
            UserDefaults.standard.setValue(token, forKey: "tokenFB")
            UserDefaults.standard.synchronize()
        } else {
            return
        }
    }
    open func getFirebaseToken()-> String {
        return UserDefaults.standard.string(forKey: "tokenFB") ?? ""
    }
    
//    internal func clearFirebaseToken() {
//        UserDefaults.standard.setValue("", forKey: "tokenFB")
//        UserDefaults.standard.synchronize()
//    }

    /// ---->>> Token process
    open func setToken(_ token: String) {
        if !token.isEmpty {
            UserDefaults.standard.setValue("Bearer " + token, forKey: "token")
            UserDefaults.standard.synchronize()
        } else {
            return
        }
    }
    open func getToken()-> String {        return UserDefaults.standard.string(forKey: "token") ?? ""
    }
    open func clearToken(){
        UserDefaults.standard.setValue("", forKey: "token")
        UserDefaults.standard.synchronize()
    }
    //// <<<-----
    
    /// ---->>> ID process
    open func setUserId(_ id: String) {
        if !id.isEmpty {
            UserDefaults.standard.setValue(id, forKey: "userId")
            UserDefaults.standard.synchronize()
        } else {
            return
        }
    }
    open func getUserId()-> String {
        return UserDefaults.standard.string(forKey: "userId") ?? ""
    }
    open func clearUserId(){
        UserDefaults.standard.setValue("", forKey: "userId")
        UserDefaults.standard.synchronize()
    }
    
    /// ---->>> Token process
//    open func setFirbaseToken(_ token: String) {
//        if !token.isEmpty {
//            UserDefaults.standard.setValue(token, forKey: "tokenFirebase")
//            UserDefaults.standard.synchronize()
//        } else {
//            return
//        }
//    }
//    open func getFirbaseToken()-> String {
//        return UserDefaults.standard.string(forKey: "tokenFirebase") ?? ""
//    }
//    open func clearFirbaseToken(){
//        UserDefaults.standard.setValue("", forKey: "tokenFirebase")
//        UserDefaults.standard.synchronize()
//    }
    //// <<<-----
    
    /// ---->>> Token process
    func setSave(_ isSave: Bool) {
        UserDefaults.standard.setValue(isSave, forKey: "isSave")
        UserDefaults.standard.synchronize()
    }
    open func getSave() -> Bool {
        return UserDefaults.standard.bool(forKey: "isSave") 
    }
    //// <<<-----
    
    func setLastId() {
        let id = self.getLastId() + 1
        UserDefaults.standard.setValue(id, forKey: "lastId")
        UserDefaults.standard.synchronize()
    }
    
    func getLastId () -> Int {
        return UserDefaults.standard.integer(forKey: "lastId")
    }
    
    //Facebook login sign
    func setFbSign() {
        UserDefaults.standard.set(true, forKey: "fbSign")
        UserDefaults.standard.synchronize()
    }
    func getFbSign() -> Bool {
        return UserDefaults.standard.bool(forKey: "fbSign")
    }
    func clearFbSign() {
        UserDefaults.standard.set(false, forKey: "fbSign")
        UserDefaults.standard.synchronize()
    }
    
    
}
