//
//  AdvertiseViewController.swift
//  VZONB
//
//  Created by PT on 2/7/18.
//  Copyright © 2018 Phuc. All rights reserved.
//

import UIKit

class AdvertiseViewController: UIViewController {
    
    @IBOutlet weak var buttonGotIt  : UIButton!
    @IBOutlet weak var imageAdvertise   : UIImageView!
    @IBOutlet weak var viewWrapImage: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layoutSubviews()
        self.view.alpha = 0.001
        UIView.animate(withDuration: 0.5, animations: {
            self.view.alpha = 1.0
        }) { (_) in
        }
        self.showLoading()
        AdvertiseService.shared.get { [weak self] (advertise, error) in
            guard let `self` = self else { return }
            self.hideLoading()
            if let err = error {
                self.showMessageForUser(with: err)
            } else {
                self.imageAdvertise.kf_setImage(url: advertise.link, completion: { (_) in
                })
                self.imageAdvertise.kf_setImage(url: advertise.link)
            }
        }
        
        self.viewWrapImage.layer.shadow()
        
    }

    @IBAction func onGotItPress(_ sender: Any?) {
        self.view.alpha = 1.0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.alpha = 0.001
        }) { (_) in
            self.dismiss(animated: false, completion: nil)
        }
    }

}
