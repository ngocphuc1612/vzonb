//
//  MessageOtherCell.swift
//  VZONB
//
//  Created by PT on 8/18/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

protocol MessageOtherCellDelegate: class {
    func onClickAvatarAt(_ cell: MessageOtherCell)
}

class MessageOtherCell: UITableViewCell {

    @IBOutlet weak var labelContent : UILabel?
    @IBOutlet weak var labelTime    : UILabel?
    @IBOutlet weak var imageAvatar  : CircleImage?
    @IBOutlet weak var viewBuble    : UIView?
    weak var delegate : MessageOtherCellDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageAvatar?.isHidden = true
        self.imageAvatar?.isUserInteractionEnabled = true
        self.imageAvatar?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onGoToProfile)))
    }
    @objc func onGoToProfile () {
        self.delegate?.onClickAvatarAt(self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.viewBuble?.layer.cornerRadius = 17.5
    }

    func binding(content: String, time: String = "") {
        self.labelContent?.text = content
        self.labelTime?.text = Utility.shared.convertDateTimeToString(time)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageAvatar?.isHidden = true
    }
   
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }
    
}
