//
//  MessageOwnCell.swift
//  VZONB
//
//  Created by PT on 8/18/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class MessageOwnCell: UITableViewCell {

    @IBOutlet weak var labelTime    : UILabel?
    @IBOutlet weak var labelContent : UILabel?
    @IBOutlet weak var viewBuble    : UIView?
    @IBOutlet weak var imageArrow   : UIImageView?
    @IBOutlet weak var labelSendStatus: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutSubviews()
        self.viewBuble?.layer.cornerRadius = 17.5
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        self.viewBuble?.layer.cornerRadius = 17.5
//    }
    
    func binding(content: String, time: String = "", isLast: Bool = false) {
        self.labelContent?.text = content
//        if isLast {
//            self.labelTime?.text = "Just now"
//        } else {
        self.labelTime?.text = Utility.shared.convertDateTimeToString(time)
//        }
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.labelTime?.text = ""
        self.labelSendStatus?.text = ""
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }
}
