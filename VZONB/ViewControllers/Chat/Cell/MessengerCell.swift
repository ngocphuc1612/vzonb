//
//  MessengerCell.swift
//  VZONB
//
//  Created by Phuc on 7/23/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

protocol ProtocolSelectToDel: class {
    func onSelect(_ noti: MessageNotiModel)
}

class MessengerCell: UITableViewCell {
    
    @IBOutlet weak var contraintWidthEdit   : NSLayoutConstraint?
    @IBOutlet weak var labelName    : UILabel?
    @IBOutlet weak var labelContent : UILabel?
    @IBOutlet weak var imageAvatar  : CircleImage?
    @IBOutlet weak var labelTime    : UILabel?
    @IBOutlet weak var buttonCheck   : UIButton?
    @IBOutlet weak var labelBadge   : UILabel?
    
    var unreadMessage: Int = 0 {
        didSet {
            if unreadMessage == 0 {
                self.labelBadge?.isHidden = true
                self.labelBadge?.text = 0.description
                self.backgroundColor = UIColor.white
            } else {
                self.labelBadge?.isHidden = false
                self.labelBadge?.text = unreadMessage.description
                self.backgroundColor = UIColor(rgb: 0xF5F5F5)
            }
        }
    }
    
    fileprivate var isSelecting: Bool = false
    weak var delegate: ProtocolSelectToDel? = nil
    var messageModel    : MessageNotiModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contraintWidthEdit?.constant = 0
        self.labelBadge?.layer.cornerRadius = 8.5
        self.labelBadge?.clipsToBounds = true
//        self.labelBadge?.isHidden = true
    }
    
    func binding(noti: MessageNotiModel) {
        guard noti.members.count > 0 else { return }
        self.labelName?.text    = noti.members[0].fullname
        self.labelContent?.text = noti.lastMessage.content
        self.imageAvatar?.kf_setImage(url: noti.members[0].avatar)
        self.labelTime?.text = Utility.shared.convertDateTimeToString(noti.lastMessage.createdAt)
        self.messageModel = noti
        self.unreadMessage = noti.unreadMessage
    }
    
    @IBAction func onSwitchSelect(_ sender: UIButton?) {
        if sender?.imageView?.image == UIImage(named: "icon_checked") {
            sender?.setImage(UIImage(named: "icon_unchecked"), for: .normal)
            self.backgroundColor = UIColor.white
        } else {
            sender?.setImage(UIImage(named: "icon_checked"), for: .normal)
            self.backgroundColor = UIColor(rgb: 0xf4fbff)
        }
        if let noti = messageModel {
            self.delegate?.onSelect(noti)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.labelBadge?.isHidden = true
        self.backgroundColor = UIColor.white
        self.contraintWidthEdit?.constant = 0
        self.backgroundColor = UIColor.white
        self.buttonCheck?.setImage(UIImage(named: "icon_unchecked"), for: UIControlState.normal)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }
}
