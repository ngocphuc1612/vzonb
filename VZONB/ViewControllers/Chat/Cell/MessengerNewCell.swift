//
//  MessengerNewCell.swift
//  VZONB
//
//  Created by PT on 8/17/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class MessengerNewCell: UITableViewCell {

    @IBOutlet weak var iconAvatar   : UIImageView?
    @IBOutlet weak var labelName    : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func binding(user: ShortUser) {
        self.iconAvatar?.kf_setImage(url: user.avatar)
        self.labelName?.text = user.fullname
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }
    
}
