//
//  MessengerDetailViewController.swift
//  VZONB
//
//  Created by PT on 8/18/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
import IQKeyboardManagerSwift
import SVPullToRefresh
import Popover

class MessengerDetailViewController: BlackChildViewController {

    @IBOutlet weak var tableView    : UITableView?
    @IBOutlet weak var textviewComment : KMPlaceholderTextView?
    @IBOutlet weak var constraintBtm: NSLayoutConstraint?
    
    final private lazy var popover: Popover = {
        let options = [
            .type(.down),
            .animationIn(0.3),
            .blackOverlayColor(UIColor.black.withAlphaComponent(0.2)),
            .arrowSize(CGSize(width: 6.0, height: 6.0)),
            .cornerRadius(CGFloat(0)),
            ] as [PopoverOption]
        return Popover(options: options, showHandler: nil, dismissHandler: nil)
    }()
    
    final private var buttonBlock: UIButton?
    
    final private var popOverView: UIView?
    
    ///
    var isNewChat   : Bool = false
    var idUserChatting: String = ""
    var chatGroupId    : String = ""
    var avatarUrl   : String = ""
    
    var isSystemMessage: Bool = false
    
    fileprivate var indexLast: Int = 0
    fileprivate var iqkeyboardEnable: Bool = false {
        didSet {
            DispatchQueue.main.async { [unowned self] in
                IQKeyboardManager.shared.enable = self.iqkeyboardEnable
                IQKeyboardManager.shared.enableAutoToolbar = self.iqkeyboardEnable
            }
        }
    }
//    private var isFirstLoadData: Bool = true
    ///
    fileprivate var messages: Array<Message> = Array<Message>()
    fileprivate var lastPage    : UInt8 = 1
    fileprivate var current     : UInt8 = 1
    fileprivate var isUpdating  : Bool  = false
    
    private func getMessages(id: String) {
        DispatchQueue.global().async { [weak self] in
            guard let `self` = self else { return }
            MessageService.shared.getMessageWith(id: id, callback: { (result) in
                if result.status == 200 {
                    DispatchQueue.main.async {
                        
                        self.messages = result.items.reversed()
                        
                        self.current = result.current
                        self.lastPage = result.lastPage
                        
                        self.tableView?.reloadData()
                        if self.messages.count > 1 {
                            self.tableView?.scrollToRow(at: IndexPath(row: self.messages.count - 1, section: 0), at: .bottom, animated: false)
                        }
                    }
                }
            })
        }
    }
    private func configUis() {
        /// Block button
        if !self.isSystemMessage {
            self.addBlockFunction ()
        }
        
        // TextView
        self.textviewComment?.layer.cornerRadius = 5
        self.textviewComment?.layer.borderColor = UIColor(rgb: 0x0074b0).cgColor
        self.textviewComment?.layer.borderWidth = 0.5
        self.textviewComment?.clipsToBounds = true
        ///
//        self.title = name
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        // System check
        if isSystemMessage {
            self.constraintBtm?.constant = -55
            self.view.layoutSubviews()
        }
    }
    
    /// Add more icon to UI
    final private func addBlockFunction () {
        if self.buttonBlock == nil {
            
            self.buttonBlock = UIButton(type: UIButtonType.system)
            
            buttonBlock?.frame = CGRect(x: 0, y: 0, width: 45, height: 30)
            buttonBlock?.setImage(UIImage(named: "icon_more"), for: UIControlState.normal)
            buttonBlock?.addTarget(self, action: #selector(self.onShowBlockViewPressed), for: UIControlEvents.touchUpInside)
            buttonBlock?.tintColor = UIColor.black
            
        }

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.buttonBlock!)
    }
    
    /// Show Block popover
    @objc final private func onShowBlockViewPressed () {
        
        if self.popOverView == nil {
            self.popOverView = UIView(frame: CGRect(x: 0, y: 0, width: 100.0, height: 30.0))
            
            let button: UIButton = UIButton(type: UIButtonType.system)
            
            button.translatesAutoresizingMaskIntoConstraints = false
            
            button.addTarget(self, action: #selector(self.onBlockThisUserPressed), for: UIControlEvents.touchUpInside)
            
            button.setTitle("Block", for: UIControlState.normal)
            button.setTitleColor(UIColor.black, for: UIControlState.normal)
            
            self.popOverView?.addSubview(button)
            
            NSLayoutConstraint.activate([
                button.centerXAnchor.constraint(equalTo: self.popOverView!.centerXAnchor),
                button.centerYAnchor.constraint(equalTo: self.popOverView!.centerYAnchor),
                button.leftAnchor.constraint(equalTo: self.popOverView!.leftAnchor)
            ])
        }
        
        self.popover.show(self.popOverView!, fromView: self.buttonBlock ?? UIView())
    }
    
    /// Block listener
    @objc final private func onBlockThisUserPressed () {
        
        self.popover.dismiss()
        
        self.showMessageWithAction(with: "Notice", message: "You are blocking this user. Are you sure?", title: "Block", cancelTitle: "Cancel", okStyle: UIAlertActionStyle.destructive) {
            self.showLoading()
            UserService.shared.blockUser(id: self.idUserChatting) { (code) in
                self.hideLoading()
                if code == "200" {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.showMessageForUser(with: "Some thing went wrong, please try again later")
                }
            }
        }
        
        
    }
    
    override func viewDidLoad() {
        if self.isNewChat {
            MessageService.shared.createConversationTo(id: self.idUserChatting, callback: { [weak self] (chatGroup: String?) in
                guard let `self` = self else { return }
                if let key = chatGroup {
                    GlobalInfo.shared.idChatting = key
                    self.getMessages(id: key)
                    self.chatGroupId = key
                }
            })
        } else {
            let id: String = self.chatGroupId
            GlobalInfo.shared.idChatting = id
            MessageService.shared.markRead(id: id)
            getMessages(id: id)
        }
        UserService.shared.getInfo(id: self.idUserChatting) { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                self.avatarUrl = result.item.avatarUrl
//                self.name = result.item.fullname
                self.title = result.item.fullname
                
            }
        }
        super.viewDidLoad()
        self.configUis()
        self.tableView?.register(MessageOwnCell.nib, forCellReuseIdentifier: MessageOwnCell.identifier)
        self.tableView?.register(MessageOtherCell.nib, forCellReuseIdentifier: MessageOtherCell.identifier)
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
//        self.addPullToRefresh()
    }
    
//    private func addPullToRefresh () {
//        
//        self.tableView?.addPullToRefresh(actionHandler: { [weak self] in
//            guard let `self` = self else { return }
//            let next = self.current + 1
//            if next <= self.lastPage {
//                MessageService.shared.getMessageWith(id: self.chatGroupId, page: next, callback: { (result) in
//                    if result.status == 200 {
//                        DispatchQueue.main.async {
//                            self.messages.insert(contentsOf: result.items.reversed(), at: 0)
//                            self.current = result.current
//                            
//                            self.tableView?.beginUpdates()
//                            var arrIndex = Array<IndexPath>()
//                            for iter in 0..<result.items.count {
//                                arrIndex.append(IndexPath(row: iter, section: 0))
//                            }
//                            self.tableView?.insertRows(at: arrIndex, with: .automatic)
//                            self.tableView?.endUpdates()
//                            if result.items.count > 0 {
//                                self.tableView?.scrollToRow(at: IndexPath(row: result.items.count - 1, section: 0), at: .top, animated: false)
//                            }
//                        }
//                    }
//                })
//                self.tableView?.pullToRefreshView.stopAnimating()
//            } else {
//                self.tableView?.pullToRefreshView.stopAnimating()
//            }
//        })
//        ///
//        self.tableView?.pullToRefreshView.setTitle("Pull to load more messages", forState: 0)
//        self.tableView?.pullToRefreshView.setTitle("Release for load new messages", forState: 0)
//    }
    
    private func onListenEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onSentMessage), name: NSNotification.Name.init(NotiName.chatSendResult), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onReceiveMessge(_:)), name: NSNotification.Name.init(NotiName.receiveMessage), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onShowKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onSeenMessage(_:)), name: NSNotification.Name.init(NotiName.Noti.seen), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.onHideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func onShowKeyboard(_ notification: Notification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.constraintBtm?.constant = 0.0
            } else {
                self.constraintBtm?.constant = (endFrame?.size.height ?? 0)// - 40.0
            }
            UIView.animate(withDuration: duration, animations: {
                self.view.layoutSubviews()
            }, completion: { (_) in
                if self.messages.count > 0 {
                    self.tableView?.scrollToRow(at: IndexPath(row: self.messages.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                }
            })
        }
    }
    // ->
    @objc func onSeenMessage(_ noti: Notification) {
        guard let success = noti.object as? Bool else { return }
        if success {
            if self.indexLast == self.messages.count - 1 {
                if self.messages.count > 1 {
                    if self.messages[self.messages.count - 1].sender.id == Setting.shared.getUserId() {
                        let cell = self.tableView?.cellForRow(at: IndexPath(row: self.messages.count - 1, section: 0)) as? MessageOwnCell
                        cell?.labelSendStatus?.text = "Seen"
                        self.messages[self.messages.count - 1].state = .seen
                    }
                }
            }
        }
    }
    
    @objc func onReceiveMessge(_ noti: Notification) {
        guard let message = noti.object as? MessageNotiModel else { return }
        MessageService.shared.markRead(id: message.id)
        self.messages.append(message.lastMessage)
        if self.messages.count > 1 {
            if self.messages[self.messages.count - 2].sender.id != Setting.shared.getUserId() {
                let cell = self.tableView?.cellForRow(at: IndexPath(row: self.messages.count - 2, section: 0)) as? MessageOtherCell
                cell?.labelTime?.text = ""
                cell?.imageAvatar?.isHidden = true
//                self.indexLast = self.messages.count - 1
            }
        }
        
        self.tableView?.insertRows(at: [IndexPath(row: self.messages.count - 1, section: 0)], with: .automatic)
        self.tableView?.scrollToRow(at: IndexPath(row: self.messages.count - 1, section: 0), at: .bottom, animated: true)
    }
    
    @objc func onSentMessage (_ noti: Notification) {
        guard let success: Bool = noti.object as? Bool else { return }
        if success {
            print("Sent")
            if self.messages.count > 0 {
                let cell = self.tableView?.cellForRow(at: IndexPath(row: self.indexLast, section: 0)) as? MessageOwnCell
                cell?.labelSendStatus?.text = "Delivered"
                self.messages[self.indexLast].state = .delivered
            }
        } else {
            print("Error")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        onListenEvent()
        self.iqkeyboardEnable = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        self.iqkeyboardEnable = true
    }
    
    @IBAction func onSendMessage (_ sender: Any?) {
        let message = (self.textviewComment?.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
        if message.isEmpty {
            self.textviewComment?.text = ""; return;
        }
//        var id: String = self.chatGroupId
//        if self.isNewChat {
//            id = Setting.shared.getUserId().appending("_\(id)")
//        }
        
        
        
        VZOSocketManager.shared.send(message: message, to: self.chatGroupId)
        self.textviewComment?.text = ""
        let sender = Person(name: GlobalInfo.shared.info?.fullname ?? "", id: Setting.shared.getUserId())
        let lastMessage = Message(sender: sender, content: message, createdAt: "Just now")
        
        self.tableView?.beginUpdates()
        self.messages.append(lastMessage)
//        self.indexLast = self.messages.count - 1
        if self.messages.count > 1 {
            if self.messages[self.messages.count - 2].sender.id == Setting.shared.getUserId() {
                let cell = self.tableView?.cellForRow(at: IndexPath(row: self.messages.count - 2, section: 0)) as? MessageOwnCell
                cell?.labelTime?.text = ""
                cell?.labelSendStatus?.text = ""
                
            }
        }
        self.tableView?.insertRows(at: [IndexPath(row: self.messages.count - 1, section: 0)], with: .automatic)
        self.tableView?.endUpdates()
        if let cell = tableView?.cellForRow(at: IndexPath(row: self.messages.count - 1, section: 0)) as? MessageOwnCell {
            cell.labelSendStatus?.text = "Delivering"
            self.messages[self.messages.count - 1].state = .delivering
        }
        self.tableView?.scrollToRow(at: IndexPath(row: self.messages.count - 1, section: 0), at: .bottom, animated: true)
        
    }
}

//MARK: - Scroll View Delegate
extension MessengerDetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !self.isUpdating && scrollView.contentOffset.y < 0 {
            self.isUpdating = true
            debugPrint("Updating")
            let next = self.current + 1
            if next <= self.lastPage {
                MessageService.shared.getMessageWith(id: self.chatGroupId, page: next, callback: { (result) in
                    if result.status == 200 {
                        DispatchQueue.main.async {
                            self.messages.insert(contentsOf: result.items.reversed(), at: 0)
                            self.current = result.current
                            
                            self.tableView?.beginUpdates()
                            var arrIndex = Array<IndexPath>()
                            for iter in 0..<result.items.count {
                                arrIndex.append(IndexPath(row: iter, section: 0))
                            }
                            self.tableView?.insertRows(at: arrIndex, with: .automatic)
                            self.tableView?.endUpdates()
                            if result.items.count > 0 {
                                self.tableView?.scrollToRow(at: IndexPath(row: result.items.count - 1, section: 0), at: .middle, animated: false)
                            }
                        }
                    }
                    self.isUpdating = false
                })
            }
        }
    }
}


extension MessengerDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let id: String = Setting.shared.getUserId()
        
        let message = self.messages[indexPath.row]
        var time = message.createdAt
        if message.sender.id != Setting.shared.getUserId() {
            let cell = tableView.dequeueReusableCell(withIdentifier: MessageOtherCell.identifier, for: indexPath) as! MessageOtherCell
            cell.delegate = self
            if indexPath.row + 1 < self.messages.count {
                if self.messages[indexPath.row + 1].sender.id != id {
                    cell.imageAvatar?.isHidden = true
                    time = ""
                } else {
                    cell.imageAvatar?.isHidden = false
                }
            } else if indexPath.row == self.messages.count - 1 {
                cell.imageAvatar?.isHidden = false
//                cell.imageAvatar?.kf_setImage(url: message.sender.avatar)
            }
            cell.imageAvatar?.kf_setImage(url: avatarUrl)
            cell.binding(content: message.content, time: time)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: MessageOwnCell.identifier, for: indexPath) as! MessageOwnCell

        if indexPath.row + 1 < self.messages.count {
            if self.messages[indexPath.row + 1].sender.id == id {
                time = ""
            }
        }
        if self.indexLast < indexPath.row {
            self.indexLast = indexPath.row
        }
        cell.binding(content: message.content, time: time, isLast: (indexPath.row == self.indexLast))
        if indexPath.row == self.messages.count - 1 {
            cell.labelSendStatus?.text = self.messages[indexPath.row].state.value()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
extension MessengerDetailViewController: MessageOtherCellDelegate {
    func onClickAvatarAt(_ cell: MessageOtherCell) {
        if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
            profileVc.isMe = false
            profileVc.id   = self.idUserChatting
            self.navigationController?.pushViewController(profileVc, animated: true)
        }
    }
}
