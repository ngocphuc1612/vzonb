//
//  MessengerNewViewController.swift
//  VZONB
//
//  Created by PT on 8/17/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class MessengerNewViewController: BlackChildViewController {

    @IBOutlet weak var tableView    : UITableView?
    fileprivate var searchBar       : UISearchBar?
    fileprivate var filterFollowers: Array<ShortUser> = Array<ShortUser>() {
        didSet {
            DispatchQueue.main.async { [unowned self] in
                self.tableView?.reloadData()
            }
        }
    }
    
    fileprivate var followers: Array<ShortUser> = Array<ShortUser>()
    
    private func configSearchBar () {
        self.searchBar = UISearchBar()
        self.searchBar?.placeholder = "Search user"
        self.searchBar?.barTintColor = UIColor.white
        self.searchBar?.tintColor    = UIColor.black
        self.searchBar?.backgroundColor = UIColor(rgb: 0xF9F9F9)
        self.searchBar?.searchBarStyle = .minimal
        self.searchBar?.delegate = self
    }
    
    private func getFollowers () {
        DispatchQueue.global().async {
            UserService.shared.followers(callback: { [weak self] (result) in
                if result.status == 200 {
                    guard let `self` = self else { return }
                    self.followers = result.items
                    self.filterFollowers = result.items
                }
            })
        }
    }

    override func viewDidLoad() {
        self.getFollowers()
        super.viewDidLoad()
        self.configSearchBar()
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.tableView?.register(MessengerNewCell.nib, forCellReuseIdentifier: MessengerNewCell.identifier)
        
        self.title = "New Message"
    }
}

extension MessengerNewViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 0 : self.filterFollowers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MessengerNewCell.identifier, for: indexPath) as! MessengerNewCell
        cell.binding(user: self.filterFollowers[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 //indexPath.section == 0 ? 0 : 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return self.searchBar
        }
        let header: SearchAllHeader = Bundle.main.loadNibNamed("SearchAllHeader", owner: "SearchAllHeader", options: nil)?.first as! SearchAllHeader
        header.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 40)
        header.bindingUIs(title: "Your followers", isOne: true)
        return header
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if let vc = StoryBoard.Message.viewController("MessengerDetailViewController") as? MessengerDetailViewController {
                vc.idUserChatting = self.filterFollowers[indexPath.row].id
                vc.isNewChat = true
                vc.avatarUrl = self.filterFollowers[indexPath.row].avatar
//                vc.name      = self.filterFollowers[indexPath.row].fullname
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}

extension MessengerNewViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.filterFollowers = self.followers
            return
        }
        self.filterFollowers = followers.filter({ ($0.nickname.uppercased().contains(searchText.uppercased()) || $0.fullname.uppercased().contains(searchText.uppercased())) })
    }
}
