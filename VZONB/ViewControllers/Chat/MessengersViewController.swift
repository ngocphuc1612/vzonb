//
//  MessengersViewController.swift
//  VZONB
//
//  Created by Phuc on 7/22/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import SVPullToRefresh

class MessengersViewController: BlackHomeViewController {
    
    @IBOutlet weak var tableView    : UITableView?
    fileprivate var isDeleting      : Bool = false
    
    var searchString: String { return self.searchBar.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) ?? "" }
    
    fileprivate var messages: Array<MessageNotiModel> = Array<MessageNotiModel>()
    fileprivate var deleteIds: Array<String> = Array<String>()
    
    lazy var searchBar: UISearchBar = {
        let sb = UISearchBar()
        sb.placeholder = "Search user"
//        sb.barTintColor = UIColor.white
        sb.tintColor = UIColor.black
        sb.backgroundColor = UIColor(rgb: 0xF9F9F9)
        sb.searchBarStyle = .minimal
        sb.delegate = self
        return sb
    }()
    
    func configNavigation() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(self.onEditing(_:)))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_newmessage"), style: .done, target: self, action: #selector(self.onCreateNewMessage(_:)))
    }
//    private func configSearchBar () {
//        self.searchBar?.placeholder = "Search user"
//        self.searchBar?.barTintColor = UIColor.white
//        self.searchBar?.tintColor = UIColor.black
//        self.searchBar?.backgroundColor = UIColor(rgb: 0xF9F9F9)
//        self.searchBar?.searchBarStyle = .minimal
//        self.searchBar?.delegate = self
//        self.searchBar?.setPlaceholder(color: UIColor.black, borderColor: UIColor.clear)
//    }
    
    private func getMessages () {
        DispatchQueue.global().async {
            MessageService.shared.gets({ (result) in
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
//                    if self.messages.count == 0 {
                    self.tableView?.hideLoading(completionHandler: {
                        self.tableView?.emptyDataSetDelegate = self
                        self.tableView?.emptyDataSetSource   = self
                    })
//                    }
                }
                if result.status == 200 {
                    DispatchQueue.main.async { [weak self] in
                        guard let `self` = self else { return }
                        self.messages = result.items
                        self.tableView?.reloadData()
                    }
                }
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.configSearchBar()
        configNavigation()
        //
        self.tableView?.register(UINib(nibName: "MessengerCell", bundle: nil), forCellReuseIdentifier: "messengerCell")
       
        self.tableView?.delegate   = self
        self.tableView?.dataSource = self
        self.tableView?.showLoading()
        addPulltoRefresh()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onReceiveMessge(_:)), name: NSNotification.Name.init(NotiName.updateMessage), object: nil)
    }
    
    @objc func onReceiveMessge(_ noti: Notification) {
        guard let message = noti.object as? MessageNotiModel else { return }
//        let message = MessageNotiModel(data: data)
        var update: Bool = false
        for iter in 0..<messages.count {
            if iter == 0 && messages[iter].chatGroup == message.chatGroup {
                update = true
                self.messages[iter] = message
                self.tableView?.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                break
            } else if messages[iter].chatGroup == message.chatGroup {
                update = true
                self.messages.remove(at: iter)
                self.messages.insert(message, at: 0)
                self.tableView?.reloadData()
                break
            }
        }
        
        if !update {
            messages.insert(message, at: 0)
            self.tableView?.beginUpdates()
            self.tableView?.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
            self.tableView?.endUpdates()
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ///
        if GlobalInfo.shared.userStatus.badgeMessage > 0 {
            GlobalInfo.shared.userStatus.badgeMessage = 0
            self.navigationController?.tabBarItem.badgeValue = nil
        }
        NotificationService.shared.reset(type: "message")
        ///
        self.getMessages()
    }
    
    private func addPulltoRefresh () {
        self.tableView?.addPullToRefresh(actionHandler: { 
            MessageService.shared.gets({ (result) in
                if result.status == 200 {
                    DispatchQueue.main.async { [weak self] in
                        guard let `self` = self else { return }
                        self.messages = result.items
                        self.tableView?.reloadData()
                    }
                }
                self.tableView?.pullToRefreshView.stopAnimating()
            })
        })
    }
    
    @IBAction func onCreateNewMessage(_ sender: Any?) {
        //MessageManager.shared.show(title: "Congratulation", body: "Your song had posted to server!!!")
        let vc = StoryBoard.Message.viewController("MessengerNewViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onDeleteMessengers(_ sender: UIBarButtonItem) {
        
        if self.deleteIds.count == 0 {
            self.onCancelDelete(nil)
            return
        }
        self.showMessageWithAction(with: "Do you want to delete these conversations?", message: "", title: "Delete", cancelTitle: "No", okStyle: UIAlertActionStyle.destructive) { [weak self] in
            guard let `self` = self else { return }
            MessageService.shared.delete(ids: self.deleteIds) { (result) in
                if result.status == 200 {
                    print("Successfully")
                }
            }
            for item in self.deleteIds {
                for iter in (0..<self.messages.count).reversed() {
                    if self.messages[iter].id == item {
                        self.messages.remove(at: iter)
                    }
                }
            }
            self.onCancelDelete(nil)
        }
    }
    
    @IBAction func onEditing(_ sender: Any?) {
        if self.messages.count > 0 {   
            self.isDeleting = true
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_back_black"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(self.onCancelDelete(_:)))
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_delete"), style: .done, target: self, action: #selector(self.onDeleteMessengers(_:)))
            self.tableView?.reloadData()
        }
    }
    
    @IBAction func onCancelDelete(_ sender: Any?) {
        self.isDeleting = false
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(self.onEditing(_:)))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_newmessage"), style: .done, target: self, action: #selector(self.onCreateNewMessage(_:)))
        self.tableView?.reloadData()
        self.deleteIds = []
    }
}

extension MessengersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.searchBar
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "messengerCell", for: indexPath) as! MessengerCell
        cell.binding(noti: self.messages[indexPath.row])
        cell.delegate = self
        if self.isDeleting {
            cell.contraintWidthEdit?.constant = 30
            UIView.animate(withDuration: 0.4, animations: {
                cell.layoutSubviews()
            })
        } else {
            cell.contraintWidthEdit?.constant = 0
            UIView.animate(withDuration: 0.4, animations: {
                cell.layoutSubviews()
            })
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !self.isDeleting {
            DispatchQueue.main.async {
                if let vc = StoryBoard.Message.viewController("MessengerDetailViewController") as? MessengerDetailViewController {
                    vc.chatGroupId = self.messages[indexPath.row].chatGroup
                    vc.idUserChatting = self.messages[indexPath.row].members.first?.id ?? ""
                    vc.avatarUrl = self.messages[indexPath.row].members.first?.avatar ?? ""
                    vc.isSystemMessage = (self.messages[indexPath.row].members.first?.nickname ?? "") == "schwaye"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            if self.messages[indexPath.row].unreadMessage > 0 {
                MessageService.shared.markRead(id: messages[indexPath.row].id)
                self.messages[indexPath.row].unreadMessage = 0
            }
            tableView.reloadRows(at: [indexPath], with: .fade)
        } else {
            let cell = tableView.cellForRow(at: indexPath) as? MessengerCell
            cell?.onSwitchSelect(cell?.buttonCheck)
        }
        
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return searchBar
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
//    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40
//    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if(velocity.y>0) {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        } else {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
    
}

extension MessengersViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.onReloadData), object: nil)
        self.perform(#selector(self.onReloadData), with: nil, afterDelay: 0.5)
    }
    
    @objc func onReloadData () {
        MessageService.shared.search(q: self.searchString) { (result) in
            if result.status == 200 {
                DispatchQueue.main.async { [unowned self] in
                    self.messages = result.items
                    self.tableView?.reloadData()
                }
            }
        }
    }
}

extension MessengersViewController: ProtocolSelectToDel {
    func onSelect(_ noti: MessageNotiModel) {
        if self.deleteIds.contains(noti.id) {
            for iter in 0...deleteIds.count {
                if deleteIds[iter] == noti.id {
                    self.deleteIds.remove(at: iter)
                    break
                }
            }
        } else {
            self.deleteIds.append(noti.id)
        }
        print(self.deleteIds)
    }
}

extension MessengersViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "ic_history")!
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "Empty", attributes: [NSAttributedStringKey.font: Font.emptyData])
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
