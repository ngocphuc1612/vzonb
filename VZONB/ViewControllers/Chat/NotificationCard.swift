//
//  NotificationCard.swift
//  VZONB
//
//  Created by PT on 8/25/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import SwiftMessages

class NotificationCard: MessageView {
    @IBOutlet weak var imageAvatar: CircleImage?
    @IBOutlet weak var labelContent: UILabel?
    @IBOutlet weak var viewContent: UIView?
    
    var click: (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContent?.layer.shadowColor = UIColor.black.withAlphaComponent(0.8).cgColor
        self.viewContent?.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.viewContent?.layer.shadowRadius = 10
        self.viewContent?.layer.shadowOpacity = 0.9
        
        self.viewContent?.isUserInteractionEnabled = true
        self.viewContent?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onClick)))
        
    }
    
    @objc func onClick () {
        click?()
        SwiftMessages.hideAll()
    }
    
    
}
