//
//  HomeFunctionView.swift
//  VZONB
//
//  Created by Phuc on 6/3/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class HomeCountView: UIView {

    @IBOutlet weak var labelCount: UILabel!
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var constraintImageHeight: NSLayoutConstraint!
    
    var number: Int = 0 {
        didSet {
            self.labelCount.text = number.description
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupUI()
    }
    
    func change(color: UIColor) {
        self.labelCount.textColor = color
        self.imageIcon.tintColor = color
    }
    
    private func setupUI() {
        if let view = Bundle.main.loadNibNamed("HomeCountView", owner: self, options: nil)?.first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
    }
    
    func resizeConstraintTo(_ size: CGFloat = 21) {
        self.constraintImageHeight.constant = size
    }
    
    func bindingUI(count: Int, nameImage: String, isBlackType: Bool = false, isHidden: Bool = true) {
        self.number = count
        self.labelCount.text = isHidden ? "" : count.description
        self.imageIcon.image = UIImage(named: nameImage)
        
        if isBlackType {
            self.imageIcon.image = self.imageIcon.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            self.imageIcon.tintColor = UIColor.white
            self.labelCount.textColor = UIColor.white
        }
        
    }

}
