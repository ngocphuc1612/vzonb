//
//  HomeFunctionView.swift
//  VZONB
//
//  Created by Phuc on 6/3/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class HomeFunctionView: UIView {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageIcon: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupUI()
    }
    
    //    override func layoutSubviews() {
    //        super.layoutSubviews()
    //        self.setupUI()
    //    }
    
    
    
    private func setupUI() {
        if let view = Bundle.main.loadNibNamed("HomeFunctionView", owner: self, options: nil)?.first as? UIView {
            view.frame = self.bounds
            self.addSubview(view)
        }
    }
    
    func bindingUI(name: String, nameImage: String, isBlackType: Bool = false) {
        self.labelName.text = name
        self.imageIcon.image = UIImage(named: nameImage)
        
        if isBlackType {
            self.imageIcon.image = self.imageIcon.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            self.imageIcon.tintColor = UIColor.white
            self.labelName.textColor = UIColor.white
        }
    }

}
