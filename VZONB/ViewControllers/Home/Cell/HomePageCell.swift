//
//  HomePageCell.swift
//  VZONB
//
//  Created by Phuc on 6/3/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import Popover

enum ActionType {
    case share, comment, revote, play, report, viewProfile, top5, contest
    case update, unfollow, delete
}

protocol HomeOnListenAction: class {
    func onPress(_ record: Record, action: ActionType, index: Int)
    func userDidClickMore(at cell: HomePageCell, _ index: Int)
    func userDidClick(at cell: HomePageCell, of user: ShortUser)
    func userDidClick(at cell: HomePageCell, of users: Array<ShortUser>)
}

extension HomeOnListenAction {
    func userDidClickMore(at cell: HomePageCell, _ index: Int) {}
    func userDidClick(at cell: HomePageCell, of user: ShortUser) {}
    func userDidClick(at cell: HomePageCell, of users: Array<ShortUser>) {}
}

class HomePageCell: UITableViewCell {
    
    // Vote state
    enum VoteState {
        case none, voting, voted
    }
    // State observe
    fileprivate var voteState: VoteState = .none {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                self.switchUIWith(voteState: self.voteState)
//                guard let `self` = self else { return }
//                self.viewVoteFunction.isHidden = self.voteState == .none ? false : true
//                self.constraintVote?.constant = self.voteState == .none ? 35 : 0
//                self.constraintVoting?.constant = self.voteState == .voting ? 120 : 0
//                if self.record.comments == 0 {
//                    self.constraintComment?.constant = 35
//                    self.viewCountComment.labelCount.text = ""
//                } else {
//                    self.constraintComment?.constant = 60
//                    self.viewCountComment.number = self.record.comments
//                }
//                UIView.animate(withDuration: 0.3, animations: {
//                    self.viewWrapVoteFunction?.layoutSubviews()
//                })
//                if (self.record.likes == 0 && self.record.dislikes == 0) || self.record.voted == "false" {
//                    self.labelCountVoted?.text = ""
//                } else {
//                    self.labelCountVoted?.attributedText = self.generateStringVoted()
//                }
            }
        }
    }
    
    func generateStringVoted (voteState: VoteState) -> NSMutableAttributedString {
        
        var hitColor: UIColor = UIColor.black
        
        if voteState == .voted {
            if self.record.voted == self.like {
                hitColor = Color.default
            }
        }
        
        /// Song owner name
        let boldFont : UIFont = UIFont(name: "SanFranciscoDisplay-Semibold", size: 15)!
        let attrActor: NSMutableAttributedString = NSMutableAttributedString(string: self.record.likes.description, attributes: [NSAttributedStringKey.font: boldFont, NSAttributedStringKey.foregroundColor: hitColor])
        attrActor.append(NSAttributedString(string: " Hits ", attributes: [NSAttributedStringKey.foregroundColor: hitColor]))
        attrActor.append(NSAttributedString(string: "| \(self.record.dislikes)", attributes: [NSAttributedStringKey.font: boldFont, NSAttributedStringKey.foregroundColor: UIColor.black]))
        attrActor.append(NSAttributedString(string: " Flops"))
        
        return attrActor
    }
    
    // New layout
    @IBOutlet weak var constraintVoting     : NSLayoutConstraint?
    @IBOutlet weak var constraintVote       : NSLayoutConstraint?
    @IBOutlet weak var constraintComment    : NSLayoutConstraint?
    @IBOutlet weak var constraintTagged     : NSLayoutConstraint?
    @IBOutlet weak var labelCountVoted      : UILabel?
    @IBOutlet weak var labelNameHeader      : UILabel?
    @IBOutlet weak var viewWrapVoteFunction : UIView?
    @IBOutlet weak var buttonLike       : UIButton?
    @IBOutlet weak var buttonUnlike     : UIButton?
    
    
    private let like: String    = "likes"
    private let dislike: String = "dislikes"
    weak var delegate: HomeOnListenAction? = nil
    ////
    @IBOutlet weak var labelCaption: UILabel!
    @IBOutlet weak var imageAvatar: CircleImage!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTagsUser : UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelSongName: UILabel!
    @IBOutlet weak var imageCover: UIImageView?
    @IBOutlet weak var viewContent: UIView?
    @IBOutlet weak var labelListenCount : UILabel?
    @IBOutlet weak var labelDescription : UILabel?
    ////
    @IBOutlet weak var labelCountView: UILabel?
    @IBOutlet weak var viewCountShare: HomeCountView!
    @IBOutlet weak var viewCountComment: HomeCountView!
    @IBOutlet weak var viewVoteFunction: HomeCountView!
    @IBOutlet weak var buttonMoreContent : UIButton?
    //
    @IBOutlet weak var buttonMore: UIButton!
    
    @IBOutlet weak var contestButton: CircleButton!
    @IBOutlet weak var constraintCoverHeight: NSLayoutConstraint?
    @IBOutlet weak var constraintSpaceBottom: NSLayoutConstraint?
    @IBOutlet weak var constraintFirstTagName: NSLayoutConstraint?
    
    fileprivate var popover: Popover!
    
    var record: Record = Record(data: JSON.null)
    var index: Int = 0
    fileprivate var isProfile: Bool = false
    fileprivate var action: String = ""
    fileprivate var actor: Person = Person(data: JSON.null)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.viewCountListening.bindingUI(count: 100, nameImage: "ic_home_listening")
        //self.viewCountListening.isHidden = true
        self.viewContent?.layer.shadow()
        
        self.viewCountShare.bindingUI(count: 0, nameImage: "ic_elevate")
//        self.viewCountRevote.bindingUI(count: 23, nameImage: "ic_elevate")
        self.viewCountComment.bindingUI(count: 0, nameImage: "ic_home_comment")
        self.viewCountComment.resizeConstraintTo(25)
        self.viewVoteFunction.bindingUI(count: 0, nameImage: "ic_home_vote")
        self.viewVoteFunction.resizeConstraintTo(25)
        //
        self.imageAvatar.isUserInteractionEnabled = true
        self.imageAvatar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowProfile)))
        //
        self.labelName.isUserInteractionEnabled = true
        self.labelName.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowProfile)))
        //
        self.labelCaption.isUserInteractionEnabled = true
        self.labelCaption.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onCaptionActionHandler)))
        //
        self.labelSongName.isUserInteractionEnabled = true
        self.labelSongName.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onPlayRecord(_:))))
        //
        self.addObserve()
    }
    
    final private func switchUIWith(voteState: VoteState) {
        
        // Vote couting UIs
        if record.likes == 0 && record.dislikes == 0 {
            self.labelCountVoted?.text = ""
        } else {
            self.labelCountVoted?.attributedText = self.generateStringVoted(voteState: voteState)
        }
        
        /// Comment UIs
        if record.comments == 0 {
            self.constraintComment?.constant = 35
            self.viewCountComment.labelCount.text = ""
        } else {
            self.constraintComment?.constant = 55
            self.viewCountComment.number = record.comments
        }
        
        switch voteState {
        case .none:
            // Normal UIs
            self.viewVoteFunction.isHidden = false
            self.constraintVote?.constant = 35
            self.constraintVoting?.constant = 0
            break
        case .voting:
            // Expand Voting function
            self.viewVoteFunction.isHidden = true
            self.constraintVote?.constant = 0
            self.constraintVoting?.constant = 120
            break
        default: // Voted UIs
            self.viewVoteFunction.isHidden = true
            self.constraintVote?.constant = 0
            self.constraintVoting?.constant = 0
            break
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.viewWrapVoteFunction?.layoutSubviews()
        })
        
    }

    private func addObserve() {
        self.viewCountShare.isUserInteractionEnabled = true
        self.viewCountComment.isUserInteractionEnabled = true
        self.viewCountShare.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSharePress(_:))))
        self.viewCountComment.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onCommentPress(_:))))
        
        self.viewVoteFunction.isUserInteractionEnabled = true
        self.viewVoteFunction.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onVoteClick(_:))))
    }
    
    @objc func onCommentPress(_ sender: Any?) {
        self.delegate?.onPress(self.record, action: ActionType.comment, index: self.index)
    }
    
    private func postUpdateState() {
        NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.homeUpdate), object: self)
    }
    
    func bindingUI(feed: Feed, index: Int = 0, isProfile: Bool = false, isFeed: Bool = true) {
        if !isProfile {
            self.isProfile = isProfile
            self.actor = feed.actor
            /// Action Feed
            let attrActor: NSMutableAttributedString = NSMutableAttributedString(string: feed.actor.name, attributes: [NSAttributedStringKey.font: UIFont(name: "SanFranciscoDisplay-Bold", size: 13)!])
            attrActor.append(NSAttributedString(string: " \(feed.action) this song"))
            self.labelCaption.attributedText = attrActor
            
            self.action = feed.action
            
        }
        self.binding(record: feed.record, index: index, isProfile: isProfile, isFeed: isFeed)
    }
    
    func binding(record: Record, index: Int = 0, isProfile: Bool = false, isFeed: Bool = false) {
        self.record = record
        if self.record.voted == "false" || self.record.voted == "" {
            self.voteState = .none
        } else {
            self.voteState = .voted
        }
        self.viewCountComment.number = record.comments
        
        self.labelSongName.text = record.resourceName
        self.labelTime.text = record.createdAt
        self.labelNameHeader?.text = record.owner.name
        self.labelNameHeader?.isUserInteractionEnabled = true
        self.labelNameHeader?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowProfile)))
        //
        self.labelDescription?.text = record.caption
        self.buttonMoreContent?.isHidden = !(record.caption.height(withConstrainedWidth: self.bounds.width - 40, font: UIFont(name: "SanFranciscoDisplay-Regular", size: 15)!) > 30) // ? false : true
        
        //
        if !(record.coverPath.isEmpty) {
            self.imageCover?.kf_setImage(url: record.coverPath)
        } else {
            self.imageCover?.kf_setImage(url: record.resource?.thumbnail ?? "")
        }
        self.imageAvatar.kf_setImage(url: record.owner.avatar)
        self.labelCountView?.text = record.views.description
        ///
        if isProfile {
            self.labelCaption.text = ""
        }
        //
        if record.tags.count > 0 {
            /// Song owner name
            let boldFont : UIFont = UIFont(name: "SanFranciscoDisplay-Bold", size: 15)!
            self.labelName.text = record.owner.name
            let attrActor: NSMutableAttributedString = NSMutableAttributedString(string: "")//, attributes: [NSAttributedStringKey.font: boldFont])
            if record.tags.count >= 1 {
                 attrActor.append(NSAttributedString(string: " is with "))
                attrActor.append(NSAttributedString(string: record.tags[0].firstname, attributes: [NSAttributedStringKey.font: boldFont]))
                self.constraintFirstTagName?.constant = record.tags[0].firstname.width(withConstraintedHeight: 30, font: boldFont)
            }
            if record.tags.count > 1 {
                attrActor.append(NSAttributedString(string: " and "))
                attrActor.append(NSAttributedString(string: (record.tags.count - 1).description, attributes: [NSAttributedStringKey.font: boldFont]))
                attrActor.append(NSAttributedString(string: " others"))
            }
            self.constraintTagged?.constant = 25
            self.labelTagsUser.attributedText = attrActor
        } else {
            self.labelName.text = ""
            self.labelTagsUser.text = ""
            self.constraintTagged?.constant = 0
        }
        self.index = index
        if record.current_contest.count > 0 {
            self.contestButton.isHidden = false
        } else {
            self.contestButton.isHidden = true
        }
    }
    
    func hideContestButton() {
        self.contestButton.isHidden = true
    }
    
    @objc func onCaptionActionHandler () {
        if self.action.isEmpty { return }
        if self.action == "commented" || self.action == "replied" {
            self.delegate?.onPress(self.record, action: ActionType.comment, index: index)
        } else {
            self.delegate?.onPress(record, action: ActionType.play, index: index)
        }
    }
    
    @objc func onRevoteClick () {
        self.delegate?.onPress(self.record, action: ActionType.revote, index: self.index)
    }
    
    @objc func onShowProfile () {
        self.delegate?.onPress(self.record, action: ActionType.viewProfile, index: self.index)
    }
    
    @IBAction func onMoreClick(_ sender: Any?) {
        self.delegate?.userDidClickMore(at: self, self.index)
    }
    
    @IBAction func onFavoriteClick (_ sender: UIButton) {
        
        if sender == self.buttonLike {
            self.record.likes += 1
            self.record.voted = "likes"
        } else {
            self.record.dislikes += 1
            self.record.voted = "dislikes"
        }
        self.postUpdateState()
        RecordService.shared.vote(recordId: self.record.id, vote: self.record.voted, { (result) in
            print(result.item.voted)
        })
        self.voteState = .voted
        self.viewCountComment.number = record.comments
    }
    
    @IBAction func onVoteClick(_ sender: UIButton) {
        self.voteState = .voting
    }
    
    @IBAction func onPlayRecord(_ sender: Any?) {
        self.delegate?.onPress(self.record, action: ActionType.play, index: self.index)
    }
    
    @IBAction func onSharePress(_ sender: Any?) {
        self.delegate?.onPress(self.record, action: ActionType.share, index: self.index)
    }
    
    @IBAction func onContestClick() {
        self.delegate?.onPress(self.record, action: ActionType.contest, index: self.index)
    }
    
    @IBAction func onClickFirstTag(_ sender: Any?) {
        if self.record.tags.count >= 1 {
            self.delegate?.userDidClick(at: self, of: self.record.tags[0])
        }
    }
    
    @IBAction func onClickOtherTag(_ sender : Any?) {
        if self.record.tags.count > 1 {
            self.delegate?.userDidClick(at: self, of: self.record.tags)
        }
    }
    
    @IBAction func onShowPopupAction(_ sender: UIButton) {
        
        var reports: Array<Report> = []
        if self.record.owner.id == Setting.shared.getUserId() {
            reports = self.actionsForMe(isTurnon: self.record.subscribed)
        } else {
            reports = self.actionsForOtherUserFeed(isTurnon: self.record.subscribed, isFollowed: self.record.owner.isFollowed)
        }
        let buttonYAxis = sender.superview?.convert(sender.frame, to: nil).midY ?? 0
        let heightPopUp: CGFloat = CGFloat(reports.count * 51)
        
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            let popupReport = TablePopup(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: heightPopUp), data: reports)
            popupReport.delegate = self
            let popoverOptions: [PopoverOption] = [
                .type(buttonYAxis > (heightPopUp + 80) ? .up : .down),
                .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6)),
                .cornerRadius(CGFloat(0)),
                .arrowSize(CGSize(width: 6, height: 6))
            ]
            self.popover = Popover(options: popoverOptions)
            self.popover.show(popupReport, fromView: self.buttonMore)
        }
    }
    
    private func actionsForOtherUserFeed(isTurnon: Bool = true, isFollowed: Bool = true) -> Array<Report> {
        
        var reports : Array<Report> = [
            Report(imageName: "icon_recordsong", title: "Record this song", type: ReportType.record),
//            Report(imageName: isFollowed ? "icon_unfollow" : "icon_followuser", title: isFollowed ? "Unfollow" : "Follow", type: isFollowed ? .unfollow : .follow, description: isFollowed ? "Stop seeing songs from this user" : "Register to see songs from this user"),
            Report(imageName: "icon_unfollow", title: "Unfollow", type: .unfollow, description: "Stop seeing songs from this user"),
            Report(imageName: "icon_report", title: "Report this song", type: ReportType.report),
            Report(imageName: (isTurnon ? "icon_turnoff_notification" : "icon_turnon_notification"), title: "Turn \((isTurnon ? "off" : "on")) notification for this song", type: ReportType.turnOnNoti)
        ]
        if self.record.tags.contains(where: { $0.id == Setting.shared.getUserId() }) {
            let report = Report(imageName: "ic_letter-x", title: "Remove Tag", type: ReportType.removeTag)
            reports.insert(report, at: 0)
        }
        return reports
    }
    
    private func actionsForMe(isTurnon: Bool = true) -> Array<Report> {
        return [
            Report(imageName: "icon_recordsong", title: "Re-record this song", type: ReportType.record),
            Report(imageName: "icon_delete", title: "Delete this song", type: ReportType.delete),
            Report(imageName: (isTurnon ? "icon_turnoff_notification" : "icon_turnon_notification"), title: "Turn \((isTurnon ? "off" : "on")) notification for this song", type: ReportType.turnOnNoti)
        ]
    }
    
    override func prepareForReuse() {
//        self.setState(voted: self.record.voted)
        self.voteState = .none
        self.viewCountComment.labelCount.text = ""
        self.labelCountVoted?.text = ""
        super.prepareForReuse()
    }
    
}

extension HomePageCell: DismissPopover {
    func dismiss(reportType: ReportType) {
        self.popover.dismiss()
        
        switch reportType {
        case ReportType.record:
            if let recordVc: RecordViewController = StoryBoard.Sing.viewController("RecordViewController") as? RecordViewController {
                recordVc.resource = self.record.resource
//                self.viewController()?.present(recordVc, animated: true, completion: nil)
                self.viewContainingController()?.present(recordVc, animated: true, completion: nil)
            }
        case .unfollow:
            UserService.shared.followUserBy(id: self.record.owner.id, follow: false, callback: { (result) in
                
                if result.status >= 200 && result.status < 300 {
                    self.delegate?.onPress(self.record, action: ActionType.unfollow, index: self.index)
                } else {
                    print("Unfollow response with status code: " + result.status.description)
                }
            })
            self.record.owner.isFollowed = !self.record.owner.isFollowed
//            self.delegate?.onPress(self.record, action: .unfollow, index: self.index)
//        case .follow:
//            UserService.shared.followUserBy(id: self.record.owner.id, follow: true, callback: { (result) in
//                if result.status >= 200 && result.status < 300 {
//                    self.delegate?.onPress(self.record, action: ActionType.unfollow, index: self.index)
//                } else {
//                    print("Unfollow response with status code: " + result.status.description)
//                }
//            })
//            self.record.owner.isFollowed = !self.record.owner.isFollowed
//            self.delegate?.onPress(self.record, action: .unfollow, index: self.index)
        case ReportType.report:
            DispatchQueue.main.async {
                self.delegate?.onPress(self.record, action: ActionType.report, index: self.index)
            }
        case .turnOnNoti:
            self.record.subscribed = !self.record.subscribed
            self.delegate?.onPress(self.record, action: .update, index: self.index)
            
            RecordService.shared.subscribe(id: record.id, value: self.record.subscribed, { (result) in
                print(result.status)
            })
            break
        case .delete:
            self.delegate?.onPress(self.record, action: ActionType.delete, index: self.index)
        case .removeTag:
            if let index = self.record.tags.index(where: { $0.id == Setting.shared.getUserId() }) {
                self.record.tags.remove(at: index)
                self.delegate?.onPress(self.record, action: ActionType.update, index: self.index)
            }
            self.delegate?.onPress(self.record, action: .update, index: self.index)
            RecordService.shared.removeTag(id: self.record.id, callback: { (r) in
                print(r.item.success)
            })
        default:
            print("Other action")
        }
        
    }
}
