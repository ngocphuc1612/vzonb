//
//  CommentCell.swift
//  VZONB
//
//  Created by Phuc on 7/23/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import Popover

protocol CommentCellDelegate {
    func commentCell(didClickOnTagFor id: String)
    func commentCell(didClickReplyFor comment: Comment, section: Int)
    func commentCell(didClickEditFor comment: Comment, section: Int)
    func commentCell(didClickDeleteFor comment: Comment, section: Int)
    func commentCell(loadMoreRepliesFor comment: Comment)
    func commentCell(_ cell: CommentCell, blockedUserAt section: Int)
}

class CommentCell: UITableViewCell {

    @IBOutlet weak var imageAvatar  : CircleImage!
    @IBOutlet weak var labelName    : TTTAttributedLabel!
    @IBOutlet weak var labelContent : TTTAttributedLabel!
    @IBOutlet weak var labelTime    : UILabel!
    
    @IBOutlet weak var buttonEdit   : UIButton!
    @IBOutlet weak var circleEdit   : UIView!
    @IBOutlet weak var buttonDelete   : UIButton!
    @IBOutlet weak var circleDelete   : UIView!
    @IBOutlet weak var deleteContraint: NSLayoutConstraint!
    @IBOutlet weak var replyContraint: NSLayoutConstraint!
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var heightContraint: NSLayoutConstraint!
    @IBOutlet weak var thumbRepliedView   : UIView!
    @IBOutlet weak var thumbRepliedAva   : UIImageView!
    @IBOutlet weak var thumbRepliedName   : UILabel!
    @IBOutlet weak var totalReplied   : UILabel!
    @IBOutlet weak var buttonMore: UIButton!
    @IBOutlet weak var loadMoreRepliesView   : UIView!
    
    var listRowCommentsHeight: [Int : CGFloat] = [:]
    var comment      : Comment = Comment(data: JSON.null)
    var cellIndex        : Int = 0
    var delegate     : CommentCellDelegate?
    
    fileprivate var popover: Popover!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.loadMoreRepliesView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(loadPreviousDidTapped)))
        //
        self.imageAvatar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowProfile)))
        self.labelContent.delegate = self
        self.labelName.delegate = self
    }
    
    //MARK: - Set data
    func binding(comment: Comment) {
        self.imageAvatar.kf_setImage(url: comment.from.avatar)
        //content
        self.labelContent.text = comment.content
        self.labelContent.sizeToFit()
        for tag in comment.mentions {
            if let text: String = self.labelContent.text as? String, text.contains(tag.name) {
                let linkRange = NSMakeRange(tag.startIndex, tag.name.count)
                let range = tag.startIndex + tag.name.count
                if range < comment.content.count {
                    self.labelContent.addLink(toPhoneNumber: tag.id, with: linkRange)
                }
            }
        }
        
        self.labelName.text = comment.from.name
        if let linkRange = ((self.labelName.text as? String) ?? "").range(of: comment.from.name) {
            self.labelName.addLink(toPhoneNumber: comment.from.id, with: ((self.labelName.text as? String) ?? "").nsRange(from: linkRange))
        }
        
        self.labelTime.text = comment.createdAt
        self.labelTime.sizeToFit()
        self.labelTime.layoutIfNeeded()
        self.comment  = comment
        
        self.totalReplied.text = "\(comment.commentCount) replies"

        if comment.commentCount > 0 {
            if let user = comment.lastUserReplied {
                self.thumbRepliedAva.kf_setImage(url: user.avatar)
                self.thumbRepliedName.text = "\(user.name) Replied"
                self.thumbRepliedName.boldPartString(boldString: [user.name], size: 13.0)
            }
            
            if comment.expanded {
                self.thumbRepliedView.isHidden = true
                if comment.childComment.isEmpty {
                    self.loadMoreRepliesView.isHidden = true
                    self.heightContraint.constant = 1
                    self.contentView.frame.size = CGSize(width: self.contentView.frame.width, height: self.contentView.frame.height - 45)
                } else {
                    if comment.lastPage == comment.current {
                        self.loadMoreRepliesView.isHidden = true
                        self.heightContraint.constant = 1
                        self.contentView.frame.size = CGSize(width: self.contentView.frame.width, height: self.contentView.frame.height - 45)
                    } else {
                        self.heightContraint.constant = 53
                        self.loadMoreRepliesView.isHidden = false
                    }
                }
            } else {
                self.heightContraint.constant = 53
                self.thumbRepliedView.isHidden = false
            }
        } else {
            heightContraint.constant = 1
            self.contentView.frame.size = CGSize(width: self.contentView.frame.width, height: self.contentView.frame.height - 45)
            self.thumbRepliedView.isHidden = true
            self.loadMoreRepliesView.isHidden = true
        }
    }
    
    //MARK: - Relayout
    func layoutButtons(isRecordOwner: Bool) {
        var isDelete = false
        var isEdit = false
        if isRecordOwner {
            isDelete = true
            self.buttonDelete.isHidden = false
            self.circleDelete.isHidden = false
        } else {
            isDelete = false
            self.buttonDelete.isHidden = true
            self.circleDelete.isHidden = true
        }
        
        if GlobalInfo.shared.info?.id == self.comment.from.id {
            isEdit = true
            self.buttonEdit.isHidden = false
            self.circleEdit.isHidden = false
        } else {
            isEdit = false
            self.buttonEdit.isHidden = true
            self.circleEdit.isHidden = true
        }
        
        if isEdit && isDelete {
            replyContraint.constant = 82
            deleteContraint.constant = 34
        } else if isEdit {
            replyContraint.constant = 34
            deleteContraint.constant = 0
        } else if isDelete {
            replyContraint.constant = 50
            deleteContraint.constant = 5
        } else {
            replyContraint.constant = 5
        }
        self.contentView.frame.size = CGSize(width: ScreenSize.SCREEN_WIDTH, height: self.contentView.frame.height + self.labelContent.frame.height)
    }
    //MARK: - Actions
    @objc func onShowProfile() {
        self.delegate?.commentCell(didClickOnTagFor: comment.from.id)
    }
    
    @objc func loadPreviousDidTapped() {
        self.delegate?.commentCell(loadMoreRepliesFor: self.comment)
    }
    
    @IBAction func onDelete(_ sender: UIButton) {
        self.delegate?.commentCell(didClickDeleteFor: self.comment, section: self.cellIndex)
    }
    
    @IBAction func onEdit(_ sender: UIButton) {
        self.delegate?.commentCell(didClickEditFor: self.comment, section: self.cellIndex)
    }
    
    @IBAction func onReply(_ sender: UIButton) {
        self.delegate?.commentCell(didClickReplyFor: self.comment, section: self.cellIndex)
    }
    
    @IBAction func onShowPopupAction(_ sender: AnyObject) {
        var reports: Array<Report> = []
        if self.comment.from.id != Setting.shared.getUserId() {
            reports = self.actionsForMe()
        
        let buttonYAxis = sender.superview??.convert((sender as? UIView)?.frame ?? CGRect.zero, to: nil).midY ?? 0
            let heightPopUp: CGFloat = CGFloat(reports.count * 51)
            
            let popupReport = TablePopup(frame: CGRect(x: 0, y: 0, width: self.contentView.frame.width, height: heightPopUp), data: reports)
            popupReport.delegate = self
            let popoverOptions: [PopoverOption] = [
                .type(buttonYAxis > (heightPopUp + 80) ? .up : .down),
                .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6)),
                .cornerRadius(CGFloat(0)),
                .arrowSize(CGSize(width: 8, height: 6))
            ]
            self.popover = Popover(options: popoverOptions)
            self.popover.show(popupReport, fromView: self.buttonMore)
        }
    }
    
    private func actionsForMe() -> Array<Report> {
        return [
            Report(imageName: "icon_unfollow", title: "Block this user", type: ReportType.unfollow)
        ]
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
 
}

extension CommentCell: TTTAttributedLabelDelegate {
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWithPhoneNumber phoneNumber: String!) {
        self.delegate?.commentCell(didClickOnTagFor: phoneNumber)
    }
}

extension CommentCell: DismissPopover {
    func dismiss(reportType: ReportType) {
        self.popover.dismiss()
        switch reportType {
        case .unfollow:
            self.delegate?.commentCell(self, blockedUserAt: self.cellIndex)
            break
        default:
            break
        }
       
    }
}
