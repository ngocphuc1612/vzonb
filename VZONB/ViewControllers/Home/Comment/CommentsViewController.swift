//
//  CommentsViewController.swift
//  VZONB
//
//  Created by Phuc on 7/16/17.
//  Copyright  2017 Phuc. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
import SVPullToRefresh
import IQKeyboardManagerSwift
import DZNEmptyDataSet
import Foundation

enum textViewMode: String {
    case comment = "comment"
    case reply = "reply"
    case edit = "edit"
}

enum editMode: String {
    case comment = "comment"
    case reply = "reply"
}

class CommentsViewController: DefaultChildViewController {
    
    @IBOutlet weak var labelCaption: UILabel!
    @IBOutlet weak var imageAvatar: CircleImage?
    @IBOutlet weak var labelName: TTTAttributedLabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var loadMoreCommentView: UIView?
    @IBOutlet weak var loadMoreHeightConstraint: NSLayoutConstraint?
    //
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textviewComment : KMPlaceholderTextView!
    @IBOutlet weak var buttonSend       : UIButton?
    @IBOutlet weak var constraintBtm    : NSLayoutConstraint?
    @IBOutlet weak var labelCloseReplies: UILabel!
    @IBOutlet weak var constraintCloseReplies: NSLayoutConstraint!
    
    var lastPage    : UInt8 = 1
    var current     : UInt8 = 1
    var isUpdating  : Bool =  false
    var usersTagged : Array<ShortUser> = []
    var mentions : Array<TagComment> = []
    var tagUsed     : [String: Bool] = [:]
    var taggedIndex : [Int : ShortUser] = [:]
    private let boldFont = UIFont(name: "SanFranciscoDisplay-Semibold", size: 16)!
    private let normalFont = UIFont(name: "SanFranciscoDisplay-Regular", size: 16)!
    
    fileprivate var iqkeyboardEnable: Bool = false {
        didSet {
            DispatchQueue.main.async { [unowned self] in
                IQKeyboardManager.shared.enable = self.iqkeyboardEnable
                IQKeyboardManager.shared.enableAutoToolbar = self.iqkeyboardEnable
            }
        }
    }
    
    weak var delegate: UpdateRecord? = nil
    var comments: Array<Comment> = Array<Comment>()
    var recordId: String = ""
    var record  : Record?
    var listSectionComment : [Int : CommentCell] = [:]
    var parrentID = ""
    var textMode: textViewMode = .comment
    var editMode: editMode = .comment
    var editIndex: IndexPath = IndexPath(row: 0, section: 0)
    var replyCommentID = ""
    var highlightIndex: IndexPath?
    
    func getComments() {
//        self.tableView.showLoading()
        DispatchQueue.global().async { [weak self] in
            guard let `self` = self else { return }
            RecordService.shared.getComments(id: self.recordId, callback: { (result) in
                if result.status == 200 {
                    self.comments = result.items.reversed()
                    self.current = result.current
                    self.lastPage   = result.lastPage
                    self.layoutLoadMoreComent()
                    self.tableView.reloadData()
                    //TODO: - Reload table
                    DispatchQueue.main.async {
                        //TODO: - Reload table
                        if self.parrentID.count > 0 {
                            let section = self.findSectionOf(commentID: self.parrentID)
                            if section != NSNotFound {
                                self.tableView.scrollToRow(at: IndexPath(row: NSNotFound, section: section), at: .middle, animated: true)
                            } else {
                                print("Not Found")
                            }
                        }
                    }
                }
//                DispatchQueue.main.async {
//                    self.tableView.hideLoading(completionHandler: {
//                        self.tableView.emptyDataSetDelegate = self
//                        self.tableView.emptyDataSetSource   = self
//                    })
//                }
            })
        }
    }
    
   @objc func getMoreComments() {
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            if !self.isUpdating{
                self.isUpdating = true
                let next = self.current + 1
                if next <= self.lastPage {
                    RecordService.shared.getComments(id: self.recordId, page: next, callback: { (result) in
                        if result.status == 200 {
                            self.current = result.current
                            self.layoutLoadMoreComent()
                            self.comments.insert(contentsOf: result.items.reversed(), at: 0)
                            self.tableView.reloadData()
                            self.resetView()
                        }
                        self.isUpdating = false
                    })
                }
            }
        }
    }
    
    private func configUis() {
        // TextView
        self.textviewComment?.layer.cornerRadius = 5
        self.textviewComment?.layer.borderColor = UIColor(rgb: 0x0074b0).cgColor
        self.textviewComment?.layer.borderWidth = 0.5
        self.textviewComment?.clipsToBounds = true
        self.buttonSend?.isEnabled = false
        self.buttonSend?.alpha = 0.5
        self.textviewComment?.delegate = self
        //
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(getMoreComments))
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            self.loadMoreCommentView?.addGestureRecognizer(tapGesture)
            if let imageAvatar = self.imageAvatar {
                imageAvatar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowProfile)))
            }
        }
    }
    
    override func viewDidLoad() {
        self.getComments()
        ///
        super.viewDidLoad()
        ///
        self.configUis()
        self.tableView.register(UINib(nibName: "RepliesCell", bundle: nil), forCellReuseIdentifier: "repliesCell")
        self.tableView.register(UINib(nibName: "RepliesTextCell", bundle: nil), forCellReuseIdentifier: "repliesTextCell")
        self.tableView.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: "commentCell")
        self.title = "Comments"
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = Color.default
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.addPullToRefresh()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.iqkeyboardEnable = false
        self.bindindgData()
       // self.navigationController?.setNavigationBarHidden(false, animated: false)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onShowKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
//        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.iqkeyboardEnable = true
        NotificationCenter.default.removeObserver(self)
        self.parrentID = ""
    }
    
    @objc func onShowKeyboard(_ notification: Notification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
//            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
//            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
//            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
//                UIView.animate(withDuration: duration,
//                               delay: TimeInterval(0),
//                               options: animationCurve,
//                               animations: { self.constraintBtm?.constant = 0.0 },
//                               completion: nil)
                
                self.constraintBtm?.constant = 0.0
                UIView.animate(withDuration: duration, animations: {
                    self.view.layoutSubviews()
                })
            } else {
                self.constraintBtm?.constant = (endFrame?.size.height ?? 0) // - 40.0
                UIView.animate(withDuration: duration, animations: {
                    self.view.layoutSubviews()
                })
//                UIView.animate(withDuration: duration,
//                               delay: TimeInterval(0),
//                               options: animationCurve,
//                               animations: { self.constraintBtm?.constant = (endFrame?.size.height ?? 40.0)/* - 40.0*/ },
//                               completion: nil)
            }
            
        }
    }
    override func viewWillLayoutSubviews() {
//        self.navigationController?.setNavigationBarHidden(false, animated: true)
        super.viewWillLayoutSubviews()
    }
    //MARK: - binding record data
    func bindindgData() {
        if let record = self.record {
            self.labelName.text = record.owner.name
            if record.tags.count > 0 {
                var appendText: String = ""
                
                if record.tags.count > 0 {
                    if record.tags.count > 2 {
                        appendText = " is with \(record.tags[0].fullname) and \((record.tags.count - 1).description) others"
                        self.labelName.text = ((self.labelName.text as? String) ?? "") + appendText

                        if let linkRange = ((self.labelName.text as? String) ?? "").range(of: record.tags[0].fullname) {
                            self.labelName.addLink(toPhoneNumber: record.tags[0].id, with: ((self.labelName.text as? String) ?? "").nsRange(from: linkRange))
                        }
                        if let linkRange = ((self.labelName.text as? String) ?? "").range(of: "\((record.tags.count - 1).description) others") {
                            self.labelName.addLink(toPhoneNumber: "others", with: ((self.labelName.text as? String) ?? "").nsRange(from: linkRange))
                        }
                    } else {
                        appendText = "is with \(record.tags[0].fullname)"
                        self.labelName.text = ((self.labelName.text as? String) ?? "") + appendText

                        if let linkRange = ((self.labelName.text as? String) ?? "").range(of: record.tags[0].fullname) {
                            self.labelName.addLink(toPhoneNumber: record.tags[0].id, with: ((self.labelName.text as? String) ?? "").nsRange(from: linkRange))
                        }
                    }
                }
            }
            //add link to name
            if let linkRange = ((self.labelName.text as? String) ?? "").range(of: record.owner.name) {
                self.labelName.addLink(toPhoneNumber: record.owner.id, with: ((self.labelName.text as? String) ?? "").nsRange(from: linkRange))
            }
            self.labelCaption.text = record.caption
            self.labelTime.text = record.createdAt
            self.imageAvatar?.kf_setImage(url: record.owner.avatar)
        }
    }
    //MARK: - check display loadmore comment button
    
    func layoutLoadMoreComent() {
        if self.current < self.lastPage {
            loadMoreHeightConstraint?.constant = 20
            loadMoreCommentView?.isHidden = false
        } else {
            loadMoreHeightConstraint?.constant = 0
            loadMoreCommentView?.isHidden = true
        }
    }
    
    //MARK: - Pull to refresh
 func addPullToRefresh () {
        self.tableView.addPullToRefresh { [weak self] in
            guard let `self` = self else { return }
            RecordService.shared.getComments(id: self.recordId, callback: { (result) in
                if result.status == 200 {
                    print("token \(Setting.shared.getToken())")
                    print("record id: \(self.recordId)")
                    self.comments = result.items.reversed()
                    self.current = result.current
                    self.lastPage   = result.lastPage
                    self.tableView.reloadData()
                    //TODO: - Reload table
                   
                }
                //                DispatchQueue.main.async {
                //                    self.tableView.hideLoading(completionHandler: {
                //                        self.tableView.emptyDataSetDelegate = self
                //                        self.tableView.emptyDataSetSource   = self
                //                    })
                //                }
                self.tableView?.pullToRefreshView.stopAnimating()
            })
            
        }
    }
    
    //MARK: -  Validate
    func validateTextField() {
        let content: String = self.textviewComment.text
        if content.isEmpty {
            MessageManager.shared.show(title: "Sorry", body: "Content can't be empty", style: MessageManager.Style.error)
            return
        }
    }
    //MARK: - API
    func replyComment() {
        let content = self.textviewComment.text
        self.setActiveSendButton(false)
        RecordService.shared.replyComment(id: self.replyCommentID, content: content!, mentions: mentions, callback: { [unowned self] (result) in
            if result.status == 200 {
                self.textviewComment?.text = ""
                if self.record != nil {
                    self.record?.comments += 1
                    self.delegate?.update(self.record!)
                    
                    var newReply = result.item
                    
                    if let user = GlobalInfo.shared.info {
                        newReply.from = Person(name: user.fullname, id: user.id)
                        newReply.from.avatar = user.avatarUrl
                    }
                    
                    let section = self.findSectionOf(commentID: self.replyCommentID)
                    if section != NSNotFound {
                        self.comments[section].childComment.append(newReply)
                        self.parrentID = newReply.id
                        self.tableView.reloadData()
                    }
                    self.closeReplyMode()
                }
            }
            self.setActiveSendButton(true)
        })
    }
    
    func postComment() {
        let content = self.textviewComment.text
        self.setActiveSendButton(false)
        RecordService.shared.addComment(recordId: self.recordId, content: content!, mentions: self.mentions) { [unowned self] (result) in
            if result.status == 200 {
                self.textviewComment?.text = ""
                if self.record != nil {
                    self.record?.comments += 1
                    self.delegate?.update(self.record!)

                    var newReply = result.item
                    if let user = GlobalInfo.shared.info {
                        newReply.from = Person(name: user.fullname, id: user.id)
                        newReply.from.avatar = user.avatarUrl
                    }

                    self.comments.append(newReply)
                    self.parrentID = newReply.id
                    self.tableView.reloadData()
                    for i in 0..<self.comments.count {
                        _ = self.tableView(self.tableView, viewForHeaderInSection: i)
                    }
                }
            }
            self.setActiveSendButton(true)
        }
    }
    
    func editComment() {
        let content = self.textviewComment.text
        self.setActiveSendButton(false)
        RecordService.shared.editComment(id: self.replyCommentID, content: content!, mentions: mentions) { [unowned self] (result) in
            if result.status == 200 {
                self.textviewComment?.text = ""
                
                if self.record != nil {
                    let newReply = result.item
                    
                    if self.editMode == .comment {
                        self.comments[self.editIndex.section].content = newReply.content
                        self.comments[self.editIndex.section].mentions = newReply.mentions
                    } else if self.editMode == .reply {
                        self.comments[self.editIndex.section].childComment[self.editIndex.row].content = newReply.content
                        self.comments[self.editIndex.section].childComment[self.editIndex.row].mentions = newReply.mentions
                    }
                    self.tableView.reloadData()
                    self.closeReplyMode()
//                    for i in 0..<self.comments.count {
//                        _ = self.tableView(self.tableView, viewForHeaderInSection: i)
//                    }
                }
            }
            
            self.setActiveSendButton(true)
        }
    }
    
    //MARK: - Actions
    @objc func onShowProfile() {
        if let record = self.record {
            if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
                profileVc.isMe = false
                profileVc.id   = record.owner.id
                self.navigationController?.pushViewController(profileVc, animated: true)
            }
        }
        
    }
    
    //MARK: Prevent fast click
    fileprivate func setActiveSendButton(_ active: Bool) {
        if active {
            self.buttonSend?.isEnabled = true
            self.buttonSend?.alpha = 1
        } else {
            self.buttonSend?.isEnabled = false
            self.buttonSend?.alpha = 0.5
        }
    }
    
    @IBAction func onPostComment(_ sender: Any?) {
        self.validateTextField()
        
        if self.textMode == .comment {
            self.postComment()
        } else if self.textMode == .reply {
            self.replyComment()
        } else if self.textMode == .edit {
            self.editComment()
        }
        
        self.clearTagCached()
    }
    
    @IBAction func onChooseTags(_ sender: Any?) {
//        ChooseTagUserViewController
        if let desVC = StoryBoard.Sing.viewController("ChooseTagUserViewController") as? ChooseTagUserViewController{
            desVC.users = self.usersTagged
            desVC.delegate = self
//            self.navigationController?.pushViewController(desVC, animated: true)
            self.present(desVC, animated: true, completion: nil)
        }
    }
    
}
//MARK: - Textview delegate
extension CommentsViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.trimValue().isEmpty {
            self.buttonSend?.isEnabled = false
            self.buttonSend?.alpha = 0.5
        } else {
            self.buttonSend?.isEnabled = true
            self.buttonSend?.alpha = 1
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if range.length != 0 {
            var currentIndex = range.location + range.length
            for tag in mentions {
                let startIndex = tag.startIndex
                let endIdx = startIndex + tag.name.count
                if startIndex ... endIdx ~= currentIndex {
                    currentIndex = endIdx
                }
            }
            if let user = self.taggedIndex[currentIndex], let string = self.textviewComment.text {
                //Delete from mentions array
                if let idx = self.mentions.index(where: {$0.id == user.id}) {
                    self.mentions.remove(at: idx)
                }
                //Delete from list tagged
                if let idx = self.usersTagged.index(where: {$0.id == user.id}) {
                    self.usersTagged.remove(at: idx)
                }
                self.taggedIndex.removeValue(forKey: currentIndex)
                //Allow retag
                self.tagUsed.removeValue(forKey: user.id)
                //Delete text
                let subStrings = string.components(separatedBy: user.fullname)
                var replaceText = ""
                for string in subStrings {
                    if string != user.fullname {
                        replaceText += string
                    }
                }
                self.textviewComment.text = replaceText
                self.textviewComment.boldPartString(boldTag: self.mentions, boldFont: self.boldFont, normalFont: self.normalFont)
                //re add
                for tag in mentions {
                    let shortUser = ShortUser(id: tag.id, fullname: tag.name)
                    let startIndex = (self.textviewComment.text as NSString).range(of: tag.name)
                    let endIndex = startIndex.location + tag.name.count
                    self.taggedIndex[endIndex] = shortUser
                }
                return false
            }
        } else {
            let index = range.location
            for tag in mentions {
                let startIndex = (self.textviewComment.text as NSString).range(of: tag.name)
                let endIdx = startIndex.location + tag.name.count
                if startIndex.location ... endIdx ~= index {
                    //Delete from mentions array
                    if let idx = self.mentions.index(where: {$0.id == tag.id}) {
                        self.mentions.remove(at: idx)
                    }
                    //Delete from list tagged
                    if let idx = self.usersTagged.index(where: {$0.id == tag.id}) {
                        self.usersTagged.remove(at: idx)
                    }
                    self.taggedIndex.removeValue(forKey: endIdx)
                    //Allow retag
                    self.tagUsed.removeValue(forKey: tag.id)
                    //
                    var currentText = Array(self.textviewComment.text)
                    let replaceText = Array(text)
                    currentText.insert(contentsOf: replaceText, at: index)
                    self.textviewComment.text = String(currentText)
                    self.textviewComment.boldPartString(boldTag: self.mentions, boldFont: self.boldFont, normalFont: self.normalFont)
                    return false
                }
            }
        }
        return true
    }
}

extension CommentsViewController: UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - Header
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let sectionView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 10))
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell") as! CommentCell
        
        if section == 0 {
            cell.lineView.isHidden = true
        }
        
        cell.delegate = self
        cell.binding(comment:  self.comments[section])
        cell.cellIndex = section
        if (GlobalInfo.shared.info?.id == self.record?.owner.id) {
            cell.layoutButtons(isRecordOwner: true)
        } else {
            cell.layoutButtons(isRecordOwner: false)
        }
        listSectionComment[section] = cell
        let headerTapped = UITapGestureRecognizer(target: self, action: #selector(sectionHeaderTapped))
        cell.thumbRepliedView.tag = section
        cell.thumbRepliedView.addGestureRecognizer(headerTapped)
        sectionView.addSubview(cell.contentView)
        return sectionView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.comments.count
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if self.parrentID.count > 0 {
            let _section = self.findSectionOf(commentID: self.parrentID)
            if _section == section {
                view.backgroundColor = .groupTableViewBackground
                UIView.animate(withDuration: 0.5, delay: 1.0, animations: {
                    view.backgroundColor = .white
                    self.parrentID = ""
                })
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let cell = listSectionComment[section] {
            return cell.contentView.frame.height
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 93
    }
    
    //MARK: - Footer
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }

    //MARK: - Rows
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.comments[section].childComment.count > 0 {
            return self.comments[section].childComment.count + 1
        }
        return self.comments[section].childComment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row  == self.comments[indexPath.section].childComment.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "repliesTextCell", for: indexPath) as! RepliesTextCell
            
            cell.comment = self.comments[indexPath.section]
            cell.section = indexPath.section
            cell.delegate = self
            cell.contentView.frame.size = CGSize(width: tableView.frame.width, height: cell.contentView.frame.height)
            return cell
        } else {
            let comment = self.comments[indexPath.section].childComment[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "repliesCell", for: indexPath) as! RepliesCell
            cell.binding(comment: comment)
            cell.cellIndex = indexPath
            if (GlobalInfo.shared.info?.id == self.record?.owner.id) {
                cell.layoutButtons(isRecordOwner: true)
            } else {
                cell.layoutButtons(isRecordOwner: false)
            }
            cell.delegate = self
          self.listSectionComment[indexPath.section]?.listRowCommentsHeight[indexPath.row] = cell.frame.height
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
        if indexPath.row  == self.comments[indexPath.section].childComment.count {
            let cell = self.comments[indexPath.section]
            if cell.current < cell.lastPage {
                return 95
            }
            return 49
        }
        
        if let sectionCell = listSectionComment[indexPath.section], let rowCell = sectionCell.listRowCommentsHeight[indexPath.row] {
            return rowCell
        } else {
            return 59
        }
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 10
    }
    
    // MARK: - Section header gesture
    
    @objc func sectionHeaderTapped(gestureRecognizer: UITapGestureRecognizer){
        if let view = gestureRecognizer.view{
            let section = view.tag
            let item = self.comments[section]
            if item.commentCount >= 20 {
                let desVc = self.storyboard?.instantiateViewController(withIdentifier: "DetailCommentsViewController") as? DetailCommentsViewController
                desVc?.comments.append(item)
                self.navigationController?.pushViewController(desVc!, animated: true)
            } else {
              self.getReplies(for: section)
            }
        }
        
    }
    
    func getMoreReplies(for section: Int) {
        let next = self.comments[section].current + 1
        DispatchQueue.global().async { [weak self] in
            guard let `self` = self else { return }
            RecordService.shared.getCommentReplies(id: self.comments[section].id, page: next, callback: { (result) in
                if result.status == 200 {
                    self.comments[section].current = result.current
                    self.comments[section].expanded = true
                    
                    self.current = result.current
                    self.comments[section].childComment.insert(contentsOf: result.items.reversed(), at: 0)
                    self.tableView.reloadData()
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
//                    var indexs = Array<IndexPath>()
//                    let count =  self.comments[section].childComment.count
//                    for iter in 0..<result.items.count {
//                        indexs.append(IndexPath(row: count + iter, section: section))
//                    }
//                    self.comments[section].childComment.append(contentsOf: result.items.reversed())
//                    self.tableView.beginUpdates()
//                    self.tableView.insertRows(at: indexs, with: .automatic)
//                    self.tableView.reloadData()
//                    self.tableView.endUpdates()
                }
            })
        }
    }
    
    func getReplies(for section: Int) {
        DispatchQueue.global().async { [weak self] in
            guard let `self` = self else { return }
            RecordService.shared.getCommentReplies(id: self.comments[section].id, callback: { (result) in
                if result.status == 200 {
                    self.comments[section].childComment = result.items.reversed()
                    self.comments[section].current = result.current
                    self.comments[section].lastPage = result.lastPage
                    self.comments[section].expanded = true
                    
                    self.tableView.reloadData()
                    _ = self.tableView(self.tableView, viewForHeaderInSection: section)
                    let count = self.comments[section].childComment.count
                    if count > 0 {
                        self.tableView.scrollToRow(at: IndexPath(row: count - 1, section: section), at: .top, animated: true)
                        self.highlightIndex = IndexPath(row: count - 1, section: section)
                    }
                }
            })
        }
    }
    
}

//MARK: - CommentCell Delegate

extension CommentsViewController: CommentCellDelegate {
    
    func commentCell(didClickOnTagFor id: String) {
        if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
            profileVc.isMe = false
            profileVc.id = id
            self.navigationController?.pushViewController(profileVc, animated: true)
        }
    }
    
    func commentCell(didClickReplyFor comment: Comment, section : Int) {
        if !comment.expanded && comment.commentCount > 0 {
            self.getReplies(for: section)
        } else if comment.expanded{
            let count = self.comments[section].childComment.count
            if count > 0 {
                self.tableView.scrollToRow(at: IndexPath(row: count - 1, section: section), at: .top, animated: true)
                self.highlightIndex = IndexPath(row: count - 1, section: section)
            }
        }
        self.replyCommentID = comment.id
        openReplyMode(for: comment)
    }
    
    func commentCell(didClickDeleteFor comment: Comment, section : Int) {
        self.showMessageWithAction(with: "Notice", message: "Would you like to delete this message?", title: "Delete", cancelTitle: "Cancel", okStyle: UIAlertActionStyle.destructive) { [weak self] in
            guard let `self` = self else { return }
            RecordService.shared.deleteComment(id: comment.id, callback: { status in
                switch status {
                case "200":
                    break
                default:
                    MessageManager.shared.show(title: "Sorry", body: "Something went wrong! Please try again later", style: MessageManager.Style.error)
                    break
                }
            })
            //
            self.comments.remove(at: section)
//            self.tableView.reloadData()
//            if section > self.comments.count - 1 {
//                _ = self.tableView(self.tableView, viewForHeaderInSection: self.comments.count - 1)
//            } else {
//                _ = self.tableView(self.tableView, viewForHeaderInSection: section)
//            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func commentCell(loadMoreRepliesFor comment: Comment) {
        let section = self.findSectionOf(commentID: comment.id)
        self.getMoreReplies(for: section)
    }
    
    func commentCell(didClickEditFor comment: Comment, section: Int) {
        self.editMode = .comment
        self.replyCommentID = comment.id
        self.editIndex = IndexPath(row: 0, section: section)
        openEditMode(for: comment)
    }
    
    func commentCell(_ cell: CommentCell, blockedUserAt section: Int) {
        
        self.showMessageWithAction(with: "Notice", message: "You are blocking this user. Are you sure?", title: "Block", cancelTitle: "Cancel", okStyle: UIAlertActionStyle.destructive) { [weak self] in
            UserService.shared.blockUser(id: cell.comment.from.id, callback: { status in
                switch status {
                case "200":
                    self?.getComments()
                default:
                    MessageManager.shared.show(title: "Sorry", body: "Something went wrong! Please try again later", style: MessageManager.Style.error)
                }
            })
            
        }
    }
    
    //MARK: - Cell ultility
    
    func resetView() {
        for i in 0..<self.comments.count {
            _ = self.tableView(self.tableView, viewForHeaderInSection: i)
        }
    }
    
    func findSectionOf(commentID: String) -> Int {
        for i in 0..<self.comments.count {
            if self.comments[i].id == commentID {
               return i
            }
        }
        return NSNotFound
    }
 
    func openReplyMode(for comment: Comment){
        self.textviewComment?.becomeFirstResponder()
        self.buttonSend?.setTitle("Reply", for: .normal)
        self.textviewComment?.placeholder = "Write a reply..."
        self.labelCloseReplies.text = "Replying to \(comment.from.name)"
        self.labelCloseReplies.boldPartString(boldString: [comment.from.name], size: 13.0)
        self.textMode = .reply
        UIView.animate(withDuration: 0.5, animations: {
            self.constraintCloseReplies.constant = 1
        })
        //
        
    }
    
    func openEditMode(for comment: Comment){
        self.textviewComment?.becomeFirstResponder()
        self.buttonSend?.setTitle("Done", for: .normal)
        self.textviewComment?.text = " " + comment.content + " "
        self.labelCloseReplies.text = "Editing comment"
        self.textMode = .edit
        //handle tag
        for tag in comment.mentions {
            var shortUser = ShortUser(id: tag.id, fullname: tag.name)
            shortUser.isTagged = true
            let startIndex = (self.textviewComment.text as NSString).range(of: tag.name)
            let endIndex = startIndex.location + tag.name.count
            self.usersTagged.append(shortUser)
            self.tagUsed[tag.id] = true
            self.taggedIndex[endIndex] = shortUser
            self.mentions = comment.mentions
        }
        self.textviewComment.boldPartString(boldTag: self.mentions, boldFont: self.boldFont, normalFont: self.normalFont)
        UIView.animate(withDuration: 0.5, animations: {
            self.constraintCloseReplies.constant = 1
        })
    }
    
    @IBAction func closeReplyMode() {
        self.replyCommentID = ""
        self.textMode = .comment
        self.clearTagCached()
        UIView.animate(withDuration: 0.5, animations: {
            self.buttonSend?.setTitle("Post", for: .normal)
            self.textviewComment?.placeholder = "Write a comment..."
            self.textviewComment?.text = ""
            self.constraintCloseReplies.constant = -27
        })
    }
 
    func clearTagCached() {
        self.usersTagged = []
        self.mentions = []
        self.tagUsed = [:]
        self.taggedIndex = [:]
    }
}

//MARK: - ReplyCell Delegate

extension CommentsViewController: RepliescellDelegate {
    
    func replyCell(didClickOnTagFor id: String) {
        if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
            profileVc.isMe = false
            profileVc.id = id
            self.navigationController?.pushViewController(profileVc, animated: true)
        }
    }
    
    func replyCell(didClickReplyFor comment: Comment) {
        self.replyCommentID = comment.parentId
        let section = self.findSectionOf(commentID: comment.parentId)
        let count = self.comments[section].childComment.count
        if count > 0 {
            self.tableView.scrollToRow(at: IndexPath(row: count - 1, section: section), at: .top, animated: true)
            self.highlightIndex = IndexPath(row: count - 1, section: section)
        }
        openReplyMode(for: comment)
    }
    
    func replyCell(didClickEditFor comment: Comment, index: IndexPath) {
        self.editMode = .reply
        self.replyCommentID = comment.id
        self.editIndex = index
        openEditMode(for: comment)
    }
    
    func replyCell(didClickDeleteFor comment: Comment, index: IndexPath) {
        self.showMessageWithAction(with: "Notice", message: "Are you sure delete this comment?", title: "Delete", cancelTitle: "Cancel", okStyle: UIAlertActionStyle.destructive, completion: {
            // Delete core data
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                RecordService.shared.deleteComment(id: comment.id, callback: { status in
                    switch status {
                    case "200":
                        break
                    default:
                        MessageManager.shared.show(title: "Sorry", body: "Something went wrong! Please try again later", style: MessageManager.Style.error)
                        break
                    }
                })
                ///
                self.comments[index.section].childComment.remove(at: index.row)
                self.tableView.reloadData()
            }
        })
    }
}

extension CommentsViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "ic_history")!
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "Be the first one comment on this record", attributes: [NSAttributedStringKey.font: Font.emptyData])
    }
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

//Scroll view
extension CommentsViewController {
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if let index = self.highlightIndex {
            let cell = self.tableView.cellForRow(at: index)
            cell?.contentView.backgroundColor = .groupTableViewBackground
            UIView.animate(withDuration: 2, animations: {
                cell?.contentView.backgroundColor = .white
            })
            self.highlightIndex = nil
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//
        if(velocity.y>0) {
            //Code will work without the animation block.I am using animation block incase if you want to set any delay to it.
                self.navigationController?.setNavigationBarHidden(true, animated: true)

        } else {
                self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
}

extension CommentsViewController: ChooseTagUserViewControllerDelegate {
    func didDoneSelect(with users: Array<ShortUser>) {
        self.usersTagged = users
        for user in users {
            if self.tagUsed[user.id] == nil {
                let index = self.textviewComment.text.count + 1
                self.textviewComment.text = self.textviewComment.text + " \(user.fullname) "
                let endIndex = index + user.fullname.count
                self.taggedIndex[endIndex] = user
                self.tagUsed[user.id] = true
                //
                let tagComment = TagComment(id: user.id, name: user.fullname, startIndex: index)
                mentions.append(tagComment)
            }
        }
       self.textviewComment.boldPartString(boldTag: self.mentions, boldFont: self.boldFont, normalFont: self.normalFont)
        self.textViewDidChange(self.textviewComment)
    }
}

extension CommentsViewController: TTTAttributedLabelDelegate {
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWithPhoneNumber phoneNumber: String!) {
        if let record = self.record, phoneNumber == "others"{
            if let vc = StoryBoard.User.viewController("UserTaggedViewController") as? UserTaggedViewController {
                vc.users = record.tags
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
                profileVc.isMe = false
                profileVc.id   = phoneNumber
                self.navigationController?.pushViewController(profileVc, animated: true)
            }
        }
    }
}
