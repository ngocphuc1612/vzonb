//
//  DetailCommentViewController.swift
//  VZONB
//
//  Created by Gà Nguy Hiểm on 10/4/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class DetailCommentsViewController: CommentsViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.closeReplyMode()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "\(self.comments[0].from.name)'s Comment"
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func getComments() {
        RecordService.shared.getCommentReplies(id: self.comments[0].id, callback: { (result) in
            if result.status == 200 {
                self.comments[0].childComment = result.items.reversed()
                self.comments[0].current = result.current
                self.comments[0].lastPage = result.lastPage
                self.comments[0].expanded = true
                self.tableView.reloadData()
                _ = self.tableView(self.tableView, viewForHeaderInSection: 0)
                
                if self.comments[0].childComment.count > 0 {
                    self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                }
            }
        })
    }
    
    //MARK: - Pull to refresh
    override func addPullToRefresh () {
        self.tableView.addPullToRefresh { [weak self] in
            guard let `self` = self else { return }
            RecordService.shared.getCommentReplies(id: self.comments[0].id, callback: { (result) in
                if result.status == 200 {
                    self.comments[0].childComment = result.items.reversed()
                    self.comments[0].current = result.current
                    self.comments[0].lastPage = result.lastPage
                    self.comments[0].expanded = true
                    self.tableView.reloadData()
                    _ = self.tableView(self.tableView, viewForHeaderInSection: 0)
                }
                self.tableView.pullToRefreshView.stopAnimating()
            })
        }
    }
 
    override func closeReplyMode() {
        self.textMode = .reply
        UIView.animate(withDuration: 0.5, animations: {
            self.buttonSend?.setTitle("Reply", for: .normal)
            self.textviewComment?.placeholder = "Reply to \(self.comments[0].from.name)'s comment..."
            self.textviewComment?.text = ""
            self.constraintCloseReplies.constant = -27
        })
    }
}
