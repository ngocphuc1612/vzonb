//
//  CommentCell.swift
//  VZONB
//
//  Created by Phuc on 7/23/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

protocol RepliescellDelegate {
    func replyCell(didClickOnTagFor id: String)
    func replyCell(didClickReplyFor comment: Comment)
    func replyCell(didClickEditFor comment: Comment, index: IndexPath)
    func replyCell(didClickDeleteFor comment: Comment, index: IndexPath)
}

class RepliesCell: UITableViewCell {

    @IBOutlet weak var imageAvatar  : CircleImage!
    @IBOutlet weak var labelName    : TTTAttributedLabel!
    @IBOutlet weak var labelContent : TTTAttributedLabel!
    @IBOutlet weak var labelTime    : UILabel!
    
    @IBOutlet weak var buttonEdit   : UIButton!
    @IBOutlet weak var circleEdit   : UIView!
    @IBOutlet weak var buttonDelete   : UIButton!
    @IBOutlet weak var circleDelete   : UIView!
    @IBOutlet weak var deleteContraint: NSLayoutConstraint!
    @IBOutlet weak var replyContraint: NSLayoutConstraint!
    
    var comment      : Comment = Comment(data: JSON.null)
    var cellIndex    : IndexPath = IndexPath(row: 0, section: 0)
    var delegate     : RepliescellDelegate?
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var newF = newFrame
            let frame = UIScreen.main.bounds
            newF.size.width = frame.width * 0.80 // get 80% width here
            newF.origin.x = 30
            super.frame = newF
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageAvatar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowProfile)))
        self.labelContent.delegate = self
        self.labelName.delegate = self
        // Initialization code
    }
    
    //MARK: - Set data
    func binding(comment: Comment) {
        self.imageAvatar.kf_setImage(url: comment.from.avatar)
        //content
        self.labelContent.text = comment.content
        self.labelContent.sizeToFit()
        for tag in comment.mentions {
            if let text = self.labelContent.text as? String, text.contains(tag.name) {
                let linkRange = NSMakeRange(tag.startIndex, tag.name.count)
                let range = tag.startIndex + tag.name.count
                if range < comment.content.count {
                    self.labelContent.addLink(toPhoneNumber: tag.id, with: linkRange)
                }
            }
        }
        //
        self.labelName.text = comment.from.name
        if let linkRange = ((self.labelName.text as? String) ?? "").range(of: comment.from.name) {
            self.labelName.addLink(toPhoneNumber: comment.from.id, with: ((self.labelName.text as? String) ?? "").nsRange(from: linkRange))
        }
        self.labelTime.text = comment.createdAt
        self.labelTime.sizeToFit()
//        self.labelTime.layoutIfNeeded()
        self.comment  = comment
    }
    
    //MARK: - Relayout
    func layoutButtons(isRecordOwner: Bool) {
        var isDelete = false
        var isEdit = false
        if isRecordOwner {
            isDelete = true
            self.buttonDelete.isHidden = false
            self.circleDelete.isHidden = false
        } else {
            isDelete = false
            self.buttonDelete.isHidden = true
            self.circleDelete.isHidden = true
        }
        
        if GlobalInfo.shared.info?.id == self.comment.from.id {
            isEdit = true
            self.buttonEdit.isHidden = false
            self.circleEdit.isHidden = false
        } else {
            isEdit = false
            self.buttonEdit.isHidden = true
            self.circleEdit.isHidden = true
        }
        
        if isEdit && isDelete {
            replyContraint.constant = 82
            deleteContraint.constant = 34
        } else if isEdit {
            replyContraint.constant = 34
//            deleteContraint.constant = 0
        } else if isDelete {
            replyContraint.constant = 50
            deleteContraint.constant = 5
        } else {
            replyContraint.constant = 5
        }
        
        self.frame.size = CGSize(width: self.frame.width, height: 53 + self.labelContent.frame.height)
    }
    
    //MARK: - Actions
    @IBAction func onDelete(_ sender: UIButton) {
        self.delegate?.replyCell(didClickDeleteFor: self.comment, index: self.cellIndex)
    }
    
    @IBAction func onEdit(_ sender: UIButton) {
        self.delegate?.replyCell(didClickEditFor: self.comment, index: self.cellIndex)
    }
    
    @IBAction func onReply(_ sender: UIButton) {
        self.delegate?.replyCell(didClickReplyFor: self.comment)
    }
    
    @objc func onShowProfile() {
        self.delegate?.replyCell(didClickOnTagFor: comment.from.id)
    }
}

extension RepliesCell: TTTAttributedLabelDelegate {
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWithPhoneNumber phoneNumber: String!) {
        self.delegate?.replyCell(didClickOnTagFor: phoneNumber)
    }
}
