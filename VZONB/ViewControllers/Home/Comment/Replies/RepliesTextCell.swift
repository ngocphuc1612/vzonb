//
//  CommentCell.swift
//  VZONB
//
//  Created by Phuc on 7/23/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import KMPlaceholderTextView


class RepliesTextCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet weak var imageAvatar  : CircleImage!
    @IBOutlet weak var textView    : KMPlaceholderTextView!
    
    var comment      : Comment = Comment(data: JSON.null)
    
    var section      : Int = 0
    var delegate     : CommentCellDelegate?
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var newF = newFrame
            let frame = UIScreen.main.bounds
            newF.size.width = frame.width * 0.80 // get 80% width here
            newF.origin.x = 30
            super.frame = newF
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if let user = GlobalInfo.shared.info {
            self.imageAvatar?.kf_setImage(url: user.avatarUrl)
        }
        let textViewTap = UITapGestureRecognizer(target: self, action: #selector(textViewDidTapped))
        self.textView.addGestureRecognizer(textViewTap)
    }
    
    @objc func textViewDidTapped() {
        self.delegate?.commentCell(didClickReplyFor: self.comment, section: self.section)
    }
}
