//
//  ChartCell.swift
//  DemoPj
//
//  Created by Gà Nguy Hiểm on 9/25/17.
//  Copyright © 2017 Pandabox. All rights reserved.
//

import UIKit

class ContestChartCell: UIView {

    @IBOutlet weak var chartLabel: UILabel!
    @IBOutlet weak var charView: UIView!
    @IBOutlet weak var chartAvatar: UIImageView!
    @IBOutlet weak var constraint: NSLayoutConstraint!
    @IBOutlet weak var labelFirstname: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    class func instanceFromNib() -> ContestChartCell? {
        guard let chart = UINib(nibName: "ContestChartCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? ContestChartCell else {
            return nil
        }
        chart.setupView()
        return chart
    }
    
    func setupView() {
        self.layoutSubviews()
        self.chartAvatar.layer.cornerRadius = UIScreen.main.bounds.width/28
        self.clipsToBounds = true
    }
    
    func setData(percent: Int, isWin: Bool = false) {
        let realHeight = self.charView.frame.height*63.5/100
        let origin = constraint.constant
        let temp = CGFloat(100 - percent)
        let onePercent = realHeight/100
        if temp != 0 {
            if percent == 0 {
                self.charView.backgroundColor = self.backgroundColor
            } else {
                constraint.constant = onePercent*temp + origin
            }
        }
        if isWin {
            self.charView.backgroundColor = Color.default
            self.chartLabel.textColor = Color.default
        } else {
            self.chartLabel.textColor = UIColor.black
            self.charView.backgroundColor = UIColor(rgb: 0xAAAAAA)
        }
    }
    
    func binding(record: Record) {
        self.chartAvatar.kf_setImage(url: record.owner.avatar)
        self.chartLabel.text = "\(record.likes)"
        self.labelFirstname.text = record.owner.firstname
    }

    
}
