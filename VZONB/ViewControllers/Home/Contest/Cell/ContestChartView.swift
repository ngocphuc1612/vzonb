//
//  ContestChartContainerCell.swift
//  VZONB
//
//  Created by Gà Nguy Hiểm on 9/28/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ContestChartView: UITableViewCell {
    
    @IBOutlet weak var chartView: UIView!
    @IBOutlet weak var labelContestDescription : UILabel!
    @IBOutlet weak var labelEndTime     : UILabel!
    var maxNumber = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func binding(contest: Contest) {
        self.labelContestDescription.text = contest.contestDescription
        self.chartView.frame.size = self.contentView.frame.size
        self.labelEndTime.text = contest.isReleased ? contest.releaseDate : ""
        if self.chartView.subviews.count > 0 {
            for subView in self.chartView.subviews {
                subView.removeFromSuperview()
            }
        }
        var x:CGFloat = 0
        var total:CGFloat = CGFloat(contest.records.count)
        self.maxNumber = self.getMaxNumber(contest.records)
        for record in contest.records {
            let cell = ContestChartCell.instanceFromNib()
            cell?.frame.origin = CGPoint(x: x, y: 0)
            if total <= 4 {
                total = 4
            }
                cell?.frame.size = CGSize(width: chartView.frame.width/total, height: chartView.frame.height)
                
                x+=chartView.frame.width/total
            
            var percent = 100
            if record.likes != self.maxNumber {
                percent = self.calculatePercent(number: record.likes)
            }
            let isWin: Bool = contest.winners.index(where: { $0.id == record.owner.id }) != nil
            cell?.setData(percent: percent, isWin: isWin)
            cell?.binding(record: record)
            self.chartView.addSubview(cell!)
        }
    }
    
    func calculatePercent(number: Int) -> Int {
        let temp = (number * 100)/self.maxNumber
        return temp
    }
    
    func getMaxNumber(_ records: Array<Record>) -> Int {
        var max = records[0].likes
        for record in records {
            if record.likes > max {
                max = record.likes
            }
        }
        return max
    }
    
}
