//
//  ContestViewController.swift
//  VZONB
//
//  Created by Gà Nguy Hiểm on 9/28/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class ContestViewController: BlackHomeViewController {
    @IBOutlet weak var tableView: UITableView!
    //
    fileprivate var contest: Contest = Contest()
    var contestID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        self.title = "Contest"
        self.tableView.register(UINib(nibName: "HomePageCell", bundle: nil), forCellReuseIdentifier: "homePageCell")
        self.tableView.register(UINib(nibName: "ContestChartView", bundle: nil), forCellReuseIdentifier: "contestChartView")
        self.tableView.showLoading()
        self.getContest(id: self.contestID)
        self.addPullToRefresh()
    }
    
    //Pull to refresh
    private func addPullToRefresh () {
        self.tableView.addPullToRefresh { [weak self] in
            guard let `self` = self else { return }
            UserService.shared.getContest(id: self.contestID, { [weak self] (result) in
                if result.status == 200 {
                    self?.contest = result.item
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
                self?.tableView.pullToRefreshView.stopAnimating()
            })
        }
    }
    
    func getContest(id: String) {
        DispatchQueue.global().async {
            UserService.shared.getContest(id: id, { [weak self] (result) in
                guard let `self` = self else { return }
                if result.status == 200 {
                    self.contest = result.item
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                self.tableView.hideLoading(completionHandler: {
                    self.tableView.emptyDataSetDelegate = self
                    self.tableView.emptyDataSetSource   = self
                })
            })
        }
    }
}

//MARK: - TableView
extension ContestViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.contest.records.count > 0 {
            return self.contest.records.count + 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row  == self.contest.records.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "contestChartView", for: indexPath) as! ContestChartView
            cell.frame.size = CGSize(width: self.view.frame.width, height: UIScreen.main.bounds.height - 64)
            cell.binding(contest: self.contest)
            return cell
        } else {
            let item = self.contest.records[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "homePageCell", for: indexPath) as! HomePageCell
            cell.binding(record: item, index: indexPath.row, isProfile: false)
            cell.delegate = self
            cell.labelCaption.text = ""
            cell.hideContestButton()
            ////        cell.delegate = self
            if item.type == "video" {
                cell.constraintCoverHeight?.constant = self.view.bounds.width * 11 / 16
            } else {
                cell.constraintCoverHeight?.constant = self.view.bounds.width * 9 / 16
            }
            cell.layer.shouldRasterize = true
            cell.layer.rasterizationScale = UIScreen.main.scale
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row  == self.contest.records.count {
            return 500
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if(velocity.y>0) {
            //Code will work without the animation block.I am using animation block incase if you want to set any delay to it.
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(true, animated: true)
                print("Hide")
            }, completion: nil)
            
        } else {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                print("Unhide")
            }, completion: nil)
        }
    }
}

extension ContestViewController: HomeOnListenAction {
    func onPress(_ record: Record, action: ActionType, index: Int) {
        switch action {
        case .comment:
            if let commentVc = StoryBoard.Home.viewController("CommentsViewController") as? CommentsViewController {
                commentVc.recordId = record.id
                commentVc.record    = record
//                commentVc.delegate  = self
//                self.currentIndex = index
                self.navigationController?.pushViewController(commentVc, animated: true)
            }
//        case .revote:
//            print("Revoted")
        case .play:
            if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                playVc.record = record
                playVc.contestID = record.current_contest
//                playVc.delegate = self
                self.navigationController?.pushViewController(playVc, animated: true)
            }
//            DispatchQueue.main.async { [unowned self] in
//                if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                    self.contest.records[index].views += 1
//                    playVc.record = record
//                    playVc.records = self.contest.records
//                    playVc.currentIndex = index
                    self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableViewRowAnimation.fade)
//                    self.navigationController?.pushViewController(playVc, animated: true)
//                }
//            }
//        case .report:
//            if let popup = StoryBoard.User.viewController("ReportViewController") as? ReportViewController {
//                popup.modalPresentationStyle = .overFullScreen
//                popup.delegate = self
//                popup.record = record
//                popup.index = index
//                self.present(popup, animated: false, completion: nil)
//            }
        case .share:
            //Share
            let shareText: String = "\(VZONBDomain.shareApi)record/\(record.id)"
            guard let url = URL(string: shareText) else { return }
            let text = record.resourceName
            let activityViewController = UIActivityViewController(activityItems: [url, text], applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
        case .viewProfile:
            if record.owner.id == Setting.shared.getUserId() { return }
            if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
                profileVc.isMe = false
                profileVc.id   = record.owner.id
                self.navigationController?.pushViewController(profileVc, animated: true)
            }
            //        case .unfollow:
            //            self.feeds.remove(at: index)
        //            self.tableView.reloadData()
        case .update:
            self.contest.records[index] = record
//        case .contest:
//            let desVC = ContestViewController(nibName: "ContestViewController", bundle: nil)
//            desVC.contestID = record.current_contest
//            self.navigationController?.pushViewController(desVC, animated: true)
        case .delete:
            self.showMessageWithAction(with: "Notice", message: "Your song will be delete permantly", title: "Delete", cancelTitle: "Cancel", okStyle: .destructive, completion: { [weak self] in
                guard let `self` = self else { return }
                var links: Array<String> = [record.url]
                if record.type == "video" {
                    links.append(record.coverPath)
                }
                RecordService.shared.deleteBy(id: record.id, link: links)
                self.contest.records.remove(at: index)
                self.tableView.reloadData()
            })
        default:break
        }
    }
}

extension ContestViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "ic_history")!
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "There are nothing to show", attributes: [NSAttributedStringKey.font: Font.emptyData])
    }
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
