//
//  HomeTabBar.swift
//  VZONB
//
//  Created by Phuc on 7/18/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class HomeTabBar: UITabBar {
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        if #available(iOS 11.0, *) {
            let bottomInset = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
            if bottomInset > 10 {
                return CGSize(width: size.width, height:68)
            }
            return CGSize(width: size.width, height: 40)
        } else {
            return CGSize(width: size.width, height: 40)
        }
    }
}
