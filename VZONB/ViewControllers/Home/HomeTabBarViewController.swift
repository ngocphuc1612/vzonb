//
//  HomeTabBarViewController.swift
//  VZONB
//
//  Created by Phuc on 7/21/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class HomeTabBarViewController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserService.shared.status { (result) in
            if result.status == 200 {
                GlobalInfo.shared.userStatus = result.item
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    if GlobalInfo.shared.userStatus.badgeMessage > 0 {
                        self.tabBar.items?[3].badgeValue = GlobalInfo.shared.userStatus.badgeMessage.description
                    }
                    if GlobalInfo.shared.userStatus.badgeNoti > 0 {
                        self.tabBar.items?[2].badgeValue = GlobalInfo.shared.userStatus.badgeNoti.description
                    }
                }
            }
        }
        
//        NotificationService.shared.getBadge { (result) in
//            if result.status == 200 {
//                GlobalInfo.shared.numberOfNoti = result.item.badgeNoti
//                GlobalInfo.shared.numberOfMessage = result.item.badgeMessage
//
//                DispatchQueue.main.async {
//                    if GlobalInfo.shared.numberOfMessage > 0 {
//                        self.tabBar.items?[3].badgeValue = GlobalInfo.shared.numberOfMessage.description
//                    }
//
//                    if GlobalInfo.shared.numberOfNoti > 0 {
//                        self.tabBar.items?[2].badgeValue = GlobalInfo.shared.numberOfNoti.description
//                    }
//                }
//            }
//        }
        
        self.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onPushToRecordedScreen(_:)), name: NSNotification.Name.init(NotiName.doneRecord), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onUpdateBadgeNoti(_:)), name: NSNotification.Name.init(NotiName.updateBadge), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.onUpdateBadgeMessage), name: NSNotification.Name.init(NotiName.receiveMessage), object: nil)
        
    }
    
    @objc func onUpdateBadgeNoti (_ noti: Notification) {
        guard let tabIndex: Int = noti.object as? Int else { return }
        if tabIndex == 2 {
            GlobalInfo.shared.userStatus.badgeNoti += 1
            self.tabBar.items?[2].badgeValue = GlobalInfo.shared.userStatus.badgeNoti.description
        } else if tabIndex == 3 {
            GlobalInfo.shared.userStatus.badgeMessage += 1
            self.tabBar.items?[3].badgeValue = GlobalInfo.shared.userStatus.badgeMessage.description
        }
    }
    
//    func onUpdateBadgeMessage () {
//        GlobalInfo.shared.numberOfMessage += 1
//        self.tabBar.items?[3].badgeValue = GlobalInfo.shared.numberOfMessage.description
//    }
    
    @objc func onPushToRecordedScreen(_ noti: Notification) {
        self.selectedIndex = 1
        if let singNavigation = self.viewControllers?[1] as? UINavigationController {
            if let singPager = singNavigation.viewControllers[0] as? SingPagerTabstripController {
                singPager.moveToViewController(at: 2, animated: true)
                singPager.reloadInputViews()
            }
        }
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//        let index = tabBarController.viewControllers?.index(of: viewController) ?? 0
        if let navi = viewController as? UINavigationController {
            if navi.viewControllers.count > 0 {
                if !(navi.viewControllers[0] is ProfileMeViewController) {
                    navi.topViewController?.viewWillAppear(true)
                }
            }
            
        }
//        if index == 4 || index == 3 {
//            UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
//        } else {
//            UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
//        }
        return true
    }
}
