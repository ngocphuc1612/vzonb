//
//  HomeViewController.swift
//  VZONB
//
//  Created by Phuc on 5/14/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import SVPullToRefresh
//import CoreData
import DZNEmptyDataSet
import Crashlytics

class HomeViewController: BlackHomeViewController {
    
    var currentIndex: Int = 0
    @IBOutlet weak var tableView: UITableView!
    //
    var feeds: Array<Feed> = Array<Feed>()
    fileprivate let searchBar = UISearchBar()
    fileprivate var lastPage    : UInt8 = 1
    fileprivate var current     : UInt8 = 1
    
    private func configSearchBar() {
        self.searchBar.placeholder = "Search"
        self.searchBar.delegate = self
        self.searchBar.tintColor = UIColor.black
        self.searchBar.searchBarStyle = .minimal
        self.searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.navigationItem.titleView = searchBar
    }
    
    private func loadFeeds() {
        //MARK: - Load feeds
        UserService.shared.getFeeds({ [weak self] (result) in
            guard let `self` = self else { return }
            if result.status == 200 {
                self.feeds = result.items
                self.current = result.current
                self.lastPage   = result.lastPage
                DispatchQueue.main.async {
                    if self.feeds.count > 0 {
                        self.tableView.reloadData()
                    } else {
                        self.showAdvertise()
                    }
                }
            }
            self.tableView.hideLoading(completionHandler: {
                self.tableView.emptyDataSetDelegate = self
                self.tableView.emptyDataSetSource   = self
            })
        })
    }
    
    override func viewDidLoad() {
        self.loadFeeds()
        ///
        super.viewDidLoad()
        ///Register Cell
        self.tableView.register(UINib(nibName: "HomePageCell", bundle: nil), forCellReuseIdentifier: "homePageCell")
        self.tableView.showLoading()
        
        self.configSearchBar()
        self.addPullToRefresh()
        self.addInfiniteScroll()
        
        UserService.shared.setFirebase(token: Setting.shared.getFirebaseToken(), callback: { status in
            switch status {
            case "200":
                break
            default:
                break
            }
        })
        //MARK: - Check and delete record after 24h
//        CoreDataManager.shared.delAfterDay()
        ///
        DispatchQueue.global().asyncAfter(deadline: DispatchTime.now() + 1.0) {
            RecordService.shared.reportTypes({ (result) in
                if result.status == 200 {
                    DispatchQueue.main.async {
                        Shared.feedReports = result.items
                    }
                }
            })
        }
        self.getInfo()

        //MARK: - Active socket
        VZOSocketManager.shared.connect()
//        SocketManager.shared.auth()
    }
    
    func showAdvertise() {
        let vc = AdvertiseViewController(nibName: "AdvertiseViewController", bundle: nil)
        vc.modalPresentationStyle = .overFullScreen
        
        self.present(vc, animated: false, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        //
//        NotificationCenter.default.addObserver(self, selector: #selector(self.onReceiveAction(_:)), name: NSNotification.Name.init(NotiName.report), object: nil)
//    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        self.navigationController?.setNavigationBarHidden(false, animated: false)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func onUpdateLikeState(_ noti: Notification) {
        if let cell = noti.object as? HomePageCell {
            self.feeds[cell.index].record = cell.record
        }
    }
    
    fileprivate func getInfo() {
        return DispatchQueue.global().async {
            UserService.shared.getInfo({ (result) in
                DispatchQueue.main.async {
                    GlobalInfo.shared.info = result.item
                }
            })
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onUpdateLikeState(_:)), name: NSNotification.Name.init(NotiName.homeUpdate), object: nil)
    }
    
    
    private func addInfiniteScroll() {
        self.tableView?.addInfiniteScrolling(actionHandler: { [weak self] in
            guard let `self` = self else { return }
            let next = self.current + 1
            if next <= self.lastPage {
                UserService.shared.getFeeds(page: next, { (result) in
                    if result.status == 200 {
                        
                        self.current = result.current
                        
                        var indexs = Array<IndexPath>()
                        let count = self.feeds.count
                        for iter in 0..<result.items.count {
                            indexs.append(IndexPath(row: count + iter, section: 0))
                        }
                        self.feeds.append(contentsOf: result.items)
                        self.tableView.beginUpdates()
                        self.tableView.insertRows(at: indexs, with: .automatic)
                        self.tableView.endUpdates()
                    }
                    self.tableView.infiniteScrollingView.stopAnimating()
                })
            } else {
                self.tableView.showsInfiniteScrolling = false
                self.tableView.infiniteScrollingView.stopAnimating()
            }
        })
    }
    
    //Pull to refresh
    private func addPullToRefresh () {
        self.tableView.addPullToRefresh { [weak self] in
            guard let `self` = self else { return }
            UserService.shared.getFeeds({ (result) in
                if result.status == 200 {
                    self.tableView.showsInfiniteScrolling = true
                    self.feeds = result.items
                    self.current = result.current
                    self.lastPage = result.lastPage
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                self.tableView.pullToRefreshView.stopAnimating()
            })
        }
    }
}

//MARK: - TableView
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homePageCell", for: indexPath) as! HomePageCell
        cell.bindingUI(feed: self.feeds[indexPath.row], index: indexPath.row, isProfile: false)
        cell.delegate = self
        if self.feeds[indexPath.row].record.type == "video" {
            cell.constraintCoverHeight?.constant = self.view.bounds.width * 13 / 16
        } else {
            cell.constraintCoverHeight?.constant = self.view.bounds.width * 11 / 16
        }
        cell.layer.shouldRasterize = true
        cell.layer.rasterizationScale = UIScreen.main.scale
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if(velocity.y>0) {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(true, animated: true)
            }, completion: nil)
            
        } else {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(false, animated: true)
            }, completion: nil)
        }
    }
}

extension HomeViewController: HomeOnListenAction {
    
    func userDidClickMore(at cell: HomePageCell, _ index: Int) {
        if let commentVc = StoryBoard.Home.viewController("CommentsViewController") as? CommentsViewController {
            commentVc.recordId = cell.record.id
            commentVc.record    = cell.record
            commentVc.delegate  = self
            self.currentIndex = index
            self.navigationController?.pushViewController(commentVc, animated: true)
        }
    }
    func userDidClick(at cell: HomePageCell, of user: ShortUser) {
        if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
            profileVc.isMe = false
            profileVc.id   = user.id
            self.navigationController?.pushViewController(profileVc, animated: true)
        }
    }
    func userDidClick(at cell: HomePageCell, of users: Array<ShortUser>) {
        if let vc = StoryBoard.User.viewController("UserTaggedViewController") as? UserTaggedViewController {
            vc.users = users
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func onPress(_ record: Record, action: ActionType, index: Int) {
        switch action {
        case .comment:
            if let commentVc = StoryBoard.Home.viewController("CommentsViewController") as? CommentsViewController {
                commentVc.recordId = record.id
                commentVc.record    = record
                commentVc.delegate  = self
                self.currentIndex = index
                self.navigationController?.pushViewController(commentVc, animated: true)
            }
        case .revote:
            print("Revoted")
        case .play:
            DispatchQueue.main.async { [unowned self] in
                if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                    self.feeds[index].record.views += 1
                    playVc.record = record
                    //
                    self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableViewRowAnimation.fade)
                    if record.current_contest.count == 0 {
                        let listRecord = self.feeds.map({ $0.record })
                        playVc.records = listRecord.filter(){
                            if $0.current_contest.count == 0 {
                                return true
                            } else {
                                return false
                            }
                        }
                        playVc.currentIndex = index
                    } else {
                        self.onPress(record, action: ActionType.contest, index: index)
                        return
                    }
                    //
//                    self.currentIndex = index
                    playVc.delegate = self
                    self.navigationController?.pushViewController(playVc, animated: true)
                }
            }
        case .report:
            if let popup = StoryBoard.User.viewController("ReportViewController") as? ReportViewController {
                popup.modalPresentationStyle = .overFullScreen
                popup.delegate = self
                popup.record = record
                popup.index = index
                self.present(popup, animated: false, completion: nil)
            }
        case .share:
            //Share
            let shareText: String = "\(VZONBDomain.shareApi)record/\(record.id)"
            guard let url = URL(string: shareText) else { return }
            let text = record.resourceName
            let activityViewController = UIActivityViewController(activityItems: [url, text], applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
        case .viewProfile:
            if record.owner.id == Setting.shared.getUserId() { return }
            if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
                profileVc.isMe = false
                profileVc.id   = record.owner.id
                self.navigationController?.pushViewController(profileVc, animated: true)
            }
        case .unfollow: /// Unfollow user - reload feeds
            self.loadFeeds()
        case .update:
            self.feeds[index].record = record
            self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableViewRowAnimation.fade)
        case .contest:
//            let desVC = ContestViewController(nibName: "ContestViewController", bundle: nil)
//            desVC.contestID = record.current_contest
//            self.navigationController?.pushViewController(desVC, animated: true)
            if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                playVc.record = record
                playVc.contestID = record.current_contest
                playVc.delegate = self
                self.navigationController?.pushViewController(playVc, animated: true)
            }
        case .delete:
            self.showMessageWithAction(with: "Notice", message: "Your song will be delete permantly", title: "Delete", cancelTitle: "Cancel", okStyle: .destructive, completion: { [weak self] in
                guard let `self` = self else { return }
                var links: Array<String> = [record.url]
                if record.type == "video" {
                    links.append(record.coverPath)
                }
                RecordService.shared.deleteBy(id: record.id, link: links)
                self.feeds.remove(at: index)
                self.tableView.reloadData()
            })
        default:break
        }
    }
}

extension HomeViewController: UpdateRecord {
    func update(_ record: Record) {
        self.feeds[currentIndex].record = record
        DispatchQueue.main.async {
            self.tableView.reloadRows(at: [IndexPath(row: self.currentIndex, section: 0)], with: .automatic)
        }
    }
}

//MARK: - Report
extension HomeViewController: OnReportSelect {
    func completeReport(reportVc: ReportViewController, report: FeedReport) {
        // Post report to server
        guard let id = reportVc.record?.id else { return }
        RecordService.shared.report(recordId: id, report: report) { (result) in
            if result.status == 200 {
                DispatchQueue.main.async { [unowned self] in
                    
                    let index = reportVc.index
                    self.feeds.remove(at: index)
                    ///
                    self.tableView.beginUpdates()
                    self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: UITableViewRowAnimation.automatic)
                    self.tableView.endUpdates()
                }
            }
        }
    }
}

extension HomeViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "ic_history")!
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "There are nothing to show", attributes: [NSAttributedStringKey.font: Font.emptyData])
    }
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

//MARK: - Search Bar Delegate
extension HomeViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        if let searchMain: SearchMainViewController = StoryBoard.Search.viewController("SearchMainViewController") as? SearchMainViewController {
            self.navigationController?.pushViewController(searchMain, animated: false)
        }
        return false
    }
}
