//
//  SingNavigationController.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class SingNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let singVc = StoryBoard.Sing.viewController("SingPagerTabstripController") as? SingPagerTabstripController else { return }
        self.setViewControllers([singVc], animated: true)
    }
}
