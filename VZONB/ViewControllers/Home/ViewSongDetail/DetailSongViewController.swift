//
//  DetailSongViewController.swift
//  VZONB
//
//  Created by Phuc on 7/25/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import Popover
import BMPlayer
import AVFoundation

protocol UpdateRecord: class {
    func update(_ record: Record)
}

class DetailSongViewController: UIViewController {
    
    // New layout
    @IBOutlet weak var constraintVoting     : NSLayoutConstraint!
    @IBOutlet weak var constraintVote       : NSLayoutConstraint!
    @IBOutlet weak var constraintComment    : NSLayoutConstraint?
    @IBOutlet weak var labelCountVoted      : UILabel?
    @IBOutlet weak var viewWrapVoteFunction : UIView!
    @IBOutlet weak var buttonLike       : UIButton?
    @IBOutlet weak var buttonUnlike     : UIButton?
    
    var playTimeUIProgressView = UIProgressView()
    var player: BMPlayer!
    
    @IBOutlet weak var labelCurrent : UILabel?
    @IBOutlet weak var labelDuration: UILabel?
    @IBOutlet weak var sliderTime   : UISlider?
    @IBOutlet weak var buttonPlay   : UIButton?
    @IBOutlet weak var viewControl  : UIView?
    @IBOutlet weak var viewGesture  : UIView?
    @IBOutlet weak var imageCover   : UIImageView?
    @IBOutlet weak var imageAvatar  : UIImageView?
    @IBOutlet weak var labelName    : UILabel?
    @IBOutlet weak var labelTagsUser: UILabel!
    @IBOutlet weak var labelTime    : UILabel?
    @IBOutlet weak var labelDescription: UILabel?
    @IBOutlet weak var viewPlayer   : UIView!
    
    @IBOutlet weak var viewTop : UIView!
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var viewCountShare   : HomeCountView!
    @IBOutlet weak var viewCountComment : HomeCountView!
    @IBOutlet weak var viewVoteFunction : HomeCountView!
    
    @IBOutlet weak var buttonMore       : UIButton?
    weak var replayButton: UIButton?
    ///
    fileprivate var currentHiddenStatusPlayButton : Bool = false
    fileprivate var lastY       : CGFloat = 0
    fileprivate let LIMITRANGEY : CGFloat = 100

    var isTransferToComment = false
    var contestID   : String?
    var commentParrent:String?
    
    fileprivate var isFinished: Bool = false
    var record          : Record?
    var records         : Array<Record> = Array<Record>()
    var currentIndex    : Int = -1
    weak var delegate: UpdateRecord? = nil
    fileprivate let HIDENTIME: TimeInterval = 3.0
    fileprivate var popover: Popover!
    
    // Vote state
    enum VoteState {
        case none, voting, voted
    }
    
    lazy var contestChart: ContestChartView = {
        let ctcv = Bundle.main.loadNibNamed("ContestChartView", owner: nil, options: nil)?.first as! ContestChartView
        ctcv.frame = self.viewPlayer.bounds //CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 400)
        ctcv.backgroundColor = UIColor.white
        self.viewPlayer.addSubview(ctcv)
        ctcv.isHidden = true
        return ctcv
    }()
    
    // State observe
    fileprivate var voteState: VoteState = .none {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                self.switchUIWith(voteState: self.voteState)
//                guard let `self` = self, let record = self.record else { return }
//                self.viewVoteFunction.isHidden = self.voteState == .none ? false : true
//                self.constraintVote.constant = self.voteState == .none ? 35 : 0
//                self.constraintVoting.constant = self.voteState == .voting ? 120 : 0
//                if record.comments == 0 {
//                    self.constraintComment?.constant = 35
//                    self.viewCountComment.labelCount.text = ""
//                } else {
//                    self.constraintComment?.constant = 60
//                    self.viewCountComment.number = record.comments
//                }
//                UIView.animate(withDuration: 0.3, animations: {
//                    self.viewWrapVoteFunction.layoutSubviews()
//                })
//                if record.likes == 0 && record.dislikes == 0 {
//                    self.labelCountVoted?.text = ""
//                } else {
//                    self.labelCountVoted?.attributedText = self.generateStringVoted()
//                }
            }
        }
    }
    
    final private func switchUIWith(voteState: VoteState) {
        guard let record = self.record else { return }
        
        // Vote couting UIs
        if record.likes == 0 && record.dislikes == 0 {
            self.labelCountVoted?.text = ""
        } else {
            self.labelCountVoted?.attributedText = self.generateStringVoted()
        }
        
        /// Comment UIs
        if record.comments == 0 {
            self.constraintComment?.constant = 35
            self.viewCountComment.labelCount.text = ""
        } else {
            self.constraintComment?.constant = 55
            self.viewCountComment.number = record.comments
        }
        
        switch voteState {
        case .none:
            // Normal UIs
            self.viewVoteFunction.isHidden = false
            self.constraintVote.constant = 35
            self.constraintVoting.constant = 0
            break
        case .voting:
            // Expand Voting function
            self.viewVoteFunction.isHidden = true
            self.constraintVote.constant = 0
            self.constraintVoting.constant = 120
            break
        default: // Voted UIs
            self.viewVoteFunction.isHidden = true
            self.constraintVote.constant = 0
            self.constraintVoting.constant = 0
            break
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.viewWrapVoteFunction?.layoutSubviews()
        })
        
    }
    
    func generateStringVoted () -> NSMutableAttributedString {
        guard let record = self.record else { return NSMutableAttributedString() }
        /// Song owner name
        let boldFont : UIFont = UIFont(name: "SanFranciscoDisplay-Semibold", size: 15)!
        let attrActor: NSMutableAttributedString = NSMutableAttributedString(string: record.likes.description, attributes: [NSAttributedStringKey.font: boldFont, NSAttributedStringKey.foregroundColor: UIColor.white])
        attrActor.append(NSAttributedString(string: " Hits "))
        attrActor.append(NSAttributedString(string: "| \(record.dislikes)", attributes: [NSAttributedStringKey.font: boldFont, NSAttributedStringKey.foregroundColor: UIColor.white]))
        attrActor.append(NSAttributedString(string: " Flops"))
        
        return attrActor
    }
    
    private func loadResource ( record: Record ) {
        if (record.resource?.id ?? "").isEmpty {
            RecordService.shared.getBy(id: record.id, callback: { [weak self] (result) in
                if result.status == 200 {
                    guard let `self` = self else { return }
                    self.record = result.item
                    DispatchQueue.main.async {
                        if self.record != nil {
                            self.loadResource(record: self.record!)
                        }
                    }
                }
            })
            return
        }
//        self.viewCountShare?.number     = record.shares
        self.viewCountComment?.number   = record.comments
        // Header
        self.labelName?.text = record.owner.name
        /// Song owner name
        let boldFont : UIFont = UIFont(name: "SanFranciscoDisplay-Bold", size: 15)!
        let attrActor: NSMutableAttributedString = NSMutableAttributedString(string: "")//, attributes: [NSAttributedStringKey.font: boldFont])
        if record.tags.count == 1 {
            attrActor.append(NSAttributedString(string: " is with "))
            attrActor.append(NSAttributedString(string: record.tags[0].firstname, attributes: [NSAttributedStringKey.font: boldFont]))
        }
        if record.tags.count > 1 {
            attrActor.append(NSAttributedString(string: " and "))
            attrActor.append(NSAttributedString(string: (record.tags.count - 1).description, attributes: [NSAttributedStringKey.font: boldFont]))
            attrActor.append(NSAttributedString(string: " others"))
        }
        self.labelTagsUser.attributedText = attrActor

        self.labelTime?.text = record.createdAt
        self.labelDescription?.text = record.resource?.name ?? ""
        self.imageAvatar?.kf_setImage(url: record.owner.avatar)
        
        if self.player != nil {
            guard let url = URL(string: record.url) else { return}
            let asset = BMPlayerResource(url: url, name: record.resource?.name ?? "", cover: URL(string: record.resource?.thumbnail ?? ""), subtitle: nil)
            self.player.setVideo(resource: asset)
        }
        
        // Bottom
        if record.voted == "false" || record.voted == "" {
            self.voteState = .none
        } else {
            self.voteState = .voted
        }
        // Cover
        if record.type == "audio" {
            self.imageCover?.isHidden = false
            self.viewPlayer.isHidden = true
            self.imageCover?.kf_setImage(url: record.resource?.thumbnail ?? "")
        } else {
            self.viewPlayer.isHidden = false
            self.imageCover?.isHidden = true
        }
        self.transferToCommentView()
    }
    
    func getContest(id: String) {
        UserService.shared.getContest(id: id, { [weak self] (result) in
            guard let `self` = self else { return }
            if result.status == 200 {
                self.records = result.item.records
                if let record = self.record, let index = self.records.index(where: { record.id == $0.id }) {
                    self.currentIndex = index
                }
                self.contestChart.binding(contest: result.item)
            }
        })
    }
    
    private func configUIs () {
        
        if let idContest = self.contestID {
            self.getContest(id: idContest)
        }
        
        guard let record = self.record else { return }
        self.viewCountShare?.bindingUI(count: 0, nameImage: "ic_elevate", isBlackType: true)
        self.viewCountComment?.bindingUI(count: record.comments, nameImage: "ic_home_comment", isBlackType: true)
        self.viewCountComment?.resizeConstraintTo(25)
        self.viewVoteFunction.bindingUI(count: 0, nameImage: "ic_home_vote", isBlackType: true)
        self.viewVoteFunction.resizeConstraintTo(25)
        self.sliderTime?.setThumbImage(UIImage(named: "thumb"), for: .normal)
        //
        self.imageAvatar?.isUserInteractionEnabled = true
        self.imageAvatar?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onViewProfile(_:))))
        self.labelName?.isUserInteractionEnabled    = true
        self.labelName?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onViewProfile(_:))))
        //
        self.viewGesture?.isUserInteractionEnabled = true
        self.viewGesture?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onPlayPause(_:))))
        if self.records.count > 0 || self.contestID != nil {
            self.currentIndex = self.records.index(where: { $0.id == (self.record?.id ?? "") }) ?? 0
            self.viewGesture?.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.onPanListioner(_:))))
        }
        
        self.viewCountShare.isUserInteractionEnabled = true
        self.viewCountComment.isUserInteractionEnabled = true
        self.viewCountShare.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSharePress(_:))))
        self.viewCountComment.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onCommentPress(_:))))
        
        self.viewVoteFunction.isUserInteractionEnabled = true
        self.viewVoteFunction.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onVoteClick(_:))))
        // Load resource
        self.loadResource(record: record)
    }
    
    func showControl() {
        self.viewBottom.isHidden = false
        self.viewTop.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.viewTop.alpha = 1
            self.viewBottom.alpha = 1
        }) { (_) in
            
        }
    }
    
    @objc func hideControl () {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.viewTop.alpha = 0
            self.viewBottom.alpha = 0
        }) { (_) in
            self.viewBottom.isHidden = true
            self.viewTop.isHidden = true
        }
        
        
    }
    
    @objc func onPanListioner (_ sender: UIPanGestureRecognizer) {
        let y = sender.translation(in: self.viewGesture).y
        self.viewControl?.layer.transform = CATransform3DMakeTranslation(0, y, 0)
        self.viewPlayer?.layer.transform = CATransform3DMakeTranslation(0, y, 0)
        self.imageCover?.layer.transform = CATransform3DMakeTranslation(0, y, 0)
        if sender.state == .began {
            self.currentHiddenStatusPlayButton = self.buttonPlay?.isHidden ?? false
            self.buttonPlay?.isHidden = true
        }
        if sender.state == .ended {
            self.buttonPlay?.isHidden = self.currentHiddenStatusPlayButton
            self.lastY  = y
            DispatchQueue.main.async { [weak self] in
                self?.onHandlerGesture()
            }
        }
    }
    
    func onHandlerGesture () {
        var neededHeight: CGFloat = 0
        if (self.lastY < LIMITRANGEY && self.lastY > -LIMITRANGEY) || (self.lastY > -LIMITRANGEY && self.currentIndex <= 0) || (self.lastY < LIMITRANGEY && self.currentIndex >= self.records.count && self.contestID != nil) || (self.lastY < LIMITRANGEY && self.currentIndex >= self.records.count - 1 && self.contestID == nil){
//            if self.currentIndex == self.records.count - 1 && self.contestID != nil {
//                self.currentIndex += 1
//                neededHeight = -self.view.bounds.height
//                self.playNext(with: neededHeight)
//            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.viewControl?.layer.transform = CATransform3DIdentity
                    self.viewPlayer?.layer.transform = CATransform3DIdentity
                    self.imageCover?.layer.transform = CATransform3DIdentity
                })
//            }
            return
        } else if (self.lastY > -LIMITRANGEY && self.currentIndex > 0) {
            self.currentIndex -= 1
            neededHeight = self.view.bounds.height
            
        } else if self.lastY < LIMITRANGEY && self.currentIndex < self.records.count - 1 {
            self.currentIndex += 1
            neededHeight = -self.view.bounds.height
        } else if self.contestID != nil {
            self.currentIndex += 1
        }
        self.playNext(with: neededHeight)
    }
    
    private func playNext (with neededHeight: CGFloat) {
        if (self.currentIndex <= self.records.count - 1) {
            self.contestChart.isHidden = true
            self.viewControl?.isHidden = false
            self.imageCover?.isHidden = true
            //
            self.replayButton?.isHidden = true
            self.player.pause()
            if self.currentIndex < self.records.count {
                self.record = self.records[self.currentIndex]
            }
            self.resetToFirstState()
            self.buttonPlay?.isHidden = true
            self.viewPlayer?.layer.transform = CATransform3DIdentity
            self.imageCover?.layer.transform = CATransform3DIdentity
            UIView.animate(withDuration: 0.2, animations: {
                self.viewControl?.layer.transform = CATransform3DMakeTranslation(0, neededHeight, 0)
            }, completion: { [weak self] (_) in
                guard let `self` = self else { return }
                self.viewControl?.alpha = 0.001
                self.imageCover?.layer.opacity = 0.001
                self.viewControl?.layer.transform = CATransform3DIdentity
                UIView.animate(withDuration: 0.4, animations: {
                    self.viewControl?.alpha = 1
                    self.imageCover?.layer.opacity = 1.0
                    self.showControl()
                }, completion: { (_) in
                    //                self.viewSlider?.isHidden = false
                })
            })
            self.loadResource(record: self.records[self.currentIndex])
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.viewControl?.layer.transform = CATransform3DMakeTranslation(0, neededHeight, 0)
                self.viewControl?.alpha = 0
                self.viewPlayer.layer.transform = CATransform3DIdentity
            }, completion: { (_) in
                self.viewPlayer.isHidden = false
                self.viewControl?.isHidden = true
                self.imageCover?.isHidden = true
            })
            self.contestChart.isHidden = false
        }
    }
    
    private func resetToFirstState () {
        self.labelCurrent?.text = "00:00"
        self.sliderTime?.value = 0
        self.buttonPlay?.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BMPlayerConf.shouldAutoPlay = true
        if let id = record?.id {
            if !id.isEmpty {
                RecordService.shared.view(id: self.record?.id ?? "")
            }
        }
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.configUIs()
        let controller: BMPlayerCustomControlView = BMPlayerCustomControlView()
        self.replayButton = controller.replayButton
        player = BMPlayer(customControlView: controller)
        self.viewPlayer.addSubview(player)
        player.snp.makeConstraints { (make) in
            make.edges.equalTo(self.viewPlayer)
        }
        player.delegate = self
        self.viewPlayer.layoutIfNeeded()
        guard let record = self.record else { return }
        if record.type == "audio" {
            player.backgroundColor = UIColor.clear
        }
        
        guard let url = URL(string: record.url) else { return}
        let asset = BMPlayerResource(url: url, name: record.resource?.name ?? "", cover: URL(string: record.resource?.thumbnail ?? ""), subtitle: nil)
        self.player.setVideo(resource: asset)
    }
    
    @IBAction func onChangeMediaTime (_ sender: UISlider) {
        if self.player != nil && !sender.isTracking {
            self.player.seek(TimeInterval(sender.value))
        }
    }
    
    @IBAction func onShowPopupActions (_ sender: UIButton) {
        guard let record = self.record else { return }
        var reports: Array<Report> = []
        if record.owner.id == Setting.shared.getUserId() {
            reports = self.actionsForMe(isTurnon: record.subscribed)
        } else {
            reports = self.actionsForOtherUserFeed(isTurnon: record.subscribed, isFollowed: record.owner.isFollowed)
        }
        let heightPopUp: CGFloat = CGFloat(reports.count * 51)
        let popupReport = TablePopup(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: heightPopUp), data: reports)
        popupReport.delegate = self
        let popoverOptions: [PopoverOption] = [
            .type(.down),
            .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6)),
            .cornerRadius(CGFloat(0)),
            .arrowSize(CGSize(width: 8, height: 6))
        ]
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.hideControl), object: nil)
        self.perform(#selector(self.hideControl), with: nil, afterDelay: 5.0)
        self.popover = Popover(options: popoverOptions)
        self.popover.show(popupReport, fromView: self.buttonMore!)
    }
    
    // Life cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.isStatusBarHidden = true
        
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: .defaultToSpeaker)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .default
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.player.pause()
        BMPlayerConf.shouldAutoPlay = false
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.hideControl), object: nil)
        self.showControl()
        self.buttonPlay?.isHidden = false
    }

    private func actionsForOtherUserFeed(isTurnon: Bool = true, isFollowed: Bool = true) -> Array<Report> {
        return [
            Report(imageName: "icon_recordsong", title: "Record this song", type: ReportType.record),
            Report(imageName: isFollowed ? "icon_unfollow" : "icon_followuser", title: isFollowed ? "Unfollow" : "Follow", type: isFollowed ? .unfollow : .follow, description: isFollowed ? "Stop seeing songs from this user" : "Register to see songs from this user"),
            Report(imageName: "icon_report", title: "Report this song", type: ReportType.report),
            Report(imageName: (isTurnon ? "icon_turnoff_notification" : "icon_turnon_notification"), title: "Turn \((isTurnon ? "off" : "on")) notification for this song", type: ReportType.turnOnNoti)
        ]
    }
    private func actionsForMe(isTurnon: Bool = true) -> Array<Report> {
        return [
            Report(imageName: "icon_recordsong", title: "Re-record this song", type: ReportType.record),
            Report(imageName: "icon_delete", title: "Delete this song", type: ReportType.delete),
            Report(imageName: (isTurnon ? "icon_turnoff_notification" : "icon_turnon_notification"), title: "Turn \((isTurnon ? "off" : "on")) notification for this song", type: ReportType.turnOnNoti)
        ]
    }
    @IBAction func onFavoriteClick (_ sender: UIButton) {
        if record == nil { return }
        if sender == self.buttonLike {
            self.record?.likes += 1
            self.record?.voted = "likes"
        } else {
            self.record?.dislikes += 1
            self.record?.voted = "dislikes"
        }
        RecordService.shared.vote(recordId: self.record!.id, vote: self.record!.voted, { (result) in
            print(result.item.voted)
        })
        self.voteState = .voted
        self.viewCountComment.number = record!.comments
    }
    @IBAction func onVoteClick(_ sender: UIButton) {
        self.voteState = .voting
    }
    
    @IBAction func onPlayPause (_ sender: Any?) {
        if self.player != nil {
            if self.isFinished {
                self.isFinished = false
                self.replayButton?.isHidden = true
                onPlayPause(nil)
            } else if self.player.isPlaying && self.viewTop.isHidden {
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.hideControl), object: nil)
                self.showControl()
                self.perform(#selector(self.hideControl), with: nil, afterDelay: 6.0)
            } else if self.player.isPlaying {
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.hideControl), object: nil)
                self.showControl()
                self.buttonPlay?.isHidden = false
                self.player.pause()
            } else {
                self.buttonPlay?.isHidden = true
                self.perform(#selector(self.hideControl), with: nil, afterDelay: 3.0)
                self.player.play()
            }
        }
    }
    
    @IBAction func onDismiss(_ sender: Any?) {
        if record != nil {
            self.delegate?.update(record!)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSharePress(_ sender: Any?) {
        //Share
        guard let record = self.record else { return }
        // Hide
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.hideControl), object: nil)
        self.perform(#selector(self.hideControl), with: nil, afterDelay: HIDENTIME)
        ///
        let shareText: String = "\(VZONBDomain.shareApi)record/\((record.id))"
        guard let url = URL(string: shareText) else { return }
        let text = record.resourceName
        let activityViewController = UIActivityViewController(activityItems: [url, text], applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func onCommentPress(_ sender: Any?) {
        if let commentVc = StoryBoard.Home.viewController("CommentsViewController") as? CommentsViewController {
            if let record = self.record {
                commentVc.recordId = record.id
                commentVc.record = record
                self.navigationController?.pushViewController(commentVc, animated: true)
            }
            
        }
    }
    
    @IBAction func onRevotePress(_ sender: Any?) {
        print("Some delegate here")
    }
    
    @IBAction func onViewProfile (_ sender: Any?) {
        guard let record = self.record else { return }
        if record.owner.id == Setting.shared.getUserId() { return }
        if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
            profileVc.isMe = false
            profileVc.id   = record.owner.id
            self.navigationController?.pushViewController(profileVc, animated: true)
        }
    }
    
    private func transferToCommentView() {
        if self.isTransferToComment, let commentVc = StoryBoard.Home.viewController("CommentsViewController") as? CommentsViewController, let record = self.record {
            commentVc.recordId = record.id
            commentVc.record = record
            if let parrentID = self.commentParrent{
                commentVc.parrentID = parrentID
            }
            self.navigationController?.pushViewController(commentVc, animated: true)
            self.isTransferToComment = false
            self.commentParrent = nil
        }
    }
}

extension DetailSongViewController: DismissPopover {
    func dismiss(reportType: ReportType) {
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            self.popover.dismiss()
            self.perform(#selector(self.hideControl), with: nil, afterDelay: self.HIDENTIME)
            switch reportType {
            case ReportType.record:
                self.player.pause()
                if let recordVc: RecordViewController = StoryBoard.Sing.viewController("RecordViewController") as? RecordViewController {
                    recordVc.resource = self.record?.resource
                    self.present(recordVc, animated: true, completion: nil)
                }
            case ReportType.report:
                if let popup = StoryBoard.User.viewController("ReportViewController") as? ReportViewController {
                    popup.modalPresentationStyle = .overFullScreen
                    popup.delegate = self
                    popup.record = self.record
                    self.present(popup, animated: false, completion: nil)
                }
            case .turnOnNoti:
                let subscribed = !(self.record?.subscribed ?? true)
                self.record?.subscribed = subscribed
                RecordService.shared.subscribe(id: self.record?.id ?? "", value: subscribed, { (result) in
                    print(result.status)
                })
            case .unfollow:
                UserService.shared.followUserBy(id: self.record?.owner.id ?? "", follow: false, callback: { (result) in
                    print(result.status)
                })
                guard let follow = self.record?.owner.isFollowed else { return }
                self.record?.owner.isFollowed = !follow
            case .follow:
                UserService.shared.followUserBy(id: self.record?.owner.id ?? "", follow: true, callback: { (result) in
                    print(result.status)
                })
                guard let follow = self.record?.owner.isFollowed else { return }
                self.record?.owner.isFollowed = !follow
            case .delete:
                self.showMessageWithAction(with: "Notice", message: "Your song will be delete permantly", title: "Delete", cancelTitle: "Cancel", okStyle: .destructive, completion: { [weak self] in
                    guard let `self` = self else { return }
                    guard let record = self.record else { return }
                    var links: Array<String> = [record.url]
                    if record.type == "video" {
                        links.append(record.coverPath)
                    }
                    if let navi = self.navigationController {
                        if navi.viewControllers.count > 1 {
                            if let home = navi.viewControllers[navi.viewControllers.count - 2] as? HomeViewController {
                                home.feeds.remove(at: home.currentIndex)
                                home.tableView.reloadData()
                            }
                        }
                    }
                    RecordService.shared.deleteBy(id: record.id, link: links)
                    self.record = nil
                    self.onDismiss(nil)
                })
            default:
                print("Other action")
            }
        }
    }
}

//MARK: - BMPlayerDelegate
extension DetailSongViewController: BMPlayerDelegate {
    func bmPlayer(player: BMPlayer, playerIsPlaying playing: Bool) {
        //
    }
    
    func bmPlayer(player: BMPlayer, playerOrientChanged isFullscreen: Bool) {
        //
    }
    
    // Call back when playing state changed, use to detect is playing or not
//    func bmPlayer(player: BMPlayer, playerIsPlaying playing: Bool) {
//    }
    
    // Call back when playing state changed, use to detect specefic state like buffering, bufferfinished
    func bmPlayer(player: BMPlayer, playerStateDidChange state: BMPlayerState) {
        if state == .readyToPlay {
            self.perform(#selector(self.hideControl), with: nil, afterDelay: 3.0)
        } else if state == .playedToTheEnd {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.hideControl), object: nil)
            self.showControl()
            self.isFinished = true
            player.seek(0)
        }
    }
    
    // Call back when play time change
    func bmPlayer(player: BMPlayer, playTimeDidChange currentTime: TimeInterval, totalTime: TimeInterval) {

        self.labelCurrent?.time(totalSecs: Int(currentTime))
        self.labelDuration?.time(totalSecs: Int(totalTime - currentTime), isPositive: false)
        self.sliderTime?.value = Float(currentTime)
    }
    
    // Call back when the video loaded duration changed
    func bmPlayer(player: BMPlayer, loadedTimeDidChange loadedDuration: TimeInterval, totalDuration: TimeInterval) {
        self.sliderTime?.maximumValue = Float(totalDuration)
    }
}

//MARK: - Report
extension DetailSongViewController: OnReportSelect {
    func completeReport(reportVc: ReportViewController, report: FeedReport) {
        guard let id = reportVc.record?.id else { return }
        RecordService.shared.report(recordId: id, report: report) { (result) in
            if result.status == 200 {
                print(result)
            }
        }
    }
}
