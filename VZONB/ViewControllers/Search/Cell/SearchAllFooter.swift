//
//  SearchAllFooter.swift
//  VZONB
//
//  Created by Phuc on 7/16/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class SearchAllFooter: UIView {
    
    @IBOutlet weak var buttonSeemore: UIButton!
    
    @IBAction func onSeemorePress (_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.seemore), object: sender.tag)
    }
    
}
