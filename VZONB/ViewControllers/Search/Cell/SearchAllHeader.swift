//
//  SearchAllHeader.swift
//  VZONB
//
//  Created by Phuc on 6/25/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class SearchAllHeader: UIView {
    
    @IBOutlet weak var imageIcon     : UIImageView!
    @IBOutlet weak var labelTitle    : UILabel!
    @IBOutlet weak var constraintLine: NSLayoutConstraint!
    
    func bindingUIs(imageName: String = "icon_searchresult_songs", title: String, isOne: Bool = false) {
        self.imageIcon.image = UIImage(named: imageName)
        self.labelTitle.text = title
        if isOne {
            self.constraintLine.constant = 1
        }
    }
    
}
