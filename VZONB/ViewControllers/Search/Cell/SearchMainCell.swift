//
//  SearchMainCell.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class SearchMainCell: UITableViewCell {

    @IBOutlet weak var labelSongName: UILabel!

    override func prepareForReuse() {
        self.labelSongName.textColor = UIColor.black
        super.prepareForReuse()
    }
    
    func highlightWith(text hightLight: String, fullText: String, color: UIColor = Color.default) {
        ///
        let range: NSRange = (fullText.lowercased() as NSString).range(of: hightLight.lowercased())
        ///
        let attributedString = NSMutableAttributedString(string: fullText)
        attributedString.addAttributes([NSAttributedStringKey.foregroundColor: Color.default, NSAttributedStringKey.font: UIFont(name: "SanFranciscoDisplay-Bold", size: 15)!], range: range)
        ///
        self.labelSongName.attributedText = attributedString
    }
    
}
