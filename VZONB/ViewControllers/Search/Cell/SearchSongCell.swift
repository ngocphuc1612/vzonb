//
//  SearchSongCell.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import Popover

class SearchSongCell: UITableViewCell {
    
    @IBOutlet weak var buttonMore: UIButton!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var imageCover: UIImageView?
    @IBOutlet weak var imageAvatar  : CircleImage?
    
    @IBOutlet weak var constraintCoverHeight: NSLayoutConstraint?
    
    fileprivate var popover: Popover!
    var record: Record!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func binding(record: Record) {
        self.imageAvatar?.kf_setImage(url: record.owner.avatar)
        if !(record.coverPath.isEmpty) {
            self.imageCover?.kf_setImage(url: record.coverPath)
        } else {
            self.imageCover?.kf_setImage(url: record.resource?.thumbnail ?? "")
        }
        ///
//        let attrContent: NSMutableAttributedString = NSMutableAttributedString(string: record.resourceName, attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 13)])
//        attrContent.append(NSAttributedString(string: " - \(record.caption)"))
//        self.labelDescription.attributedText = attrContent
        self.labelDescription.text = record.resourceName
        ///
        self.labelName.text = record.owner.name
        self.labelTime.text = record.createdAt
        self.record = record
    }
    
    @IBAction func onShowPopup(_ sender: UIButton) {
        let reports: Array<Report> = self.actionsForOtherUserFeed()
        //        for iter in 0..<reports.count {
        //            reports[iter].idRecord = self.feed.record.id
        //            reports[iter].idResource = self.feed.record.resourceId
        //        }
        let buttonYAxis = sender.superview?.convert(sender.frame, to: nil).midY ?? 0
        let heightPopUp: CGFloat = CGFloat(reports.count * 51)
        
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            let popupReport = TablePopup(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: heightPopUp), data: reports)
            popupReport.delegate = self
            let popoverOptions: [PopoverOption] = [
                .type(buttonYAxis > (heightPopUp + 80) ? .up : .down),
                .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6)),
                .cornerRadius(CGFloat(0)),
                .arrowSize(CGSize(width: 8, height: 6))
            ]
            self.popover = Popover(options: popoverOptions)
            self.popover.show(popupReport, fromView: self.buttonMore)
        }
    }
    
    @IBAction func onPlayThisSong(_ sender: Any?) {
//        if let playVc = StoryBoard.Sing.viewController("PlaySongViewController") as? PlaySongViewController {
//            playVc.record = self.record
//            self.viewController()?.present(playVc, animated: true, completion: nil)
//        }
        
        DispatchQueue.main.async { [unowned self] in
            if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                playVc.record = self.record
                //                self.navigationController?.pushViewController(playVc, animated: true)
//                self.viewController()?.navigationController?.pushViewController(playVc, animated: true) //present(playVc, animated: true, completion: nil)
                self.viewContainingController()?.navigationController?.pushViewController(playVc, animated: true)
            }
        }
        
    }
    
    private func actionsForOtherUserFeed(isTurnon: Bool = true) -> Array<Report> {
        return [
            Report(imageName: "icon_recordsong", title: "Record this song", type: ReportType.record),
            Report(imageName: "icon_unfollow", title: "Unfollow", type: ReportType.unfollow, description: "Stop seeing songs from this user"),
            Report(imageName: "icon_report", title: "Report this song", type: ReportType.report)
        ]
    }
    
}

extension SearchSongCell: DismissPopover {
    func dismiss(reportType: ReportType) {
        self.popover.dismiss()
        
        switch reportType {
        case ReportType.record:
            if let recordVc: RecordViewController = StoryBoard.Sing.viewController("RecordViewController") as? RecordViewController {
//                recordVc.resourceId = self.record.resourceId
                recordVc.resource = self.record.resource
//                self.viewController()?.present(recordVc, animated: true, completion: nil)
                self.viewContainingController()?.present(recordVc, animated: true, completion: nil)
            }
        case ReportType.report:
            print("This song will be reported")
        default:
            print("Other action")
        }
        
    }
}
