//
//  SearchUserCell.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

protocol SearchUserCellDelegate: class {
    func didFollowUser(at cell: SearchUserCell, index: Int)
}

class SearchUserCell: UITableViewCell {
    
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var buttonFollow: UIButton!
    
    // Assign in parent ViewController
    var index   : Int = 0
    weak var delegate: SearchUserCellDelegate?
    var user: ShortUser?
    fileprivate var isFollowing: Bool = false
    
    func binding(user: ShortUser) {
        self.imageAvatar.kf_setImage(url: user.avatar)
        self.labelName.text = user.fullname.isEmpty ? "No Name" : user.fullname
        self.labelDescription.text = user.publishSong.description.appending(" Songs")
        self.buttonFollow.setImage(UIImage(named: user.followed ? "icon_unfollowuser" : "icon_followuser"), for: UIControlState.normal)
    }
    
    //TODO: - Fix bug
    @IBAction func onFollowUser (_ sender: Any?) {
        self.delegate?.didFollowUser(at: self, index: self.index)
    }
    
}
