//
//  SearchAllViewController.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import DZNEmptyDataSet

class SearchAllViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView?
    fileprivate var records: Array<Record> = Array<Record>()
    fileprivate var users: Array<ShortUser> = Array<ShortUser>()
    fileprivate var isFollowing : Bool = false
    fileprivate var searchText  : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        //Config TableView
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.tableView?.emptyDataSetSource = self
        self.tableView?.emptyDataSetDelegate = self
        self.tableView?.register(UINib(nibName: "SearchSongCell", bundle: nil), forCellReuseIdentifier: "searchSongCell")
        self.tableView?.register(UINib(nibName: "SearchUserCell", bundle: nil), forCellReuseIdentifier: "searchUserCell")
    }
    
    @objc func onSearchBy(_ noti: Notification) {
        guard let searchString = noti.object as? String else { return }
        self.searchText = searchString
        // //
        if searchString.isEmpty {
            self.users = []
            self.records = []
            self.tableView?.reloadData()
        } else {
            SearchService.shared.all(query: searchString) { [weak self] (result) in
                guard let `self` = self else { return }
                if result.status == 200 {
                    DispatchQueue.main.async {
                        self.records = result.item.records
                        self.users = result.item.users
                        self.tableView?.reloadData()
                    }
                }
            }
        }
        
    }
    
    
    
    // -> Register receive noti
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onSearchBy(_:)), name: NSNotification.Name.init(NotiName.search), object: nil)
        
//        SearchService.shared.all(query: Shared.searchingString) { [weak self] (result) in
//            guard let `self` = self else { return }
//            if result.status == 200 {
//                self.records = result.item.records
//                self.users = result.item.users
//                DispatchQueue.main.async {
//                    self.tableView?.reloadData()
//                }
//            }
//        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
}

extension SearchAllViewController: SearchUserCellDelegate {
    func didFollowUser(at cell: SearchUserCell, index: Int) {
        guard let user = cell.user else { return }
        DispatchQueue.main.async {
            self.users[index].followed = !user.followed
            self.tableView?.reloadRows(at: [IndexPath(row: index, section: 1)], with: .fade)
        }
        if !self.isFollowing {
            self.isFollowing = true
            UserService.shared.followUserBy(id: user.id, follow: !user.followed) { (result) in
                print(result)
                self.isFollowing = false
            }
        }
    }
}

//MARK: - Empty Data
extension SearchAllViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "ic_history")!
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: self.searchText.isEmpty ? "Type text to search bar" : "There are nothing to show", attributes: [NSAttributedStringKey.font: Font.emptyData])
    }
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

//MARK: - TableView
extension SearchAllViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? self.records.count : self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell: SearchSongCell = tableView.dequeueReusableCell(withIdentifier: "searchSongCell", for: indexPath) as! SearchSongCell
            cell.binding(record: self.records[indexPath.row])
            if self.records[indexPath.row].type == "video" {
                cell.constraintCoverHeight?.constant = self.view.bounds.width * 11 / 16
            } else {
                cell.constraintCoverHeight?.constant = self.view.bounds.width * 9 / 16
            }
            return cell
        default:
            let cell: SearchUserCell = tableView.dequeueReusableCell(withIdentifier: "searchUserCell", for: indexPath) as! SearchUserCell
            cell.binding(user: self.users[indexPath.row])
            cell.user = self.users[indexPath.row]
            cell.index = indexPath.row
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
                profileVc.isMe = false
                profileVc.id   = self.users[indexPath.row].id
                self.navigationController?.pushViewController(profileVc, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? UITableViewAutomaticDimension : 50
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            if self.records.count == 0 { return 0 }
            return 45
        default:
            if self.users.count == 0 { return 0}
            return 41
        }
//        return section == 0 ? 45: 41
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: SearchAllHeader = Bundle.main.loadNibNamed("SearchAllHeader", owner: "SearchAllHeader", options: nil)?.first as! SearchAllHeader
        header.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: section == 0 ? 45: 41)
        switch section {
        case 0:
            if self.records.count == 0 {
                return nil
            }
            header.bindingUIs(title: "Songs")
        default:
            if self.users.count == 0 {
                return nil
            }
            header.bindingUIs(imageName: "icon_searchresult_users", title: "Users", isOne: true)
        }
        return header
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            if self.records.count == 0 { return 0 }
        default:
            if self.users.count == 0 { return 0 }
        }
        return 45
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch section {
        case 0:
            if records.count == 0 {
                return nil
            }
        default:
            if users.count == 0 {
                return nil
            }
        }
        let footer: SearchAllFooter = Bundle.main.loadNibNamed("SearchAllFooter", owner: nil, options: nil)?.first as! SearchAllFooter
        footer.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 45)
        footer.buttonSeemore.tag = section + 1
        return footer
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if(velocity.y>0) {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        } else {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }

}

//MARK: - Pager Child
extension SearchAllViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "All")
    }
}
