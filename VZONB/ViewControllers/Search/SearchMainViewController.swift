//
//  SearchMainViewController.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class SearchMainViewController: UIViewController {
    ///
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate let searchBar = UISearchBar()
    
    fileprivate var searchingString: String {
        return self.searchBar.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) ?? ""
    }
    
    //Resources
    fileprivate var resources: Array<Resource> = Array<Resource>()
    //Demo data
//    let songs = ["Let her go", "Don't let her go", "Go away", "this not true way", "anything will go away", "Let her go", "Don't let her go", "Go away", "this not true way", "anything will go away", "Let her go", "Don't let her go", "Go away", "this not true way", "anything will go away"]
//    var filter: Array<String> = Array<String>()
    
    //MARK: - Get resources
    fileprivate func getResources(searchText: String, _ page: UInt8 = 1) {
        DispatchQueue.global().async {
            SearchService.shared.resources(query: searchText, page: page, callback: { [weak self] (result) in
                guard let `self` = self else { return }
                if result.status == 200 {
                    self.resources = result.items
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            })
        }
    }
    
    fileprivate func configSearchBar() {
        // Search Bar
        self.searchBar.placeholder = "Search"
        self.searchBar.showsCancelButton = true
        self.searchBar.barTintColor = UIColor.black
        self.searchBar.tintColor = UIColor.black
        self.searchBar.searchBarStyle = .minimal
        self.searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
//        self.searchBar.setPlaceholder(color: UIColor.white)
        self.navigationItem.titleView = searchBar
    }
    
    override func viewDidLoad() {
        self.getResources(searchText: self.searchingString)
        self.navigationItem.hidesBackButton = true
        configSearchBar()
        
        super.viewDidLoad()
        
        //TableView
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        
        //Active keyboard
        self.searchBar.delegate = self
        self.searchBar.becomeFirstResponder()
    }
}

//MARK: - TableView
extension SearchMainViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.resources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SearchMainCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SearchMainCell
        
        cell.highlightWith(text: self.searchingString, fullText: self.resources[indexPath.row].name)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Recent Search"
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    //MARK - Select cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(false)
        UIView.animate(withDuration: 0.3) {
            self.view.layoutSubviews()
        }
        if let cell = tableView.cellForRow(at: indexPath) as? SearchMainCell {
            Shared.searchingString = cell.labelSongName.text ?? ""
        }
        if let searchPager: SearchPagerTabStripController = StoryBoard.Search.viewController("SearchPagerTabStripController") as? SearchPagerTabStripController {
            self.navigationController?.pushViewController(searchPager, animated: true)
        }
    }
}

//MARK: - SearchBar delegate 
extension SearchMainViewController: UISearchBarDelegate {
    
    @objc func onReloadData () {
        ///
//        if self.searchingString.isEmpty {
////            self.filter = self.songs
//            self.filterResources = self.resources
//        } else {
//            self.filterResources = self.resources.filter({ $0.name.lowercased().contains(self.searchingString.lowercased()) })
//        }
//        self.tableView.reloadData()
        
        self.getResources(searchText: self.searchingString)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.onReloadData), object: nil)
        self.perform(#selector(self.onReloadData), with: nil, afterDelay: 0.5)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: false)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(false)
        UIView.animate(withDuration: 0.3) {
            self.view.layoutSubviews()
        }
        Shared.searchingString = self.searchingString
        if let searchPager: SearchPagerTabStripController = StoryBoard.Search.viewController("SearchPagerTabStripController") as? SearchPagerTabStripController {
            self.navigationController?.pushViewController(searchPager, animated: true)
        }
    }
}

