//
//  SearchPagerTabStripController.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class SearchPagerTabStripController: ButtonBarPagerTabStripViewController {
    
    var searchingString: String {
        return self.searchBar.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) ?? ""
    }
    
    fileprivate let searchBar = UISearchBar()
    
    private func configPagerTab() {
        //
        self.settings.style.buttonBarBackgroundColor = UIColor.white
        //
        self.settings.style.selectedBarBackgroundColor = Color.default
        self.settings.style.selectedBarHeight = 2
        self.settings.style.buttonBarHeight = 40
        //
        self.settings.style.buttonBarItemBackgroundColor = UIColor.clear
        self.settings.style.buttonBarItemFont = UIFont(name: "SanFranciscoDisplay-Semibold", size: 15)!
        self.settings.style.buttonBarItemTitleColor = Color.inactive
//        self.settings.style.buttonBarItemsShouldFillAvailiableWidth = false
    }
    
    private func configSearchBar() {
        // Config Search Bar
        self.searchBar.placeholder = "Search"
        self.searchBar.tintColor = UIColor.black
        self.searchBar.barTintColor = UIColor.black
        self.searchBar.searchBarStyle = .minimal
        self.searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
//        self.searchBar.setPlaceholder(color: UIColor.white)
        self.navigationItem.titleView = searchBar
        self.searchBar.delegate = self
    }
    
    override func viewDidLoad() {
        self.configPagerTab()
        self.configSearchBar()
        ///
        super.viewDidLoad()
        self.searchBar.text = Shared.searchingString
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_back"), style: UIBarButtonItemStyle.done, target: self, action: #selector(self.onPopViewController))
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        
        self.changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true, let `self` = self else { return }
            oldCell?.label.textColor = Color.inactive
            newCell?.label.textColor = Color.default
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.onPostNewSearch), object: nil)
            self.perform(#selector(self.onPostNewSearch), with: nil, afterDelay: 0.5)
        }
        
        guard let navigation: UINavigationController = self.navigationController else { return }
        navigation.viewControllers.remove(at: navigation.viewControllers.count - 2)
    }
    
    @objc func onPostNewSearch() {
        NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.search), object: self.searchingString)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
//        self.navigationController?.navigationBar.tintColor = UIColor.white
//        self.navigationController?.navigationBar.barTintColor = Color.default
//        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont(name: "SanFranciscoDisplay-Semibold", size: 16)!]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(onSeemorePress(_:)), name: NSNotification.Name.init(NotiName.seemore), object: nil)
//        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    @objc func onSeemorePress(_ noti: Notification) {
        guard let tag: Int = noti.object as? Int else { return }
        self.moveToViewController(at: tag)
        self.reloadInputViews()
    }
    
    @objc func onPopViewController () {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        ///
        guard let searchAll: SearchAllViewController = StoryBoard.Search.viewController("SearchAllViewController") as? SearchAllViewController,
            let searchSong: SearchSongViewController = StoryBoard.Search.viewController("SearchSongViewController") as? SearchSongViewController,
            let searchUser: SearchUserViewController = StoryBoard.Search.viewController("SearchUserViewController") as? SearchUserViewController else { return [] }
        ///
        return [searchAll, searchSong, searchUser]
    }
}

//MARK: - SearchDelegate
extension SearchPagerTabStripController: UISearchBarDelegate {
    ///
    @objc func onReloadData () {
        Shared.searchingString = self.searchingString
        NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.search), object: self.searchingString)
    }
    ///
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.onReloadData), object: nil)
        self.perform(#selector(self.onReloadData), with: nil, afterDelay: 0.5)
    }
}

//MARK: - Add pop gesture
extension SearchPagerTabStripController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
