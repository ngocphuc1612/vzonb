//
//  SearchSongViewController.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import DZNEmptyDataSet

class SearchSongViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    //
    fileprivate var records: Array<Record> = Array<Record>()
    private var searchString    : String = ""
    fileprivate var lastPage    : UInt8 = 1
    fileprivate var current     : UInt8 = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Config TableView
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.tableView.register(UINib(nibName: "SearchSongCell", bundle: nil), forCellReuseIdentifier: "searchSongCell")
        self.addInfiniteScroll()
    }
    
    @objc func onSearchBy(_ noti: Notification) {
        let searchingString = (noti.object as? String) ?? ""
        self.searchString = searchingString
        if searchingString.isEmpty {
            self.records = []
            self.tableView?.reloadData()
        } else {
            SearchService.shared.records(query: searchingString) { [weak self] (result) in
                guard let `self` = self else { return }
                if result.status == 200 {
                    self.records = result.items
                    self.current = result.current
                    self.lastPage = result.lastPage
                    DispatchQueue.main.async {
                        self.tableView.showsInfiniteScrolling = true
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    private func addInfiniteScroll () {
        self.tableView?.addInfiniteScrolling(actionHandler: { [weak self] in
            guard let `self` = self else { return }
            let next = self.current + 1
            if next <= self.lastPage {
                
                SearchService.shared.records(query: self.searchString, page: next, callback: { (result) in
                    if result.status == 200 {
                        self.current = result.current
                        var indexs = Array<IndexPath>()
                        let count = self.records.count
                        for iter in 0..<result.items.count {
                            indexs.append(IndexPath(row: count + iter, section: 0))
                        }
                        self.records.append(contentsOf: result.items)
                        self.tableView.beginUpdates()
                        self.tableView.insertRows(at: indexs, with: .automatic)
                        self.tableView.endUpdates()
                    }
                    self.tableView.infiniteScrollingView.stopAnimating()
                })
            } else {
                self.tableView.showsInfiniteScrolling = false
                self.tableView.infiniteScrollingView.stopAnimating()
            }
        })
    }
    
    
    // -> Register receive noti
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onSearchBy(_:)), name: NSNotification.Name.init(NotiName.search), object: nil)
//        SearchService.shared.records(query: Shared.searchingString) { [weak self] (result) in
//            guard let `self` = self else { return }
//            if result.status == 200 {
//                self.records = result.items
//                DispatchQueue.main.async {
//                    self.tableView.reloadData()
//                }
//            }
//        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension SearchSongViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.records.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SearchSongCell = tableView.dequeueReusableCell(withIdentifier: "searchSongCell", for: indexPath) as! SearchSongCell
        cell.binding(record: self.records[indexPath.row])
        if self.records[indexPath.row].type == "video" {
            cell.constraintCoverHeight?.constant = self.view.bounds.width * 11 / 16
        } else {
            cell.constraintCoverHeight?.constant = self.view.bounds.width * 9 / 16
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if(velocity.y>0) {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        } else {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }

}

extension SearchSongViewController: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "ic_history")!
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: self.searchString.isEmpty ? "Type text to search bar" : "There are nothing to show", attributes: [NSAttributedStringKey.font: Font.emptyData])
    }
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}


extension SearchSongViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Songs")
    }
}
