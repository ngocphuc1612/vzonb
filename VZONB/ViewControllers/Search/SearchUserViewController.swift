//
//  SearchUserViewController.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import DZNEmptyDataSet

class SearchUserViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView?
    
    fileprivate var users: Array<ShortUser> = Array<ShortUser>()
    fileprivate var searchString    : String = ""
    fileprivate var lastPage    : UInt8 = 1
    fileprivate var current     : UInt8 = 1
    fileprivate var isFollowing : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Config TableView
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.tableView?.emptyDataSetSource = self
        self.tableView?.emptyDataSetDelegate = self
        self.tableView?.register(UINib(nibName: "SearchUserCell", bundle: nil), forCellReuseIdentifier: "searchUserCell")
        self.addInfiniteScroll()
    }
    
    @objc func onSearchBy(_ noti: Notification) {
//        print((noti.object as? String) ?? "")
        guard let searchText: String = noti.object as? String else { return }
        self.searchString = searchText
        if searchText.isEmpty {
            self.users = []
            self.tableView?.reloadData()
        } else {
            SearchService.shared.users(query: searchText) { [weak self] (result) in
                guard let `self` = self else { return }
                if result.status == 200 {
                    self.current = result.current
                    self.lastPage = result.lastPage
                    DispatchQueue.main.async {
                        self.users = result.items
                        self.tableView?.showsInfiniteScrolling = true
                        self.tableView?.reloadData()
                    }
                }
            }
        }
    }
    
    private func addInfiniteScroll () {
        self.tableView?.addInfiniteScrolling(actionHandler: { [weak self] in
            guard let `self` = self else { return }
            let next = self.current + 1
            if next <= self.lastPage {
                
                SearchService.shared.users(query: self.searchString, page: next, callback: { (result) in
                    if result.status == 200 {
                        self.current = result.current
                        var indexs = Array<IndexPath>()
                        let count = self.users.count
                        for iter in 0..<result.items.count {
                            indexs.append(IndexPath(row: count + iter, section: 0))
                        }
                        self.users.append(contentsOf: result.items)
                        self.tableView?.beginUpdates()
                        self.tableView?.insertRows(at: indexs, with: .automatic)
                        self.tableView?.endUpdates()
                    }
                    self.tableView?.infiniteScrollingView.stopAnimating()
                })
            } else {
                self.tableView?.showsInfiniteScrolling = false
                self.tableView?.infiniteScrollingView.stopAnimating()
            }
        })
    }
    
    // -> Register receive noti
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onSearchBy(_:)), name: NSNotification.Name.init(NotiName.search), object: nil)
        
//        SearchService.shared.users(query: Shared.searchingString) { [weak self] (result) in
//            guard let `self` = self else { return }
//            if result.status == 200 {
//                self.users = result.items
//                DispatchQueue.main.async {
//                    self.tableView.reloadData()
//                }
//            }
//        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension SearchUserViewController: SearchUserCellDelegate {
    func didFollowUser(at cell: SearchUserCell, index: Int) {
        guard let user = cell.user else { return }
        DispatchQueue.main.async {
            self.users[index].followed = !user.followed
            self.tableView?.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
        }
        if !self.isFollowing {
            self.isFollowing = true
            UserService.shared.followUserBy(id: user.id, follow: !user.followed) { (result) in
                print(result)
//                self.users[index] = result.item
                self.isFollowing = false
            }
        }
    }
}

extension SearchUserViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "ic_history")!
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: self.searchString.isEmpty ? "Type text to search bar" : "There are nothing to show", attributes: [NSAttributedStringKey.font: Font.emptyData])
    }
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

//MARK: - TableView
extension SearchUserViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SearchUserCell = tableView.dequeueReusableCell(withIdentifier: "searchUserCell", for: indexPath) as! SearchUserCell
        cell.binding(user: self.users[indexPath.row])
        cell.user = self.users[indexPath.row]
        cell.index = indexPath.row
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 //UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
            profileVc.isMe = false
            profileVc.id   = self.users[indexPath.row].id
            self.navigationController?.pushViewController(profileVc, animated: true)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if(velocity.y>0) {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        } else {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }

    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 65
//    }
}

extension SearchUserViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Users")
    }
}
