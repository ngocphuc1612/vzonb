//
//  LyricCell.swift
//  VZONB
//
//  Created by Phuc on 7/13/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class LyricCell: UITableViewCell {
    
    @IBOutlet weak var labelLyric: UILabel!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var constraintTop : NSLayoutConstraint?
    @IBOutlet weak var constraintBot : NSLayoutConstraint?
    
    private var animatedView = UIView()
    private var animateView = UIView()
    
    fileprivate var isPause: Bool = false
    fileprivate var numberOfLines: CGFloat = 0
    fileprivate var width       : CGFloat = -1
    fileprivate var space       : CGFloat = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        self.animatedView.backgroundColor = Color.default
        self.labelLyric.textColor = UIColor(rgb: 0xCCCCCC)
        self.labelLyric.layer.shadowRadius = 7
        self.viewContent.backgroundColor = UIColor.clear
        self.viewContent.insertSubview(self.animatedView, belowSubview: self.viewContent)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.isPause = false
        self.numberOfLines = 0
        self.width = -1
        ///
        self.animatedView.frame = CGRect.zero
        self.animateView.removeFromSuperview()
        self.animateView = UIView()
        ///
        self.labelLyric.textColor = UIColor(rgb: 0xCCCCCC) //UIColor(rgb: 0x939393)
        
        self.viewContent.backgroundColor = UIColor.clear
        self.viewContent.addSubview(self.labelLyric)
        self.viewContent.mask = nil
    }
    
    func binding(lyric: String, space: CGFloat = -2.0) {
        self.labelLyric.text = lyric
        self.space = space
    }
    
    func animateLyric(duration: TimeInterval, color: UIColor = Color.default /*UIColor.yellow*/, count: CGFloat = 1) {
        ///
        self.viewContent.backgroundColor = UIColor.white //UIColor(rgb: 0x939393)
        self.viewContent.mask = self.labelLyric
        
        if self.animatedView.superview == nil {
            self.viewContent.addSubview(self.animatedView)
        }
        
        let size = self.labelLyric.intrinsicContentSize
        if self.width <= 0 {
            self.width = self.labelLyric.text!.width(withConstraintedHeight: 24, font: self.labelLyric.font)
            self.numberOfLines = ceil(self.width / size.width)
        }
        
        var currentWidth: CGFloat
        if self.numberOfLines > 1 && count == self.numberOfLines {
            currentWidth = self.width - size.width * (count - 1) + 20
        } else {
            currentWidth = size.width
        }
        
        self.animateView.frame.size = CGSize(width: currentWidth, height: 24)
        self.animateView.center = CGPoint(x: self.labelLyric.center.x, y: (count - 1) * 25 + 10 + (self.space / 2)) //self.labelLyric.center
        self.animateView.backgroundColor = color
        self.viewContent.insertSubview(animateView, belowSubview: self.viewContent)
        ///
        CATransaction.begin()
        let animation = CABasicAnimation(keyPath: "transform.translation.x")
        animation.duration = TimeInterval(currentWidth / self.width) * duration //duration
        animation.fromValue = -self.animateView.bounds.width
        animation.toValue = 0
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        animation.speed = 1.0
        ///
        CATransaction.setCompletionBlock { [weak self] in
            guard let `self` = self else { return }
            if count == self.numberOfLines {
                self.viewContent.addSubview(self.labelLyric)
                self.labelLyric.textColor = UIColor(rgb: 0xCCCCCC) //UIColor(rgb: 0x939393)
                self.viewContent.backgroundColor = UIColor.clear
                self.animateView.removeFromSuperview()
                self.animatedView.removeFromSuperview()
                if self.isPause {
                    NotificationCenter.default.post(name: NSNotification.Name.init("continuePlay"), object: nil)
                }
            } else {
                self.animateLyric(duration: duration, count: count + 1)
                self.animatedView.frame = CGRect(x: 0, y: 0, width: self.viewContent.bounds.width, height: count * 24 + 4) //CGPoint(x: self.labelLyric.center.x, y:  + 16)
            }
        }
        ///
        self.animateView.layer.add(animation, forKey: "runLyric")
        CATransaction.commit()
    }
    private func calculateValuesForAnimation() -> Array<CGFloat> {
        let currentWidth: CGFloat = self.labelLyric.intrinsicContentSize.width
        var values: Array<CGFloat> = [-currentWidth]
        guard let str: String = self.labelLyric.text else { return [] }
        let arrStrings: Array<String> = str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).components(separatedBy: " ")
        guard arrStrings.count > 0 else { return [] }
        var tempString: String = ""
        for iter in 0..<arrStrings.count {
            if iter == arrStrings.count - 1 {
//                tempString += arrStrings[iter]
                values.append(0)
                return values
            } else {
                tempString += arrStrings[iter] + " "
            }
            values.append(-currentWidth + tempString.width(withConstraintedHeight: 24, font: self.labelLyric.font))
        }
        return values
    }
    func animateLyrics(timing: Array<Double>, color: UIColor = Color.default, count: CGFloat = 1) {
        ///
        self.viewContent.backgroundColor = UIColor.white
        self.viewContent.mask = self.labelLyric
        
        if self.animatedView.superview == nil {
            self.viewContent.addSubview(self.animatedView)
        }
        
        let currentWidth: CGFloat = self.labelLyric.intrinsicContentSize.width

        self.animateView.frame.size = CGSize(width: currentWidth, height: 24)
        self.animateView.center = CGPoint(x: self.labelLyric.center.x, y: (count - 1) * 25 + 10 + (self.space / 2)) //self.labelLyric.center
        self.animateView.backgroundColor = color
        self.viewContent.insertSubview(animateView, belowSubview: self.viewContent)
        ///
        CATransaction.begin()
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        if timing.count > 0 {
            let newTiming = timing[1...]
            let totalTime: TimeInterval = newTiming.reduce(0, {$0 + $1})
            var times: Array<NSNumber> = [0]
            var current: Double = 0
            for time in newTiming {
                current += time
                times.append(NSNumber(value: current/totalTime))
            }
            animation.keyTimes = times
            animation.values = self.calculateValuesForAnimation()
            animation.duration = totalTime
            animation.isAdditive = true
        }
        var timingFunctions: Array<CAMediaTimingFunction> = []
        for iter in 0..<timing.count {
            timingFunctions.append(CAMediaTimingFunction(name: iter % 2 == 0 ? kCAMediaTimingFunctionEaseOut : kCAMediaTimingFunctionEaseIn))
        }
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        animation.speed = 1.0
        ///
        CATransaction.setCompletionBlock { [weak self] in
            guard let `self` = self else { return }
//            if count == self.numberOfLines {
                self.viewContent.addSubview(self.labelLyric)
                self.labelLyric.textColor = UIColor(rgb: 0xCCCCCC) //UIColor(rgb: 0x939393)
                self.viewContent.backgroundColor = UIColor.clear
                self.animateView.removeFromSuperview()
                self.animatedView.removeFromSuperview()
                if self.isPause {
                    NotificationCenter.default.post(name: NSNotification.Name.init("continuePlay"), object: nil)
                }
//            } else {
//                self.animateLyric(duration: duration, count: count + 1)
//                self.animatedView.frame = CGRect(x: 0, y: 0, width: self.viewContent.bounds.width, height: count * 24 + 4) //CGPoint(x: self.labelLyric.center.x, y:  + 16)
//            }
        }
        ///
        self.animateView.layer.add(animation, forKey: "transform.translation.x")
        CATransaction.commit()
    }
    
    func pauseLyric() {
        self.isPause = true
        let pauseTime = self.animateView.layer.convertTime(CACurrentMediaTime(), from: nil)
        self.animateView.layer.speed = 0.0
        self.animateView.layer.timeOffset = pauseTime
    }
    
    func resumeLyric() {
        let pauseTime = self.animateView.layer.timeOffset
        self.animateView.layer.speed = 1.0
        self.animateView.layer.timeOffset = 0.0
        self.animateView.layer.beginTime = 0.0
        self.animateView.layer.beginTime = layer.convertTime(CACurrentMediaTime(), from: nil) - pauseTime
    }
    
    
    func stopLyric() {
        self.animateView.layer.removeAllAnimations()
        self.animateView.backgroundColor = UIColor.white
    }
    
}
