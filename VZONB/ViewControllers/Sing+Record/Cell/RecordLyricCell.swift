//
//  RecordLyricCell.swift
//  VZONB
//
//  Created by Phuc on 5/10/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class RecordLyricCell: UITableViewCell {

    @IBOutlet weak var lyric: UILabel!
    @IBOutlet weak var viewContent: UIView!
    let animateView = UIView()
    
    fileprivate var isPause: Bool = false
    
//    func animateContent(duration: TimeInterval) {
//
//        ///
//        animateView.frame.size = self.lyric.intrinsicContentSize
//        animateView.center = self.lyric.center
//        animateView.backgroundColor = UIColor.yellow
//        ///
//        self.viewContent.insertSubview(animateView, belowSubview: self.viewContent)
//
//        animateView.layer.transform = CATransform3DMakeTranslation(-animateView.frame.width, 0, 0)
//        UIView.animate(withDuration: duration, delay: 0, options: [.curveEaseOut], animations: {
//            self.animateView.layer.transform = CATransform3DIdentity
//        }) { (_) in
//            self.animateView.removeFromSuperview()
//        }
//    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.isPause = false
        self.lyric.text = ""
//        self.viewContent.backgroundColor = UIColor.white
        
    }

//    override func layoutIfNeeded() {
//        super.layoutIfNeeded()
//        ////
////        self.viewContent.mask = self.lyric
//    }
//
//    func animateLyric(duration: TimeInterval, color: UIColor = UIColor.yellow) {
//        ///
//        self.animateView.frame.size = self.lyric.intrinsicContentSize
//        self.animateView.center = self.lyric.center
//        self.animateView.backgroundColor = color
//        self.viewContent.insertSubview(animateView, belowSubview: self.viewContent)
//        ///
//        CATransaction.begin()
//        let animation = CABasicAnimation(keyPath: "transform.translation.x")
//        animation.duration = duration
//        animation.fromValue = -self.animateView.bounds.width
//        animation.toValue = 0
//        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
//        animation.speed = 1.0
//        ///
//        CATransaction.setCompletionBlock {
//            self.animateView.removeFromSuperview()
//            if self.isPause {
//                NotificationCenter.default.post(name: NSNotification.Name.init("continuePlay"), object: nil)
//            }
//        }
//        ///
//        self.animateView.layer.add(animation, forKey: "runLyric")
//        CATransaction.commit()
//    }
    
//    func pauseLyric() {
//        self.isPause = true
//        let pauseTime = self.animateView.layer.convertTime(CACurrentMediaTime(), from: nil)
//        self.animateView.layer.speed = 0.0
//        self.animateView.layer.timeOffset = pauseTime
//    }
//
//    func resumeLyric() {
//        let pauseTime = self.animateView.layer.timeOffset
//        self.animateView.layer.speed = 1.0
//        self.animateView.layer.timeOffset = 0.0
//        self.animateView.layer.beginTime = 0.0
//        self.animateView.layer.beginTime = layer.convertTime(CACurrentMediaTime(), from: nil) - pauseTime
//    }
    
    
//    func stopLyric() {
//        self.animateView.layer.removeAllAnimations()
//        self.animateView.backgroundColor = UIColor.white
//    }
}
