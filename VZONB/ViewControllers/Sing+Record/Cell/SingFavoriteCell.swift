//
//  SingFavoriteCell.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

protocol OnSingFavorite: class {
    func action(_ type: SingFavoriteCell.ActionType, _ cell: SingFavoriteCell, _ index: Int)
}

class SingFavoriteCell: UITableViewCell {

    enum ActionType {
        case sing, favorite
    }
    
    @IBOutlet weak var labelSongName: UILabel!
    @IBOutlet weak var labelArtist: UILabel!
    @IBOutlet weak var buttonFavorite: UIButton!
    @IBOutlet weak var buttonSing: UIButton!
    ///
    weak var delegate: OnSingFavorite?
    var resource: Resource?
    var index: Int = 0
    fileprivate var isFavoriting: Bool = false
    ///
    func bindingUIs(resource: Resource, index: Int) {
        self.labelSongName.text = resource.name
        self.labelArtist.text = resource.singer
        self.buttonFavorite.setImage(UIImage(named: resource.favorited ? "ic_heart_full" : "ic_heart_empty"), for: UIControlState.normal)
        self.index = index
    }
    
    @IBAction func onSingThisSong(_ sender: Any?) {
        self.delegate?.action(.sing, self, self.index)
    }
    
    @IBAction func onLike(_ sender: Any?) {
        
        // Favorite
        guard let favorite: Bool = self.resource?.favorited else { return }
        self.resource?.favorited = !favorite
        self.buttonFavorite.setImage(UIImage(named: favorite ? "ic_heart_empty" : "ic_heart_full"), for: UIControlState.normal)
        
        self.delegate?.action(.favorite, self, self.index)
        self.favorite(value: self.resource?.favorited ?? false)
    }
    
    private func favorite(value: Bool) {
        if !self.isFavoriting {
            self.isFavoriting = true
            ResourceService.shared.favorite(id: resource?.id ?? "", favorite: value, callback: { [unowned self] (result) in
                if result.status == 200 {
                    print(result.item.favorited)
                    if result.item.favorited != self.resource?.favorited ?? false {
                        self.favorite(value: self.resource?.favorited ?? false)
                    }
                }
                self.isFavoriting = false
            })
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.buttonFavorite.setImage(UIImage(named: "ic_heart_empty"), for: UIControlState.normal)
    }
    
        //Demo
//    func onReloadData() {
//        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.onReloadData), object: nil)
//        self.perform(#selector(self.onReloadData), with: nil, afterDelay: 0.5)
//    }
}
