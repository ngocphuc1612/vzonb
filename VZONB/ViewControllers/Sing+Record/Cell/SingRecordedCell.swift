//
//  SingRecordedCell.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import Popover

class SingRecordedCell: UITableViewCell {
    
    //
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
//    @IBOutlet weak var viewPlayer: SingReviewView?
    @IBOutlet weak var viewPosting: UIView?
    @IBOutlet weak var indicatorView : UIActivityIndicatorView?
    @IBOutlet weak var imageCover   : UIImageView?
    @IBOutlet weak var viewContent  : UIView?
    @IBOutlet weak var buttonPlay   : UIButton?
//    @IBOutlet weak var constraintHeight: NSLayoutConstraint?
//    @IBOutlet weak var buttonPost: UIButton?
//    fileprivate let activity = UIActivityIndicatorView()
    //
//    fileprivate var popover: Popover!
    weak var delegate       : OnSingAction? = nil
    var record              : Record!
    var index               : Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewPosting?.isHidden = true
        self.viewContent?.layer.shadow()
    }
    
    func binding (record: Record, index: Int, isPublishing: Bool = false) {
        self.imageAvatar.kf_setImage(url: GlobalInfo.shared.info?.avatarUrl ?? "")
        if record.type == "audio" {
            self.imageCover?.kf_setImage(url: record.coverPath) //???
        } else {
            let urlString = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let url = urlString.appendingPathComponent(record.coverPath)
            self.imageCover?.kf_setImage(url: url.absoluteString)
        }
//        self.labelName.text = record.owner.name
        ///
        self.labelDescription.text = record.resourceName
        /// Assign value
        self.labelTime.text = Utility.shared.convertDateTimeToString(record.createdAt)
        self.record = record
        self.index = index
        //Demo
        self.labelName.text = GlobalInfo.shared.info?.fullname ?? "" //MyInfo.shared.name
//        self.imageAvatar.kf_setImage(url: MyInfo.shared.avatarUrl)
        //
//        self.viewPlayer?.binding(record: record)
//        self.viewPlayer?.delegate = self
        ///
        if isPublishing {
//            self.publishing()
            self.viewPosting?.isHidden = false
            self.buttonPlay?.isHidden = true
            self.indicatorView?.startAnimating()
        } else {
            self.buttonPlay?.isHidden = false
            self.indicatorView?.stopAnimating()
        }
    }
    
//    @IBAction func onShowPopup(_ sender: UIButton) {
//        let reports: Array<Report> = self.actionsForMe()
//        let buttonYAxis = sender.superview?.convert(sender.frame, to: nil).midY ?? 0
//        let heightPopUp: CGFloat = CGFloat(reports.count * 51)
//        
//        DispatchQueue.main.async { [weak self] in
//            guard let `self` = self else { return }
//            let popupReport = TablePopup(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: heightPopUp), data: reports)
//            popupReport.delegate = self
//            let popoverOptions: [PopoverOption] = [
//                .type(buttonYAxis > (heightPopUp + 80) ? .up : .down),
//                .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6)),
//                .cornerRadius(CGFloat(0)),
//                .arrowSize(CGSize(width: 8, height: 6))
//            ]
//            self.popover = Popover(options: popoverOptions)
//            self.popover.show(popupReport, fromView: self.buttonMore)
//        }
//    }
    
    @IBAction func onPostThisSong(_ sender: Any?) {
        
//        self.publishing()
        
//        self.viewPosting?.isHidden = false
        self.delegate?.onActionPress(self.record, type: SingAction.post, index: self.index)
    }
    
//    private func publishing() {
//        let frame = self.buttonPost?.bounds ?? CGRect.zero
//        activity.frame = CGRect(x: (frame.width - 15) / 2, y: (frame.height - 15) - 2, width: 15, height: 15)
//        activity.color = UIColor.black
//        self.buttonPost?.setTitle("", for: UIControlState.normal)
//        self.buttonPost?.isUserInteractionEnabled = false
//        self.buttonPost?.addSubview(activity)
//        activity.center = self.buttonPost?.center ?? CGPoint.zero
//        activity.startAnimating()
//    }
    
    @IBAction func onDeleteThisSong(_ sender: Any?) {
        self.delegate?.onActionPress(self.record, type: SingAction.delete, index: self.index)
    }
    
    @IBAction func onEditThisSong(_ sender: Any?) {
        self.delegate?.onActionPress(self.record, type: SingAction.edit, index: self.index)
    }
    
    @IBAction func onRerecordThisSong(_ sender: Any?) {
        self.delegate?.onActionPress(self.record, type: SingAction.reRecord, index: self.index)
    }
    @IBAction func onPlayThisSong(_ sender: Any?) {
        self.delegate?.onActionPress(self.record, type: SingAction.play, index: self.index)
    }
    
//    private func actionsForMe(isTurnon: Bool = true) -> Array<Report> {
//        return [
//            Report(imageName: "icon_recordsong", title: "Re-record this song", type: ReportType.record)/*,
//            Report(imageName: (isTurnon ? "icon_turnoff_notification" : "icon_turnon_notification"), title: "Turn \((isTurnon ? "off" : "on")) notification for this song", type: isTurnon ? ReportType.turnOffNoti : ReportType.turnOnNoti)*/
//        ]
//    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.viewPosting?.isHidden = true
        self.buttonPlay?.isHidden = false
//        self.viewPlayer?.buttonPlay?.isHidden = false
//        self.viewPlayer?.viewControl?.isHidden = true
//        self.viewPlayer?.imageCover?.isHidden = false
//        self.constraintHeight?.constant = 230
    }
    
}

//extension SingRecordedCell: SingReviewDelegate {
//    func updateConstraint() {
//        self.viewPlayer?.delegate = nil
//        self.constraintHeight?.constant = self.bounds.width
//        UIView.animate(withDuration: 0.2, animations: { 
//            self.layoutSubviews()
//        }) { (_) in
//            //MARK: - Play
//            self.delegate?.onActionPress(self.record, type: SingAction.play, index: self.index)
//        }
//    }
//}

//extension SingRecordedCell: DismissPopover {
//    func dismiss(reportType: ReportType) {
//        self.popover.dismiss()
//        
//        switch reportType {
//        case ReportType.record:
//            if let recordVc: RecordViewController = StoryBoard.Sing.viewController("RecordViewController") as? RecordViewController {
//                recordVc.resourceId = self.record.resourceId
//                self.viewController()?.present(recordVc, animated: true, completion: nil)
//            }
//        case ReportType.report:
//            print("This song will be reported")
//        default:
//            print("Other action")
//        }
//        
//    }
//}
