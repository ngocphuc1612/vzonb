
//
//  SongUserCell.swift
//  VZONB
//
//  Created by Phuc on 7/1/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class SongUserCell: UITableViewCell {
    
    @IBOutlet weak var buttonSing: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.buttonSing.layer.cornerRadius = self.buttonSing.bounds.height / 2
        self.buttonSing.layer.borderColor = Color.default.cgColor
        self.buttonSing.layer.borderWidth = 0.5
        self.buttonSing.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
