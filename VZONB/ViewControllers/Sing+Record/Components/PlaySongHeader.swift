//
//  PlaySongHeader.swift
//  VZONB
//
//  Created by Phuc on 6/30/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import AVFoundation

class PlaySongHeader: UIView {
    
    @IBOutlet weak var labelSongName: UILabel!
    @IBOutlet weak var viewPlayer: UIView!
    @IBOutlet weak var imageAvatar: CircleImage!
    @IBOutlet weak var labelArtistName: UILabel!
    @IBOutlet weak var labelLikeCount: UILabel!
    
    var mediaPlay: MediaPlayView?
    //Transfer from parent
    var resourceId: String?
    // Get through API
    private var resource: Resource?
    
    override func awakeFromNib() {
        self.getResource()
        super.awakeFromNib()
        
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.defaultToSpeaker)
        
        if let media: MediaPlayView = Bundle.main.loadNibNamed("MediaPlayView", owner: self, options: nil)?.first as? MediaPlayView {
            self.mediaPlay = media
            media.frame = self.viewPlayer.bounds
            self.viewPlayer.addSubview(media)
        }
        
    }
    
    private func getResource() {
        guard let id: String = self.resourceId else { return }
        DispatchQueue.global().async {
            ResourceService.shared.getBy(id: id, callback: { (result) in
                if result.status == 200 {
                    self.resource = result.item
                }
            })
        }
    }
    
    @IBAction func onDismissViewController (_ sender: Any?) {
//        self.viewController()?.dismiss(animated: true, completion: nil)
        self.viewContainingController()?.dismiss(animated: true, completion: nil)
    }
    
    
}
