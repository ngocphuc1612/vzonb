//
//  SingReviewView.swift
//  VZONB
//
//  Created by PT on 8/9/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import AVFoundation

protocol SingReviewDelegate: class {
    func updateConstraint()
}

class SingReviewView: UIView {

    @IBOutlet weak var sliderTime       : UISlider?
    @IBOutlet weak var labelCurrent     : UILabel?
    @IBOutlet weak var labelTotal       : UILabel?
    @IBOutlet weak var imageCover       : UIImageView?
    @IBOutlet weak var viewPlayer       : UIView?
    @IBOutlet weak var buttonPlay       : UIButton?
    @IBOutlet weak var viewControl      : UIView?
    ////
    fileprivate var audioPlayer         : AVAudioPlayer?
    fileprivate var mp3Player           : AVAudioPlayer?
    fileprivate var avplayer            : AVPlayer?
    fileprivate var avplayerLayer       : AVPlayerLayer?
    weak var delegate: SingReviewDelegate? = nil
    ///
    fileprivate var timer: Timer?
    fileprivate var secs: Int = 0
    fileprivate var mins: Int = 0
    fileprivate var duration: Int = 0
    ////
    fileprivate var documentURL         : URL { return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        if let selfView = Bundle.main.loadNibNamed("SingReviewView", owner: self, options: nil)?.first as? UIView {
            selfView.frame = self.bounds
            self.addSubview(selfView)
        }
        ///
        self.sliderTime?.setThumbImage(UIImage(named: "icon_thumb_slider"), for: UIControlState.normal)
        self.viewPlayer?.isUserInteractionEnabled = true
        self.viewControl?.isHidden = true
        self.viewPlayer?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.play(_:))))
    }
    
    func binding(record: Record) {
        self.imageCover?.kf_setImage(url: record.type == "video" ? self.documentURL.appendingPathComponent(record.coverPath).absoluteString : record.coverPath)
        do {
            self.audioPlayer = try AVAudioPlayer(contentsOf: self.documentURL.appendingPathComponent(record.voicePath))
            self.audioPlayer?.volume = record.voiceVolume
            self.mp3Player   = try AVAudioPlayer(contentsOf: self.documentURL.appendingPathComponent(record.beatPath))
            self.mp3Player?.volume = record.beatVolume
            if record.type == "video" {
                self.avplayerLayer = AVPlayerLayer()
                self.avplayerLayer?.backgroundColor = UIColor.white.cgColor
                self.avplayerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
                self.viewPlayer?.layer.addSublayer(self.avplayerLayer!)
                self.avplayerLayer?.frame = self.viewPlayer?.bounds ?? CGRect.zero
                //
                let playerItem = AVPlayerItem(url: self.documentURL.appendingPathComponent(record.videoPath ?? ""))
                self.avplayer = AVPlayer(playerItem: playerItem)
                self.avplayer?.isMuted = false
                self.avplayerLayer?.player = avplayer
                ///
                self.imageCover?.isHidden = true
            } else {
                self.imageCover?.isHidden = false
            }
            self.duration = Int(self.audioPlayer?.duration ?? 0)
            self.sliderTime?.maximumValue = Float(self.duration)
            self.labelTotal?.time(totalSecs: self.duration, isPositive: false)
        } catch let err {
            print(err.localizedDescription)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.avplayerLayer?.frame = self.viewPlayer?.bounds ?? CGRect.zero
    }
    
    func stop() {
        self.secs = 0
        self.mins = 0
        self.audioPlayer?.stop()
        self.mp3Player?.stop()
        self.avplayer?.seek(to: kCMTimeZero)
        self.avplayer?.pause()
        self.audioPlayer?.currentTime = 0
        self.mp3Player?.currentTime = 0
        self.sliderTime?.value = 0
        self.labelCurrent?.time(totalSecs: 0)
        self.labelTotal?.time(totalSecs: self.duration, isPositive: false)
        self.viewControl?.isHidden = true
        self.buttonPlay?.isHidden = false
        self.timer?.invalidate()
        self.timer = nil
        
    }
    
    func resetMedias() {
        self.mp3Player = nil
        self.audioPlayer = nil
        self.avplayer = nil
    }
    
    @IBAction func play(_ sender: Any?) {
        if self.mp3Player?.isPlaying ?? false {
            self.mp3Player?.pause()
            self.audioPlayer?.pause()
            self.avplayer?.pause()
            self.buttonPlay?.isHidden = false
            self.timer?.invalidate()
            self.timer = nil
        } else {
            self.delegate?.updateConstraint()
            self.viewControl?.isHidden = false
            self.audioPlayer?.play()
            self.mp3Player?.play()
            self.avplayer?.play()
            self.buttonPlay?.isHidden = true
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.runTime), userInfo: nil, repeats: true)
        }
    }
    
    @IBAction func onChangeTime(_ sender: UISlider) {
        if !sender.isTracking {
            let currentSecs: Int = Int(sender.value)
            self.secs       = currentSecs % 60
            self.mins       = currentSecs / 60
            self.labelTotal?.time(totalSecs: currentSecs, isPositive: false)
            self.labelCurrent?.time(totalSecs: self.duration - currentSecs)
            self.audioPlayer?.currentTime = TimeInterval(currentSecs)
            self.mp3Player?.currentTime = TimeInterval(currentSecs)
            if self.timer == nil {
                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.runTime), userInfo: nil, repeats: true)
            }
            self.mp3Player?.play()
            self.audioPlayer?.play()
            
            if self.avplayer != nil {
                self.avplayer?.seek(to: CMTime(seconds: Double(currentSecs), preferredTimescale: 1))
                self.avplayer?.play()
            }
        }
    }
    
    
    
    ///
    @objc func runTime () {
        self.secs += 1
        if self.secs > 59 {
            self.secs = 0
            self.mins += 1
        }
        let totalCurrent: Int = self.mins * 60 + self.secs
        self.sliderTime?.value = Float(totalCurrent)
        self.labelCurrent?.time(totalSecs: totalCurrent, isPositive: true)
        self.sliderTime?.value = Float(totalCurrent)
        let estimate = self.duration - totalCurrent
        if estimate >= 0 {
            self.labelTotal?.time(totalSecs: estimate, isPositive: false)
        } else {
            //TODO: - Stop
            self.timer?.invalidate()
            self.timer = nil
            self.secs = 0
            self.mins = 0
            self.mp3Player?.stop()
            self.audioPlayer?.stop()
            self.mp3Player?.currentTime = 0
            self.audioPlayer?.currentTime = 0
            self.avplayer?.seek(to: CMTime(seconds: Double(0), preferredTimescale: 1))
            self.buttonPlay?.isHidden = false
//            self.stop()
        }
    }
    
}
