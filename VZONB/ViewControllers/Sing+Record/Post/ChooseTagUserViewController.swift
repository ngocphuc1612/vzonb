//
//  ChooseTagUserViewController.swift
//  VZONB
//
//  Created by PT on 10/31/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

protocol ChooseTagUserViewControllerDelegate: class {
    func didDoneSelect(with users: Array<ShortUser>)
}

class ChooseTagUserViewController: DefaultChildViewController {

    @IBOutlet weak var tableView    : UITableView?
    @IBOutlet weak var searchBar    : UISearchBar!
//    @IBOutlet weak var buttonDone   : UIButton?
    weak var delegate : ChooseTagUserViewControllerDelegate? = nil
    var users: Array<ShortUser> = Array<ShortUser>()
    var usersSearched : Array<ShortUser> = []
    
    //
    private func configSearchBar() {
        // Search Bar
        self.searchBar.placeholder = "Search"
        self.searchBar.showsCancelButton = true
        self.searchBar.barTintColor = UIColor.black
        self.searchBar.tintColor = UIColor.black
        self.searchBar.searchBarStyle = .minimal
        self.searchBar.showsCancelButton = true
        self.searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        //        self.searchBar.setPlaceholder(color: UIColor.white)
        self.navigationItem.titleView = searchBar
    }
    override func configNavigation() {
        self.configSearchBar()
    }
    //
    override func viewDidLoad() {
//        if self.users.count <= 0 {
        UserService.shared.followers(id: Setting.shared.getUserId(), isFollower: true, callback: { [weak self] (result) in
            guard let `self` = self else { return }
            if result.status == 200 {
                if self.users.count <= 0 {
                    self.users = result.items
                } else {
                    var items = result.items
                    for item in self.users {
                        for iter in 0..<items.count {
                            if item.id == items[iter].id {
                                items[iter].isTagged = true
                                break
                            }
                        }
                    }
                    self.users = items
                }
                
                self.usersSearched = self.users
                
                DispatchQueue.main.async {
                    self.tableView?.reloadData()
                }
            }
        })
//        } else { self.usersSearched = self.users }
        super.viewDidLoad()
        
        self.tableView?.register(ChooseUserCell.nib, forCellReuseIdentifier: ChooseUserCell.identifier)
        self.tableView?.delegate   = self
        self.tableView?.dataSource = self
        
        //Active keyboard
        self.searchBar.delegate = self
        self.searchBar.becomeFirstResponder()
        self.navigationItem.leftBarButtonItem = nil
    }

    @IBAction func onDone(_ sender : Any?) {
        let users = self.users.filter({ $0.isTagged })
        self.delegate?.didDoneSelect(with: users)
//        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension ChooseTagUserViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.usersSearched = self.users
        } else {
            self.usersSearched = self.users.filter({ $0.firstname.uppercased().contains(searchText.uppercased()) || $0.lastname.uppercased().contains(searchText.uppercased()) })
        }
        self.tableView?.reloadData()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ChooseTagUserViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.usersSearched.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ChooseUserCell.identifier, for: indexPath) as! ChooseUserCell
        cell.binding(user: self.usersSearched[indexPath.row])
        cell.labelSelected?.isHidden = !self.usersSearched[indexPath.row].isTagged
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath) as? ChooseUserCell
//        if let index = self.usersSelected.index(where: { $0.id == self.users[indexPath.row].id }) {
//            self.usersSelected.remove(at: index)
//            cell?.labelSelected?.isHidden = true
//        } else {
//            self.usersSelected.append(self.users[indexPath.row])
////            cell?.accessoryType = .checkmark
//            cell?.labelSelected?.isHidden = false
//        }
//        print(self.usersSelected.count)
        self.usersSearched[indexPath.row].isTagged = !self.usersSearched[indexPath.row].isTagged
        if let index = self.users.index(where: { $0.id == self.usersSearched[indexPath.row].id }) {
            self.users[index].isTagged = !self.users[index].isTagged
        }
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
}
