//
//  ChooseUserCell.swift
//  VZONB
//
//  Created by PT on 10/31/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ChooseUserCell: UITableViewCell {

    @IBOutlet weak var imageAvatar  : CircleImage?
    @IBOutlet weak var labelName    : UILabel?
    
    @IBOutlet weak var labelSelected : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.labelSelected?.isHidden = true
    }
    
    func binding( user: ShortUser ) {
        self.imageAvatar?.kf_setImage(url: user.avatar )
        self.labelName?.text = user.fullname
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }
    
}
