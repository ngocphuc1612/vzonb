//
//  PostNewRecordViewController.swift
//  VZONB
//
//  Created by PT on 10/31/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

class PostNewRecordViewController: DefaultChildViewController {

    @IBOutlet weak var imageAvatar : CircleImage?
    @IBOutlet weak var labelName    : UILabel?
    @IBOutlet weak var labelUserTagged  : UILabel?
    @IBOutlet weak var textviewDescription  : KMPlaceholderTextView?
    
    fileprivate var usersSelected : Array<ShortUser> = []
    fileprivate var documentURL : URL { return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! }
    var record : Record = Record(data: JSON.null)
    
    @objc func onPost (_ sender: UIBarButtonItem) {
        
        if !GlobalInfo.shared.userStatus.group.canSubmit {
            self.post(record: record, isPostToContest: false)
        } else {
            self.postSelection(record: record)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ///
        self.textviewDescription?.delegate = self
        
        //
        self.labelName?.text = GlobalInfo.shared.info?.fullname ?? ""
        self.imageAvatar?.kf_setImage(url: GlobalInfo.shared.info?.avatarUrl ?? "")
        //
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Post", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.onPost(_:)))
        
        self.labelUserTagged?.isUserInteractionEnabled = true
        self.labelUserTagged?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelectUserToTag(_:))))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @objc func onSelectUserToTag(_ sender: Any?){
        if let vc = StoryBoard.Sing.viewController("ChooseTagUserViewController") as? ChooseTagUserViewController {
            vc.delegate = self
            vc.users = self.usersSelected
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func postSelection(record: Record) {
        
        guard let description: String = self.textviewDescription?.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), !description.isEmpty else {
            self.showMessageForUser(with: "Write something about your feeling")
            return
        }
        
        let alert: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let alertNormal = UIAlertAction(title: "Publish Recording", style: .default) { (_) in
            self.post(record: record, isPostToContest: false)
        }
        
        let alertContest = UIAlertAction(title: "Publish Contest Recording", style: .default) { (_) in
            self.post(record: record, isPostToContest: true)
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(alertContest)
        alert.addAction(alertNormal)
        alert.addAction(actionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func post(record: Record, isPostToContest: Bool = false) {
        //
        let description: String = self.textviewDescription?.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) ?? ""
        if description.isEmpty {
            self.showMessageForUser(with: "Please write some feeling for this new song")
            return
        }
        let tags: Array<String> = self.usersSelected.filter({ $0.isTagged }).map({ $0.id })
//        self.showLoading()
        DispatchQueue.global().async { [weak self] in
            guard let `self` = self else { return }
            MediaProcess.shared.mix(audios: [self.documentURL.appendingPathComponent(record.voicePath).absoluteString, self.documentURL.appendingPathComponent(record.beatPath).absoluteString], volumes: [record.voiceVolume, record.voiceVolume], callback: { (success, url) in
//                DispatchQueue.main.async { [weak self] in
//                    self?.hideLoading()
//                }
                if success {
                    // Noti to SingRecordedVC.swift
                    NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.posting), object: record.localId)
                    guard let url = url else { return }
                    let coverPath = self.documentURL.appendingPathComponent(record.coverPath)
                    if record.type == "video" {
                        MediaProcess.shared.mix(audio: url.absoluteString, video: self.documentURL.appendingPathComponent(record.videoPath ?? "").absoluteString, callback: { (success, url) in
                            guard let url = url else { return }
                            UploadThread.shared.upload(tags: tags, mediaUrl: url, name: record.resourceName, resourceId: record.resourceId, isVideo: true, description: description, ext: "mp4", record: record, coverPath: coverPath, isContestGroup: isPostToContest)
                            DispatchQueue.main.async {
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                        })
                        return
                    }
                    
                    UploadThread.shared.upload(tags: tags, mediaUrl: url, name: record.resourceName, resourceId: record.resourceId, isVideo: false, description: description, record: record, coverPath: coverPath, isContestGroup: isPostToContest)
                    self.navigationController?.popToRootViewController(animated: true)
                }
            })
        }
    }
}

extension PostNewRecordViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count // for Swift use count(newText)
        return numberOfChars <= 400;
    }
}

extension PostNewRecordViewController: ChooseTagUserViewControllerDelegate {
    func didDoneSelect(with users: Array<ShortUser>) {
        self.usersSelected = users
        let users = users.filter({ $0.isTagged })
        let boldFont : UIFont = UIFont(name: "SanFranciscoDisplay-Bold", size: 15)!
        let regularFont : UIFont = UIFont(name: "SanFranciscoDisplay-Regular", size: 15)!
        let attrActor: NSMutableAttributedString = NSMutableAttributedString(string: " ", attributes: [NSAttributedStringKey.font: boldFont])
        if users.count > 0 {
            attrActor.append(NSAttributedString(string: users.first?.firstname ?? "", attributes: [NSAttributedStringKey.font: boldFont]))
        }
        if users.count > 1 {
            attrActor.append(NSAttributedString(string: " and ", attributes: [NSAttributedStringKey.font: regularFont]))
            attrActor.append(NSAttributedString(string: (users.count - 1).description, attributes: [NSAttributedStringKey.font: boldFont]))
            attrActor.append(NSAttributedString(string: " others", attributes: [NSAttributedStringKey.font: regularFont]))
        }
        self.labelUserTagged?.attributedText = attrActor
    }
}

////MARK: - Profile Edit
//extension PostNewRecordViewController: UITextViewDelegate {
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//        let numberOfChars = newText.count
//        return numberOfChars <= 200;
//    }
//}

