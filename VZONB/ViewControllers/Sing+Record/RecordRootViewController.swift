//
//  RecordRootViewController.swift
//  VZONB
//
//  Created by Phuc on 5/10/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import AVFoundation

class RecordRootViewController: UIViewController {

    ////
    @IBOutlet weak var labelNameSong: UILabel!
    @IBOutlet weak var labelArtist: UILabel!
    @IBOutlet weak var imageCover: UIImageView!
    @IBOutlet weak var tableLyric: UITableView!
    @IBOutlet weak var labelCurrentTime: UILabel!
    @IBOutlet weak var labelTotalTime: UILabel!
    @IBOutlet weak var viewContentAudioControl: UIView!
    @IBOutlet weak var sliderCurrentTime: UISlider!
    @IBOutlet weak var buttonPlay: UIButton!
    ///
    var mp3Player: AVAudioPlayer?
    var isPlaying:Bool = false
    ///
    var resource: Resource?
    ///
    var arrayLyric = Array<String>()
    var arrTime = Array<TimeInterval>()
    var nearestTime = Array<TimeInterval>()
    var timerTime:Timer? = nil
    var timer:Timer?
    var currentRow:Int = 0
    /// ->
    var sec:Int = 0
    var minutes:Int = 0
    
    var documentURL: URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    var voiceUrl: URL {
        return self.documentURL.appendingPathComponent("recording.caf")
    }
    
    var beatUrl: URL {
        return self.documentURL.appendingPathComponent("beat.mp3")
    }
    
    //Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        ///
        self.labelArtist.text = self.resource?.name ?? ""
        self.labelNameSong.text = self.resource?.name ?? ""
        ///
        self.tableLyric.delegate = self
        self.tableLyric.dataSource = self
        
        /// -> Config UIs
        self.sliderCurrentTime.setThumbImage(UIImage(named: "ic_slider"), for: UIControlState.normal)
        self.imageCover.layer.cornerRadius = self.imageCover.layer.bounds.height / 2
        self.imageCover.clipsToBounds = true
        
    }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//    }
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        
//    }
    //<---
    
    func reset() {
        ///
        self.sec = 0
        self.minutes = 0
        self.currentRow = 0
        ///
        self.timer?.invalidate()
        self.timer = nil
        self.timerTime?.invalidate()
        self.timerTime = nil
        ///
        self.imageCover.layer.removeAllAnimations()
        self.imageCover.layer.transform = CATransform3DIdentity
    }
    
    /// dowwnload lyric
    func downloadLyric(lyricLink: String/* = "Noinaycoanh"*/) {
        if lyricLink.isEmpty { return }
        let queue = DispatchQueue.global()
        queue.async {
            guard let url = URL(string: lyricLink) else { return }
//            guard let url = Bundle.main.url(forResource: lyricName, withExtension: ".lrc") else { return }
//            let str = try? String(contentsOf: url)
            guard let data = try? Data(contentsOf: url) else { return }
            let str = String(data: data, encoding: String.Encoding.utf8)
            if let strComplete = str {
                let arrLrc = strComplete.components(separatedBy: "\r\n").filter({ !$0.isEmpty })
//                var iter = 0
//                while iter < 10 {
//
//                    guard arrLrc.count > 1 else { return }
//                    if arrLrc[0].characters.last! != "]" {
//                        break
//                    }
//                    arrLrc.remove(at: 0)
//                    iter += 1
//                }
                ////
                var first:TimeInterval = -1
                var current:TimeInterval = 0
                // For the first start
                self.arrTime.append(0)
                self.nearestTime.append(0)
                
                ////
                for string in arrLrc {
                    var strArr = string.components(separatedBy: "]")
                    guard strArr.count > 0 else { return }
                    if strArr.count > 1 {
                        if strArr[1].isEmpty { continue }
                    }
                    strArr[0] = strArr[0].replacingOccurrences(of: "[", with: "")
                    
                    let arrTime = strArr[0].components(separatedBy: ":")
                    if arrTime.count < 2 && (arrTime.index(of: ".") ?? -1) < 0 { continue }
                    let minu = TimeInterval(arrTime.first ?? "0") ?? 0
                    let secondString = (arrTime.last ?? "0.0").components(separatedBy: ".")
                    let second = TimeInterval(secondString.first ?? "0") ?? 0
                    let miliSec = TimeInterval(secondString.last ?? "0") ?? 0
                    
                    ///
                    if first == -1 {
                        first = minu * 60 + second + miliSec / 100
                    } else {
                        first = current
                        current = minu * 60 + second + miliSec / 100
                        let value = current - first
                        self.arrTime.append(value)
                        self.nearestTime.append((self.nearestTime.last ?? 0) + value)
                    }
                    
                    
                    guard strArr.count > 1 else { continue }
                    self.arrayLyric.append(strArr[1].isEmpty ? "------" : strArr[1])
                }
                
                DispatchQueue.main.async {
                    self.tableLyric.reloadData()
                }
            }
        }
    }
    ///
    func downloadMp3(songName: String = "NoiNayCoAnh") {
        let queue = DispatchQueue.global()
        queue.async {
//            guard let url = URL(string: "https://drive.google.com/uc?id=0B67lPFsFyXNTbFFtQU9heXZTaVU&authuser=0") else { return }
            guard let url = Bundle.main.url(forResource: songName, withExtension: ".mp3") else { return }
            guard let data = try? Data(contentsOf: url) else { return }
            self.mp3Player = try? AVAudioPlayer(data: data)
            guard let player = self.mp3Player else { return }
            
            let secTime = Int(player.duration) % 60
            let minutesTime = Int(player.duration / 60)
            
            DispatchQueue.main.async {
                self.sliderCurrentTime.maximumValue = Float(player.duration)
                self.mp3Player?.prepareToPlay()
                let strTime = (minutesTime < 10 ? "0\(minutesTime)" : "\(minutesTime)") + (secTime < 10 ? ":0\(secTime)" : ":\(secTime)")
                self.labelTotalTime.text = strTime
            }
        }
    }
    
    ////
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    //MARK: - TIMER TASK
    @objc func countingTime() {
        self.sec += 1
        if self.sec > 59 {
            self.sec = 0
            self.minutes += 1
        }
        self.sliderCurrentTime.value = Float(self.minutes * 60 + self.sec)
        if self.sliderCurrentTime.value >= self.sliderCurrentTime.maximumValue {
            self.timerTime?.invalidate()
            self.timerTime = nil
            self.timer?.invalidate()
            self.timer = nil
            ///
            self.mp3Player?.stop()
            return
        }
        self.labelCurrentTime.text = (self.minutes < 10 ? "0\(self.minutes)" : String(self.minutes)) + (self.sec < 10 ? ":0\(self.sec)" : ":\(self.sec)")
    }
    
    @objc func runLyric() {
        
        let indexPath = IndexPath(row: self.currentRow, section: 0)
        let cell = self.tableLyric.cellForRow(at: indexPath) as? RecordLyricCell
        
        self.currentRow += 1
        ////
        self.tableLyric.scrollToRow(at: indexPath, at: UITableViewScrollPosition.middle, animated: true)
        if self.currentRow < self.arrTime.count {
//            cell?.animateLyric(duration: self.arrTime[self.currentRow])
            cell?.lyric.textColor = UIColor.yellow
//            cell?.lyric.font = UIFont.systemFont(ofSize: 20)
            if self.currentRow > 1 {
                let oldCell = self.tableLyric.cellForRow(at: IndexPath(row: self.currentRow - 2, section: 0)) as? RecordLyricCell
                oldCell?.lyric.textColor = UIColor.white
//                oldCell?.lyric.font = UIFont.systemFont(ofSize: 17)
            }
        }
        ///
        if self.currentRow < self.arrTime.count - 1 {
            self.timer = Timer.scheduledTimer(timeInterval: self.arrTime[self.currentRow], target: self, selector: #selector(self.runLyric), userInfo: nil, repeats: false)
        }
        
    }
}

//MARK: - TableView Delegate, DataSource
extension RecordRootViewController: UITableViewDelegate, UITableViewDataSource {
    ////
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayLyric.count
    }
    ////
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RecordLyricCell
        
        ///
        cell.lyric.text = self.arrayLyric[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
}
