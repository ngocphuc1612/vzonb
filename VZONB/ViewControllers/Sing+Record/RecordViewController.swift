//
//  DemoRecordViewController.swift
//  VZONB
//
//  Created by Phuc on 7/12/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import AVFoundation

protocol Dismiss: class {
    func dismiss()
}

class RecordViewController: UIViewController {
    
    fileprivate let DURATION: TimeInterval = 0.3
    ///
    @IBOutlet weak var tableLyric   : UITableView!
    @IBOutlet weak var labelNameSong: UILabel!
    @IBOutlet weak var labelArtist  : UILabel!
    @IBOutlet weak var labelCountDown: UILabel?
    @IBOutlet weak var viewPlay     : ViewPlay!
    @IBOutlet weak var labelCurrentTime : UILabel!
    @IBOutlet weak var labelEstimateTime: UILabel?
    @IBOutlet weak var sliderTime   : UISlider?
    @IBOutlet weak var buttonPlay   : UIButton!
    @IBOutlet weak var buttonSwitch : UIButton!
    @IBOutlet weak var cameraView   : UIView!
    @IBOutlet weak var sliderBeatVol: UISlider!
    @IBOutlet weak var sliderVoiceVol: UISlider!
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var viewControl  : UIView!
    ///
    @IBOutlet weak var constraintVolumeBar  : NSLayoutConstraint!
    @IBOutlet weak var constraintTopNavi    : NSLayoutConstraint?
    @IBOutlet weak var constraintLyricTable : NSLayoutConstraint?
    @IBOutlet weak var constraintTableHeight: NSLayoutConstraint!
    ///
    var resource    : Resource?
    fileprivate var task: URLSessionDownloadTask?
    ///
    var documentURL : URL = FileManager.default.documentUrl
    var voiceUrl    : URL { return self.documentURL.appendingPathComponent("recording.caf") }
    var beatUrl     : URL { return self.documentURL.appendingPathComponent("beat.mp3") }
    var mergeVideo  : URL { return self.documentURL.appendingPathComponent("mergeVideo.mp4") }
    
    // Song handler
    fileprivate var arrayLyric  = Array<String>()
    fileprivate var timings: Array<Array<Double>> = []
    fileprivate var arrTime     = Array<TimeInterval>()
    fileprivate var nearestTime = Array<TimeInterval>()
    // AVFoudation
    fileprivate var mp3Player: AVAudioPlayer?
    fileprivate var audioRecorder: AVAudioRecorder?
    fileprivate let videoFileOutput = AVCaptureMovieFileOutput()
    fileprivate var engine: AudioEngine?
    ///\\\
    fileprivate var isFirst     : Bool = true
    fileprivate var isHeadphonePluged: Bool = false
    fileprivate var isPlayed    : Bool = false
    fileprivate var isCameraOn  : Bool = false
    private var isPaused        : Bool = false
//    fileprivate var isFinished  : Bool = false
    fileprivate var isAccessible: Bool = false
    fileprivate var isDownloaded: Bool = false
    fileprivate var isCalculating: Bool = false
    fileprivate var numberOfVideos: Int = 1
    // Timer
    fileprivate var sec         :Int    = 0
    fileprivate var minutes     :Int    = 0
    fileprivate var duration    : Int   = 0
    fileprivate var timerTime   :Timer? = nil
    fileprivate var timer       :Timer? = nil
    fileprivate var currentRow  :Int    = 0
    fileprivate var calculatingRow: Int = 0
    fileprivate var space       : CGFloat = 10
    
    // Count down handler vars
    fileprivate var timerCountdown: Timer?
    fileprivate var countdown   : Int = 4
    /// Camera Config
    lazy var cameraSession: AVCaptureSession = {
        let s = AVCaptureSession()
        s.sessionPreset = AVCaptureSession.Preset.high
        return s
    }()
    lazy var previewLayer: AVCaptureVideoPreviewLayer? = {
        let preview =  AVCaptureVideoPreviewLayer(session: self.cameraSession)
        preview.frame = self.cameraView.bounds
        preview.videoGravity = AVLayerVideoGravity.resizeAspectFill
        return preview
    }()
    // Get lyric
    private func resourceInfoBy(id: String) {
        ResourceService.shared.getBy(id: id, callback: { (result) in
            if result.status == 200 {
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    self.resource = result.item
                    self.labelNameSong.text = result.item.name
                    self.labelArtist.text   = result.item.singer
                    if /*self.isAccessible &&*/ !self.isDownloaded {
                        self.onDownloadMediaResource()
                    }
                }
            }
        })
    }
    
    private func onDownloadMediaResource () {
        if (resource?.beatUrl ?? "").isEmpty || self.isDownloaded { return }
        self.isDownloaded = true
        self.downloadBeat(beatUrl: self.resource?.beatUrl ?? "")
        self.downloadLyric(lyricLink: self.resource?.lyricUrl ?? "")
    }
    
    override func viewDidLoad() {
        ///
        self.resourceInfoBy(id: self.resource?.id ?? "")
        ///
        super.viewDidLoad()
//        self.isHeadphonePluged = AKSettings.headPhonesPlugged
        // Session settings
//        AKSettings.bufferLength = .medium
//
//        do {
//            try AKSettings.setSession(category: .playAndRecord, with: .defaultToSpeaker)
//        } catch {
//            AKLog("Could not set session category.")
//        }
//        AKSettings.defaultToSpeaker = true
//        // Patching
//        //            inputPlot.node = mic
//        micMixer = AKMixer(mic)
//        micBooster = AKBooster(micMixer)
//
//        micBooster.gain = 0
//        recorder = try? AKNodeRecorder(node: micMixer)
//        if let file = recorder.audioFile {
//            print(file.url.absoluteString)
//        }
        
//        moogLadder = AKMoogLadder(player)
        
//        mainMixer = AKMixer(micBooster)
//
//        AudioKit.output = mainMixer
        
        
        print(resource?.id ?? "Fail create kkkk")
        ///
        let availablePorts = AVAudioSession.sharedInstance().currentRoute.outputs
        for port in availablePorts {
            if port.portType == AVAudioSessionPortHeadphones {
                self.isHeadphonePluged = true
                break
            }
        }
        
        self.tableLyric.tableFooterView = UIView()
        
        AVAudioSession.sharedInstance().requestRecordPermission() { [unowned self] allowed in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                if allowed {
                    self.isAccessible = true
                    if !self.isDownloaded {
                        DispatchQueue.main.async {
                            self.onDownloadMediaResource()
                        }
                    }
                } else {
                    // failed to record!
                    self.showPopup(title: "Notice", description: "Sorry! You need to provide microphone access permission to record this song", okTitle: "Setting", cancelTitle: "Cancel", {
                        if let url = URL(string: UIApplicationOpenSettingsURLString + (Bundle.main.bundleIdentifier ?? "")) {
                            UIApplication.shared.openURL(url)
                        }
                    })
                }
            }
        }
        self.configUIs()
        
        if !self.isHeadphonePluged {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0, execute: { [weak self] in
                self?.showMessageForUser(with: "Please use headsets with microphones for highest quality and effects", title: "Maximum Enjoyment", okTitle: "Okay")
            })
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
            UIApplication.shared.isStatusBarHidden = true
            UIApplication.shared.isIdleTimerDisabled = true
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.continuePlay), name: NSNotification.Name.init("continuePlay"), object: nil)
        //TODO: - Hanlde Input Port Change
//        NotificationCenter.default.addObserver(self, selector: #selector(self.onInputPortChange(_:)), name: NSNotification.Name.AVAudioSessionRouteChange, object: nil)
        if self.isCameraOn {
            self.cameraSession.startRunning()
        }
    }
    
    //MARK: - Continue Play
    @objc func continuePlay() {
        /// -> Start time calculate
        self.timer = Timer.scheduledTimer(timeInterval: 0, target: self, selector: #selector(self.runLyric), userInfo: nil, repeats: false)
    }
    
    //MARK: Change input port
    @objc func onInputPortChange (_ notification: Notification) {
        if let /*audioRouteChangeReason*/ _ = notification.userInfo![AVAudioSessionRouteChangeReasonKey] as? UInt, self.isPlayed {
//            if AKSettings.headPhonesPlugged != self.isHeadphonePluged {
                self.showMessageOkWithCompletion(with: "Your input port has been change, please restart your record", message: "", completion: { [unowned self] in
                    self.cancelView()
                    self.dismiss(animated: true, completion: nil)
                })
//            }
//            switch audioRouteChangeReason {
//            case AVAudioSessionRouteChangeReason.newDeviceAvailable.rawValue:
//                //                self.engine?.reset()
//                self.isHeadphonePluged = true
//            case AVAudioSessionRouteChangeReason.oldDeviceUnavailable.rawValue:
//                //                self.configAudioSesssion()
//                self.isHeadphonePluged = false
//            default:
//                break
//            }
        }
    }
    
    ////
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.previewLayer != nil {
            self.cameraView.layer.addSublayer(self.previewLayer!)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.previewLayer?.frame = self.cameraView.bounds
    }
    
//    private func updatePreviewLayer(layer: AVCaptureConnection, orientation: AVCaptureVideoOrientation) {
//        layer.videoOrientation = orientation
//        self.previewLayer?.frame = self.cameraView.bounds
//    }
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        
//        if let connection =  self.previewLayer?.connection  {
//            
//            let currentDevice: UIDevice = UIDevice.current
//            let orientation: UIDeviceOrientation = currentDevice.orientation
//            let previewLayerConnection : AVCaptureConnection = connection
//            if previewLayerConnection.isVideoOrientationSupported {
//                switch (orientation) {
//                case .portrait: updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
//                    break
//                case .landscapeRight: updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeLeft)
//                    break
//                case .landscapeLeft: updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeRight)
//                    break
//                case .portraitUpsideDown: updatePreviewLayer(layer: previewLayerConnection, orientation: .portraitUpsideDown)
//                    break
//                default: updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
//                    break
//                }
//            }
//        }
//    }
    
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransition(to: size, with: coordinator)
//        if UIDevice.current.orientation.isLandscape {
//            self.viewNavigation.isHidden = true
//            self.tableLyric.backgroundColor = UIColor.black.withAlphaComponent(0.4)
//            self.viewControl.backgroundColor = UIColor.black.withAlphaComponent(0.4)
//            return
//        }
//        self.viewNavigation.isHidden = false
//        self.tableLyric.backgroundColor = UIColor.clear
//        self.viewControl.backgroundColor = UIColor.clear
//    }
    ////
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.cameraSession.stopRunning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isIdleTimerDisabled = false
        NotificationCenter.default.removeObserver(self)
    }
    
    /// CONFIG UIs
    private func configUIs() {
//        self.buttonFinish.isHidden = true
        self.buttonPlay.layer.cornerRadius = self.buttonPlay.bounds.height / 2
        self.buttonPlay.clipsToBounds = true
        self.buttonPlay.isEnabled = false
        ////
        self.constraintVolumeBar.constant = -150
        self.sliderBeatVol.setThumbImage(UIImage(named: "icon_thumb_slider"), for: UIControlState.normal)
        self.sliderVoiceVol.setThumbImage(UIImage(named: "icon_thumb_slider"), for: UIControlState.normal)
        self.sliderTime?.setThumbImage(UIImage(named: "thumb"), for: UIControlState.normal)

        
        self.constraintTableHeight.constant = self.view.bounds.height - (Utility.shared.isPhoneX ? 265 : 210) // self.view.bounds.width
        
        self.tableLyric.rowHeight = UITableViewAutomaticDimension
        self.tableLyric.estimatedRowHeight = 57
        
    }
    ///camera
    private func configCameraView() {
        self.cameraSession.sessionPreset = AVCaptureSession.Preset.high
        if #available(iOS 10.0, *) { ///===========
            let devices = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInTelephotoCamera,AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified).devices
            for device in devices {
                if (device.hasMediaType(AVMediaType.video)) {
                    if device.position == AVCaptureDevice.Position.front {
                        
                        guard let input = try? AVCaptureDeviceInput(device: device) else { return }
                        if self.cameraSession.canAddInput(input) {
                            self.cameraSession.addInput(input)
                        }
                    }
                }
            }
        } else {
            // Fallback on earlier versions
            let devices = AVCaptureDevice.devices(for: AVMediaType.video)
            
            for device in devices {
                if device.position == AVCaptureDevice.Position.front {
                    guard let input = try? AVCaptureDeviceInput(device: device) else { return }
                    if (cameraSession.canAddInput(input) == true) {
                        cameraSession.addInput(input)
                    }
                }
            }
            
        }
        self.cameraSession.sessionPreset = AVCaptureSession.Preset.high
        self.cameraSession.addOutput(self.videoFileOutput)
    }
    
    /// dowwnload lyric // Parse time lyric here
    private func downloadLyric(lyricLink: String) {
        if lyricLink.isEmpty { return }
        let queue = DispatchQueue.global()
        queue.async {
//            guard let path = Bundle.main.path(forResource: "demo", ofType: ".lrc"), let str = try? String(contentsOfFile: path) else { return }
//            let string = str.addingPercentEncoding(withAllowedCharacters: CharacterSet.)
            
            guard let url = URL(string: lyricLink) else { return }
            guard let data = try? Data(contentsOf: url) else { return }
            let str = String(data: data, encoding: String.Encoding.utf8)
            
            (self.arrayLyric, self.timings) = LyricHandler.parseLyric(from: str ?? "")
            self.nearestTime = self.timings.map({ $0.first ?? 0 })
//            if let strComplete = str {
//                let arrLrc = strComplete.components(separatedBy: "\r\n").filter({ !$0.isEmpty })
//                var before:TimeInterval = -1
//                var current:TimeInterval = 0
//                // For the first start
//                self.nearestTime.append(0)
//
//                self.arrayLyric.append(" ")
//                ////
//                for string in arrLrc {
//                    var strArr = string.components(separatedBy: "]")
//                    guard strArr.count > 0 else { return }
//                    if strArr.count > 1 {
//                        if strArr[1].isEmpty { continue }
//                    }
//                    // Time
//                    strArr[0] = strArr[0].replacingOccurrences(of: "[", with: "")
//                    let arrTime = strArr[0].components(separatedBy: ":")
//                    if arrTime.count < 2 && (arrTime.index(of: ".") ?? -1) < 0 { continue }
//                    let minu = TimeInterval(arrTime.first ?? "0") ?? 0
//                    let secondString = (arrTime.last ?? "0.0").components(separatedBy: ".")
//                    let second = TimeInterval(secondString.first ?? "0") ?? 0
//                    let miliSec = TimeInterval(secondString.last ?? "0") ?? 0
//                    ///
//                    if before == -1 {
//                        before = minu * 60 + second + miliSec / 100
//                        self.arrTime.append(before)
//                        self.nearestTime.append(before)
//                    } else {
//                        current = minu * 60 + second + miliSec / 100
//                        let value = current - before
//                        before = current
//                        self.arrTime.append(value)
//                        self.nearestTime.append((self.nearestTime.last ?? 0) + value)
//                    }
//                    guard strArr.count > 1 else { continue }
//                    self.arrayLyric.append(strArr[1].isEmpty ? "" : strArr[1])
//                }
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    self.tableLyric.reloadData()
                    if self.arrayLyric.count > 1 {
                        self.isCalculating = true
//                        if !self.isCameraOn {
                            self.tableLyric.scrollToRow(at: IndexPath(row: 1, section: 0), at: .top, animated: true)
//                        }
                    }
                }
//            }
        }
    }
    private func downloadBeat(beatUrl: String) {
        //Download Mp3
        if beatUrl.isEmpty { return }
        if let audioUrl = URL(string: beatUrl) {
            try? FileManager.default.removeItem(at: self.beatUrl)
            try? FileManager.default.removeItem(at: self.voiceUrl)
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
            self.task = session.downloadTask(with: audioUrl)
            self.task?.resume()
            
            // Demo Local Download Zone
//            let url = Bundle.main.path(forResource: "beat1", ofType: "mp3")!
//            
//            self.viewPlay.downloadCompleted()
//            self.buttonPlay.isEnabled = true
//            
//            //MARK: - Prepare recorder
//            self.configAudioSesssion()
//            ///
//            self.engine = AudioEngine(beatUrl: URL(string: url)!)
//            self.engine?.setMicVolume(0.95)
//            self.engine?.setBeatVolume(self.isHeadphonePluged ? 0.8 : 0.28)
            // End demo Local Zone
            
        }
    }
    
    //MARK: - Record done or pause
    fileprivate func onShowPopupFinish() {
        let alert:UIAlertController = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.alert)
        let alertActionOk: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel) { [weak self] (_) in
            guard let `self` = self else { return }
//            self.save()
            if self.duration - self.minutes * 60 - self.sec > 60 {
                self.onFinishPress(nil)
            } else {
                self.showMessageForUser(with: "Your record must be at least 60 seconds")
            }
        }
        let alertActionCancel: UIAlertAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { [weak self] (_) in
            guard let `self` = self else { return }
            self.cancelView()
        }
        ///
        let titleFont = [NSAttributedStringKey.font: Font.header /*UIFont(name: "SanFranciscoDisplay-Semibold", size: 17.0)!*/]
        let messageFont = [NSAttributedStringKey.font: UIFont(name: "SanFranciscoDisplay-Regular", size: 14.0)!, NSAttributedStringKey.foregroundColor: UIColor(rgb: 0x4f4f4f)]
        let titleAttrString = NSMutableAttributedString(string: "Congratulations", attributes: titleFont)
        let messageAttrString = NSMutableAttributedString(string: "Do you want to save this song?", attributes: messageFont)
        alert.setValue(titleAttrString, forKey: "attributedTitle")
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        ///
        alert.addAction(alertActionOk)
        alert.addAction(alertActionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    private func save() {
        self.buttonPlay.isUserInteractionEnabled = false
        if self.isCameraOn {
            MediaProcess.shared.mergeVideo(numberOfVideos: self.numberOfVideos, callback: { (success, url) in
                if success {
                    self.onSaveToCoreData()
                }
            })
        } else {
            self.onSaveToCoreData()
        }
    }
    private func onShowPopupDelete () {
        let alert:UIAlertController = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.alert)
        let alertActionOk: UIAlertAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.default) { [weak self] (_) in
            guard let `self` = self else { return }
//            self.save()
            self.onFinishPress(nil)
        }
        let alertActionCancel: UIAlertAction = UIAlertAction(title: "Delete", style: UIAlertActionStyle.destructive) { [weak self] (_) in
            guard let `self` = self else { return }
            //Delete and dimiss
            self.audioRecorder  = nil
            self.mp3Player      = nil
            self.engine         = nil
            self.videoFileOutput.stopRecording()
            
            try? FileManager.default.removeItem(at: self.beatUrl)
            try? FileManager.default.removeItem(at: self.voiceUrl)
            if self.numberOfVideos > 0 {
                for iter in 1..<self.numberOfVideos {
                    try? FileManager.default.removeItem(at: self.documentURL.appendingPathComponent("movie\(iter).mov"))
                }
            }
            self.dismiss(animated: true, completion: nil)
        }
        ///
        let titleFont = [NSAttributedStringKey.font: UIFont(name: "SanFranciscoDisplay-Semibold", size: 17.0)!]
        let messageFont = [NSAttributedStringKey.font: UIFont(name: "SanFranciscoDisplay-Regular", size: 14.0)!, NSAttributedStringKey.foregroundColor: UIColor(rgb: 0x4f4f4f)]
        let titleAttrString = NSMutableAttributedString(string: "Notice", attributes: titleFont)
        let messageAttrString = NSMutableAttributedString(string: "Are you sure delete this song?", attributes: messageFont)
        alert.setValue(titleAttrString, forKey: "attributedTitle")
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        ///
        alert.addAction(alertActionOk)
        alert.addAction(alertActionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onChnageMicVol(_ sender: UISlider) {
        if self.isHeadphonePluged {
            self.engine?.setMicVolume(sender.value)
        }
    }
    @IBAction func onChangeBeatVolume(_ sender: UISlider) {
        if self.isHeadphonePluged {
            self.engine?.setBeatVolume(sender.value)
        } else {
            self.mp3Player?.volume = sender.value
        }
    }
    
    //MARK: - TIMER TASK
    @objc func countingTime() {
        self.sec -= 1
        if self.sec < 0 {
            self.sec = 59
            self.minutes -= 1
        }
        
        if (self.sec + self.minutes * 60) < 0 {
            self.timerTime?.invalidate()
            self.timerTime = nil
            self.timer?.invalidate()
            self.timer = nil
//            self.mp3Player?.stop()
            
//            self.isFinished = true
            
            self.stop()
            self.onShowPopupFinish()
            
//            self.pause()
//            self.buttonPlay.isEnabled = false
            ///
            return
        }
//        self.labelCurrentTime.text = (self.minutes < 10 ? "0\(self.minutes)" : String(self.minutes)) + (self.sec < 10 ? ":0\(self.sec)" : ":\(self.sec)")
        let current: Int = self.minutes * 60 + self.sec
        self.labelEstimateTime?.time(totalSecs: current, isPositive: false)
        self.labelCurrentTime.time(totalSecs: self.duration - current, isPositive: true)
        self.sliderTime?.value = Float(self.duration - current)
        
    }
    
    @objc func runLyric() {
        
        let indexPath = IndexPath(row: self.currentRow, section: 0)
        self.calculatingRow = self.currentRow
        let cell = self.tableLyric.cellForRow(at: indexPath) as? LyricCell
//        cell?.labelLyric.textColor = UIColor.white
        if self.currentRow < self.timings.count && self.currentRow >= 0 {
//            cell?.animateLyric(duration: self.arrTime[self.currentRow])
            cell?.animateLyrics(timing: timings[self.currentRow])
        } else if self.currentRow == self.timings.count { // For last lyric
//            cell?.animateLyric(duration: self.arrTime[self.currentRow - 1])
            cell?.animateLyrics(timing: timings[self.currentRow - 1])
        }
        ////
        self.isCalculating = true
        if (self.currentRow > 1 && self.isCameraOn) || (self.currentRow == 0 && !self.isCameraOn) {
            self.tableLyric.scrollToRow(at: indexPath, at: UITableViewScrollPosition.middle, animated: true)
        } else if self.currentRow > 3 {
            self.tableLyric.scrollToRow(at: IndexPath(row: self.currentRow - 3, section: 0), at: .top, animated: true)
        }
//        if self.currentRow <= self.arrTime.count && self.currentRow > 0 {
//                let oldCell = self.tableLyric.cellForRow(at: IndexPath(row: self.currentRow - 1, section: 0)) as? LyricCell
//                oldCell?.labelLyric.textColor = UIColor(rgb: 0x939393)
//        }
        ///
        self.currentRow += 1
        
        if self.currentRow < self.timings.count {
            let countTime: Double = self.currentRow <= 1 ? self.timings[0][0] : (self.timings[self.currentRow][0] - self.timings[self.currentRow - 1][0])
            self.timer = Timer.scheduledTimer(timeInterval: countTime, target: self, selector: #selector(self.runLyric), userInfo: nil, repeats: false)
        } else {
            self.timer?.invalidate()
            self.timer = nil
        }
        
    }
    
    //Button Finish backup
    @IBAction func onFinishPress(_ sender: Any?) {
        self.stop()
//        guard let url = recorder.audioFile?.url else { return }
//        do {
//            try FileManager.default.moveItem(at: url, to: self.voiceUrl)
        if let playVC = StoryBoard.Sing.viewController("SingReviewViewController") as? SingReviewViewController {
            playVC.delegate = self
            playVC.isHasVideo = self.isCameraOn
            playVC.numberOfVideos = self.numberOfVideos - 1
            playVC.resource = self.resource
            playVC.isHeadphonePlugged = self.isHeadphonePluged
            self.present(playVC, animated: true, completion: nil)
        }
//        } catch let err {
//            print(err.localizedDescription)
//        }
    }
    
    // Actions
    @IBAction func onSwitch(_ sender: UIButton) {
        
        // Switch to audio
        if sender.imageView?.image == UIImage(named: "icon_switch_video") {
            self.space = 10
            self.constraintLyricTable?.constant = -40
            self.constraintTableHeight.constant =  self.view.bounds.height - (Utility.shared.isPhoneX ? 265 : 210) //self.view.bounds.width
            UIView.animate(withDuration: DURATION, animations: {
                self.view.layoutSubviews()
            })
            self.tableLyric.reloadData()
            if self.arrayLyric.count > 1 {
                self.tableLyric.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
            }
            self.previewLayer?.removeFromSuperlayer()
            self.cameraSession.stopRunning()
            sender.setImage(UIImage(named: "icon_switch_audio"), for: UIControlState.normal)
            self.isCameraOn = false
            return
        }
        // Switch to video
//        if self.arrayLyric.count > 3 {
//            var height: CGFloat = 0
//            for iter in 1...3 {
//                let eachHeight: CGFloat = self.arrayLyric[iter].height(withConstrainedWidth: self.view.bounds.width - 36, font: UIFont(name: "SanFranciscoDisplay-Semibold", size: 21)!)
//                height += eachHeight
//            }
//            if self.constraintTableHeight.constant != height {
//                self.constraintTableHeight.constant = height
//            }
//        }
        
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { [weak self] (success: Bool) in
            guard let `self` = self else { return }
            if !success {
                // Re-Request Camera access
                self.showMessageWithAction(message: "You need provide camera access permission", title: "Setting", cancelTitle: "Cancel", okStyle: UIAlertActionStyle.cancel, completion: {
                    UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                })
            }
        }
        
        if self.isFirst {
            self.isFirst = false
            self.configCameraView()
        }
        
        var tableHeight: CGFloat = 0
        let height: CGFloat = self.arrayLyric.count > 1 ? self.arrayLyric[1].height(withConstrainedWidth: self.view.bounds.width - 36, font: UIFont(name: "SanFranciscoDisplay-Semibold", size: 21)!) : 28
        if height <= 28 {
            tableHeight = height + 52
        } else if height > 28 && height < 60 {
            tableHeight = height + 26
        } else {
            tableHeight = height + 26
        }
        if self.constraintTableHeight.constant != tableHeight {
            self.constraintTableHeight.constant = tableHeight
        }
//        }
        self.isCameraOn = true
        self.space = 2
        self.constraintLyricTable?.constant = -(self.view.bounds.height - (Utility.shared.isPhoneX ? 200 : 115) - self.constraintTableHeight.constant) / 2 // + 27
        UIView.animate(withDuration: self.DURATION, animations: {
            self.view.layoutSubviews()
        })
        //
        self.tableLyric.reloadData()
        //
        if self.arrayLyric.count > 1 {
            self.tableLyric.scrollToRow(at: IndexPath(row: 1, section: 0), at: .top, animated: false)
        }
        if self.previewLayer?.superlayer == nil {
            self.cameraView.layer.addSublayer(self.previewLayer!)
        }
        self.cameraSession.startRunning()
        sender.setImage(UIImage(named: "icon_switch_video"), for: UIControlState.normal)
    }
    
    @IBAction func onDismiss(_ sender: Any?) {
        UIApplication.shared.isStatusBarHidden = false
        if self.isPlayed {
            self.showMessageWithAction(with: "Notice", message: "Your record will be delete if you quit this screen", title: "Delete", cancelTitle: "Cancel") { [weak self] in
                guard let `self` = self else { return }
                self.cancelView()
//                self.audioRecorder  = nil
//                self.mp3Player      = nil
//                self.engine         = nil
//                self.videoFileOutput.stopRecording()
//                
//                try? FileManager.default.removeItem(at: self.beatUrl)
//                try? FileManager.default.removeItem(at: self.voiceUrl)
//                if self.numberOfVideos > 0 {
//                    for iter in 1..<self.numberOfVideos {
//                        try? FileManager.default.removeItem(at: self.documentURL.appendingPathComponent("movie\(iter).mov"))
//                    }
//                }
//                self.dismiss(animated: true, completion: nil)
            }
        } else {
            self.task?.cancel(byProducingResumeData: { (nil) in
                print("Canceled")
            })
            self.task = nil
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func cancelView () {
        UIApplication.shared.isStatusBarHidden = false
        self.audioRecorder  = nil
        self.mp3Player      = nil
        self.engine         = nil
        self.videoFileOutput.stopRecording()
        
        self.task?.cancel(byProducingResumeData: { (nil) in
            print("Canceled")
        })
        self.task = nil
        
        try? FileManager.default.removeItem(at: self.beatUrl)
        try? FileManager.default.removeItem(at: self.voiceUrl)
        if self.numberOfVideos > 0 {
            for iter in 1..<self.numberOfVideos {
                try? FileManager.default.removeItem(at: self.documentURL.appendingPathComponent("movie\(iter).mov"))
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onStartRecord(_ sender: Any?) {
        
        if (self.engine?.isRunning ?? false) || (self.mp3Player?.isPlaying ?? false) {
            self.pause()
        } else {
            if self.isPlayed {
//                self.resume()
                // Will resume after countdown
                if self.currentRow > 1 {
                    self.countdown = 3
                    self.buttonPlay.isUserInteractionEnabled = false
                    if self.isCameraOn {
                        self.labelCountDown?.isHidden = false
                    } else {
                        // -> Change color, text before counting
                        if let cell = self.tableLyric.cellForRow(at: IndexPath(row: self.currentRow - 2, section: 0)) as? LyricCell {
                            cell.labelLyric.textColor = UIColor.white
                            cell.labelLyric.text = 3.description
                        }
                        if let cell = self.tableLyric.cellForRow(at: IndexPath(row: self.currentRow - 1, section: 0)) as? LyricCell {
                            cell.labelLyric.textColor = UIColor(rgb: 0xCCCCCC)
                        }
                    }
                    self.timerCountdown = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.countdownLyricContinue), userInfo: nil, repeats: true)
                    // end change
                } else {
                    NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.onStartCountdown), object: nil)
                    self.resume()
                    self.prepareCountdown()
                }
                
                return
            }
            self.start()
            self.prepareCountdown()
        }
    }
    
    private func prepareCountdown () {
//        if self.arrTime.count < 1 { return }
        if self.timings.count < 1 { return }
        if self.timings[0].count < 1 { return }
        let timeStart: Double = self.timings[0][0]
        if timeStart < 3 {
            self.buttonPlay.isUserInteractionEnabled = false
            self.timerCountdown = Timer.scheduledTimer(timeInterval: timeStart, target: self, selector: #selector(self.countdownLyric), userInfo: nil, repeats: true)
        } else {
            self.perform(#selector(self.onStartCountdown), with: nil, afterDelay: (timeStart - 4))
        }
    }
    
    @objc func onStartCountdown() {
        self.buttonPlay.isEnabled = false
        if !self.isCameraOn {
            self.tableLyric.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: true)
        }
        self.timerCountdown = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.countdownLyric), userInfo: nil, repeats: true)
    }
    
    // When start
    @objc func countdownLyric() {
        self.countdown -= 1
        if self.countdown < 1 {
            self.timerCountdown?.invalidate()
            self.timerCountdown = nil
            self.buttonPlay.isUserInteractionEnabled = true
            if self.isCameraOn {
                self.labelCountDown?.text = "3"
                self.labelCountDown?.isHidden = true
            } else if let cell = self.tableLyric.cellForRow(at: IndexPath(row: 0, section: 0)) as? LyricCell {
                cell.labelLyric.text = " " //self.resource?.name ?? ""
            }
            self.countdown = 4
            self.buttonPlay.isEnabled = true
            // After countdown, ignore dummy row (0)
            self.currentRow += 1
            self.timer = Timer.scheduledTimer(timeInterval: 0, target: self, selector: #selector(self.runLyric), userInfo: nil, repeats: false)
            return
        }
        if self.isCameraOn {
            self.labelCountDown?.isHidden = false
            self.labelCountDown?.text = self.countdown.description
            self.labelCountDown?.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
            UIView.animate(withDuration: 0.4, animations: {
                self.labelCountDown?.layer.transform = CATransform3DIdentity
            })
        } else if let cell = self.tableLyric.cellForRow(at: IndexPath(row: 0, section: 0)) as? LyricCell {
            let label = cell.labelLyric
            label?.text = self.countdown.description
            label?.layer.transform = CATransform3DMakeScale(2, 2, 2)
            UIView.animate(withDuration: 0.4, animations: { [weak label = label] in
                label?.layer.transform = CATransform3DIdentity
            })
        }
    }
    @objc func countdownLyricContinue() {
        self.countdown -= 1
        if self.currentRow > 1 {
            if self.countdown < 1 {
                self.timerCountdown?.invalidate()
                self.timerCountdown = nil
                // Put back color hightlight and continue playing
                if self.isCameraOn {
                    self.labelCountDown?.text = "3"
                    self.labelCountDown?.isHidden = true
                } else {
                    if let cell = self.tableLyric.cellForRow(at: IndexPath(row: self.currentRow - 2, section: 0)) as? LyricCell {
                        cell.labelLyric.text = self.arrayLyric[self.currentRow - 2]
                        cell.labelLyric.textColor = UIColor(rgb: 0xCCCCCC)
                    }
                    if let cell = self.tableLyric.cellForRow(at: IndexPath(row: self.currentRow - 1, section: 0)) as? LyricCell {
                        cell.labelLyric.textColor = UIColor.white
                    }
                }
                self.buttonPlay.isUserInteractionEnabled = true
                /// end
                self.resume()
                return
            }
            if self.isCameraOn {
                self.labelCountDown?.isHidden = false
                self.labelCountDown?.text = self.countdown.description
                self.labelCountDown?.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
                UIView.animate(withDuration: 0.4, animations: {
                    self.labelCountDown?.layer.transform = CATransform3DIdentity
                })
            } else if let cell = self.tableLyric.cellForRow(at: IndexPath(row: self.currentRow - 2, section: 0)) as? LyricCell {
                let label = cell.labelLyric
                label?.text = self.countdown.description
                label?.layer.transform = CATransform3DMakeScale(2, 2, 2)
                UIView.animate(withDuration: 0.4, animations: { [weak label = label] in
                    label?.layer.transform = CATransform3DIdentity
                })
            }
        } else { // If don't
            
        }
    }
    fileprivate func popupSorry() {
        self.showPopup(title: "Notice", description: "Something went wrong! please re-record this song for a better quality", imageName: "icon_sorry", okTitle: "Ok") { [weak self] in
            guard let `self` = self else { return }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                self.task?.cancel(byProducingResumeData: { (nil) in
                    print("Canceled")
                })
                self.task = nil
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
    ///Record Video
    func recordVideo() {
        let filePath = self.documentURL.appendingPathComponent("movie\(self.numberOfVideos).mov")
        do {
            if FileManager.default.fileExists(atPath: filePath.absoluteString) {
                try FileManager.default.removeItem(at: filePath)
            }
        } catch let err {
            print(err.localizedDescription)
        }
        self.numberOfVideos += 1
        self.videoFileOutput.startRecording(to: filePath, recordingDelegate: self)
    }
    
//    func onSave () {
//        self.showMessageWithAction(with: "Congratulations", message: "Do you want to save this song?", title: "OK", cancelTitle: "No", okStyle: UIAlertActionStyle.cancel) { [unowned self] in
//            self.buttonPlay.isUserInteractionEnabled = false
//            if self.isCameraOn {
//                MediaProcess.shared.mergeVideo(numberOfVideos: self.numberOfVideos, callback: { (success, url) in
//                    if success {
//                        self.onSaveToCoreData()
//                    }
//                })
//            } else {
//                self.onSaveToCoreData()
//            }
//        }
//    }
    
    private func onSaveToCoreData () {
        // Reset
        self.mp3Player = nil
        self.audioRecorder = nil
        self.engine = nil
        
        do {
            let name = resource?.name ?? ""
            let time = Date().timeIntervalSince1970.description
            
            let recordedFinder = self.documentURL.appendingPathComponent("recorded")
            //            if !FileManager.default.fileExists(atPath: recordedFinder.absoluteString) {
            try? FileManager.default.createDirectory(at: recordedFinder, withIntermediateDirectories: false, attributes: nil)
            //            }
            
            let newVoiceUrl: URL = self.documentURL.appendingPathComponent("recorded/\(time)_\(name)_voice.caf")
            let newBeatUrl: URL = self.documentURL.appendingPathComponent("recorded/\(time)_\(name)_beat.mp3")
            let newVideoUrl: URL = self.documentURL.appendingPathComponent("recorded/\(time)_\(name)_video.mp4")
            let coverPath   : URL = self.documentURL.appendingPathComponent("recorded/\(time)_\(name)_image.jpg")
            
            try FileManager.default.moveItem(at: self.voiceUrl, to: newVoiceUrl)
            try FileManager.default.moveItem(at: self.beatUrl, to: newBeatUrl)
            if self.isCameraOn {
                try FileManager.default.moveItem(at: self.mergeVideo, to: newVideoUrl)
                if let image = Utility.shared.getThumbnailFrom(path: newVideoUrl) {
                    if let data = UIImageJPEGRepresentation(image, 1) {
                        try? data.write(to: coverPath)
                        
                    }
                }
            }
            var record: Record
            if self.isCameraOn {
                record = Record(beatPath: "recorded/\(time)_\(name)_beat.mp3", voicePath: "recorded/\(time)_\(name)_voice.caf", videoPath: "recorded/\(time)_\(name)_video.mp4", cover: "recorded/\(time)_\(name)_image.jpg", resourceId: self.resource?.id ?? "", resourceName: self.resource?.name ?? "", type: "video")
            } else {
                record = Record(beatPath: "recorded/\(time)_\(name)_beat.mp3", voicePath: "recorded/\(time)_\(name)_voice.caf", videoPath: nil, cover: resource?.thumbnail ?? "", resourceId: self.resource?.id ?? "", resourceName: self.resource?.name ?? "", type: "audio")
            }
            
            if record.save(beatVolume: self.isHeadphonePluged ? 0.7 : 0.2, voiceVolume: 1.0) {
                DispatchQueue.main.async {
                    MessageManager.shared.show(title: "Congratulation", body: "Your song has been saved!!!")
                    self.dismiss(animated: true, completion: {
                        ///
                        NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.doneRecord), object: nil)
                    })
                }
            } else {
                MessageManager.shared.show(title: "Sorry", body: "Something went wrong! Please try again later", style: MessageManager.Style.error)
                self.dismiss(animated: true, completion: nil)
            }
        } catch (let err) {
            print(err.localizedDescription)
        }
    }
}

//MARK: - Recording Handler
extension RecordViewController {
    ////
    fileprivate func start() {
        
        if !self.isAccessible {
            self.showPopup(title: "Notice", description: "Sorry! You need to provide microphone access permission to record this song", okTitle: "Setting", cancelTitle: "Cancel")  {
                if let url = URL(string: UIApplicationOpenSettingsURLString + (Bundle.main.bundleIdentifier ?? "")) {
                    UIApplication.shared.openURL(url)
                }
            }
            return
        }
        //Layout
//        self.constraintNavigation?.constant = 0
        
        
        self.constraintTopNavi?.constant -= 44
        self.constraintLyricTable?.constant -= 35
        UIView.animate(withDuration: DURATION, animations: { [weak self] in
            self?.view.layoutSubviews()
        }) { [unowned self] (_) in
            self.viewNavigation.isHidden = true
        }
        
        self.buttonSwitch.isUserInteractionEnabled = false
        if self.isHeadphonePluged {
            guard let engine = self.engine, self.arrayLyric.count > 0 else { print("Fail to start"); return }
            engine.start(voiceUrl: self.voiceUrl)
        } else {

            // Check all conditions
            guard self.arrayLyric.count > 0, self.mp3Player != nil else { return }
            self.mp3Player?.play()
            self.audioRecorder?.record()

        }
        
//        if AKSettings.headPhonesPlugged {
//            micBooster.gain = 1
//        }
//
//        do {
//            try recorder.record()
//            self.mp3Player?.play()
        if self.isCameraOn {
            self.recordVideo()
        }
//        } catch {
//            self.popupSorry()
//            print("Errored recording.")
//        }
        
        
        
        UIView.animate(withDuration: 0.3, animations: {
            self.buttonPlay.backgroundColor = UIColor.clear
            self.buttonPlay.setTitle("", for: UIControlState.normal)
            self.buttonPlay.setImage(UIImage(named: "icon_pause"), for: UIControlState.normal)
        }, completion: nil)
        
        self.viewPlay.startRecord(duration: Float(self.mp3Player?.duration ?? 0))
        ////
        self.isPlayed = true
        ////
        /// -> Start time calculate
//        self.timer = Timer.scheduledTimer(timeInterval: 0, target: self, selector: #selector(self.runLyric), userInfo: nil, repeats: false)
        self.timerTime = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countingTime), userInfo: nil, repeats: true)
    }
    
    ////
    fileprivate func pause() {
        
        guard let engine = self.engine, self.arrayLyric.count > 0 else { print("Fail to start"); return }

        self.isPaused = true
        if self.isHeadphonePluged {
            if engine.isRunning {
                engine.pause()
            }
        } else {
            self.mp3Player?.pause()
            self.audioRecorder?.pause()
        }
        
//        self.mp3Player?.pause()
//        self.recorder.stop()
//        AudioKit.stop()
        
        let cell = self.tableLyric.cellForRow(at: IndexPath(row: self.currentRow - 1, section: 0)) as? LyricCell
        cell?.pauseLyric()
        ///
        self.timer?.invalidate()
        self.timer = nil
        self.timerTime?.invalidate()
        /// UIs
//        self.buttonFinish.isHidden = false
        self.viewPlay.pauseAnimation()
        if (self.mp3Player?.currentTime ?? 0) < (self.arrTime.first ?? 0) {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.onStartCountdown), object: nil)
            self.timerCountdown?.invalidate()
            self.timerCountdown = nil
        }
        self.buttonPlay.setImage(UIImage(named: "icon_start"), for: UIControlState.normal)
        
        if self.isCameraOn {
            self.videoFileOutput.stopRecording()
        }
        
        self.onShowPopupFinish()
    }
    
    ////
    fileprivate func resume() {
        guard let engine = self.engine, self.arrayLyric.count > 0 else { print("Fail to resume"); return }
        if self.isHeadphonePluged && engine.isStarted {
            engine.resume()
        } else {
            self.mp3Player?.play()
            self.audioRecorder?.record()
        }
        
        self.mp3Player?.play()
        // UIs
        self.viewPlay.resumeAnimation()
        let cell = self.tableLyric.cellForRow(at: IndexPath(row: self.currentRow - 1, section: 0)) as? LyricCell
        cell?.resumeLyric()
        self.buttonPlay.setImage(UIImage(named: "icon_pause"), for: UIControlState.normal)
        
        if self.isCameraOn {
            self.recordVideo()
        }
        
        self.timerTime = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countingTime), userInfo: nil, repeats: true)
        // Update current time value
//        if self.currentRow == 1 && !self.isCameraOn {
//            self.currentRow -= 1
//        }
        if self.currentRow < self.timings.count {
            var isAddedOne = false
            if self.currentRow == 0 { self.currentRow += 1; isAddedOne = true }
            self.timings[self.currentRow - 1][0] = abs(self.timings[self.currentRow][0] - (self.mp3Player?.currentTime ?? 0))
//            if self.currentRow == 1 && self.timings.count > 0 {
//                self.timings[1][0] = self.timings[0][0]
//            }
            if isAddedOne { self.currentRow = 0 }
        }
    }
    
    ////
    fileprivate func stop() {
        ///
        self.isPlayed = false
//        self.reset()
        let cell = self.tableLyric.cellForRow(at: IndexPath(row: self.currentRow - 1, section: 0)) as? LyricCell
        cell?.pauseLyric()
        if self.isHeadphonePluged {
            self.engine?.stop()
        } else {
            self.audioRecorder?.stop()
            self.mp3Player?.stop()
        }
//        self.recorder.stop()
//        self.mp3Player?.stop()
        /// UIs
        self.viewPlay.stopAnimation()
        if self.isCameraOn {
            self.videoFileOutput.stopRecording()
        }
    }
}

//MARK: - Config recorder
extension RecordViewController {
    ////
    func configAudioSesssion() {

        let settings = [
            AVFormatIDKey: Int(kAudioFormatAppleIMA4),
            AVSampleRateKey: 48000,
            AVNumberOfChannelsKey: 2,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue,
            AVEncoderBitRateKey:128000,
            AVEncoderBitDepthHintKey : 16,
            AVEncoderBitRatePerChannelKey: 9
        ]
        
        do {
            self.audioRecorder = try AVAudioRecorder(url: self.voiceUrl, settings: settings)
            self.audioRecorder?.delegate = self
            self.audioRecorder?.prepareToRecord()
            try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord, with: .defaultToSpeaker)
            try? AVAudioSession.sharedInstance().setActive(true)
            self.mp3Player = try AVAudioPlayer(contentsOf: self.beatUrl)
            //
            
            let volume : Float = self.isHeadphonePluged ? 0.8 : 0.28
            self.sliderBeatVol.maximumValue = self.isHeadphonePluged ? 0.8 : 0.3
            self.mp3Player?.volume = volume
            self.sliderBeatVol.value = volume
            //
            self.sliderVoiceVol.value = 0.95
            
            self.mp3Player?.prepareToPlay()
            
            DispatchQueue.main.async {
                self.duration = Int(self.mp3Player?.duration ?? 0)
                self.sec = self.duration % 60
                self.minutes = self.duration / 60
                
                self.mp3Player?.prepareToPlay()
//                let strTime = (self.minutes < 10 ? "0\(self.minutes)" : "\(self.minutes)") + (self.sec < 10 ? ":0\(self.sec)" : ":\(self.sec)")
//                self.labelCurrentTime.text = strTime
                self.labelEstimateTime?.time(totalSecs: self.duration, isPositive: false)
                self.sliderTime?.maximumValue = Float(self.duration)
            }
        } catch {
            self.popupSorry()
        }
    }
}

//MARK: - Recorder delegate
extension RecordViewController: AVAudioRecorderDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            self.popupSorry()
        }
    }
}

//MARK: - Video recording
extension RecordViewController: AVCaptureFileOutputRecordingDelegate {
    
    func fileOutput(_ captureOutput: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]) {
        print("Recording")
    }
    
    func fileOutput(_ captureOutput: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        print("Recorded")
    }
}

extension RecordViewController: URLSessionDelegate, URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        do {
            // after downloading your file you need to move it to your destination url
            
            if FileManager.default.fileExists(atPath: self.beatUrl.absoluteString) {
                Utility.shared.deleteFiles(filesUrl: self.beatUrl)
            }
            
            try FileManager.default.moveItem(at: location, to: self.beatUrl)
            DispatchQueue.main.async {
                
                if self.arrayLyric.count < 1 {
                    self.popupSorry()
                    return
                }
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.buttonPlay.setTitle("", for: UIControlState.normal)
                    self.buttonPlay.setImage(UIImage(named: "icon_start"), for: UIControlState.normal)
                    
                })
                self.viewPlay.downloadCompleted()
                self.buttonPlay.isEnabled = true
                
                //MARK: - Prepare recorder
                self.configAudioSesssion()
                ///
                self.engine = AudioEngine(beatUrl: self.beatUrl)
                self.engine?.setMicVolume(0.95)
                self.engine?.setBeatVolume(self.isHeadphonePluged ? 0.8 : 0.28)
                //TODO: - Handler audio
//                self.sliderMicVolume.value = self.engine?.getMicVolume() ?? 0
//                self.sliderBeatVolume.value = self.engine?.getBeatVolume() ?? 0
            }
            
            print("File moved to documents folder")
        } catch let error as NSError {
            NSLog(error.localizedDescription)
            self.popupSorry()
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite > 0 ? totalBytesExpectedToWrite : 7340032)
        DispatchQueue.main.async {
            self.buttonPlay.setTitle("\(Int(progress * 99.0))%", for: UIControlState.normal)
            self.viewPlay.downloaded(percent: CGFloat(progress))
        }
    }
}

//MARK: - Volume control
extension RecordViewController {
    
    @IBAction func onHideVolumeBar(_ sender: Any?) {
        self.constraintVolumeBar.constant = -150
        UIView.animate(withDuration: DURATION) {
            self.view.layoutSubviews()
        }
    }
    
    @IBAction func onShowVolumeBar(_ sender: Any?) {
        self.constraintVolumeBar.constant = Utility.shared.isPhoneX ? -1 : -31
        UIView.animate(withDuration: DURATION) {
            self.view.layoutSubviews()
        }
    }
    
}

//Dismiss when done
extension RecordViewController: Dismiss {
    func dismiss() {
        self.dismiss(animated: true) {
            self.task?.cancel(byProducingResumeData: { (nil) in
                print("Canceled")
            })
            self.task = nil
//            UIApplication.shared.isStatusBarHidden = false
            self.dismiss(animated: true, completion: nil)
        }
    }
}

//MARK: - TableView Delegate, DataSource
extension RecordViewController: UITableViewDelegate, UITableViewDataSource {
    ////
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayLyric.count
    }
    ////
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LyricCell
        ///
        cell.binding(lyric: self.arrayLyric[indexPath.row], space: self.space)
        cell.constraintBot?.constant = self.space
        cell.constraintTop?.constant = self.space
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
}

extension RecordViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.isCameraOn && self.isCalculating {
            self.isCalculating = false
            var tableHeight: CGFloat = 0
            let height: CGFloat = self.arrayLyric[self.calculatingRow].height(withConstrainedWidth: self.view.bounds.width - 36, font: UIFont(name: "SanFranciscoDisplay-Semibold", size: 21)!)
            if height <= 28 {
                tableHeight = height + 52
            } else if height > 28 && height < 60 {
                tableHeight = height + 26
            } else {
                tableHeight = height + 26
            }
            if self.constraintTableHeight.constant != tableHeight {
                self.constraintTableHeight.constant = tableHeight
            }
            if self.isPlayed && self.calculatingRow > 0 {
                self.tableLyric.scrollToRow(at: IndexPath(row: self.calculatingRow, section: 0), at: .top, animated: true)
            } else {
                self.tableLyric.scrollToRow(at: IndexPath(row: 1, section: 0), at: .top, animated: true)
            }
        }
        
    }
}
