//
//  SingFavoriteViewController.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class SingFavoriteViewController: UIViewController {

    //
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var resources: Array<Resource> = Array<Resource>()
    fileprivate var lastPage    : UInt8 = 1
    fileprivate var current     : UInt8 = 1
    fileprivate var searchString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Config Table View
        self.tableView.register(UINib(nibName: "SingFavoriteCell", bundle: nil), forCellReuseIdentifier: "singFavoriteCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.addInfiniteScroll()
        self.tableView.tableFooterView = UIView()
    }
    
    @objc func onSearchBy(_ noti: Notification) {
        guard let searchString = noti.object as? String else { return }
        self.searchString = searchString
        SearchService.shared.favorite(query: searchString) { [weak self] (result) in
            guard let `self` =  self else { return }
            if result.status == 200 {
                self.tableView.showsInfiniteScrolling = true
                self.current = result.current
                self.lastPage = result.lastPage
                self.resources = result.items
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onSearchBy(_:)), name: NSNotification.Name.init(NotiName.search), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    private func addInfiniteScroll () {
        self.tableView?.addInfiniteScrolling(actionHandler: { [weak self] in
            guard let `self` = self else { return }
            let next = self.current + 1
            if next <= self.lastPage {
                SearchService.shared.favorite(query: self.searchString, page: next, callback: { (result) in
                    if result.status == 200 {
                        self.current = result.current
                        let count = self.resources.count
                        var indexs = Array<IndexPath>()
                        for iter in 0..<result.items.count {
                            indexs.append(IndexPath(row: count + iter, section: 0))
                        }
                        self.resources.append(contentsOf: result.items)
                        self.tableView.beginUpdates()
                        self.tableView.insertRows(at: indexs, with: .automatic)
                        self.tableView.endUpdates()
                    }
                    self.tableView.infiniteScrollingView.stopAnimating()
                })
            } else {
                self.tableView.infiniteScrollingView.stopAnimating()
                self.tableView.showsInfiniteScrolling = false
            }
        })
    }
    
}

//MARK: - TableView
extension SingFavoriteViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.resources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "singFavoriteCell", for: indexPath) as! SingFavoriteCell
        //
        cell.resource = self.resources[indexPath.row]
        cell.bindingUIs(resource: self.resources[indexPath.row], index: indexPath.row)
        cell.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        cell.delegate = self
        //
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if(velocity.y>0) {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        } else {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
}

//MARK: - Favorite action
extension SingFavoriteViewController: OnSingFavorite {
    func action(_ type: SingFavoriteCell.ActionType, _ cell: SingFavoriteCell, _ index: Int) {
        switch type {
        case .sing:
            if let recordVc: RecordViewController = StoryBoard.Sing.viewController("RecordViewController") as? RecordViewController {
//                recordVc.resourceId = cell.resource?.id ?? ""
                recordVc.resource = cell.resource
                self.present(recordVc, animated: true, completion: nil)
            }
        case .favorite:
            self.resources.remove(at: index)
//            self.tableView.beginUpdates()
//            self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: UITableViewRowAnimation.automatic)
            self.tableView.reloadData()
//            self.tableView.endUpdates()
//        default:
//            break
        }
    }
}

extension SingFavoriteViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Favorite")
    }
}
