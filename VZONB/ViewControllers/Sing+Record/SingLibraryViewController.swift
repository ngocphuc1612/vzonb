//
//  SingLibraryViewController.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class SingLibraryViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var resources: Array<Resource> = Array<Resource>()
    fileprivate var lastPage    : UInt8 = 1
    fileprivate var current     : UInt8 = 1
    fileprivate var searchString: String = ""
    
//    private func downloadResources() {
//        DispatchQueue.global().async {
//            ResourceService.shared.getBy(page: 1, callback: { [weak self] (result) in
//                guard let `self` =  self else { return }
//                if result.status == 200 {
//                    self.resources = result.items
//                    DispatchQueue.main.async {
//                        self.tableView.reloadData()
//                    }
//                }
//            })
//        }
//    }
    
    private func addInfiniteScroll () {
        self.tableView.addInfiniteScrolling(actionHandler: { [weak self] in
            guard let `self` = self else { return }
            let next = self.current + 1
            if next <= self.lastPage {
                SearchService.shared.resources(query: self.searchString, page: next, callback: { (result) in
                    if result.status == 200 {
                        self.current = result.current
                        let count = self.resources.count
                        var indexs = Array<IndexPath>()
                        for iter in 0..<result.items.count {
                            indexs.append(IndexPath(row: count + iter, section: 0))
                        }
                        self.resources.append(contentsOf: result.items)
                        self.tableView.beginUpdates()
                        self.tableView.insertRows(at: indexs, with: .automatic)
                        self.tableView.endUpdates()
                    }
                    self.tableView.infiniteScrollingView.stopAnimating()
                })
            } else {
                self.tableView.infiniteScrollingView.stopAnimating()
                self.tableView.showsInfiniteScrolling = false
            }
        })
    }
    
    override func viewDidLoad() {
//        self.downloadResources()
        //
        super.viewDidLoad()
        self.searchBy(text: "")
        //
        self.tableView.register(UINib(nibName: "SingFavoriteCell", bundle: nil), forCellReuseIdentifier: "singFavoriteCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.addInfiniteScroll()
        
        self.tableView.tableFooterView = UIView()
        
    }
    
    @objc func onSearchBy(_ noti: Notification) {
        guard let searchString = noti.object as? String else { return }
        self.searchBy(text: searchString)
    }
    
    private func searchBy(text: String) {
        SearchService.shared.resources(query: text) { [weak self] (result) in
            guard let `self` =  self else { return }
            if result.status == 200 {
                self.tableView.showsInfiniteScrolling = true
                self.resources = result.items
                self.current = result.current
                self.lastPage = result.lastPage
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onSearchBy(_:)), name: NSNotification.Name.init(NotiName.search), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension SingLibraryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.resources.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "singFavoriteCell", for: indexPath) as! SingFavoriteCell
        cell.resource = self.resources[indexPath.row]
        cell.bindingUIs(resource: self.resources[indexPath.row], index: indexPath.row)
        cell.delegate = self
        cell.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if(velocity.y>0) {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        } else {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
}

//MARK: - Favorite action
extension SingLibraryViewController: OnSingFavorite {
    func action(_ type: SingFavoriteCell.ActionType, _ cell: SingFavoriteCell, _ index: Int) {
        switch type {
        case .sing:
            if let recordVc: RecordViewController = StoryBoard.Sing.viewController("RecordViewController") as? RecordViewController {
//                recordVc.resourceId = cell.resource?.id ?? ""
                recordVc.resource = cell.resource
                self.present(recordVc, animated: true, completion: nil)
            }
        case .favorite:
            guard let resource = cell.resource else { return }
            self.resources[index] = resource
//        default:
//            break
        }
    }
}

extension SingLibraryViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Library")
    }
}
