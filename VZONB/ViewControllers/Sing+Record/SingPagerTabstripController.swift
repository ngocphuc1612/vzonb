//
//  SingPagerTabstripController.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class SingPagerTabstripController: ButtonBarPagerTabStripViewController {
    
    var isRecordedLoaded: Bool = false
    let searchBar = UISearchBar()
    var searchingString: String {
        return self.searchBar.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) ?? ""
    }
    
//    private func configNavigation() {
//        self.navigationController?.navigationBar.barTintColor = Color.default
//        self.navigationController?.navigationBar.tintColor = UIColor.white
//    }
    
    override func viewDidLoad() {
        //
        self.settings.style.buttonBarBackgroundColor = UIColor.white //Constant.colorDefault
        self.settings.style.buttonBarHeight = 40
        //
        self.settings.style.selectedBarBackgroundColor = Color.default
        self.settings.style.selectedBarHeight = 2
        //
        self.settings.style.buttonBarItemBackgroundColor = UIColor.clear
        self.settings.style.buttonBarItemFont = UIFont(name: "SanFranciscoDisplay-Semibold", size: 15)!
        self.settings.style.buttonBarItemTitleColor = Color.inactive
        
        ///
        super.viewDidLoad()
        //
        
        searchBar.placeholder = "Search by Artists or Song"
        searchBar.tintColor = UIColor.black
        self.searchBar.barTintColor = UIColor.black
        self.searchBar.searchBarStyle = .minimal
        self.searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
//        searchBar.setPlaceholder(color: UIColor.white)
        searchBar.delegate = self
        self.navigationItem.titleView = searchBar
        
        self.changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.label.textColor = Color.inactive
            newCell?.label.textColor = Color.default
            
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.onPostNewSearch), object: nil)
            self.perform(#selector(self.onPostNewSearch), with: nil, afterDelay: 0.5)
        }
    }
    ///
    @objc func onPostNewSearch() {
        NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.search), object: self.searchingString)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ///
//        self.configNavigation()
        //
//        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.isStatusBarHidden = false
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        ///
        guard let library: SingLibraryViewController = StoryBoard.Sing.viewController("SingLibraryViewController") as? SingLibraryViewController,
            let favorite: SingFavoriteViewController = StoryBoard.Sing.viewController("SingFavoriteViewController") as? SingFavoriteViewController,
            let recorded: SingRecordedViewController = StoryBoard.Sing.viewController("SingRecordedViewController") as? SingRecordedViewController else { return [] }
        ///
        return [library, favorite, recorded]
    }
    
}

extension SingPagerTabstripController: UISearchBarDelegate {
    ///
    @objc func onReloadData () {
        Shared.searchingString = self.searchingString
        NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.search), object: self.searchingString)
    }
    ///
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.onReloadData), object: nil)
        self.perform(#selector(self.onReloadData), with: nil, afterDelay: 0.5)
    }
}
