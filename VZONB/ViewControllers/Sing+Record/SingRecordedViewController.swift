//
//  SingRecordedViewController.swift
//  VZONB
//
//  Created by Phuc on 6/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SVPullToRefresh
import CoreData
import AVFoundation
import DZNEmptyDataSet

enum SingAction {
    case post, reRecord, delete, play, edit, none
}

protocol OnSingAction: class {
    func onActionPress(_ record: Record, type: SingAction, index: Int)
}

class SingRecordedViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    ////
    fileprivate let context     = DataManager().managedObjectContext
    fileprivate var records     : Array<Record> = Array<Record>()
    fileprivate var filterRecords: Array<Record> = Array<Record>()
    fileprivate var documentURL : URL { return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! }
    fileprivate var currentIndex: Int = -1
    
    //Save the isdex of posting
    fileprivate var listPosting: Dictionary<Int, Bool> = Dictionary<Int, Bool>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Register
        self.tableView.register(UINib(nibName: "SingRecordedCell", bundle: nil), forCellReuseIdentifier: "singRecordedCell")
        self.tableView.emptyDataSetDelegate = self
        self.tableView.emptyDataSetSource   = self
        ///
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: .defaultToSpeaker)
        self.addPullToRefresh ()
        
        self.tableView.tableFooterView = UIView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onReceivePostingIndex(_:)), name: NSNotification.Name.init(NotiName.posting), object: nil)
        
    }
    
    @objc func onSearchBy(_ noti: Notification) {
        guard let searchString = noti.object as? String else { return }
        
        if searchString.isEmpty {
            self.filterRecords = self.records
        } else {
            //
            self.filterRecords = self.records.filter({ $0.resourceName.uppercased().contains(searchString.uppercased()) })
        }
        if self.filterRecords.count > 0 {
            self.tableView.reloadData()
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAllRecorded()
        NotificationCenter.default.addObserver(self, selector: #selector(self.onSearchBy(_:)), name: NSNotification.Name.init(NotiName.search), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onReloadTable), name: NSNotification.Name.init(NotiName.posted), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        if self.currentIndex > -1 {
//            if let cell = self.tableView.cellForRow(at: IndexPath(row: self.currentIndex, section: 0)) as? SingRecordedCell {
//                cell.constraintHeight?.constant = 130
//                cell.viewPlayer?.stop()
//                self.currentIndex = -1
//            }
//        }
//        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.init(NotiName.search), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.init(NotiName.posted), object: nil)
    }
    
    @objc func onReceivePostingIndex (_ noti: Notification) {
        guard let localId: Int = noti.object as? Int else { return }
        self.listPosting[localId] = true
        self.tableView.reloadData()
    }
    
    @objc func onReloadTable(_ noti: Notification) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: { [unowned self] in
            self.getAllRecorded()
        })
    }
    
    private func getAllRecorded () {
        DispatchQueue.global().async {
            let req:NSFetchRequest<NSManagedObject> = NSFetchRequest(entityName: "Recorded")
            do
            {
                let contactsRusult = try self.context.fetch(req)
                self.records = Array<Record>()
                for sp in contactsRusult
                {
                    self.records.append(Record(perStore: sp))
                }
                ////
                self.records = self.records.sorted(by: { $0.localId > $1.localId })
                self.filterRecords = self.records
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            catch
            {
                print("Loi \(error)")
            }
            
            
        }
    }
    
    //Pull to refresh
    private func addPullToRefresh () {
        self.tableView.addPullToRefresh { [weak self] in
            guard let `self` = self else { return }
//            UserService.shared.getRecords(callback: { (result) in
//                self.records = result.items
//                DispatchQueue.main.async {
//                    self.tableView.reloadData()
//                }
//                self.tableView.pullToRefreshView.stopAnimating()
//            })
            
            let req:NSFetchRequest<NSManagedObject> = NSFetchRequest(entityName: "Recorded")
            do
            {
                let contactsRusult = try self.context.fetch(req)
                self.records = Array<Record>()
                for sp in contactsRusult
                {
                    self.records.append(Record(perStore: sp))
                }
                self.records = self.records.sorted(by: { $0.localId > $1.localId })
                self.filterRecords = self.records
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.tableView.pullToRefreshView.stopAnimating()
                }
            }
            catch
            {
                print("Loi \(error)")
                self.tableView.pullToRefreshView.stopAnimating()
            }
        }
    }
    
}

//MARK: - TableView
extension SingRecordedViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterRecords.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "singRecordedCell", for: indexPath) as! SingRecordedCell
        //
        cell.delegate = self
        cell.binding(record: self.filterRecords[indexPath.row], index: indexPath.row, isPublishing: self.listPosting[self.filterRecords[indexPath.row].localId] ?? false)
        
        return cell
    }
    ////
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    ////
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if(velocity.y>0) {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        } else {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
}

extension SingRecordedViewController: OnSingAction {
    
    func postSelection(record: Record, index: Int) {
        let alert: UIAlertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        let alertNormal = UIAlertAction(title: "Publish Recording", style: .default) { (_) in
//            print("Normal post here !!! ")
            self.post(record: record, index: index, isPostToContest: false)
        }
        
        let alertContest = UIAlertAction(title: "Publish Contest Recording", style: .default) { (_) in
            //            print("ContestGroup !!! ")
            self.post(record: record, index: index, isPostToContest: true)
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(alertContest)
        alert.addAction(alertNormal)
        alert.addAction(actionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func post(record: Record, index: Int, isPostToContest: Bool = false) {
        self.listPosting[self.records[index].localId] = true
        if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? SingRecordedCell {
            cell.viewPosting?.isHidden = false
            cell.indicatorView?.startAnimating()
            cell.buttonPlay?.isHidden = true
        }
        //
        DispatchQueue.global().async {
            MediaProcess.shared.mix(audios: [self.documentURL.appendingPathComponent(record.voicePath).absoluteString, self.documentURL.appendingPathComponent(record.beatPath).absoluteString], volumes: [record.voiceVolume, record.voiceVolume], callback: { (success, url) in
                if success {
                    guard let url = url else { return }
                    let coverPath = self.documentURL.appendingPathComponent(record.coverPath)
                    if record.type == "video" {
                        MediaProcess.shared.mix(audio: url.absoluteString, video: self.documentURL.appendingPathComponent(record.videoPath ?? "").absoluteString, callback: { (success, url) in
                            guard let url = url else { return }
                            UploadThread.shared.upload(mediaUrl: url, name: record.resourceName, resourceId: record.resourceId, isVideo: true, description: "", ext: "mp4", record: record, coverPath: coverPath, isContestGroup: isPostToContest)
                        })
                        return
                    }
                    
                    UploadThread.shared.upload(mediaUrl: url, name: record.resourceName, resourceId: record.resourceId, isVideo: false, description: "", record: record, coverPath: coverPath, isContestGroup: isPostToContest)
                    
                }
            })
        }
    }
    
    func onActionPress(_ record: Record, type: SingAction, index: Int) {
        switch type {
        case .post:
            if let vc = StoryBoard.Sing.viewController("PostNewRecordViewController") as? PostNewRecordViewController {
                self.currentIndex = index
                vc.record = record
                self.navigationController?.pushViewController(vc, animated: true)
            }
//            if !GlobalInfo.shared.userStatus.group.canSubmit {
//                self.post(record: record, index: index, isPostToContest: false)
//            } else {
//                self.postSelection(record: record, index: index)
//            }
            break
        case .delete:
            self.showMessageWithAction(with: "Notice", message: "Are you sure delete this record?", title: "Delete", cancelTitle: "Cancel", okStyle: UIAlertActionStyle.destructive, completion: {
                // Delete core data
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    record.delete(id: record.localId)
//                    if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? SingRecordedCell {
//                        cell.viewPlayer?.stop()
//                        cell.viewPlayer?.resetMedias()
//                    }
                    
                    // Delete file local
//                    do {
                    try? FileManager.default.removeItem(at: self.documentURL.appendingPathComponent(record.voicePath))
                    try? FileManager.default.removeItem(at: self.documentURL.appendingPathComponent(record.beatPath))
                    if record.type == "video" && record.videoPath != nil {
                        if !(record.videoPath ?? "").isEmpty {
                            try? FileManager.default.removeItem(at: self.documentURL.appendingPathComponent(record.videoPath ?? ""))
                        }
                    }
                    
                    ///
                    self.records.remove(at: index)
                    self.filterRecords.remove(at: index)
                    self.tableView.reloadData()
                }
            })
            
        case .play:
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                if let vc = StoryBoard.Sing.viewController("SingReviewRecordViewController") as? SingReviewRecordViewController {
                    //                vc.record = record
                    vc.records = self.records
                    vc.currentIndex = index
                    vc.delegate = self
                    self.navigationController?.pushViewController(vc, animated: true)
//                    self.present(vc, animated: true, completion: nil)
                }
            }
//            self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableViewRowAnimation.automatic)
//            if self.currentIndex > -1 {
//                if let cell = self.tableView.cellForRow(at: IndexPath(row: self.currentIndex, section: 0)) as? SingRecordedCell {
//                    cell.constraintHeight?.constant = 230
//                    cell.viewPlayer?.stop()
//                    cell.viewPlayer?.delegate = cell
//                }
//                self.currentIndex = index
//            } else {
//                self.currentIndex = index
//            }
//
//            self.tableView.beginUpdates()
//            self.tableView.endUpdates()
//            self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: UITableViewScrollPosition.middle, animated: true)
        case .edit :
            if let playVC = StoryBoard.Sing.viewController("SingReviewViewController") as? SingReviewViewController {
                playVC.record = record
                playVC.isHasVideo = record.type == "video"
                print(record)
                self.present(playVC, animated: true, completion: nil)
            }
        case .reRecord:
            if let recordVc: RecordViewController = StoryBoard.Sing.viewController("RecordViewController") as? RecordViewController {
                let resource = Resource(id: record.resourceId, name: record.resourceName)
                recordVc.resource = resource
                self.present(recordVc, animated: true, completion: nil)
            }
        default: break
        }
    }
}

extension SingRecordedViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "ic_history")!
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "Empty", attributes: [NSAttributedStringKey.font: Font.emptyData])
    }
}

extension SingRecordedViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Recorded")
    }
}
