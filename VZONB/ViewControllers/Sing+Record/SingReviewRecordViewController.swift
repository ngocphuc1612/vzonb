//
//  SingReviewRecordViewController.swift
//  VZONB
//
//  Created by PT on 9/23/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import AVFoundation

class SingReviewRecordViewController: UIViewController {

    @IBOutlet weak var labelName    : UILabel?
    @IBOutlet weak var labelDescription : UILabel?
    @IBOutlet weak var labelTime    : UILabel?
    @IBOutlet weak var imageAvatar  : UIImageView?
    @IBOutlet weak var sliderTime   : UISlider?
    @IBOutlet weak var labelCurrent : UILabel?
    @IBOutlet weak var labelTotal   : UILabel?
    @IBOutlet weak var imageCover   : UIImageView?
    @IBOutlet weak var viewPlayer   : UIView?
    @IBOutlet weak var viewGesture  : UIView?
    @IBOutlet weak var viewControl  : UIView?
//    @IBOutlet weak var constraintTopHeader : NSLayoutConstraint?
//    @IBOutlet weak var constraintBottomSlider : NSLayoutConstraint?
    @IBOutlet weak var viewHeader   : UIView?
    @IBOutlet weak var viewSlider   : UIView?
    @IBOutlet weak var buttonPlay   : UIButton?
    
    ///
    var documentURL         : URL { return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! }
    ////
//    var record      : Record?
    weak var delegate       : OnSingAction? = nil
    var type: SingAction = .none
    var currentIndex: Int = 0
    var records     : Array<Record> = Array<Record>()
    var isHasVideo  : Bool = false
    ////
    private var audioPlayer     : AVAudioPlayer?
    fileprivate var mp3Player   : AVAudioPlayer?
    private var avplayer        : AVPlayer?
    private var avplayerLayer   : AVPlayerLayer?
    // Timer
    fileprivate var secs        : Int    = 0
    fileprivate var minutes     : Int    = 0
    fileprivate var duration    : Int    = 0
    fileprivate var timer       : Timer? = nil
    ///
//    fileprivate var gesture     : UIPanGestureRecognizer?
    fileprivate var lastY       : CGFloat = 0
    fileprivate let LIMITRANGEY : CGFloat = 100
    fileprivate var currentHiddenStatusPlayButton : Bool = false
    
    private func configUIs () {
        self.sliderTime?.setThumbImage(UIImage(named: "thumb"), for: UIControlState.normal)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func loadResource( record: Record ) {
        self.isHasVideo = record.type == "video" ? true : false
        self.imageAvatar?.kf_setImage(url: GlobalInfo.shared.info?.avatarUrl ?? "")
        self.labelName?.text = GlobalInfo.shared.info?.fullname ?? ""
        self.labelTime?.text = Utility.shared.convertDateTimeToString(record.createdAt)
        self.labelDescription?.text = record.resourceName
        if self.isHasVideo {
            self.imageCover?.isHidden = true
        } else {
            self.imageCover?.kf_setImage( url: record.coverPath )
            self.imageCover?.isHidden = false
        }
        self.prepareToPlay()
    }
    
    private func prepareToPlay() {
        ///
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            if self.records.count > self.currentIndex {
                let record = self.records[self.currentIndex]
                self.audioPlayer = try? AVAudioPlayer(contentsOf: self.documentURL.appendingPathComponent( record.beatPath ))
                self.audioPlayer?.prepareToPlay()
                self.audioPlayer?.volume = record.beatVolume
                self.mp3Player = try? AVAudioPlayer(contentsOf: self.documentURL.appendingPathComponent(record.voicePath))
                self.mp3Player?.prepareToPlay()
                self.mp3Player?.volume = record.voiceVolume
                if let player = self.mp3Player {
                    self.duration = Int(player.duration)
                    self.labelTotal?.time(totalSecs: self.duration, isPositive: false)
                    self.sliderTime?.maximumValue = Float(self.duration)
                }
                ///
                if self.isHasVideo {
                    self.configPlayer()
                    self.configAVPlayer(url: self.documentURL.appendingPathComponent( record.videoPath ?? "" ))
                    self.viewPlayer?.isHidden = false
                } else {
                    self.viewPlayer?.isHidden = true
                }
                self.buttonPlay?.isHidden = false
            }
        }
    }
    
    ///config video player
    private func configPlayer() {
        self.avplayerLayer = AVPlayerLayer()
        avplayerLayer?.backgroundColor = UIColor.white.cgColor
        avplayerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        avplayerLayer?.backgroundColor = UIColor.clear.cgColor
        self.viewPlayer?.layer.addSublayer(self.avplayerLayer!)
        self.avplayerLayer?.frame = self.view.bounds
    }
    /// -> Create player session
    private func configAVPlayer(url: URL) {
        DispatchQueue.main.async {
            let playerItem = AVPlayerItem(url: url)
            self.avplayer = AVPlayer(playerItem: playerItem)
            self.avplayer?.isMuted = false
            self.avplayerLayer?.player = self.avplayer
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: .defaultToSpeaker)
        self.configUIs()
        if self.records.count > self.currentIndex {
            self.loadResource( record: self.records[self.currentIndex] )
        }
        if self.records.count > 0 {
            self.viewGesture?.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.onPanListioner(_:))))
        }
    }
    
    @objc func onPanListioner (_ sender: UIPanGestureRecognizer) {
        let y = sender.translation(in: self.viewGesture).y
        self.viewControl?.layer.transform = CATransform3DMakeTranslation(0, y, 0)
        self.viewPlayer?.layer.transform = CATransform3DMakeTranslation(0, y, 0)
        if sender.state == .began {
            self.currentHiddenStatusPlayButton = self.buttonPlay?.isHidden ?? false
            self.buttonPlay?.isHidden = true
        }
        if sender.state == .ended {
            self.buttonPlay?.isHidden = self.currentHiddenStatusPlayButton
            self.lastY  = y
            self.onHandlerGesture()
        }
    }
    
    func onHandlerGesture () {
        var neededHeight: CGFloat = 0
        if (self.lastY < LIMITRANGEY && self.lastY > -LIMITRANGEY) || (self.lastY > -LIMITRANGEY && self.currentIndex == 0) || (self.lastY < LIMITRANGEY && self.currentIndex == self.records.count - 1) {
            UIView.animate(withDuration: 0.2, animations: {
                self.viewControl?.layer.transform = CATransform3DIdentity
                self.viewPlayer?.layer.transform = CATransform3DIdentity
            })
            return
        } else if (self.lastY > -LIMITRANGEY && self.currentIndex > 0) {
            self.currentIndex -= 1
            neededHeight = self.view.bounds.height
            
        } else if self.lastY < LIMITRANGEY && self.currentIndex < self.records.count - 1 {
            self.currentIndex += 1
            neededHeight = -self.view.bounds.height
        }
//        self.stop()
        self.pause()
        self.resetToFirstState()
        self.buttonPlay?.isHidden = true
        self.viewPlayer?.layer.transform = CATransform3DIdentity
        self.viewSlider?.isHidden = true
        UIView.animate(withDuration: 0.2, animations: {
            self.viewControl?.layer.transform = CATransform3DMakeTranslation(0, neededHeight, 0)
        }, completion: { [weak self] (_) in
            guard let `self` = self else { return }
            self.viewControl?.alpha = 0.001
            self.avplayerLayer?.opacity = 0.001
            self.viewControl?.layer.transform = CATransform3DIdentity
            UIView.animate(withDuration: 0.4, animations: {
                self.viewControl?.alpha = 1
                self.avplayerLayer?.opacity = 1
                self.showControl()
            }, completion: { (_) in
                self.viewSlider?.isHidden = false
            })
        })
        self.loadResource(record: self.records[self.currentIndex])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
    }
    
    ///
    @objc func runTime () {
        self.secs += 1
        if self.secs > 59 {
            self.secs = 0
            self.minutes += 1
        }
        
        let estimate = self.duration - (self.minutes * 60 + self.secs)
        if estimate  <= 0 {
            //TODO: - Stop
            self.timer?.invalidate()
            self.timer = nil
            
            self.mp3Player?.pause()
            self.mp3Player?.currentTime = 0
            self.audioPlayer?.pause()
            self.audioPlayer?.currentTime = 0
            self.avplayer?.seek(to: CMTime(seconds: 0, preferredTimescale: 1))
            self.avplayer?.pause()
            
            self.showControl()
            self.resetToFirstState()
            
        }
        
        let currentTime = self.secs + self.minutes * 60
        self.labelCurrent?.time(totalSecs: currentTime)
        self.labelTotal?.time(totalSecs: self.duration - currentTime, isPositive: false)
        self.sliderTime?.value = Float(currentTime)
    }
    
    //MARK: - Play
    func play() {
        ////
        self.audioPlayer?.play()
        self.mp3Player?.play()
        if self.isHasVideo {
            self.avplayer?.play()
        }
        /// -> Start time calculate
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.runTime), userInfo: nil, repeats: true)
        self.perform(#selector(self.hideControl), with: nil, afterDelay: 3.0)
    }
    
    //MARK: - Resume
    func pause() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.hideControl), object: nil)
        self.audioPlayer?.pause()
        self.mp3Player?.pause()
        if self.isHasVideo {
            self.avplayer?.pause()
        }
        self.timer?.invalidate()
        self.timer = nil
    }
    
    @objc func hideControl () {
//        self.constraintTopHeader?.constant = -130
//        self.constraintBottomSlider?.constant = -50
        UIView.animate(withDuration: 0.3, animations: {
//            self.viewControl?.layoutSubviews()
            self.viewHeader?.alpha = 0
            self.viewSlider?.alpha = 0
        }) { (_) in
            self.viewHeader?.isHidden = true
            self.viewSlider?.isHidden = true
        }
    }
    
    func showControl () {
        self.viewHeader?.isHidden = false
        self.viewSlider?.isHidden = false
//        self.constraintTopHeader?.constant = 0
//        self.constraintBottomSlider?.constant = 0
        UIView.animate(withDuration: 0.3) {
//            self.viewControl?.layoutSubviews()
            self.viewHeader?.alpha = 1
            self.viewSlider?.alpha = 1
        }
    }
    
    @IBAction func onChangeTime(_ sender: UISlider) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.hideControl), object: nil)
        self.perform(#selector(self.hideControl), with: nil, afterDelay: 3.0)
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
        
        if !sender.isTracking {
            let currentSecs: Int = Int(sender.value)
            self.secs       = currentSecs % 60
            self.minutes    = currentSecs / 60
            self.labelCurrent?.time(totalSecs: currentSecs, isPositive: true)
            self.labelTotal?.time(totalSecs: self.duration - currentSecs, isPositive: false)
            self.avplayer?.seek(to: CMTime(seconds: Double(currentSecs), preferredTimescale: 1))
            self.mp3Player?.currentTime = TimeInterval(currentSecs)
            self.audioPlayer?.currentTime = TimeInterval(currentSecs)
            self.mp3Player?.play()
            self.audioPlayer?.play()
            self.avplayer?.play()
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.runTime), userInfo: nil, repeats: true)
            self.buttonPlay?.isHidden = true
        }
    }
    
    @IBAction func onPlayPausePress(_ sender: Any) {
        
        if !(sender is UIButton) && (self.mp3Player?.isPlaying ?? false) && (self.viewHeader?.isHidden ?? false) {
            self.showControl()
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.hideControl), object: nil)
            self.perform(#selector(self.hideControl), with: nil, afterDelay: 3.0)
            return
        }
        
        if self.mp3Player?.isPlaying ?? false {
            self.buttonPlay?.isHidden = false
            self.pause()
        } else {
            self.buttonPlay?.isHidden = true
            self.play()
        }
    }
    
    private func stop () {
        self.mp3Player?.stop()
        self.audioPlayer?.stop()
        self.avplayer?.pause()
        
        self.mp3Player = nil
        self.audioPlayer = nil
        self.avplayer = nil
        self.resetToFirstState()
    }
    
    private func resetToFirstState () {
        self.timer?.invalidate()
        self.timer = nil
        self.secs = 0
        self.minutes = 0
        self.labelCurrent?.text = "00:00"
        self.sliderTime?.value = 0
        self.buttonPlay?.isHidden = false
    }
    
    // Edit Actions
    @IBAction func onPostPress(_ sender: Any?) {
        
        if let vc = StoryBoard.Sing.viewController("PostNewRecordViewController") as? PostNewRecordViewController {
//            self.currentIndex = index
            vc.record = records[self.currentIndex]
            self.navigationController?.pushViewController(vc, animated: true)
        }
//        self.type = .post
//        self.navigationController?.popViewController(animated: true)
//        self.onDismiss(nil)
    }
    
    @IBAction func onDelete(_ sender: Any?) {
//        self.type = .delete
//        self.onDismiss(nil)
        self.showMessageWithAction(with: "Notice", message: "Are you sure delete this record?", title: "Delete", cancelTitle: "Cancel", okStyle: UIAlertActionStyle.destructive, completion: {
            // Delete core data
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                self.stop()
                let record = self.records[self.currentIndex]
                record.delete(id: record.localId)
                try? FileManager.default.removeItem(at: self.documentURL.appendingPathComponent(record.voicePath))
                try? FileManager.default.removeItem(at: self.documentURL.appendingPathComponent(record.beatPath))
                if record.type == "video" && record.videoPath != nil {
                    if !(record.videoPath ?? "").isEmpty {
                        try? FileManager.default.removeItem(at: self.documentURL.appendingPathComponent(record.videoPath ?? ""))
                    }
                }
                self.navigationController?.popViewController(animated: true)
//                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    @IBAction func onRerecordPress (_ sender: Any?) {
//        if let recordVc: RecordViewController = StoryBoard.Sing.viewController("RecordViewController") as? RecordViewController {
//            let resource = Resource(id: self.records[self.currentIndex].resourceId, name: self.records[self.currentIndex].resourceName)
//            recordVc.resource = resource
//            self.present(recordVc, animated: true, completion: nil)
//        }
        self.type = .reRecord
        self.onDismiss(nil)
    }
    @IBAction func onEditPress(_ sender: Any?) {
        self.type = .edit
        self.onDismiss(nil)
    }
    
    
    @IBAction func onDismiss(_ sender: Any?) {
        
        self.stop()
        
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
        self.delegate?.onActionPress(self.records[self.currentIndex], type: self.type, index: self.currentIndex)
//        self.dismiss(animated: true) { [weak self] in
//            guard let `self` = self else { return }
//            if self.delegate != nil {
//                self.delegate?.onActionPress(self.records[self.currentIndex], type: self.type, index: self.currentIndex)
//            }
//        }
    }
}
