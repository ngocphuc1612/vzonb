//
//  SingReviewViewController.swift
//  VZONB
//
//  Created by PT on 8/24/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import AVFoundation

class SingReviewViewController: UIViewController {

    @IBOutlet weak var viewCamera       : UIView?
    @IBOutlet weak var viewControlAudio : UIView?
    @IBOutlet weak var buttonPlay       : UIButton?
    @IBOutlet weak var sliderMicVolume  : UISlider?
    @IBOutlet weak var sliderBeatVolume : UISlider?
    @IBOutlet weak var imageCover       : UIImageView?
    
    var imageFirstFrame:    UIImage?
    
    fileprivate var volumeBeat: Float = 0.3 {
        didSet {
            DispatchQueue.main.async { [unowned self] in
                self.beatPlayer?.volume = self.volumeBeat
                self.sliderBeatVolume?.value = self.volumeBeat
            }
        }
    }
    
    fileprivate var volumeVoice: Float = 1 {
        didSet {
            DispatchQueue.main.async { [unowned self] in
                self.voicePlayer?.volume = self.volumeVoice
                self.sliderMicVolume?.value = self.volumeVoice
            }
        }
    }
    
    ///
    let documentURL         : URL = FileManager.default.documentUrl
    var voiceUrl            : URL { return self.documentURL.appendingPathComponent("recording.caf") }
    var beatUrl             : URL { return self.documentURL.appendingPathComponent("beat.mp3") }
    var mergeUrl            : URL { return self.documentURL.appendingPathComponent("merge.m4a") }
    var mergeVideo          : URL { return self.documentURL.appendingPathComponent("mergeVideo.mp4") }
    var completionVideoUrl  : URL { return self.documentURL.appendingPathComponent("completion.mp4") }
    ////
    var resource: Resource?
    var record  : Record?
    var isHasVideo          : Bool = false
    var numberOfVideos      : Int = 1
    var isHeadphonePlugged  : Bool = false
//    fileprivate var isPlayed: Bool = false
    weak var delegate       : Dismiss?
    ////
    private var beatPlayer : AVAudioPlayer?
    fileprivate var voicePlayer: AVAudioPlayer?
    private var avplayer: AVPlayer?
    private var avplayerLayer: AVPlayerLayer?
    ///
    // Timer
    fileprivate var secs        : Int    = 0
    fileprivate var minutes     : Int    = 0
    fileprivate var duration    : Int    = 0
    fileprivate var timer       : Timer? = nil
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private func configUIs () {
        ///
        self.sliderMicVolume?.setThumbImage(UIImage(named: "icon_thumb_slider"), for: UIControlState.normal)
        self.sliderBeatVolume?.setThumbImage(UIImage(named: "icon_thumb_slider"), for: UIControlState.normal)
        self.viewControlAudio?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onPlayPausePress(_:))))
        
        //TODO: - Delete it
        if self.record == nil {
            if self.isHeadphonePlugged {
                self.volumeBeat = 0.8
            } else {
                self.volumeBeat = 0.2
            }
            self.volumeVoice = 0.95
        }
        if !self.isHasVideo {
            self.viewCamera?.isHidden = true
            self.imageCover?.kf_setImage(url: self.resource?.thumbnail ?? "")
        } else {
            self.imageCover?.isHidden = true
        }
        
        //Re play older record
//        if record != nil {
//            self.buttonSavePrivate?.isHidden = true
//        }
    }
    
    private func loadResource() {
        guard let record = self.record else { return }
        
        do {
            
            let defaultManager: FileManager = FileManager.default
            
            do {
                try defaultManager.removeItem(at: self.beatUrl)
                try defaultManager.removeItem(at: self.voiceUrl)
                try defaultManager.removeItem(at: self.mergeVideo)
            } catch (let err) {
                print(err)
            }
            
            try defaultManager.copyItem(at: self.documentURL.appendingPathComponent(record.beatPath), to: self.beatUrl)
            try defaultManager.copyItem(at: self.documentURL.appendingPathComponent(record.voicePath), to: self.voiceUrl)
            //
            self.isHasVideo = record.type == "video" ? true : false
            self.volumeBeat = record.beatVolume
            self.volumeVoice = record.voiceVolume
            
            self.imageCover?.kf_setImage(url: self.record?.coverPath ?? "")
            //
            if self.isHasVideo && record.videoPath != nil {
                try defaultManager.copyItem(at: self.documentURL.appendingPathComponent(record.videoPath!), to: self.mergeVideo)
            }
        } catch (let err) {
            //TODO: - Popup sorry
            print(err.localizedDescription)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUIs()
        self.loadResource()
        ///
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.defaultToSpeaker)
        
        ///
        if self.isHasVideo {
            self.configPlayer()
            //            self.configAVPlayer()
            if record == nil {
                self.mergeVideo(numberOfVideos: self.numberOfVideos)
            } else {
                self.configAVPlayer(url: self.mergeVideo)
            }
        } else {
            self.viewCamera?.isHidden = true
        }
        
        self.prepareToPlay()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.avplayer = nil
        self.beatPlayer = nil
        self.voicePlayer = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.record == nil && self.isHasVideo {
            self.showLoading()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.avplayerLayer?.frame = self.viewCamera?.bounds ?? CGRect.zero
    }
    
    ///config video player
    private func configPlayer() {
        self.avplayerLayer = AVPlayerLayer()
        avplayerLayer?.backgroundColor = UIColor.white.cgColor
        avplayerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        avplayerLayer?.backgroundColor = UIColor.clear.cgColor
        self.viewCamera?.layer.addSublayer(self.avplayerLayer!)
//        self.avplayerLayer?.frame = self.viewCamera?.bounds ?? CGRect.zero
    }
    /// -> Create player session
    private func configAVPlayer(url: URL) {
        DispatchQueue.main.async {
            let playerItem = AVPlayerItem(url: url)
            self.avplayer = AVPlayer(playerItem: playerItem)
//        self.avplayer = AVPlayer(url: self.mergeVideo)
            self.avplayer?.isMuted = false
            
            self.avplayerLayer?.player = self.avplayer
        }
        //        self.avplayer?.play()
        
    }
    private func prepareToPlay() {
        ///
        DispatchQueue.main.async {
            self.beatPlayer = try? AVAudioPlayer(contentsOf: self.beatUrl)
            self.beatPlayer?.prepareToPlay()
            self.voicePlayer = try? AVAudioPlayer(contentsOf: self.voiceUrl)
            self.voicePlayer?.prepareToPlay()
            guard let player = self.voicePlayer else { return }
            
            self.duration = Int(player.duration)
        
//            self.voicePlayer?.volume = self.volumeVoice //0.65
            self.beatPlayer?.volume = self.volumeVoice
            player.volume = self.volumeVoice
            //                let strTime = (minutesTime < 10 ? "-0\(minutesTime)" : "\(minutesTime)") + (secTime < 10 ? ":0\(secTime)" : ":\(secTime)")
            //                self.labelTotal?.text = strTime
        }
    }
    
    ///
    @objc func runTime () {
        self.secs += 1
        if self.secs > 59 {
            self.secs = 0
            self.minutes += 1
        }
        
        let estimate = self.duration - (self.minutes * 60 + self.secs)
        if estimate  <= 0 {
            //TODO: - Stop
            self.timer?.invalidate()
            self.timer = nil
            ///
            self.secs   = 0
            self.minutes = 0
            self.buttonPlay?.isHidden = false
            ///
            self.voicePlayer?.currentTime = 0
            self.voicePlayer?.pause()
            self.beatPlayer?.currentTime = 0
            self.beatPlayer?.pause()
            self.avplayer?.seek(to: CMTime(seconds: 0, preferredTimescale: 1))
            self.avplayer?.pause()
        }
        
    }
    
    private func onProblemHappend() {
        self.showMessageOkWithCompletion(with: "Sorry", message: "Something went wrong, please re-record this song", completion: {
            if self.delegate != nil {
                self.delegate?.dismiss()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    //MARK: - Play
    func play() {
        ////
        self.beatPlayer?.play()
        self.voicePlayer?.play()
        if self.isHasVideo {
            self.avplayer?.play()
            print(self.avplayerLayer?.duration ?? "Can't get duration")
        }
        /// -> Start time calculate
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.runTime), userInfo: nil, repeats: true)
    }
    
    //MARK: - Resume
    func pause() {
        self.beatPlayer?.pause()
        self.voicePlayer?.pause()
        if self.isHasVideo {
            self.avplayer?.pause()
        }
        self.timer?.invalidate()
        self.timer = nil
    }
    
    private func mergeVideo (numberOfVideos: Int) {
//        Utility.shared.deleteFiles(filesUrl: self.mergeVideo)
        
        let urlImage = self.documentURL.appendingPathComponent("movie1.mov")
        if let image = Utility.shared.getThumbnailFrom(path: urlImage) {
            self.imageFirstFrame = image
        }
        
        
//        self.showLoading()
        MediaProcess.shared.mergeVideo(numberOfVideos: numberOfVideos) { [weak self] (success, link) in
            guard let `self` = self, let url = link else { return }
//            self.hideLoading()
            if success {
                DispatchQueue.main.async {
                    self.hideLoading()
                    print("Link nè: ", url)
                    self.configAVPlayer(url: url)
                    for iter in 1...numberOfVideos {
                        let url = self.documentURL.appendingPathComponent("movie\(iter).mov")
                        if FileManager.default.fileExists(atPath: url.absoluteString) {
                            try? FileManager.default.removeItem(at: url)
                        }
                    }
                }
            } else {
                print("Error " )
            }
        }
    }
    
    
    @IBAction func onSave (_ sender: Any?) {
        
        self.voicePlayer?.stop()
        self.beatPlayer?.stop()
        self.avplayer?.pause()
        
        self.voicePlayer = nil
        self.avplayer = nil
        self.beatPlayer = nil
        
        self.buttonPlay?.isUserInteractionEnabled = false
        self.viewControlAudio?.isUserInteractionEnabled = false
        if self.record == nil {
            self.onSaveToCoreData()
        } else {
            self.record?.update(localId: record?.localId ?? 0, beatVol: self.volumeBeat, voiceVol: self.volumeVoice)
            MessageManager.shared.show(title: "Congratulation", body: "Edit successfully")
            UIApplication.shared.isStatusBarHidden = false
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func onSaveToCoreData () {
        
        do {
            let name = resource?.name ?? ""
            let time = Date().timeIntervalSince1970.description
            
            let recordedFinder = self.documentURL.appendingPathComponent("recorded")
            try? FileManager.default.createDirectory(at: recordedFinder, withIntermediateDirectories: false, attributes: nil)
            
            let newVoiceUrl: URL = self.documentURL.appendingPathComponent("recorded/\(time)_\(name)_voice.caf")
            let newBeatUrl: URL = self.documentURL.appendingPathComponent("recorded/\(time)_\(name)_beat.mp3")
            let newVideoUrl: URL = self.documentURL.appendingPathComponent("recorded/\(time)_\(name)_video.mp4")
            let coverPath   : URL = self.documentURL.appendingPathComponent("recorded/\(time)_\(name)_image.jpg")
            
            try FileManager.default.moveItem(at: self.voiceUrl, to: newVoiceUrl)
            try FileManager.default.moveItem(at: self.beatUrl, to: newBeatUrl)
            
            if self.isHasVideo {
                try FileManager.default.moveItem(at: self.mergeVideo, to: newVideoUrl)
                if let image = self.imageFirstFrame {
                    if let data = UIImageJPEGRepresentation(image, 1) {
                        try? data.write(to: coverPath)
                    }
                }
            }
            
            var record: Record
            if self.isHasVideo {
                record = Record(beatPath: "recorded/\(time)_\(name)_beat.mp3", voicePath: "recorded/\(time)_\(name)_voice.caf", videoPath: "recorded/\(time)_\(name)_video.mp4", cover: "recorded/\(time)_\(name)_image.jpg", resourceId: self.resource?.id ?? "", resourceName: self.resource?.name ?? "", type: "video")
            } else {
                record = Record(beatPath: "recorded/\(time)_\(name)_beat.mp3", voicePath: "recorded/\(time)_\(name)_voice.caf", videoPath: nil, cover: resource?.thumbnail ?? "", resourceId: self.resource?.id ?? "", resourceName: self.resource?.name ?? "", type: "audio")
            }
            
            if record.save(beatVolume: self.volumeVoice, voiceVolume: self.volumeVoice) {
                MessageManager.shared.show(title: "Congratulation", body: "Your song has been saved!!!")
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: {
                        ///
                        self.delegate?.dismiss()
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                            NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.doneRecord), object: nil)
                        })
                    })
                }
            } else {
//                self.dismiss(animated: true, completion: nil)
                self.delegate?.dismiss()
            }
        } catch (let err) {
            print(err.localizedDescription)
        }
    }
    
    @IBAction func onPlayPausePress(_ sender: UIButton) {
        if self.voicePlayer?.isPlaying ?? false {
            ///
            self.buttonPlay?.isHidden = false
            self.pause()
        } else {
            self.buttonPlay?.isHidden = true
            self.play()
        }
    }
    
    @IBAction func onChnageMicVol(_ sender: UISlider) {
        self.volumeVoice = sender.value
    }
    
    @IBAction func onChangeBeatVolume(_ sender: UISlider) {
//        self.voicePlayer?.volume = sender.value
        self.volumeBeat = sender.value
    }
    
    @IBAction func onDismissPress(_ sender: Any?) {
        if self.record != nil {
            self.destroyObjects()
            return
        }
        self.showMessageWithAction(with: "Notice", message: "Your record will be delete if you quit this screen", title: "Delete", cancelTitle: "Cancel") { [weak self] in
            guard let `self` = self else { return }
            self.destroyObjects()
        }
    }
    
    private func destroyObjects () {
        self.beatPlayer = nil
        self.voicePlayer   = nil
        self.avplayerLayer?.player = nil
        
        if self.record != nil {
            try? FileManager.default.removeItem(at: self.beatUrl)
            try? FileManager.default.removeItem(at: self.voiceUrl)
            try? FileManager.default.removeItem(at: self.mergeVideo)
            UIApplication.shared.isStatusBarHidden = false
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        try? FileManager.default.removeItem(at: self.beatUrl)
        try? FileManager.default.removeItem(at: self.voiceUrl)
        try? FileManager.default.removeItem(at: self.mergeVideo)
        
        if self.delegate != nil {
            self.delegate?.dismiss()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
