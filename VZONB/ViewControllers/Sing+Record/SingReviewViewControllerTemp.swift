//
//  SingReviewViewController.swift
//  VZONB
//
//  Created by Phuc on 7/28/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import AVFoundation

class SingReviewViewControllerTemp: UIViewController {
    
    @IBOutlet weak var labelSongName    : UILabel?
    @IBOutlet weak var labelArtist      : UILabel?
    @IBOutlet weak var labelCurrent     : UILabel?
    @IBOutlet weak var labelTotal       : UILabel?
    @IBOutlet weak var viewCamera       : UIView?
    @IBOutlet weak var buttonPlay       : UIButton?
    @IBOutlet weak var sliderTime       : UISlider?
    @IBOutlet weak var sliderMicVolume  : UISlider?
    @IBOutlet weak var sliderBeatVolume : UISlider?
    
    @IBOutlet weak var buttonSavePrivate: UIButton?
    
    ///
    var documentURL         : URL { return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! }
    var voiceUrl            : URL { return self.documentURL.appendingPathComponent("recording.aac") }
    var beatUrl             : URL { return self.documentURL.appendingPathComponent("beat.mp3") }
    var mergeUrl            : URL { return self.documentURL.appendingPathComponent("merge.m4a") }
    var mergeVideo          : URL { return self.documentURL.appendingPathComponent("mergeVideo.mp4") }
    var completionVideoUrl  : URL { return self.documentURL.appendingPathComponent("completion.mp4") }
    ////
    var resource: Resource?
    var record  : Record?
    var isHasVideo          : Bool = false
    var numberOfVideos      : Int = 0
    fileprivate var isPlayed: Bool = false
    weak var delegate       : Dismiss?
    ////
    private var audioPlayer : AVAudioPlayer?
    fileprivate var mp3Player: AVAudioPlayer?
    private var avplayer: AVPlayer?
    private var avplayerLayer: AVPlayerLayer?
    ///
    // Timer
    fileprivate var secs        : Int    = 0
    fileprivate var minutes     : Int    = 0
    fileprivate var duration    : Int    = 0
    fileprivate var timer       : Timer? = nil
    
    private func configUIs () {
        ///
        self.sliderTime?.setThumbImage(UIImage(named: "icon_thumb_slider"), for: UIControlState.normal)
        self.sliderMicVolume?.setThumbImage(UIImage(named: "icon_thumb_slider"), for: UIControlState.normal)
        self.sliderBeatVolume?.setThumbImage(UIImage(named: "icon_thumb_slider"), for: UIControlState.normal)
        ///
        self.labelSongName?.text = resource?.name ?? ""
        self.labelArtist?.text   = resource?.singer ?? ""
        
        if !self.isHasVideo {
            self.viewCamera?.isHidden = true
        }
        
        //Re play older record
        if record != nil {
            self.buttonSavePrivate?.isHidden = true
        }
    }
    
    private func loadResource() {
        guard let record = self.record else { return }
        
        do {
            if FileManager.default.fileExists(atPath: self.beatUrl.absoluteString) {
                try FileManager.default.removeItem(at: self.beatUrl)
            }
            if FileManager.default.fileExists(atPath: self.voiceUrl.absoluteString) {
                try FileManager.default.removeItem(at: self.voiceUrl)
            }
            if FileManager.default.fileExists(atPath: self.mergeVideo.absoluteString) {
                try FileManager.default.removeItem(at: self.mergeVideo)
            }
            
            try FileManager.default.copyItem(at: self.documentURL.appendingPathComponent(record.beatPath), to: self.beatUrl)
            try FileManager.default.copyItem(at: self.documentURL.appendingPathComponent(record.voicePath), to: self.voiceUrl)
            self.isHasVideo = record.type == "video" ? true : false
            if self.isHasVideo && record.videoPath != nil {
                try FileManager.default.copyItem(at: self.documentURL.appendingPathComponent(record.videoPath!), to: self.mergeVideo)
            }
        } catch (let err) {
            //TODO: - Popup sorry
            print(err.localizedDescription)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadResource()
        self.configUIs()
        ///
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.defaultToSpeaker)
        
        ///
        if self.isHasVideo {
            self.configPlayer()
            //            self.configAVPlayer()
            if record == nil {
                self.mergeVideo(numberOfVideos: self.numberOfVideos)
            } else {
                self.configAVPlayer()
            }
        } else {
            self.viewCamera?.isHidden = true
        }
        
        self.prepareToPlay()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.avplayer = nil
        self.audioPlayer = nil
        self.mp3Player = nil
    }
    
    ///config video player
    private func configPlayer() {
        self.avplayerLayer = AVPlayerLayer()
        avplayerLayer?.backgroundColor = UIColor.white.cgColor
        avplayerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        self.viewCamera?.layer.addSublayer(self.avplayerLayer!)
        self.avplayerLayer?.frame = self.viewCamera?.bounds ?? CGRect.zero
        
    }
    /// -> Create player session
    private func configAVPlayer() {
        let playerItem = AVPlayerItem(url: self.mergeVideo)
        self.avplayer = AVPlayer(playerItem: playerItem)
        self.avplayer?.isMuted = false
        
        self.avplayerLayer?.player = avplayer
        //        self.avplayer?.play()
        
    }
    
    private func prepareToPlay() {
        ///
        DispatchQueue.global().async {
            self.audioPlayer = try? AVAudioPlayer(contentsOf: self.voiceUrl)
            self.audioPlayer?.prepareToPlay()
            //            guard let mp3Url = Bundle.main.url(forResource: self.song?.mp3Link ?? "NoiNayCoAnh", withExtension: ".mp3") else { return }
            //            guard let data = try? Data(contentsOf: mp3Url) else { return }
            //            self.mp3Player = try? AVAudioPlayer(data: data)
            self.mp3Player = try? AVAudioPlayer(contentsOf: self.beatUrl)
            guard let player = self.audioPlayer else { return }
            
            self.duration = Int(player.duration)
            let secTime = self.duration % 60
            let minutesTime = self.duration / 60
            
            DispatchQueue.main.async {
                self.sliderTime?.maximumValue = Float(player.duration)
                self.mp3Player?.prepareToPlay()
                self.mp3Player?.volume = 0.65
                self.audioPlayer?.prepareToPlay()
                self.audioPlayer?.volume = 1
                let strTime = (minutesTime < 10 ? "-0\(minutesTime)" : "\(minutesTime)") + (secTime < 10 ? ":0\(secTime)" : ":\(secTime)")
                self.labelTotal?.text = strTime
            }
        }
    }
    
    ///
    @objc func runTime () {
        self.secs += 1
        if self.secs > 59 {
            self.secs = 0
            self.minutes += 1
        }
        self.sliderTime?.value = Float(self.minutes * 60 + self.secs)
        
        let strTime  = (self.minutes < 10 ? "0\(self.minutes)" : "\(self.minutes)") + (self.secs < 10 ? ":0\(self.secs)" : ":\(self.secs)")
        self.labelCurrent?.text = strTime
        
        let estimate = self.duration - (self.minutes * 60 + self.secs)
        if estimate > 0 {
            let min = estimate / 60
            let sec = estimate % 60
            self.labelTotal?.text = "-" + (min < 10 ? "0\(min)" : "\(min)") + (sec < 10 ? ":0\(sec)" : ":\(sec)")
        } else {
            //TODO: - Stop
            self.timer?.invalidate()
            self.timer = nil
            
            self.mp3Player?.stop()
//            self.audioPlayer?.stop()
        }
        
    }
    
    private func onProblemHappend() {
        self.showMessageOkWithCompletion(with: "Sorry", message: "Something went wrong, please re-record this song", completion: {
            if self.delegate != nil {
                self.delegate?.dismiss()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    private func mergeVideo (numberOfVideos: Int) {
        
        let composition = AVMutableComposition()
        let compositionVideo = composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        
        for index in 1...numberOfVideos {
            let fileUrl = self.documentURL.appendingPathComponent("movie\(index).mov")
            
            let videoAsset = AVAsset(url: fileUrl)
            let videoTrack = videoAsset.tracks(withMediaType: AVMediaType.video).first!
            if videoTrack.isPlayable && (compositionVideo?.isPlayable)! {
                compositionVideo?.preferredTransform = videoTrack.preferredTransform
            }
            do {
                try compositionVideo?.insertTimeRange(CMTimeRange(start: kCMTimeZero, duration: videoAsset.duration), of: videoTrack, at: composition.duration)
            } catch {
                print("Can't add")
                self.onProblemHappend()
            }
        }
        
        Utility.shared.deleteFiles(filesUrl: self.mergeVideo)
        
        let exporter: AVAssetExportSession = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetMediumQuality)!
        exporter.outputURL = self.mergeVideo
        exporter.outputFileType = AVFileType.mp4
        
        
        exporter.exportAsynchronously { [weak self] in
            guard let `self` = self else { return }
            
            if exporter.status == .completed {
                //TODO: - Play
                DispatchQueue.main.async {
                    print("Link nè: ", self.mergeVideo.absoluteString)
                    self.configAVPlayer()
                    for iter in 1...numberOfVideos {
                        let url = self.documentURL.appendingPathComponent("movie\(iter).mov")
                        if FileManager.default.fileExists(atPath: url.absoluteString) {
                            try? FileManager.default.removeItem(at: url)
                        }
                    }
                }
            } else {
                print("Error")
                self.onProblemHappend()
            }
        }
    }
    //
    func mixAudio() {
        if FileManager.default.fileExists(atPath: self.mergeUrl.path) {
            Utility.shared.deleteFiles(filesUrl: self.mergeUrl)
        }
        self.mp3Player = nil
        self.audioPlayer = nil
        
        let composition = AVMutableComposition()
        let compositionMusic = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        let compositionVoice = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        //-=-//
        let musicAsset = AVAsset(url: self.beatUrl)
        let voiceAsset = AVAsset(url: self.voiceUrl)
        //-=-//
        let musicAssetTrack = musicAsset.tracks(withMediaType: AVMediaType.audio).first
        let voiceAssetTrack = voiceAsset.tracks(withMediaType: AVMediaType.audio).first
        //-=-//
        let exportAudioMix = AVMutableAudioMix()
        let inputParameters = NSMutableArray()
        //Voice setting zone
        let exportAudioMicParam = AVMutableAudioMixInputParameters(track: voiceAssetTrack)
        exportAudioMicParam.setVolume(self.sliderMicVolume?.value ?? 0, at: kCMTimeZero)
        exportAudioMicParam.trackID = (compositionVoice?.trackID)!
        inputParameters.add(exportAudioMicParam)
        //Beat Zone
        let exportBeatParam = AVMutableAudioMixInputParameters(track: musicAssetTrack)
        exportBeatParam.setVolume(self.sliderBeatVolume?.value ?? 0, at: kCMTimeZero)
        exportBeatParam.trackID = (compositionMusic?.trackID)!
        inputParameters.add(exportBeatParam)
        
        if let inputParams = inputParameters as? [AVAudioMixInputParameters] {
            exportAudioMix.inputParameters = inputParams
        }
        
        do {
            try compositionVoice?.insertTimeRange(CMTimeRange(start: kCMTimeZero, duration: voiceAsset.duration), of: voiceAssetTrack!, at: kCMTimeZero)
            try compositionMusic?.insertTimeRange(CMTimeRange(start: kCMTimeZero, duration: voiceAsset.duration), of: musicAssetTrack!, at: kCMTimeZero)
            
            let exporter: AVAssetExportSession = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetAppleM4A)!
            exporter.outputURL = self.mergeUrl
            exporter.outputFileType = AVFileType.m4a
            exporter.audioMix = exportAudioMix
            ////
            exporter.exportAsynchronously { [weak self] in
                guard let `self` = self else { return }
                
                if exporter.status == .completed {
                    //TODO: - Play
                    DispatchQueue.main.async { [weak self] in
                        guard let `self` = self else { return }
                        Utility.shared.deleteFiles(filesUrl: self.voiceUrl, self.beatUrl)
                        
                        if self.isHasVideo {
                            self.mergeAudioWithVideo()
                            return
                        }
//                        UploadThread.shared.upload(mediaUrl: self.mergeUrl, name: self.resource?.name ?? "", resourceId: self.resource?.id ?? "", record: self.record)
                        
                        self.showMessageOkWithCompletion(with: "Congratulation", message: "Your song is publishing, went done, we will notice you on your notification, thank you!", completion: { [weak self] in
                            if self?.delegate != nil {
                                self?.delegate?.dismiss()
                            } else {
                                self?.dismiss(animated: true, completion: nil)
                            }
                        })
                        
//                        self.showPopup(title: "Notice", completion: { [weak self] in
//                            guard let `self` = self else { return }
////                            self.delegate?.dismiss()
//                            }, description: "This song is uploading to server")
                        
                        
                        //                        self.mp3Player = AVAudioPlayer(contentsOf: self.mergeUrl)
                        //                        self.mp3Player?.volume = 1
                        //                        self.mp3Player?.prepareToPlay()
                        //                        self.mp3Player?.play()
                        
                    }
                } else {
                    print("Error")
                }
            }
        } catch {
            print("Can't merge file")
        }
    }
    
    func onDismissView () {
        self.dismiss(animated: true, completion: nil)
    }
    
    //
    private func mergeAudioWithVideo() {
        //Del file
        if FileManager.default.fileExists(atPath: self.completionVideoUrl.absoluteString) {
            Utility.shared.deleteFiles(filesUrl: self.completionVideoUrl)
        }
        self.avplayerLayer = nil
        self.avplayer = nil
        
        let composition = AVMutableComposition()
        let compositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        let compositionVideo = composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        // Audio First
        let audioAsset = AVAsset(url: self.mergeUrl)
        let videoAsset = AVAsset(url: self.mergeVideo)
        
        guard let audioTrack = audioAsset.tracks(withMediaType: AVMediaType.audio).first, let videoTrack = videoAsset.tracks(withMediaType: AVMediaType.video).first else { print("error cmnr"); return }
        if videoTrack.isPlayable && (compositionVideo?.isPlayable)! {
            compositionVideo?.preferredTransform = videoTrack.preferredTransform
        }
        
        let videoComp = AVMutableVideoComposition()
        videoComp.renderSize = CGSize(width: 640, height: 480)
        videoComp.frameDuration = CMTimeMake(1, 30)
        
        do {
            
            try compositionTrack?.insertTimeRange(CMTimeRange(start: kCMTimeZero, duration: videoAsset.duration), of: audioTrack, at: kCMTimeZero)
            try compositionVideo?.insertTimeRange(CMTimeRange(start: kCMTimeZero, duration: videoAsset.duration), of: videoTrack, at: kCMTimeZero)
            
            ///
            let exporter: AVAssetExportSession = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetMediumQuality)!
            exporter.outputURL = self.completionVideoUrl
            exporter.outputFileType = AVFileType.mp4
            //            exporter.videoComposition = videoComp
            //
            exporter.exportAsynchronously { [weak self] in
                guard let `self` = self else { return }
                
                if exporter.status == .completed {
                    DispatchQueue.main.async {
//                        UploadThread.shared.upload(mediaUrl: self.completionVideoUrl, name: self.resource?.name ?? "", resourceId: self.resource?.id ?? "", ext: "mp4", record: self.record)
//                        self.showPopup(title: "Notice", completion: { [weak self] in
//                            guard let `self` = self else { return }
//                            self.delegate?.dismiss()
//                            }, description: "This song is uploading to server")

                        self.showMessageOkWithCompletion(with: "Congratulation", message: "Your song is publishing, went done, we will notice you on your notification, thank you!", completion: { [weak self] in
                            if self?.delegate != nil {
                                self?.delegate?.dismiss()
                            } else {
                                self?.dismiss(animated: true, completion: nil)
                            }
                        })
                        
                        //                        let playerItem = AVPlayerItem(url: self.completionVideoUrl)
                        //                        self.avplayer = AVPlayer(playerItem: playerItem)
                        //                        self.avplayer?.isMuted = false
                        //
                        //                        self.avplayerLayer.player = self.avplayer
                        //                        self.avplayer?.play()
                    }
                } else {
                    print("Error")
                    self.onProblemHappend()
                }
            }
            
        } catch (let err) {
            print(err.localizedDescription)
        }
    }
    
    //MARK: - Play
    func play() {
        ///
        self.buttonPlay?.setImage(UIImage(named: "icon_pause"), for: UIControlState.normal)
        ////
        self.mp3Player?.play()
        self.audioPlayer?.play()
        if self.isHasVideo {
            self.avplayer?.play()
            print(self.avplayerLayer?.duration ?? "Can't get duration")
        }
        /// -> Start time calculate
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.runTime), userInfo: nil, repeats: true)
    }
    
    //MARK: - Resume
    func pause() {
        self.audioPlayer?.pause()
        self.mp3Player?.pause()
        if self.isHasVideo {
            self.avplayer?.pause()
        }
        self.buttonPlay?.setImage(UIImage(named: "ic_play"), for: UIControlState.normal)
        self.timer?.invalidate()
        self.timer = nil
    }
    
    //MARK: - Publish
    @IBAction func onPublish(_ sender: UIButton) {
        sender.isEnabled = false
        
        self.mp3Player = nil
        self.avplayer = nil
        self.avplayerLayer = nil
        
        MediaProcess.shared.mix(audios: [self.voiceUrl.absoluteString, self.beatUrl.absoluteString], volumes: [self.sliderMicVolume?.value ?? 0, self.sliderBeatVolume?.value ?? 0]) { [weak self] (success, link) in
            guard let `self` = self else { return }
            if success {
                guard let url = link else { return }
                if self.isHasVideo {
                    MediaProcess.shared.mix(audio: url.absoluteString, video: self.documentURL.appendingPathComponent(self.record?.videoPath ?? "").absoluteString, callback: { (success, url) in
                        if success {
                            guard let _ = url else { return }
//                            UploadThread.shared.upload(mediaUrl: url, name: self.resource?.name ?? "", resourceId: self.resource?.id ?? "", isVideo: true, description: "", ext: "mp4", record: self.record)
                            self.showPopup(title: "Congratulations", description: "This song is uploading to server") {
                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                                    self.onDismissView()
                                })
                            }
                        }
                    })
                    return
                }
                
//                UploadThread.shared.upload(mediaUrl: url, name: self.resource?.name ?? "", resourceId: self.resource?.id ?? "", isVideo: false, description: "", ext: "mp3", record: self.record)
                self.showPopup(title: "Congratulations", description: "This song is posting to server") {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: {
                        self.onDismissView()
                    })
                }
            }
        }
        
    }
    
    //MARK: - Save Private
    @IBAction func onSavePrivate(_ sender: UIButton) {
        
        sender.isEnabled = false
        
        // Reset
        self.mp3Player = nil
        self.audioPlayer = nil
        self.avplayer = nil
        self.avplayerLayer?.player = nil
        
        do {
            
            let name = resource?.name ?? ""
            let time = Date().timeIntervalSince1970.description
            
            let recordedFinder = self.documentURL.appendingPathComponent("recorded")
//            if !FileManager.default.fileExists(atPath: recordedFinder.absoluteString) {
            try? FileManager.default.createDirectory(at: recordedFinder, withIntermediateDirectories: false, attributes: nil)
//            }
            
            let newVoiceUrl: URL = self.documentURL.appendingPathComponent("recorded/\(time)_\(name)_voice.caf")
            let newBeatUrl: URL = self.documentURL.appendingPathComponent("recorded/\(time)_\(name)_beat.mp3")
            let newVideoUrl: URL = self.documentURL.appendingPathComponent("recorded/\(time)_\(name)_video.mp4")
            
            try FileManager.default.moveItem(at: self.voiceUrl, to: newVoiceUrl)
            try FileManager.default.moveItem(at: self.beatUrl, to: newBeatUrl)
            if self.isHasVideo {
                try FileManager.default.moveItem(at: self.mergeVideo, to: newVideoUrl)
            }
            
            let record = Record(beatPath: "recorded/\(time)_\(name)_beat.mp3", voicePath: "recorded/\(time)_\(name)_voice.caf", videoPath: self.isHasVideo ? "recorded/\(time)_\(name)_video.mp4" : nil, cover: resource?.thumbnail ?? "", resourceId: self.resource?.id ?? "", resourceName: self.resource?.name ?? "", type: self.isHasVideo ? "video" : "audio")
            
            if record.save(beatVolume: self.sliderBeatVolume?.value ?? 0, voiceVolume: self.sliderMicVolume?.value ?? 0) {
                //TODO: - Show Message for user
                print("Success")
                self.delegate?.dismiss()
            } else {
                print("Fail")
            }
            
            
        } catch (let err) {
            print(err.localizedDescription)
        }
        
        
        
    }
    
    //MARK: - Play Pause
    @IBAction func onPlayPausePress(_ sender: UIButton) {
        if self.mp3Player?.isPlaying ?? false {
            self.pause()
        } else {
            self.play()
        }
    }
    
    //MARK: - Slider Time change
    @IBAction func onSliderTimerChange(_ sender: UISlider) {
        if !sender.isTracking {
            let currentSecs: Int = Int(sender.value)
            self.secs       = currentSecs % 60
            self.minutes    = currentSecs / 60
            self.labelTotal?.time(totalSecs: currentSecs, isPositive: false)
            self.labelCurrent?.time(totalSecs: self.duration - currentSecs)
            self.audioPlayer?.currentTime = TimeInterval(currentSecs)
            self.mp3Player?.currentTime = TimeInterval(currentSecs)
            
            if self.isHasVideo {
//                self.avplayer?.currentItem?.asset.
                self.avplayer?.seek(to: CMTime(seconds: Double(currentSecs), preferredTimescale: 1))
                self.avplayer?.play()
            }
        }
    }
    
    //MARK: - Volume control
    @IBAction func onChnageMicVol(_ sender: UISlider) {
        self.audioPlayer?.volume = sender.value
    }
    
    @IBAction func onChangeBeatVolume(_ sender: UISlider) {
        self.mp3Player?.volume = sender.value
    }
    
    //MARK: - Dismiss
    @IBAction func onDismissPress(_ sender: Any?) {
        if self.record != nil {
            self.audioPlayer = nil
            self.mp3Player   = nil
            self.avplayerLayer?.player = nil
            self.avplayer    = nil
            
            try? FileManager.default.removeItem(at: self.beatUrl)
            try? FileManager.default.removeItem(at: self.voiceUrl)
            try? FileManager.default.removeItem(at: self.mergeVideo)
            
            self.dismiss(animated: true, completion: nil)
            return
        }
        self.showMessageWithAction(with: "Notice", message: "Your record will be delete if you quit this screen", title: "Delete", cancelTitle: "Cancel") { [weak self] in
            guard let `self` = self else { return }
            self.audioPlayer = nil
            self.mp3Player   = nil
            self.avplayerLayer?.player = nil
            self.avplayer    = nil
            
            try? FileManager.default.removeItem(at: self.beatUrl)
            try? FileManager.default.removeItem(at: self.voiceUrl)
            try? FileManager.default.removeItem(at: self.mergeVideo)
            
            if self.delegate != nil {
                self.delegate?.dismiss()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
}
