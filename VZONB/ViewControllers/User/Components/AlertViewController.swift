//
//  AlertViewController.swift
//  VZONB
//
//  Created by Phuc on 7/1/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {
    
    @IBOutlet weak var viewWrapper:         UIView!
    @IBOutlet weak var labelTitle:          UILabel!
    @IBOutlet weak var labelDecription:     UILabel!
    @IBOutlet weak var imageCover:          UIImageView!
    @IBOutlet weak var buttonOk:            UIButton!
    @IBOutlet weak var buttonCancel:        UIButton!
    
    var completion: (() -> Void)?
    private var ANIMATE_DURATION: TimeInterval = 0.2
    override func viewDidLoad() {
        super.viewDidLoad()        
//        self.view.layoutIfNeeded()
        self.viewWrapper.layer.cornerRadius = 4
        self.viewWrapper.layer.shadowColor = UIColor.black.cgColor
        self.viewWrapper.layer.shadowOffset = CGSize(width: 1, height: 3)
        self.viewWrapper.layer.shadowRadius = 10
        self.viewWrapper.layer.shadowOpacity = 1
        self.viewWrapper.clipsToBounds = true
        //
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onCancelPress)))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.viewWrapper.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5)
        self.view.alpha = 0
        self.viewWrapper.alpha = 0
        UIView.animate(withDuration: self.ANIMATE_DURATION, delay: 0, options: [UIViewAnimationOptions.curveLinear], animations: {
            self.viewWrapper.layer.transform = CATransform3DIdentity
            self.viewWrapper.alpha = 1
            self.view.alpha = 1
        }, completion: nil)
        
    }
    
    func setContents(title: String, description: String = "", imageName: String = "", okTitle: String = "Ok", cancelTitle: String = "") {
        self.view.layoutIfNeeded()
        self.labelTitle.text = title
        // Description
        if !description.isEmpty {
            self.labelDecription.text = description
        } else { self.labelDecription.isHidden = true }
        
        // Image Cover
        if !imageName.isEmpty {
            self.imageCover.image = UIImage(named: imageName)
        } else { self.imageCover.isHidden = true }
        
        // okTitle
        self.buttonOk.setTitle(okTitle, for: UIControlState.normal)
        self.buttonOk.addTarget(self, action: #selector(self.onOkPress), for: UIControlEvents.touchUpInside)
        
        // Cancel title
        if cancelTitle.isEmpty {
           self.buttonCancel.isHidden = true
        } else {
            self.buttonCancel.setTitle(cancelTitle, for: UIControlState.normal)
            self.buttonCancel.addTarget(self, action: #selector(self.onCancelPress), for: UIControlEvents.touchUpInside)
        }
    }
    
    @objc func onOkPress () {
        self.onCancelPress()
        if self.completion != nil {
            self.completion!()
            return
        }
    }
    
    @objc func onCancelPress () {
//        self.view.removeFromSuperview()
        UIView.animate(withDuration: self.ANIMATE_DURATION, delay: 0, options: [.curveLinear], animations: {
            self.viewWrapper.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5)
            self.viewWrapper.alpha = 0
            self.view.alpha = 0
        }) { (_) in
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.completion = nil
        self.view.gestureRecognizers = nil
        
    }
    
}
