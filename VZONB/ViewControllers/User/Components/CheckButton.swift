//
//  CheckButton.swift
//  VZONB
//
//  Created by Phuc on 6/30/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class CheckButton: UIButton {
    
    private var checkIcon: CALayer = CALayer()
    
    var isChecked: Bool = true {
        didSet {
            self.checkIcon.contents = nil
            self.checkIcon.contents = UIImage(named: isChecked ? "ic_stick_checked" : "ic_stick_notcheck")?.cgImage
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    private func setup() {
        self.layoutIfNeeded()
        self.checkIcon.frame = CGRect(x: 0, y: 2, width: 20, height: 20)
        self.checkIcon.contents = UIImage(named: isChecked ? "ic_stick_checked" : "ic_stick_notcheck")?.cgImage
        self.checkIcon.contentsGravity = kCAGravityResizeAspectFill
        self.checkIcon.masksToBounds = true
        self.layer.addSublayer(self.checkIcon)
    }
    
    
}
