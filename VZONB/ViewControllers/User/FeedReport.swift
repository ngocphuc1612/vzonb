//
//  FeedReport.swift
//  VZONB
//
//  Created by Phuc on 7/30/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation

class FeedReport: BaseModel {
//    "name": "Pornography",
//    "createdAt": "2017-07-04T17:09:02.593Z",
//    "updatedAt": "2017-07-04T17:09:02.593Z",
//    "id": "595bcbae29bec5680b4e3a73"
    
    var name    : String = ""
    var id      : String = ""
    
    required init(data: JSON) {
        self.name = data["name"].stringValue
        self.id   = data["id"].stringValue
    }
    
    
}
