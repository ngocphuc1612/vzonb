//
//  ForgotChangePassViewController.swift
//  VZONB
//
//  Created by PT on 10/16/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ForgotChangePassViewController: UIViewController {

    var token : String = ""
    @IBOutlet weak var textfieldPassword    : UITextField?
    @IBOutlet weak var textfieldConfirm     : UITextField?
    @IBOutlet weak var buttonDone           : UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textfieldPassword?.delegate = self
        self.textfieldConfirm?.delegate  = self
        //
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard)))
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    @IBAction func onChangePass(_ sender: UIButton?) {
        guard let pass = self.textfieldPassword?.text, let confirm = self.textfieldConfirm?.text else { return }
        var message : String = ""
        var valid   : Bool = true
        
        if pass.isEmpty {
            message = "Your new password can't be empty"
            valid = false
        } else if pass != confirm {
            message = "Your confirm isn't correctly"
            valid = false
        }
        
        if valid {
            sender?.isEnabled = false
            self.showLoading()
            AuthService.shared.forgotChangePassTo(pass, token: self.token, callback: { [weak self, sender = sender] (success) in
                if success {
                    MessageManager.shared.show(title: "Congratulation", body: "Password is change successfully", style: MessageManager.Style.success)
                } else {
                    MessageManager.shared.show(title: "Hey", body: "Something went from, please try again later", style: MessageManager.Style.error)
                }
                guard let `self` = self else { return }
                self.hideLoading()
                self.navigationController?.popToRootViewController(animated: true)
                sender?.isEnabled = true
            })
        } else {
            self.showMessageForUser(with: message)
        }
    }
    
    @IBAction func onBackPress () {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ForgotChangePassViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.textfieldPassword {
            self.textfieldConfirm?.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            self.onChangePass(self.buttonDone)
        }
        
        return false
    }
}
