//
//  ForgotEnterCodeViewController.swift
//  VZONB
//
//  Created by PT on 10/16/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ForgotEnterCodeViewController: UIViewController {

    @IBOutlet weak var textfieldInputToken: UITextField?
    @IBOutlet weak var buttonNext : UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textfieldInputToken?.delegate = self
        //
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard)))
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    @IBAction func onValidateToken(_ sender: UIButton?) {
        guard let token = self.textfieldInputToken?.text, !token.isEmpty else { return }
        sender?.isEnabled = false
        self.showLoading()
        AuthService.shared.forgotValidToken(token) { [weak self] (success) in
            guard let `self` = self else {return}
            self.hideLoading()
            if success {
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    if let vc = StoryBoard.User.viewController("ForgotChangePassViewController") as? ForgotChangePassViewController {
                        vc.token = token
                        self.textfieldInputToken?.text = ""
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            } else {
                self.showMessageForUser(with: "Your token is invalid")
            }
            sender?.isEnabled = true
        }
    }

    @IBAction func onBackPress () {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ForgotEnterCodeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.onValidateToken(self.buttonNext)
        return false
    }
}
