//
//  ForgotEnterEmailViewController.swift
//  VZONB
//
//  Created by PT on 10/16/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ForgotEnterEmailViewController: UIViewController {

    @IBOutlet weak var textfieldEmail   : UITextField?
    @IBOutlet weak var buttonNext   : UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textfieldEmail?.delegate = self
        //
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard)))
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    @IBAction func onEnterEmailPress (_ sender: UIButton?) {
        guard let email = self.textfieldEmail?.text else { return }
        guard !email.isEmpty && email.isEmail() else {
            self.showMessageForUser(with: "Your email is invalid")
            return
        }
        
        sender?.isEnabled = false
        self.showLoading()
        AuthService.shared.forgotSend(email: email) { [weak self, sender = sender] (success) in
            guard let `self` = self else {return}
            self.hideLoading()
            if success {
                if let vc = StoryBoard.User.viewController("ForgotEnterCodeViewController") as? ForgotEnterCodeViewController {
                    self.textfieldEmail?.text = ""
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                self.showMessageForUser(with: "Your email is invalid or not register yet")
            }
            sender?.isEnabled = true
        }
    }

    @IBAction func onBackPress () {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ForgotEnterEmailViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.onEnterEmailPress(nil)
        return false
    }
}
