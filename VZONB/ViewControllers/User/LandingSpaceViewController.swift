//
//  LandingSpaceViewController.swift
//  VZONB
//
//  Created by Phuc on 7/9/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import FBSDKLoginKit

final class LandingSpaceViewController: UIViewController {

    @IBOutlet weak var labelTerm    : UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let allText: String = (self.labelTerm?.text ?? "")
        let attributedText = NSMutableAttributedString(string: allText)
        attributedText.addAttributes([NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue], range: (allText as NSString).range(of: "Term of Service"))
        attributedText.addAttributes([NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue], range: (allText as NSString).range(of: "Privacy Policy"))
        self.labelTerm?.attributedText = attributedText
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        super.viewDidAppear(animated)
    }
    
    @IBAction func onTermPress(_ sender: Any?) {
        if let termVc = StoryBoard.User.viewController("ProfileWebTermViewController") as? ProfileWebTermViewController {
            termVc.link = Link.TermOfServiceUrl
            termVc.customTitle = "Terms of Service"
            self.navigationController?.pushViewController(termVc, animated: true)
        }
    }
    
    @IBAction func onPrivacyPress (_ sender: Any?) {
        if let termVc = StoryBoard.User.viewController("ProfileWebTermViewController") as? ProfileWebTermViewController {
            termVc.link = Link.PrivacyUrl
            termVc.customTitle = "Privacy Policy"
            self.navigationController?.pushViewController(termVc, animated: true)
        }
    }
    
    //Facebook request
    @IBAction func onFacebookLogin(_ sender: Any?) {
        FBSDKLoginManager().logIn(withReadPermissions: ["public_profile", "email"], from: self) { (results, error) in
            if error != nil {
                DispatchQueue.main.async {
                    MessageManager.shared.show(title: "Sorry", body: "Something went wrong! Please try again!!", style: MessageManager.Style.error)
                }
            } else {
                //Success get token from facebook
                guard let unwrapResult = results else { return }
                
                if !unwrapResult.isCancelled {
                    self.showLoading()
                    AuthService.shared.facebook(facebookId: unwrapResult.token.userID, userId: "", { [unowned self] (result) in
                        self.hideLoading()
                        if result.status == 200 {
                            DispatchQueue.main.async {
                                //Login success
                                Setting.shared.setToken(result.item.token)
                                Setting.shared.setUserId(result.item.user.id)
                                Setting.shared.setSave(true)
                                Setting.shared.setFbSign()
                                //Go to home Page
                                if let homeVc = StoryBoard.Home.viewController("HomeTabBar") as? UITabBarController {
                                    self.present(homeVc, animated: true, completion: nil)
                                }
                            }
                        } else {
                            MessageManager.shared.show(title: "Sorry", body: result.message, style: MessageManager.Style.error)
                        }
                    })
                    
                    
                } else {
                    print("Canceled")
                }
            }
        }
    }
}
