//
//  NotificationViewModel.swift
//  VZONB
//
//  Created by Phuc on 7/15/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class NotificationViewModel: NSObject {
    
    fileprivate var notifications: Array<NotiModel> = Array<NotiModel>()
    fileprivate var lastPage    : UInt8 = 1
    fileprivate var current     : UInt8 = 1
    
    var loadDataCompletion: (() -> Void)? = nil
    var onNavigation: ((_ noti: NotiModel) -> Void)? = nil
    var insertNew: ((_ index: Array<IndexPath>?) -> Void)? = nil
    var navigation: ((_ isHide: Bool) -> Void)? = nil
    
    func fetchNotifications () {
//        DispatchQueue.global().async {
            NotificationService.shared.gets(page: 1, { [weak self] (result) in
//                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    self.notifications  = result.items //.reversed()
                    self.current        = result.current
                    self.lastPage       = result.lastPage
                    self.loadDataCompletion?()
//                }
            })
//        }
    }
    
    func loadMore() {
        let next = self.current + 1
        if next <= self.lastPage {
            NotificationService.shared.gets(page: next, { (result) in
                if result.status == 200 {
                    self.current = result.current
                    let count = self.notifications.count
                    var indexs = Array<IndexPath>()
                    for iter in 0..<result.items.count {
                        indexs.append(IndexPath(row: count + iter, section: 0))
                    }
                    self.notifications.append(contentsOf: result.items) //.reversed())
                    
                    self.insertNew?(indexs)
                } else {
                    self.insertNew?(nil)
                }
            })
        } else {
            self.insertNew?(nil)
        }
    }
    
}

extension NotificationViewModel: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: NotificationCell = tableView.dequeueReusableCell(withIdentifier: NotificationCell.identifier, for: indexPath) as! NotificationCell
        cell.separatorInset = UIEdgeInsets.zero
        cell.binding(noti: self.notifications[indexPath.row])
        return cell
    }
}

extension NotificationViewModel: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.onNavigation?(self.notifications[indexPath.row])
        
        //Mark read
        if !self.notifications[indexPath.row].isViewed {
            NotificationService.shared.markRead(id: notifications[indexPath.row].id)
            notifications[indexPath.row].isViewed = true
//            let cell = tableView.cellForRow(at: indexPath) as? NotificationCell
//            cell?.backgroundColor = UIColor.white
        }
        
        tableView.reloadRows(at: [indexPath], with: .fade)
        
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if(velocity.y>0) {
//            self.navigationController?.setNavigationBarHidden(true, animated: true)
            self.navigation?(true)
        } else {
//            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigation?(false)
        }
    }
    
}
