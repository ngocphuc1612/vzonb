//
//  NotificationCell.swift
//  VZONB
//
//  Created by Phuc on 7/15/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var imageAvatar  : UIImageView!
    @IBOutlet weak var labelContent : UILabel!
    @IBOutlet weak var labelTime    : UILabel!
    
    func binding(noti: NotiModel) {
        
        self.imageAvatar.kf_setImage(url: noti.actor?.avatar ?? "")
        
        let attrBold: Dictionary<NSAttributedStringKey, Any> = [NSAttributedStringKey.font: UIFont(name: "SanFranciscoDisplay-Bold", size: 15)!, NSAttributedStringKey.foregroundColor: UIColor.black]
        let attributeStr = NSMutableAttributedString(string: noti.actor?.name ?? "", attributes: attrBold)
        
        if noti.action == "followed" {
            attributeStr.append(NSAttributedString(string: " follow you"))
        } else {
            if noti.action == "tagged" {
                attributeStr.append(NSAttributedString(string: " \(noti.action) on "))
                attributeStr.append(NSAttributedString(string: noti.record?.name ?? "", attributes: attrBold))
            } else if noti.action == "mentioned" {
                attributeStr.append(NSAttributedString(string: " \(noti.action) on a comment"))
            } else {
                attributeStr.append(NSAttributedString(string: " \(noti.action) "))
                attributeStr.append(NSAttributedString(string: noti.record?.name ?? "", attributes: attrBold))
            }
        }
        
        self.labelContent.attributedText = attributeStr
        self.labelTime.text = noti.createdAt
        if !noti.isViewed {
            self.backgroundColor = UIColor(rgb: 0xf4fbff)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.backgroundColor = UIColor.white
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}
