//
//  NotificationsViewController.swift
//  VZONB
//
//  Created by Phuc on 7/9/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class NotificationsViewController: DefaultHomeViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate let notificationVM: NotificationViewModel = NotificationViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if GlobalInfo.shared.userStatus.badgeNoti > 0 {
            GlobalInfo.shared.userStatus.badgeNoti = 0
            self.navigationController?.tabBarItem.badgeValue = nil
        }
        NotificationService.shared.reset()
        //MARK: - Delete app noti count
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        self.tableView.dataSource = self.notificationVM
        self.tableView.delegate = self.notificationVM
        
        self.tableView.tableFooterView = UIView()
        
        
        self.tableView.register(NotificationCell.nib, forCellReuseIdentifier: NotificationCell.identifier)
        
        self.tableView.showLoading()
        
        self.notificationVM.loadDataCompletion = { [weak self] in
            guard let `self` = self else { return }
            // Addition
            self.tableView.pullToRefreshView.stopAnimating()
            self.tableView.showsInfiniteScrolling = true
            //
            self.tableView.reloadData()
            self.tableView.hideLoading(completionHandler: { 
                self.tableView.emptyDataSetSource = self
                self.tableView.emptyDataSetDelegate = self
            })
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onUpdateNotiList), name: NSNotification.Name.init(NotiName.updateNoti), object: nil)
        
        self.notificationVM.onNavigation = { [weak self] (noti) in
            guard let `self` = self else { return }
//            if noti.action != "followed" && noti.action != "commented" && noti.action != "replied" && noti.record != nil {
//                DispatchQueue.main.async { [unowned self] in
//                    if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
//                        playVc.record = noti.record!
//                        self.navigationController?.pushViewController(playVc, animated: true)
//                    }
//                }
                
//            } else
        if noti.action == "voted" || noti.action == "tagged" {
                DispatchQueue.main.async {
                    if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                        playVc.record = noti.record
                        //
                        self.navigationController?.pushViewController(playVc, animated: true)
                    }
                }
            } else if noti.action == "followed" {
                DispatchQueue.main.async {
                    if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
                        profileVc.isMe = false
                        profileVc.id   = noti.actor?.id ?? ""
                        self.navigationController?.pushViewController(profileVc, animated: true)
                    }
                }
            } else if noti.action == "commented" || noti.action == "mentioned" {
                if let detailVC = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                    detailVC.record = noti.record
                    detailVC.commentParrent = noti.comment
                    detailVC.isTransferToComment = true
                    self.navigationController?.pushViewController(detailVC, animated: true)
                }
            } else if noti.action == "replied" {
                if let detailVC = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                    detailVC.record = noti.record
                    detailVC.isTransferToComment = true
                    detailVC.commentParrent = noti.comment
                    self.navigationController?.pushViewController(detailVC, animated: true)
                }
            }
        }
        //Load more
        self.addInfiniteScroll()
        self.notificationVM.insertNew = { [weak self] (indexs) in
            guard let `self` = self else { return }
            guard let indexs = indexs else {
                self.tableView.showsInfiniteScrolling = false
                self.tableView.infiniteScrollingView.stopAnimating()
                return
            }
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: indexs, with: .automatic)
            self.tableView.endUpdates()
            self.tableView.infiniteScrollingView.stopAnimating()
        }
        //Pull to refresh
        self.addPullToRefresh()
        
        self.notificationVM.navigation = { [weak self] (_ isHide: Bool) in
            guard let `self` = self else { return }
            self.navigationController?.setNavigationBarHidden(isHide, animated: true)
        }
    }
    
    private func addInfiniteScroll() {
        self.tableView.addInfiniteScrolling { [weak self] in
            guard let `self` = self else { return }
            self.notificationVM.loadMore()
        }
    }
    
    private func addPullToRefresh () {
        self.tableView.addPullToRefresh { [weak self] in
            guard let `self` = self else { return }
            self.notificationVM.fetchNotifications()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
//        UIApplication.shared.statusBarStyle = .lightContent
        ///
        self.notificationVM.fetchNotifications()
        GlobalInfo.shared.userStatus.badgeNoti = 0
        self.navigationController?.tabBarItem.badgeValue = nil
        
    }
    
    @objc func onUpdateNotiList () {
        self.notificationVM.fetchNotifications()
        GlobalInfo.shared.userStatus.badgeNoti += 1
        self.navigationController?.tabBarItem.badgeValue = GlobalInfo.shared.userStatus.badgeNoti.description
    }
    
}

extension NotificationsViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "ic_history")!
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "You don't have any notification", attributes: [NSAttributedStringKey.font: Font.emptyData])
    }
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
}
