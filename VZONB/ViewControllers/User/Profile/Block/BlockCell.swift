//
//  BlockCell.swift
//  VZONB
//
//  Created by PT on 10/6/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

protocol BlockCellDelegate: class {
    func userDidUnblock(_ cell: BlockCell)
}

class BlockCell: UITableViewCell {

    @IBOutlet weak var imageAvatar  : CircleImage?
    @IBOutlet weak var labelName    : UILabel?
    
    weak var delegate : BlockCellDelegate? = nil
    var index: Int = 0
    var id: String = ""
    @IBAction func onUnblockPress(_ sender: Any?) {
        self.delegate?.userDidUnblock(self)
        if !self.id.isEmpty {
            print("UnBlocked")
            UserService.shared.unblock(userId: self.id)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    static var identifier: String { return String(describing: self) }
    static var nib: UINib { return UINib(nibName: self.identifier, bundle: nil) } 
}
