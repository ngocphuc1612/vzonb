//
//  BlockUsersViewController.swift
//  VZONB
//
//  Created by PT on 10/6/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class BlockUsersViewController: DefaultChildViewController {

    lazy var tableView: UITableView = {
        let tb = UITableView()
        tb.register(BlockCell.nib, forCellReuseIdentifier: BlockCell.identifier)
        tb.backgroundColor = UIColor.white
//        tb.separatorInset = UIEdgeInsets.zero
        tb.separatorStyle = .none
        tb.delegate = self
        tb.dataSource = self
        return tb
    }()
    
    fileprivate var blockedUsers = Array<ShortUser>()
    fileprivate var lastPage    : UInt8 = 1
    fileprivate var current     : UInt8 = 1
    
    override func viewDidLoad() {
        
        DispatchQueue.global().async { [weak self] in
            guard let `self` = self else { return }
            UserService.shared.blockedUsers(1, { (result) in
                self.blockedUsers = result.items
                DispatchQueue.main.async {
                    self.tableView.hideLoading(completionHandler: {
                        self.tableView.emptyDataSetDelegate = self
                        self.tableView.emptyDataSetSource   = self
                    })
                    self.tableView.reloadData()
                }
            })
        }
        super.viewDidLoad()
        
        self.tableView.showLoading()
        self.addConstraints()
        self.view.backgroundColor = UIColor.white
        self.title = "Block Users"
    }
}

extension BlockUsersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.blockedUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BlockCell.identifier, for: indexPath) as! BlockCell
        cell.labelName?.text = self.blockedUsers[indexPath.row].fullname
        cell.imageAvatar?.kf_setImage(url: self.blockedUsers[indexPath.row].avatar)
        cell.index = indexPath.row
        cell.id     = self.blockedUsers[indexPath.row].id
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension BlockUsersViewController: BlockCellDelegate {
    func userDidUnblock(_ cell: BlockCell) {
        if cell.index >= self.blockedUsers.count { return }
        self.blockedUsers.remove(at: cell.index)
        self.tableView.reloadData()
    }
}

extension BlockUsersViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "ic_history")!
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "List blocked is empty", attributes: [NSAttributedStringKey.font: Font.emptyData])
    }
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

extension BlockUsersViewController {
    fileprivate func addConstraints () {
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topLayoutGuide.snp.bottom)
            make.left.right.bottom.equalTo(self.view)
        }
    }
}
