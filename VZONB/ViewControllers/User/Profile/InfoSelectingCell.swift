//
//  InfoSelectingCell.swift
//  VZONB
//
//  Created by PT on 8/14/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class InfoSelectingCell: UITableViewCell {
    
    @IBOutlet weak var viewCircle   : UIView?
    @IBOutlet weak var labelName    : UILabel?
    
    private var calayer:  CALayer = CALayer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layoutIfNeeded()
        self.viewCircle?.layer.cornerRadius = 7.5
        self.viewCircle?.layer.borderColor = UIColor(rgb: 0x0680bf).cgColor
        self.viewCircle?.layer.borderWidth = 1
        
        self.calayer.frame = CGRect(x: 2, y: 2, width: 11, height: 11)
        self.calayer.cornerRadius = 5.5
        self.calayer.masksToBounds  = true
        self.calayer.backgroundColor = UIColor(rgb: 0x0680bf).cgColor
        
    }
    
    func selectedRow () {
        self.viewCircle?.layer.addSublayer(self.calayer)
    }
    
    func unselected() {
        self.calayer.removeFromSuperlayer()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.unselected()
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
}
