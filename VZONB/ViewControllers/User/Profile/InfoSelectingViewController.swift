//
//  InfoSelectingViewController.swift
//  VZONB
//
//  Created by PT on 8/14/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

protocol OnProfileUpdateInfo: class {
    func update(type: InfoSelectingViewController.InfoType, value: String, key: Any)
}

class InfoSelectingViewController: UIViewController {

    enum InfoType {
        case country, state, month, day
    }

    @IBOutlet weak var tableView    : UITableView?
    
    weak var delegate: OnProfileUpdateInfo?
    
    fileprivate var states: Array<Country> = Array<Country>()
    var infoType: InfoType = .month
    var selectingMonth  : Int = 0
    var selectingIndex: Int = 0
    var selectingState: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Pick one"
        self.tableView?.register(InfoSelectingCell.nib, forCellReuseIdentifier: InfoSelectingCell.identifier)
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        
        if self.infoType == .state {
            DispatchQueue.global().async {
                UserService.shared.stateOf(country: "us", callback: { (result) in
                    self.states = result.items
                    DispatchQueue.main.async { [weak self] in
                        guard let `self` = self else { return }
                        self.selectingIndex = result.items.index(where: { $0.key == self.selectingState }) ?? 0
                        self.tableView?.reloadData()
                        self.tableView?.scrollToRow(at: IndexPath(row: self.selectingIndex, section: 0), at: .middle, animated: false)
                    }
                })
            }
        } else {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) { [weak self] in
                guard let `self` = self else { return }
                self.tableView?.scrollToRow(at: IndexPath(row: self.selectingIndex, section: 0), at: .middle, animated: false)
            }
        }
    }
    
    @IBAction func onDismiss(_ sender: Any?) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension InfoSelectingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.infoType {
        case .month:
            return 12 //self.months.count
        case .day:
            switch self.selectingMonth {
            case 2:
                return 29
            case 1, 3, 5, 7, 8, 10, 12:
                return 31
            default:
                return 30
            }
        case .state: return self.states.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: InfoSelectingCell.identifier, for: indexPath) as! InfoSelectingCell
        switch self.infoType {
        case .month:
            cell.labelName?.text = Utility.shared.intToMonth(intMonth: indexPath.row + 1) //self.months[indexPath.row + 1]
        case .day:
            cell.labelName?.text = (indexPath.row + 1).description
        case .state:
            cell.labelName?.text = self.states[indexPath.row].name
        default:
            break
        }
        
        if self.selectingIndex == indexPath.row {
            cell.selectedRow()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.selectingIndex != indexPath.row {
            let oldCell = tableView.cellForRow(at: IndexPath(row: self.selectingIndex, section: 0)) as? InfoSelectingCell
            oldCell?.unselected()
            let cell = tableView.cellForRow(at: indexPath) as? InfoSelectingCell
            self.selectingIndex = indexPath.row
            cell?.selectedRow()
            
            switch self.infoType {
            case .month:
                self.delegate?.update(type: .month, value: Utility.shared.intToMonth(intMonth: indexPath.row + 1), key: indexPath.row + 1)
            case .day:
                self.delegate?.update(type: .day, value: (indexPath.row + 1).description, key: self.selectingMonth)
            case .state:
                self.delegate?.update(type: .state, value: self.states[indexPath.row].name, key: self.states[indexPath.row].key)
            default:
                return
            }
            
        }
    }
}
