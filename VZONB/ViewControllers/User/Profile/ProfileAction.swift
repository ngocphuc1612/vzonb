//
//  ProfileAction.swift
//  VZONB
//
//  Created by Phuc on 7/27/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import Foundation

enum ProfileActionType {
    case saved, term, feedback, block, policy, changePass, signout
    case analytics, setting, editProfile, back, follow, follower, following
}

class ProfileAction {
    var type: ProfileActionType
    var name: String
    var icon: String
    
    init(type: ProfileActionType, name: String, icon: String) {
        self.type = type
        self.name = name
        self.icon = icon
    }
}
