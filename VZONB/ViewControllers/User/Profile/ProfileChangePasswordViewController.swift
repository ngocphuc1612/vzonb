//
//  ProfileChangePasswordViewController.swift
//  VZONB
//
//  Created by Phuc on 7/27/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ProfileChangePasswordViewController: BlackChildViewController {

    @IBOutlet weak var textfiledOldPassword : UITextField?
    @IBOutlet weak var textfieldNewPassword : UITextField?
    @IBOutlet weak var textfieldConfirmPass : UITextField?
    @IBOutlet weak var buttonChangePass     : UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Change Password"
        
        self.textfiledOldPassword?.delegate = self
        self.textfieldNewPassword?.delegate = self
        self.textfieldConfirmPass?.delegate = self
        
    }
    
    @IBAction func onChangePass(_ sender: UIButton?) {
        
        let oldPass: String = self.textfiledOldPassword?.trimText() ?? ""
        let newPass: String = self.textfieldNewPassword?.trimText() ?? ""
        let confirm: String = self.textfieldConfirmPass?.trimText() ?? ""
        
        var valid: Bool = true
        var message: String = ""
        
        if oldPass.isEmpty {
            valid = false
            message = "Please fill out your current password"
        } else if newPass.isEmpty {
            valid = false
            message = "Please fill out new password"
        } else if newPass != confirm {
            valid = false
            message = "Your confirm password not similar"
        } else if newPass.count < 6 {
            valid = false
            message = "Password must be at least 6 characters"
        } else if oldPass == newPass {
            valid = false
            message = "You must enter different password for change"
        }
        
        if valid {
            sender?.isEnabled = false
            UserService.shared.changePass(oldPass: oldPass, newPass: newPass, callback: { (result) in
                if result.status == 200 {
                    MessageManager.shared.show(title: "Congratulation", body: "Your password have been change successfully")
                    self.navigationController?.popViewController(animated: true)
                } else {
                    MessageManager.shared.show(title: "Sorry", body: result.message, style: .error)
                }
                sender?.isEnabled = true
            })
        } else {
            MessageManager.shared.show(title: "Sorry", body: message, style: .error)
        }
    }
}

extension ProfileChangePasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let next = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            next.becomeFirstResponder()
        } else if textField == self.textfieldConfirmPass {
            self.onChangePass(self.buttonChangePass)
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}
