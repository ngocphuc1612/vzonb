//
//  ProfileEditInfoViewController.swift
//  VZONB
//
//  Created by Phuc on 7/27/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import STPopup
//import KMPlaceholderTextView

class ProfileEditInfoViewController: UIViewController {

    @IBOutlet weak var imageAvatar          : CircleImage?
    @IBOutlet weak var textfieldFirstname   : UITextField?
    @IBOutlet weak var textfieldLastname    : UITextField?
    @IBOutlet weak var textfieldNickname    : UITextField?
    @IBOutlet weak var textviewDescription  : UITextView?
    @IBOutlet weak var textfieldCity        : UITextField?
    @IBOutlet weak var textfieldState       : UITextField?
    @IBOutlet weak var textfieldCountry     : UITextField?
    @IBOutlet weak var textfieldDayofBirth  : UITextField?
    @IBOutlet weak var textfieldMonthofBirth: UITextField?
    
    fileprivate var profile     : User?
    
    // Control state:
    fileprivate var wasPickImage: Bool = false
    fileprivate var imageResized: UIImage?
    var imageAvatarBefore: UIImage? = nil
    
    func binding(profile: User) {
        if self.imageAvatarBefore != nil {
            self.imageAvatar?.image = imageAvatarBefore!
        } else {
            self.imageAvatar?.kf_setImage(url: profile.avatarUrl)
        }
        self.textfieldFirstname?.text = profile.firstname
        self.textfieldLastname?.text  = profile.lastname
        self.textfieldNickname?.text  = profile.nickname
        self.textviewDescription?.text = profile.description
        self.textfieldCity?.text      = profile.address?.city ?? ""
        self.textfieldState?.text     = profile.address?.state ?? ""
//        self.textfieldCountry?.text   = profile.address?.country ?? ""
//        self.selectingState = profile.address?.state.key ?? ""
        ///
        if let dob = profile.dob {
            if dob.day == 0 {
                self.textfieldDayofBirth?.text = 1.description
                self.selectingDay = 1
            } else {
                self.selectingDay = dob.day
                self.textfieldDayofBirth?.text = dob.day.description
            }
            
            if dob.month == 0 {
                self.textfieldMonthofBirth?.text = "January"
                self.selectingMonth = 1
            } else {
                self.selectingMonth = dob.month
                self.textfieldMonthofBirth?.text = Utility.shared.intToMonth(intMonth: dob.month)
            }
        }
    }
    
    private func configNavigation () {
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: Font.header]
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_back_black"), style: UIBarButtonItemStyle.done, target: self, action: #selector(self.onDismiss))
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    @objc func onDismiss () {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        self.onGetProfile()
        self.configNavigation()
        super.viewDidLoad()
        self.title = "Edit Profile"
        self.textfieldNickname?.delegate = self
        ///
        self.textviewDescription?.layer.cornerRadius = 4
        self.textviewDescription?.layer.borderColor = UIColor(rgb: 0xCCCCCC).cgColor
        self.textviewDescription?.layer.borderWidth = 0.5
        self.textviewDescription?.clipsToBounds = true
//        self.textviewDescription?.delegate = self
        self.textviewDescription?.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.textviewDescription?.contentSize = CGSize(width: self.view.bounds.width - 52, height: 70)
    }
    
    fileprivate var selectingMonth: Int = 1
    fileprivate var selectingDay  : Int = 1
    fileprivate var selectingState: String = "us"
    
    private func onGetProfile() {
        DispatchQueue.global().async {
            UserService.shared.getInfo(id: Setting.shared.getUserId(), { (result) in
                if result.status == 200 {
                    DispatchQueue.main.async { [weak self] in
                        guard let `self` = self else { return }
                        self.profile = result.item
                        self.binding(profile: result.item)
                    }
                }
            })
        }
    }
    
    @IBAction func onPickImage(_ sender: UIButton) {
        
        let imgPicker = UIImagePickerController()
        imgPicker.delegate = self
        
        let alertChooseMethod:UIAlertController = UIAlertController(title: "Pick your avatar from", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let actionChoosePhoto:UIAlertAction = UIAlertAction(title: "Library", style: UIAlertActionStyle.default) { (_) in
            imgPicker.allowsEditing = false
            imgPicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            
            self.present(imgPicker, animated: true, completion: nil)
        }
        let actionChooseCamera: UIAlertAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                
                imgPicker.sourceType = UIImagePickerControllerSourceType.camera
                imgPicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureMode.photo
                
                self.present(imgPicker, animated: true, completion: nil)
            }
        }
        
        let alertCancel: UIAlertAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        
        alertChooseMethod.addAction(actionChoosePhoto)
        alertChooseMethod.addAction(actionChooseCamera)
        alertChooseMethod.addAction(alertCancel)
        
        self.present(alertChooseMethod, animated: true, completion: nil)
    }
    
    // Tag: 1 State - 2 Country - 3 Day - 4 Month
    @IBAction func onPickDatePress (_ sender: UIButton) {
        
        guard let vc = StoryBoard.User.viewController("InfoSelectingViewController") as? InfoSelectingViewController else { return }
        let popup = STPopupController(rootViewController: vc)
        vc.contentSizeInPopup = CGSize(width: 200, height: 300)
        vc.landscapeContentSizeInPopup = CGSize(width: 300, height: 200)
        vc.delegate = self
        ////
        switch sender.tag {
        case 1:
            vc.infoType = .state
            vc.selectingState = self.selectingState
        case 2:
            return
        case 3:
            vc.infoType = .day
            vc.selectingIndex = self.selectingDay - 1
            vc.selectingMonth = self.selectingMonth
        case 4:
            vc.infoType = .month
            vc.selectingIndex = self.selectingMonth - 1
        default:
            break
        }
        
        popup.present(in: self)
    }
    
    @IBAction func onSaveUpdateProfile(_ sender: UIButton) {
        
        var valid: Bool = true
        var message: String = ""
        
        let firstname: String = self.textfieldFirstname?.trimText() ?? ""
        let lastname: String  = self.textfieldLastname?.trimText() ?? ""
        let nickname: String  = self.textfieldNickname?.trimText() ?? ""
        let description: String = self.textviewDescription?.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) ?? ""
        let city: String = self.textfieldCity?.trimText() ?? ""
        let province: String = self.textfieldState?.trimText() ?? ""
        let country : String = self.textfieldCountry?.trimText() ?? ""
        
        if firstname.isEmpty {
            message = "Firstname can't be empty"
            valid = false
        } else if lastname.isEmpty {
            message = "Lastname can't be empty"
            valid = false
        } else if nickname.isEmpty {
            message = "Nickname can't be empty"
            valid = false
        } else if city.isEmpty {
            message = "City can't be empty"
            valid = false
        } else if province.isEmpty {
            message = "State can't be empty"
            valid = false
        }
        
        if valid {
            sender.isUserInteractionEnabled = false
            let address: VZAddress = VZAddress(state: province, country: country, city: city)
            let dob    : VZDOB     = VZDOB(month: self.selectingMonth, day: self.selectingDay)
            
            var info: Dictionary<String, Any> = [
                "firstName": firstname,
                "lastName" : lastname,
                "description": description,
                "dob": dob.json(),
                "address": address.json()
            ]
            
            if nickname != (self.profile?.nickname ?? "") {
                info["nickName"] = nickname
            }
            if self.wasPickImage, let image = self.imageAvatar?.image {
                // Decrease size of image
                var resizeImage: UIImage
                if max(image.size.width, image.size.height) > 1000 {
                    resizeImage = image.resized(withPercentage: 0.5) ?? image
                } else {
                    resizeImage = image
                }
                self.imageResized = resizeImage
                AmazoneService.shared.uploadAvatar(image: resizeImage, callback: { (success, link) in
                    if success {
                        info["avatar"] = link ?? ""
                        print(link ?? "Can't get link")
                    }
                    self.updateProfile(info: info, isUpdateAvatar: true) { [weak sender = sender] in
                        sender?.isUserInteractionEnabled = true
                    }
                })
            } else {
                self.updateProfile(info: info) { [weak sender = sender] in
                    sender?.isUserInteractionEnabled = true
                }
            }
            
        } else {
            MessageManager.shared.show(title: "Sorry", body: message, style: .error)
        }
    }
    
    func updateProfile(info: Dictionary<String, Any>, isUpdateAvatar: Bool = false, _ completionHandler: (() -> Void)? = nil) {
        UserService.shared.update(info: info, callback: { (result) in
            if result.status == 200 {
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    MessageManager.shared.show(title: "Congratulation", body: "Your profile have been updated \((isUpdateAvatar ? "Your avatar is uploading" : ""))")
                    // Update local user infor
                    GlobalInfo.shared.info?.avatarUrl = result.item.avatarUrl
                    GlobalInfo.shared.info?.fullname  = result.item.fullname
                    NotificationCenter.default.post(name: NSNotification.Name.init(NotiName.updateProfile), object: self.imageResized)
                    self.navigationController?.popViewController(animated: true)
                }
            } else {
                completionHandler?()
                MessageManager.shared.show(title: "Sorry", body: result.message, style: .error)
            }
        })
    }
    
}

////MARK: - Profile Edit
//extension ProfileEditInfoViewController: UITextViewDelegate {
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//        let numberOfChars = newText.characters.count // for Swift use count(newText)
//        return numberOfChars <= 120;
//    }
//}

extension ProfileEditInfoViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldNickname {
            if textField.text!.contains(" ") {
                self.textfieldNickname?.text = textField.text?.replacingOccurrences(of: " ", with: "")
                return false
            }
            return true
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextText = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextText.becomeFirstResponder()
        } else if let nextText = textField.superview?.viewWithTag(textField.tag + 1) as? UITextView {
            nextText.becomeFirstResponder()
        } else if textField.tag > 6 { textField.resignFirstResponder() }
        return false
    }
    
}

//MARK: - Pick an image
extension ProfileEditInfoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imageAvatar?.image = image
            self.wasPickImage = true
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension ProfileEditInfoViewController: OnProfileUpdateInfo {
    func update(type: InfoSelectingViewController.InfoType, value: String, key: Any) {
        switch type {
        case .month:
            self.textfieldMonthofBirth?.text = value
            self.selectingMonth = key as? Int ?? 0
            print("Month => \(value) => \(key)")
        case .day:
            self.textfieldDayofBirth?.text = value
            self.selectingDay = Int(value) ?? 0
            print("day => \(value) => \(key)")
        case .state:
            self.textfieldState?.text = value
            self.selectingState = key as? String ?? ""
        default: break
        }
    }
}
