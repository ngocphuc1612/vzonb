//
//  ProfileFollowersViewController.swift
//  VZONB
//
//  Created by PT on 9/15/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ProfileFollowersViewController: BlackChildViewController {

    @IBOutlet weak var tableView: UITableView?
    
    var id: String = ""
    var isFollower: Bool = true
    fileprivate var users: Array<ShortUser> = Array<ShortUser>()
    fileprivate var lastPage    : UInt8 = 1
    fileprivate var current     : UInt8 = 1
    fileprivate var isFollowing : Bool = false
    
    override func viewDidLoad() {
        
        DispatchQueue.global().async {
            UserService.shared.followers(id: self.id, isFollower: self.isFollower, callback: { (result) in
                if result.status == 200 {
                    self.users = result.items
                    DispatchQueue.main.async { [weak self] in
                        guard let `self` = self else { return }
                        self.tableView?.reloadData()
                    }
                }
            })
        }
        
        super.viewDidLoad()

        self.title = self.isFollower ? "Followers" : "Followings"
        //Config TableView
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.tableView?.register(UINib(nibName: "SearchUserCell", bundle: nil), forCellReuseIdentifier: "searchUserCell")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
}

//MARK: - TableView
extension ProfileFollowersViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SearchUserCell = tableView.dequeueReusableCell(withIdentifier: "searchUserCell", for: indexPath) as! SearchUserCell
        cell.user = self.users[indexPath.row]
        cell.index = indexPath.row
        cell.binding(user: self.users[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 //UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
            profileVc.isMe = false
            profileVc.id   = self.users[indexPath.row].id
            self.navigationController?.pushViewController(profileVc, animated: true)
        }
    }
}

extension ProfileFollowersViewController: SearchUserCellDelegate {
    func didFollowUser(at cell: SearchUserCell, index: Int) {
        guard let user = cell.user else { return }
        DispatchQueue.main.async {
            self.users[index].followed = !user.followed
            self.tableView?.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
        }
        if !self.isFollowing {
            self.isFollowing = true
            UserService.shared.followUserBy(id: user.id, follow: !user.followed) { (result) in
                print(result)
                self.isFollowing = false
            }
        }
    }
}
