//
//  ProfileMeHeader.swift
//  VZONB
//
//  Created by Phuc on 7/21/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ProfileMeHeader: UIView {
    
    weak var delegate: OnProfileAction? = nil
    @IBOutlet weak var buttonSetting    : UIButton?
    @IBOutlet weak var buttonAnalytics  : UIButton?
    @IBOutlet weak var buttonBack       : UIButton?
    @IBOutlet weak var labelFullname    : UILabel?
    @IBOutlet weak var labelNickname    : UILabel?
    @IBOutlet weak var labelDescription : UILabel?
    @IBOutlet weak var labelFollowing   : UILabel?
    @IBOutlet weak var labelFollower    : UILabel?
    @IBOutlet weak var labelDob         : UILabel?
    @IBOutlet weak var labelAddress     : UILabel?
    @IBOutlet weak var imageAvatar      : CircleImage?
    
    private var isMe: Bool = true
    private var isFollowing = false
    
    @IBAction func onSettingPress(_ sender: Any?) {
        if self.isMe {
            let action = ProfileAction(type: ProfileActionType.setting, name: "", icon: "")
            self.delegate?.onActionPress(action: action)
        } else {
            if self.buttonSetting?.imageView?.image == UIImage(named: "ic_user_followed") {
                self.buttonSetting?.setImage(UIImage(named: "icon_profile_add"), for: UIControlState.normal)
                self.buttonAnalytics?.isHidden = true
            } else {
                self.buttonSetting?.setImage(UIImage(named: "ic_user_followed"), for: UIControlState.normal)
                self.buttonAnalytics?.isHidden = false
            }
            self.delegate?.onActionPress(action: ProfileAction(type: ProfileActionType.follow, name: "", icon: ""))
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageAvatar?.layer.shadow()
        self.labelFollower?.isUserInteractionEnabled = true
        self.labelFollower?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowFollower)))
        self.labelFollowing?.isUserInteractionEnabled = true
        self.labelFollowing?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowFollowing)))
    }
    
    @objc func onShowFollower () {
        let action = ProfileAction(type: ProfileActionType.follower, name: "", icon: "")
        self.delegate?.onActionPress(action: action)
    }
    
    @objc func onShowFollowing () {
        let action = ProfileAction(type: ProfileActionType.following, name: "", icon: "")
        self.delegate?.onActionPress(action: action)
    }
    
    @IBAction func onAnalyticsPress(_ sender: Any?) {
        let action = ProfileAction(type: ProfileActionType.analytics, name: "", icon: "")
        self.delegate?.onActionPress(action: action)
    }
    
    @IBAction func onBackPress(_ sender: Any?) {
        let action = ProfileAction(type: ProfileActionType.back, name: "", icon: "")
        self.delegate?.onActionPress(action: action)
    }
    
    func binding(user: User, isMe: Bool = true, imageAvatar: UIImage? = nil) {
        if imageAvatar != nil {
            self.imageAvatar?.image = imageAvatar!
        } else {
            self.imageAvatar?.kf_setImage(url: user.avatarUrl)
        }
        self.isMe = isMe
        if !isMe {
//            self.buttonSetting?.setImage(UIImage(named: user.followed ? "icon_unfollowuser" : "icon_profile_add"), for: UIControlState.normal)
            self.buttonBack?.isHidden = false
            self.buttonAnalytics?.setImage(UIImage(named: "icon_profile_chat"), for: UIControlState.normal)
            if user.followed {
                self.buttonSetting?.setImage(UIImage(named: "ic_user_followed"), for: UIControlState.normal)
            } else {
                self.buttonAnalytics?.isHidden = true
                self.buttonSetting?.setImage(UIImage(named: "icon_profile_add"), for: UIControlState.normal)
            }
        } else {
            self.buttonBack?.isHidden = true
        }
        
        self.labelFullname?.text = user.fullname
        self.labelNickname?.text = user.nickname.isEmpty ? "Edit your nickname" : ("@" + user.nickname) //user.email
        //
        let attrString = NSMutableAttributedString()
        attrString.append(NSAttributedString(string: (user.following).description, attributes: [NSAttributedStringKey.font: UIFont(name: "SanFranciscoDisplay-Bold", size: 15)!]))
        attrString.append(NSAttributedString(string: " Following"))
        self.labelFollowing?.attributedText = attrString
        
        //
        let followerString = NSMutableAttributedString()
        followerString.append(NSAttributedString(string: (user.followers).description, attributes: [NSAttributedStringKey.font: UIFont(name: "SanFranciscoDisplay-Bold", size: 15)!]))
        followerString.append(NSAttributedString(string: " Followers"))
        self.labelFollower?.attributedText = followerString
        //
        self.labelDob?.text = Utility.shared.intToMonth(intMonth: user.dob?.month ?? 0) + " \(user.dob?.day ?? 1)"
        self.labelDescription?.text = user.description.isEmpty ? "Missing description" : user.description
        ///
        guard let address = user.address else { return }
        if address.city.isEmpty {
            self.labelAddress?.text = "No address"
        } else {
            self.labelAddress?.text = "\(address.city), \(address.state), \(address.country)"
        }
    }
    
}
