//
//  ProfileMeViewController.swift
//  VZONB
//
//  Created by Phuc on 7/21/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import Kingfisher
import SVPullToRefresh

// MARK: - Use on this file, ProfileSettingVC.swift, ProfileSettingCell.swift, ProfileSettingInfo.swift
protocol OnProfileAction: class {
    func onActionPress(action: ProfileAction)
}


final class ProfileMeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView?
    fileprivate var currentStyle: UIStatusBarStyle = UIStatusBarStyle.default
    fileprivate weak var header : ProfileMeHeader?
    fileprivate var headerHight: CGFloat = 0
    ///
    var isMe        : Bool = true
    var id : String = ""
    //
    fileprivate var imageAvatar: UIImage? = nil
    fileprivate var lastPage    : UInt8 = 1
    fileprivate var current     : UInt8 = 1
    //
    var currentIndex: Int = 0
    fileprivate var profile     : User?
    var records     : Array<Record> = Array<Record>()
    
    fileprivate var timerGuard: Timer?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
    override func viewDidLoad() {
        self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: UIBarButtonItemStyle.done, target: nil, action: nil)
        super.viewDidLoad()
        
        self.tableView?.register(UINib(nibName: "ProfileTopSongCell", bundle: nil), forCellReuseIdentifier: "topSongCell")
        self.tableView?.register(UINib(nibName: "HomePageCell", bundle: nil), forCellReuseIdentifier: "homePageCell")
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        
        if self.isMe {
            self.id = Setting.shared.getUserId()
        }
        
        self.addInfiniteScroll()
        
        if self.isMe {
            NotificationCenter.default.addObserver(self, selector: #selector(self.onUpdateProfile(_:)), name: NSNotification.Name.init(NotiName.updateProfile), object: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onUpdateLikeState(_:)), name: NSNotification.Name.init(NotiName.homeUpdate), object: nil)
        self.getInfo()
    }
    
    private func getInfo () {
        let group = DispatchGroup()
        let otherQueue = DispatchQueue(label: "com.vzonb.inforThread")
        self.showLoading()
        group.enter()
        otherQueue.async {
            UserService.shared.getInfo(id: self.id, { [weak self] (result) in
                guard let `self` = self else { return }
                self.profile = result.item
                group.leave()
            })
        }
        
        group.enter()
        otherQueue.async { [weak self] in
            guard let `self` = self else { return }
            UserService.shared.getRecords(id: self.id, callback: { (result) in
                self.records = result.items
                self.current = result.current
                self.lastPage = result.lastPage
                group.leave()
            })
        }
        
        group.notify(queue: otherQueue) {
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                self.hideLoading()
                self.tableView?.reloadData()
            }
        }
    }
    
    // Clean timer if needed
    @objc private func stopAnimateIfNeeded() {
        if UInt(self.tableView?.infiniteScrollingView.state ?? 0) == SVInfiniteScrollingStateLoading {
            self.tableView?.infiniteScrollingView.stopAnimating()
        }
        ///
        if self.timerGuard != nil {
            self.timerGuard?.invalidate()
            self.timerGuard = nil
        }
    }
    
    private func addInfiniteScroll() {
        self.tableView?.addInfiniteScrolling(actionHandler: { [weak self] in
            guard let `self` = self else { return }
            let next = self.current + 1
            if next <= self.lastPage {
                
                /// reset timer
                if self.timerGuard != nil {
                    self.timerGuard?.invalidate()
                    self.timerGuard = nil
                }

                self.timerGuard = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.stopAnimateIfNeeded), userInfo: nil, repeats: false)
                
                UserService.shared.getRecords(id: self.id, page: next, callback: { [weak self] (result) in
                    guard let `self` = self else {return}
                    if result.status == 200 {
                        self.current = result.current
                        var indexs = Array<IndexPath>()
                        let count = self.records.count
                        for iter in 0..<result.items.count {
                            indexs.append(IndexPath(row: count + iter, section: 1))
                        }
                        self.records.append(contentsOf: result.items)
                        self.tableView?.beginUpdates()
                        self.tableView?.insertRows(at: indexs, with: .automatic)
                        self.tableView?.endUpdates()
                    }
                    self.tableView?.infiniteScrollingView.stopAnimating()
                    
                    self.stopAnimateIfNeeded()
                })
            } else {
                self.tableView?.showsInfiniteScrolling = false
                self.tableView?.infiniteScrollingView.stopAnimating()
            }
        })
    }
    
    @objc func onUpdateLikeState(_ noti: Notification) {
        if let cell = noti.object as? HomePageCell {
            self.records[cell.index] = cell.record
        }
    }
    
    @objc func onUpdateProfile(_ noti: Notification) {
        
        if let image = noti.object as? UIImage {
            self.header?.imageAvatar?.image = image
            self.imageAvatar = image
        } else {
            KingfisherManager.shared.cache.removeImage(forKey: GlobalInfo.shared.info?.avatarUrl ?? "")
            UserService.shared.getInfo(id: self.id, { (result) in
                if result.status == 200 {
                    self.profile = result.item
                    DispatchQueue.main.async { [weak self] in
                        guard let `self` = self else { return }
                        self.header?.binding(user: result.item)
                    }
                }
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.init(NotiName.homeUpdate), object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
}

extension ProfileMeViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : self.records.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "topSongCell", for: indexPath) as! ProfileTopSongCell
            cell.binding(top5: self.profile?.top5 ?? Array<Record>())
//            cell.delegate = self
            cell.constraintHeight?.constant = (self.view.bounds.width * 9 / 16) * 2
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "homePageCell", for: indexPath) as! HomePageCell
        cell.binding(record: self.records[indexPath.row], index: indexPath.row, isProfile: true)
        cell.delegate = self
        if self.records[indexPath.row].type == "video" {
            cell.constraintCoverHeight?.constant = self.view.bounds.width * 13 / 16
        } else {
            cell.constraintCoverHeight?.constant = self.view.bounds.width * 11 / 16
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 345
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            if self.header == nil {
                self.header = Bundle.main.loadNibNamed("ProfileMeHeader", owner: nil, options: nil)?.first as? ProfileMeHeader
            }
            self.header?.delegate = self
            if profile != nil {
                self.header?.binding(user: self.profile!, isMe: self.isMe)
            }
            return self.header
        } else {
            let uiview = UIView(frame: CGRect(x: 20, y: 0, width: self.view.bounds.width - 20, height: 30))
            uiview.backgroundColor = UIColor.white
            let title = UILabel()
            title.frame = CGRect(x: 20, y: 0, width: self.view.bounds.width - 20, height: 30)
            title.text = "New recordings"
            title.font = UIFont(name: "SanFranciscoDisplay-Semibold", size: 15)!
            uiview.addSubview(title)
            
            return uiview
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return UITableViewAutomaticDimension
        }
        return 30
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 30 : 800
    }
}

extension ProfileMeViewController: HomeOnListenAction {
    func userDidClickMore(at cell: HomePageCell, _ index: Int) {
        if let commentVc = StoryBoard.Home.viewController("CommentsViewController") as? CommentsViewController {
            commentVc.recordId = cell.record.id
            commentVc.record    = cell.record
            commentVc.delegate  = self
            self.currentIndex = index
            self.navigationController?.pushViewController(commentVc, animated: true)
        }
    }
    func userDidClick(at cell: HomePageCell, of users: Array<ShortUser>) {
        if let vc = StoryBoard.User.viewController("UserTaggedViewController") as? UserTaggedViewController {
            vc.users = users
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func onPress(_ record: Record, action: ActionType, index: Int) {
        switch action {
        case .comment:
            if let commentVc = StoryBoard.Home.viewController("CommentsViewController") as? CommentsViewController {
                commentVc.recordId = record.id
                commentVc.record    = record
                commentVc.delegate  = self
                self.currentIndex = index
                self.navigationController?.pushViewController(commentVc, animated: true)
            }
        case .revote:
            print("Revoted")
        case .play:
            DispatchQueue.main.async { [unowned self] in
                if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                    self.records[index].views += 1
                    playVc.record = record
                    self.currentIndex = index
                    if !record.current_contest.isEmpty {
                        playVc.contestID = record.current_contest
                    }
                    if self.records.count > index {
                        self.tableView?.reloadRows(at: [IndexPath(row: index, section: 1)], with: UITableViewRowAnimation.fade)
                    }
                    ///
                    if record.current_contest.count == 0 {
                        playVc.records = self.records.filter(){
                            if $0.current_contest.count == 0 {
                                return true
                            } else {
                                return false
                            }
                        }
                        playVc.currentIndex = index
                    } else {
                        self.onPress(record, action: ActionType.contest, index: index)
                        return
                    }
                    ///
                    playVc.delegate = self
                    
                    self.navigationController?.pushViewController(playVc, animated: true)
                }
            }
        case .report:
            print("reported")
        case .share: //Share
            let shareText: String = "\(VZONBDomain.shareApi)record/\(record.id)"
            guard let url = URL(string: shareText) else { return }
            let text = record.resourceName
            let activityViewController = UIActivityViewController(activityItems: [url, text], applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
        case .top5:
            guard let top5 = self.profile?.top5 else { return }
            if index >= top5.count { return }
            if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                playVc.record = top5[index]
                self.navigationController?.pushViewController(playVc, animated: true)
            }
        case .delete:
            self.showMessageWithAction(with: "Notice", message: "Your song will be delete permantly", title: "Delete", cancelTitle: "Cancel", okStyle: .destructive, completion: { [weak self] in
                guard let `self` = self else { return }
                var links: Array<String> = [record.url]
                if record.type == "video" {
                    links.append(record.coverPath)
                }
                RecordService.shared.deleteBy(id: record.id, link: links)
                self.records.remove(at: index)
                self.tableView?.reloadData()
            })
        case .contest:
//            let desVC = ContestViewController(nibName: "ContestViewController", bundle: nil)
//            desVC.contestID = record.current_contest
//            self.navigationController?.pushViewController(desVC, animated: true)
            if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController {
                playVc.record = record
                playVc.contestID = record.current_contest
                playVc.delegate = self
                self.navigationController?.pushViewController(playVc, animated: true)
            }
        case .update:
            self.records[index] = record
        default: break
        }
    }
}

extension ProfileMeViewController: UpdateRecord {
    func update(_ record: Record) {
        self.records[currentIndex] = record
        DispatchQueue.main.async {
            self.tableView?.reloadRows(at: [IndexPath(row: self.currentIndex, section: 1)], with: .automatic)
        }
    }
}

extension ProfileMeViewController: OnProfileAction {
    func onActionPress(action: ProfileAction) {
        switch action.type {
        case .setting:
            if self.isMe {
                if let settingVC = StoryBoard.User.viewController("ProfileSettingViewController") as? ProfileSettingViewController {
                    settingVC.imageAvatar = self.imageAvatar
                    self.navigationController?.pushViewController(settingVC, animated: true)
                }
            } else {
                
            }
        case .analytics:
            if self.isMe {
                print("Analytics here")
            } else {
                if let vc = StoryBoard.Message.viewController("MessengerDetailViewController") as? MessengerDetailViewController {
                    vc.avatarUrl = profile?.avatarUrl ?? ""
                    vc.idUserChatting = profile?.id ?? ""
//                    vc.name = profile?.fullname ?? ""
                    vc.isNewChat = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        case .follow:
            let follow = !(self.profile?.followed ?? false)
            self.profile?.followed = follow
            UserService.shared.followUserBy(id: profile?.id ?? "", follow: follow, callback: { (result) in
                print(result)
            })
        case .back:
            self.navigationController?.popViewController(animated: true)
        case .follower:
            if let follow = StoryBoard.User.viewController("ProfileFollowersViewController") as? ProfileFollowersViewController {
                follow.id = self.id
                self.navigationController?.pushViewController(follow, animated: true)
            }
        case .following:
            if let follow = StoryBoard.User.viewController("ProfileFollowersViewController") as? ProfileFollowersViewController {
                follow.id = self.id
                follow.isFollower = false
                self.navigationController?.pushViewController(follow, animated: true)
            }
        default:
            break
        }
    }
}


