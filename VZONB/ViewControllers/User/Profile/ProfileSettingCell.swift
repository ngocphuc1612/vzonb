//
//  ProfileSettingCell.swift
//  VZONB
//
//  Created by Phuc on 7/26/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ProfileSettingCell: UITableViewCell {

    @IBOutlet weak var imageIcon    : UIImageView?
    @IBOutlet weak var labelSetting : UILabel?
    
    var action: ProfileAction?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func binding(action: ProfileAction) {
        self.imageIcon?.image   = UIImage(named: "icon_profile_".appending(action.icon))
        self.labelSetting?.text = action.name
        self.action = action
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}
