//
//  ProfileSettingInfo.swift
//  VZONB
//
//  Created by Phuc on 7/26/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ProfileSettingInfo: UITableViewCell {

    @IBOutlet weak var imageAvatar  : CircleImage?
    @IBOutlet weak var labelName    : UILabel?
    
//    weak var delegate: OnProfileAction? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func binding(avatar: String, name: String, imageAvatar: UIImage? = nil) {
        if imageAvatar != nil {
            self.imageAvatar?.image = imageAvatar!
        } else {
            self.imageAvatar?.kf_setImage(url: avatar)
        }
        self.labelName?.text    = name
    }
    
//    @IBAction func onEditPress(_ sender: Any?) {
//        let action = ProfileAction(type: ProfileActionType.editProfile, name: "", icon: "")
//        self.delegate?.onActionPress(action: action)
//    }
    
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}
