//
//  ProfileSettingViewController.swift
//  VZONB
//
//  Created by Phuc on 7/27/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import CoreData
import Kingfisher
import SafariServices

class ProfileSettingViewController: BlackChildViewController {

    @IBOutlet weak var tableView    : UITableView?
    
    var imageAvatar: UIImage?
    fileprivate var actions: Array<ProfileAction> = [
        //First item for title
        ProfileAction(type: ProfileActionType.setting, name: "", icon: ""),
//        ProfileAction(type: ProfileActionType.saved, name: "Saved", icon: "saved"),
        ProfileAction(type: ProfileActionType.block, name: "Blocked Users", icon: "blocked"),
        ProfileAction(type: ProfileActionType.term, name: "Terms of Service", icon: "term"),
        ProfileAction(type: ProfileActionType.policy, name: "Privacy Policy", icon: "policy"),
//        ProfileAction(type: ProfileActionType.changePass, name: "Change Password", icon: "changePass"),
        ProfileAction(type: ProfileActionType.feedback, name: "Feedback", icon: "feedback"),
        ProfileAction(type: ProfileActionType.signout, name: "Sign Out", icon: "signout")
    ]
    
    fileprivate var documentURL : URL { return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! }
    
    override func viewDidLoad() {
//        self.navigationController?.isNavigationBarHidden = false
        if !Setting.shared.getFbSign() {
            self.actions.insert(ProfileAction(type: ProfileActionType.changePass, name: "Change Password", icon: "changePass"), at: 5)
        }
        super.viewDidLoad()
        self.title = "Settings"
        
        self.tableView?.register(ProfileSettingInfo.nib, forCellReuseIdentifier: ProfileSettingInfo.identifier)
        self.tableView?.register(ProfileSettingCell.nib, forCellReuseIdentifier: ProfileSettingCell.identifier)
        
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onUpdateProfile(_:)), name: NSNotification.Name.init(NotiName.updateProfile), object: nil)
    }
    
    @objc func onUpdateProfile(_ noti: Notification) {
        if let image = noti.object as? UIImage {
            if let cell = tableView?.cellForRow(at: IndexPath(row: 0, section: 0)) as? ProfileSettingInfo {
                cell.imageAvatar?.image = image
            }
        } else {
            KingfisherManager.shared.cache.removeImage(forKey: GlobalInfo.shared.info?.avatarUrl ?? "")
            self.tableView?.reloadRows(at: [IndexPath(row: 0, section: 0)], with: UITableViewRowAnimation.automatic)
        }
        
//        if noti.object != nil {
//            KingfisherManager.shared.cache.clearDiskCache()
//            KingfisherManager.shared.cache.clearMemoryCache()
//            KingfisherManager.shared.cache.removeImage(forKey: GlobalInfo.shared.info?.avatarUrl ?? "")
//        }
    }
    
}

extension ProfileSettingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.actions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileSettingInfo.identifier, for: indexPath) as! ProfileSettingInfo
            cell.binding(avatar: GlobalInfo.shared.info?.avatarUrl ?? "", name: GlobalInfo.shared.info?.fullname ?? "", imageAvatar: self.imageAvatar)
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileSettingCell.identifier, for: indexPath) as! ProfileSettingCell
        cell.binding(action: self.actions[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            if indexPath.row > 0 {
                if let cell = tableView.cellForRow(at: indexPath) as? ProfileSettingCell {
                    guard let action = cell.action else { return }
                    self.onActionPress(action: action)
                }       
            } else {
                if let editVc = StoryBoard.User.viewController("ProfileEditInfoViewController") as? ProfileEditInfoViewController {
                    editVc.imageAvatarBefore = self.imageAvatar
                    self.navigationController?.pushViewController(editVc, animated: true)
                }
            }
            tableView.reloadRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 64 : 50
    }
    
}

//Listen push navi event
extension ProfileSettingViewController: OnProfileAction {
    func onActionPress(action: ProfileAction) {
        switch action.type {
        case .editProfile:
            if let editVc = StoryBoard.User.viewController("ProfileEditInfoViewController") as? ProfileEditInfoViewController {
                editVc.imageAvatarBefore = self.imageAvatar
                self.navigationController?.pushViewController(editVc, animated: true)
            }
        case .changePass:
            if let changePassVc = StoryBoard.User.viewController("ProfileChangePasswordViewController") as? ProfileChangePasswordViewController {
                self.navigationController?.pushViewController(changePassVc, animated: true)
            }
        case .term:
                if let termVc = StoryBoard.User.viewController("ProfileWebTermViewController") as? ProfileWebTermViewController {
                    termVc.link = VZONBDomain.termOfServiceUrl
                    termVc.customTitle = "Terms of Service"
                    self.navigationController?.pushViewController(termVc, animated: true)
                }
        case .policy:
            if let termVc = StoryBoard.User.viewController("ProfileWebTermViewController") as? ProfileWebTermViewController {
                termVc.link = VZONBDomain.privacy
                termVc.customTitle = "Privacy Policy"
                self.navigationController?.pushViewController(termVc, animated: true)
            }
        case .signout:
            self.showMessageWithAction(with: "Notice", message: "Do you want to logout?", title: "Logout", cancelTitle: "Cancel", okStyle: UIAlertActionStyle.cancel, completion: {
                Setting.shared.clearToken()
                Setting.shared.clearUserId()
                Setting.shared.clearFbSign()
                VZOSocketManager.shared.socket.disconnect()
                
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                appDelegate?.showLoginPage()
                
                self.delAllRecorded()
            })
        case .feedback:
            self.showMessageForUser(with: "This function is developing, it will be released in as soon as possible")
        case .block:
            let vc = BlockUsersViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
    private func delAllRecorded () {
        DispatchQueue.global().async {
            let req:NSFetchRequest<NSManagedObject> = NSFetchRequest(entityName: "Recorded")
            do
            {
                let context = DataManager().managedObjectContext
                let contactsRusult = try context.fetch(req)
                var records: Array<Record> = Array<Record>()
                for sp in contactsRusult
                {
                    records.append(Record(perStore: sp))
                    context.delete(sp)
                }
                try context.save()
                // Delele local file
                
                print(self.documentURL.absoluteString)
                for record in records {
                    Utility.shared.deleteFiles(filesUrl: self.documentURL.appendingPathComponent(record.beatPath), self.documentURL.appendingPathComponent(record.voicePath))
                    if record.type == "video" {
                        try? FileManager.default.removeItem(at: self.documentURL.appendingPathComponent(record.videoPath ?? ""))
                    }
                }
            }
            catch
            {
                print("Loi \(error)")
            }
            
            
        }
    }
}


