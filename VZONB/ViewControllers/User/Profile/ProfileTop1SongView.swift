//
//  ProfileTop1SongView.swift
//  VZONB
//
//  Created by PT on 9/21/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ProfileTop1SongView: UICollectionReusableView {

    @IBOutlet weak var imageCover   : UIImageView?
    @IBOutlet weak var buttonPlay   : UIButton?
    
    private var record: Record?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.buttonPlay?.addTarget(self, action: #selector(self.onNavigateToDetail(_:)), for: UIControlEvents.touchUpInside)
    }
    
    @IBAction func onNavigateToDetail (_ sender: Any?) {
        guard record != nil else { return }
        DispatchQueue.main.async { [unowned self] in
            if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController, let record = self.record {
                playVc.record = record
                UIApplication.topViewController()?.navigationController?.pushViewController(playVc, animated: true)
            }
        }
    }
    
    func binding(record: Record) {
        self.record = record
        if !(record.coverPath.isEmpty) {
            self.imageCover?.kf_setImage(url: record.coverPath)
        } else {
            self.imageCover?.kf_setImage(url: record.resource?.thumbnail ?? "")
        }
    }
}
