//
//  ProfileTopSongCell.swift
//  VZONB
//
//  Created by Phuc on 7/21/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ProfileTopSongCell: UITableViewCell {
    
    var top5: Array<Record> = Array<Record>()
    
//    @IBOutlet weak var imageTop1    : UIImageView?
//    @IBOutlet weak var imageTop2    : UIImageView?
//    @IBOutlet weak var imageTop3    : UIImageView?
//    @IBOutlet weak var imageTop4    : UIImageView?
//    @IBOutlet weak var imageTop5    : UIImageView?
    
    weak var delegate: HomeOnListenAction? = nil
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var constraintHeight: NSLayoutConstraint?
    
    fileprivate var header: ProfileTop1SongView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView?.register(ProfileTopSongChildCell.nib, forCellWithReuseIdentifier: ProfileTopSongChildCell.identifier)
        self.collectionView?.register(UINib(nibName: "ProfileTop1SongView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ProfileTop1SongView")
        self.collectionView?.dataSource = self
        self.collectionView?.delegate   = self
        self.collectionView?.isScrollEnabled = false
        
    }
    
    
    
    func binding(top5: Array<Record>) {
//        self.border(imageViews: imageTop1, imageTop2, imageTop3, imageTop4, imageTop5)
        self.top5 = top5
        self.collectionView?.reloadData()
//        if self.top5.count > 0 {
//            self.imageTop1?.kf_setImage(url: self.top5[0].resource?.thumbnail ?? "")
//            self.imageTop1?.isUserInteractionEnabled = true
//            self.imageTop1?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowTopSong1)))
//        } else {
//            self.imageTop1?.image = UIImage(named: "image_placeholder")
//        }
//        if self.top5.count > 1 {
//            self.imageTop2?.kf_setImage(url: self.top5[1].resource?.thumbnail ?? "")
//            self.imageTop2?.isUserInteractionEnabled = true
//            self.imageTop2?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowTopSong2)))
//        } else {
//            self.imageTop2?.image = UIImage(named: "image_placeholder")
//        }
//        if self.top5.count > 2 {
//            self.imageTop3?.kf_setImage(url: self.top5[2].resource?.thumbnail ?? "")
//            self.imageTop3?.isUserInteractionEnabled = true
//            self.imageTop3?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowTopSong3)))
//        } else {
//            self.imageTop3?.image = UIImage(named: "image_placeholder")
//        }
//        if self.top5.count > 3 {
//            self.imageTop4?.kf_setImage(url: self.top5[3].resource?.thumbnail ?? "")
//            self.imageTop4?.isUserInteractionEnabled = true
//            self.imageTop4?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowTopSong4)))
//        } else {
//            self.imageTop4?.image = UIImage(named: "image_placeholder")
//        }
//        if self.top5.count > 4 {
//            self.imageTop5?.kf_setImage(url: self.top5[4].resource?.thumbnail ?? "")
//            self.imageTop5?.isUserInteractionEnabled = true
//            self.imageTop5?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowTopSong5)))
//        } else {
//            self.imageTop5?.image = UIImage(named: "image_placeholder")
//        }
    }
    
//    private func border(imageViews: UIImageView?...) {
//        for imageView in imageViews {
//            imageView?.layer.borderColor = UIColor.white.cgColor
//            imageView?.layer.borderWidth = 0.5
//            imageView?.layer.shadow()
//        }
//    }
    
//    func onShowTopSong1 () {
//        if self.top5.count > 0 {
//            self.delegate?.onPress(self.top5[0], action: .top5, index: 0)
//        }
//    }
//
//    func onShowTopSong2 () {
//        if self.top5.count > 1 {
//            self.delegate?.onPress(self.top5[1], action: .top5, index: 1)
//        }
//    }
//    func onShowTopSong3 () {
//        if self.top5.count > 2 {
//            self.delegate?.onPress(self.top5[2], action: .top5, index: 2)
//        }
//    }
//    func onShowTopSong4 () {
//        if self.top5.count > 3 {
//            self.delegate?.onPress(self.top5[3], action: .top5, index: 3)
//        }
//    }
//    func onShowTopSong5 () {
//        if self.top5.count > 4 {
//            self.delegate?.onPress(self.top5[4], action: .top5, index: 4)
//        }
//    }
}

extension ProfileTopSongCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ProfileTop1SongView", for: indexPath) as! ProfileTop1SongView
            self.header = header
            if top5.count > 0 {
                header.binding(record: top5[0])
            }
            return header
        }
        return UICollectionReusableView(frame: CGRect.zero)
//        assert(false, "Unexpected element kind")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.bounds.width - 6, height: (self.bounds.width - 6) * 9 / 16 + 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProfileTopSongChildCell.identifier, for: indexPath) as! ProfileTopSongChildCell
        if self.top5.count > indexPath.row + 1 {
            cell.binding(record: top5[indexPath.row + 1])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = self.bounds.width / 2 - 4.5
        return CGSize(width: width, height: width * 9 / 16)
    }
    
    
}


