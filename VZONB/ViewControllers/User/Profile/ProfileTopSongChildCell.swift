//
//  ProfileTopSongChildCell.swift
//  VZONB
//
//  Created by PT on 9/20/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ProfileTopSongChildCell: UICollectionViewCell {

    @IBOutlet weak var imageCover: UIImageView?
    @IBOutlet weak var buttonPlay: UIButton?

    fileprivate var record: Record?
    
    static var identifier: String { return String(describing: self) }
    static var nib: UINib { return UINib(nibName: self.identifier, bundle: nil) }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.buttonPlay?.addTarget(self, action: #selector(self.onNavigateToDetail(_:)), for: UIControlEvents.touchUpInside)
    }
    
    @IBAction func onNavigateToDetail (_ sender: Any?) {
        guard record != nil else { return }
        
        DispatchQueue.main.async { [unowned self] in
            if let playVc = StoryBoard.Home.viewController("DetailSongViewController") as? DetailSongViewController, let record = self.record {
                playVc.record = record
                UIApplication.topViewController()?.navigationController?.pushViewController(playVc, animated: true)
            }
        }
    }
    
    func binding(record: Record) {
        self.record = record
        if !(record.coverPath.isEmpty) {
            self.imageCover?.kf_setImage(url: record.coverPath)
        } else {
            self.imageCover?.kf_setImage(url: record.resource?.thumbnail ?? "")
        }
    }
    
}
