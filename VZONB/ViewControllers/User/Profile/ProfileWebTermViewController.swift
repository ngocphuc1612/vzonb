//
//  ProfileWebTermViewController.swift
//  VZONB
//
//  Created by PT on 9/17/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ProfileWebTermViewController: BlackChildViewController {

    @IBOutlet weak var webView: UIWebView?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
    var link: String = ""
    var customTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.loadWebViewFrom(url: link)
        self.title = customTitle
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    private func loadWebViewFrom(url: String) {
        guard let url = URL(string: url) else { return }
        self.webView?.loadRequest(URLRequest(url: url))
    }
    
    
}
