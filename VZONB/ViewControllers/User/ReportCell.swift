//
//  ReportCell.swift
//  VZONB
//
//  Created by Phuc on 7/30/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class ReportCell: UITableViewCell {

    @IBOutlet weak var viewCircle: UIView?
    @IBOutlet weak var labelName : UILabel?
    @IBOutlet weak var viewSeparator: UIView?
    
    var calayer:  CALayer = CALayer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layoutIfNeeded()
        self.viewCircle?.layer.cornerRadius = 7.5
        self.viewCircle?.layer.borderColor = UIColor(rgb: 0x0680bf).cgColor
        self.viewCircle?.layer.borderWidth = 1
        
        self.calayer.frame = CGRect(x: 2, y: 2, width: 11, height: 11)
        self.calayer.cornerRadius = 5.5
        self.calayer.masksToBounds  = true
        self.calayer.backgroundColor = UIColor(rgb: 0x0680bf).cgColor
//        self.layer.addSublayer(self.calayer)
        
    }

    func selected () {
        self.viewCircle?.layer.addSublayer(self.calayer)
    }
    
    func unselected() {
        self.calayer.removeFromSuperlayer()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.unselected()
        self.viewSeparator?.isHidden = false
//        self.viewCircle?.isHidden = true
    }
    
}
