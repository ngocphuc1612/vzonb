//
//  ReportViewController.swift
//  VZONB
//
//  Created by Phuc on 7/30/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

protocol OnReportSelect: class {
    func completeReport(reportVc: ReportViewController, report: FeedReport)
}

class ReportViewController: UIViewController {

    @IBOutlet weak var tableView : UITableView?
    @IBOutlet weak var constraintTableHeight: NSLayoutConstraint?
    @IBOutlet weak var viewWrapper: UIView?
    
//    var reports: Array<FeedReport> = Array<FeedReport>()
    var delegate: OnReportSelect? = nil
    var record: Record?
    var index: Int = 0
    fileprivate var currentIndex: Int = -1
    fileprivate let ANIMATE_DURATION: TimeInterval = 0.3
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        
        self.constraintTableHeight?.constant = CGFloat(Shared.feedReports.count * 35 + 10)
        
        self.viewWrapper?.layer.cornerRadius = 4
        self.viewWrapper?.layer.shadowColor = UIColor.black.cgColor
        self.viewWrapper?.layer.shadowOffset = CGSize(width: 1, height: 3)
        self.viewWrapper?.layer.shadowRadius = 10
        self.viewWrapper?.layer.shadowOpacity = 1
        self.viewWrapper?.clipsToBounds = true
        //
//        self.view.isUserInteractionEnabled = true
//        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onCancelPress)))
    }
    
    func onCancelPress () {
        //        self.view.removeFromSuperview()
        UIView.animate(withDuration: self.ANIMATE_DURATION, delay: 0, options: [.curveLinear], animations: {
            self.viewWrapper?.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5)
            self.viewWrapper?.alpha = 0
            self.view.alpha = 0
        }) { (_) in
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.viewWrapper?.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5)
        self.view.alpha = 0
        self.viewWrapper?.alpha = 0
        UIView.animate(withDuration: self.ANIMATE_DURATION, delay: 0, options: [UIViewAnimationOptions.curveLinear], animations: {
            self.viewWrapper?.layer.transform = CATransform3DIdentity
            self.viewWrapper?.alpha = 1
            self.view.alpha = 1
        }, completion: nil)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.view.gestureRecognizers = nil
    }
    
    @IBAction func onCancel (_ sender: Any?) {
        self.onCancelPress()
    }
    
    @IBAction func onOkPress(_ sender: Any?) {
        if self.currentIndex >= 0 && self.record != nil {
            self.delegate?.completeReport(reportVc: self, report: Shared.feedReports[self.currentIndex])
        }
        self.onCancelPress()
    }
    
}

extension ReportViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Shared.feedReports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ReportCell
        cell.labelName?.text = Shared.feedReports[indexPath.row].name
        if indexPath.row == Shared.feedReports.count - 1 {
            cell.viewSeparator?.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.currentIndex < 0 {
            let cell = tableView.cellForRow(at: indexPath) as? ReportCell
            self.currentIndex = indexPath.row
            cell?.selected()
        } else if self.currentIndex != indexPath.row {
            let oldCell = tableView.cellForRow(at: IndexPath(row: self.currentIndex, section: 0)) as? ReportCell
            oldCell?.unselected()
            let cell = tableView.cellForRow(at: indexPath) as? ReportCell
            self.currentIndex = indexPath.row
            cell?.selected()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    
}
