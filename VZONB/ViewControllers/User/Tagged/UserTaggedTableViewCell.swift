//
//  UserTaggedTableViewCell.swift
//  VZONB
//
//  Created by PT on 11/8/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class UserTaggedTableViewCell: UITableViewCell {

    @IBOutlet weak var imageAvatar      : CircleImage?
    @IBOutlet weak var labelName        : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func binding (user: ShortUser) {
        self.imageAvatar?.kf_setImage(url: user.avatar)
        self.labelName?.text = user.fullname
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
