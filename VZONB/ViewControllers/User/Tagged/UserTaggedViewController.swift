//
//  UserTaggedViewController.swift
//  VZONB
//
//  Created by PT on 11/8/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class UserTaggedViewController: DefaultChildViewController {

    @IBOutlet weak var tableView : UITableView?
    
    var users : Array<ShortUser> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Tagged Users"
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
}

extension UserTaggedViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UserTaggedTableViewCell
        cell.binding(user: self.users[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if users[indexPath.row].id == Setting.shared.getUserId() {
            return
        }
        if let profileVc = StoryBoard.Home.viewController("ProfileMeViewController") as? ProfileMeViewController {
            profileVc.isMe = false
            profileVc.id   = users[indexPath.row].id
            self.navigationController?.pushViewController(profileVc, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 //UITableViewAutomaticDimension
    }
}
