//
//  UserLoginViewController.swift
//  VZONB
//
//  Created by Phuc on 5/25/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class UserLoginViewController: UIViewController {

    ///
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    private var isFirst: Bool = true
    private var isSave: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ///
        self.configUIs()
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isFirst {
            self.isFirst = false
            self.textFieldEmail.becomeFirstResponder()
        }
    }
    
    //
    private func configUIs () {
        // -> Navigation
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        //
        self.textFieldEmail.delegate = self
        self.textFieldPassword.delegate = self
        //
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard)))
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func onForgotPassPress (_ sender: Any?) {
        if let vc = StoryBoard.User.viewController("ForgotEnterEmailViewController") as? ForgotEnterEmailViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onLoginButtonClick(_ sender: UIButton?) {
        
        let email: String = self.textFieldEmail.text!
        let password: String = self.textFieldPassword.text!
        
        if email.isEmpty || password.isEmpty {
            //
            self.showPopup(title: "Notice", description: "Please complete your info to login")
            return
        }
        self.showLoading()
        AuthService.shared.login(email: email, password: password) { [weak self] (result) in
            guard let `self` = self else { return }
            self.hideLoading()
            if result.status == 200 {
                //Login success
                Setting.shared.setToken(result.item.token)
                Setting.shared.setUserId(result.item.user.id)
                if self.isSave {
                    Setting.shared.setSave(true)
                }
                //Go to home Page
                if let homeVc = StoryBoard.Home.viewController("HomeTabBar") as? UITabBarController {
                    self.present(homeVc, animated: true, completion: nil)
                }
            } else {
                self.showPopup(title: "Notice", description: result.message)
            }
        }
        
    }
    
    @IBAction func onSwitchSavePass(_ sender: CheckButton) {
        sender.isChecked = !sender.isChecked
        self.isSave = sender.isChecked
    }
    
    ///
    @IBAction func onBackPress () {
        self.navigationController?.popViewController(animated: true)
    }
}

extension UserLoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.textFieldEmail {
            self.textFieldPassword.becomeFirstResponder()
            return false
        }
        self.onLoginButtonClick(nil)
        self.textFieldPassword.resignFirstResponder()
        return false
    }
}

//MARK: - Add pop gesture
extension UserLoginViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
