//
//  UserRegisterS1ViewController.swift
//  VZONB
//
//  Created by Phuc on 6/20/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class UserRegisterS1ViewController: UIViewController {

    ///
    @IBOutlet weak var textFieldFirstname: UITextField!
    @IBOutlet weak var textFieldLastname: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    ///
    private var isFirst: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ///
        self.configUIs()
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        if self.isFirst {
//            self.isFirst = false
//            self.textFieldFirstname.becomeFirstResponder()
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isFirst {
            self.isFirst = false
            self.textFieldFirstname.becomeFirstResponder()
        }
    }
    //
    private func configUIs () {
        // -> Navigation
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        self.textFieldFirstname.delegate = self
        self.textFieldLastname.delegate = self
        self.textFieldEmail.delegate = self
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard)))
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    /// Transfer data to next screen
    @IBAction func onSendDataToRegister(_ sender: Any?) {
        let firstname: String = self.textFieldFirstname.trimText()
        let lastname: String = self.textFieldLastname.trimText()
        let email: String = self.textFieldEmail.trimText()
        ///
        if firstname.isEmpty || lastname.isEmpty || email.isEmpty {
            self.showPopup(title: "Notice", description: "Please complete all fields above")
            return
        }
        
        if !email.isEmail() {
            self.showPopup(title: "Notice", description: "Please enter your correct email")
            return
        }
        self.showLoading()
        UserService.shared.checkEmail(email) { [weak self] (result) in
            guard let `self` = self else { return }
            self.hideLoading()
            if result.status == 200 {
                if result.item.success {
                    if let step2Register: UserRegisterS2ViewController = StoryBoard.User.viewController("UserRegisterS2ViewController") as? UserRegisterS2ViewController {
                        step2Register.userRegister = User(firstname: firstname, lastname: lastname, email: email)
                        self.navigationController?.pushViewController(step2Register, animated: true)
                    }
                } else {
                    self.showMessageForUser(with: "Email was already used. Please try another one")
                }
            } else {
                self.showMessageForUser(with: result.message)
            }
        }
        
        
    }
    
    ///
    @IBAction func onBackPress () {
        self.navigationController?.popViewController(animated: true)
    }
}

extension UserRegisterS1ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.textFieldFirstname {
            self.textFieldLastname.becomeFirstResponder()
            return true
        }
        if textField == self.textFieldLastname {
            self.textFieldEmail.becomeFirstResponder()
            return true
        }
        self.onSendDataToRegister(nil)
        //If fail. this line will not run :)
        self.textFieldEmail.resignFirstResponder()
        return true
    }
}

//MARK: - Add pop gesture
extension UserRegisterS1ViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
