//
//  UserRegisterS2ViewController.swift
//  VZONB
//
//  Created by Phuc on 6/20/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

class UserRegisterS2ViewController: UIViewController {
    
    ///
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldConfirm: UITextField!
    
    var userRegister: User?
    private var isFirst: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ///
        self.configUIs()
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        //
//        if self.isFirst {
//            self.isFirst = false
//            self.textFieldPassword.becomeFirstResponder()
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isFirst {
            self.isFirst = false
            self.textFieldPassword.becomeFirstResponder()
        }
    }
    
    //
    private func configUIs () {
        // -> Navigation
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.textFieldPassword.delegate = self
        self.textFieldConfirm.delegate = self
        //
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard)))
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    //
    @IBAction func onRegisterUser (_ sender: Any?) {
        
        guard var user: User = self.userRegister else { return }
        
        let password: String = self.textFieldPassword.trimText()
        let confirm: String = self.textFieldConfirm.trimText()
        ///
        if password.isEmpty || password != confirm || self.userRegister == nil{
//            self.showPopup(title: "Notice", description: "Please fill out your password")
            self.showMessageForUser(with: "Please fill out your password")
            return
        } else if password.count < 6 {
            self.showMessageForUser(with: "Password must be at least 6 characters")
            return
        }
        user.password = password
        self.showLoading()
        // Register API here
        UserService.shared.createNew(user: user) { result in
            self.hideLoading()
            if result.status == 200 {
//                self.showPopup(title: "Notice", description: "Your account has been created. Please check your email to comfirm and active your account") { [weak self] in
//                    guard let `self` = self else { return }
//                    self.navigationController?.popToRootViewController(animated: true)
//                }
                
                MessageManager.shared.show(title: "Congratulations", body: "Your account have been created successfully")
                self.navigationController?.popToRootViewController(animated: true)
            } else {
//                self.showPopup(title: "Notice", description: result.message)
                self.showMessageForUser(with: result.message)
            }
        }
    }
    
    ///
    @IBAction func onBackPress () {
        self.navigationController?.popViewController(animated: true)
    }
}

extension UserRegisterS2ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.textFieldPassword {
            self.textFieldConfirm.becomeFirstResponder()
            return true
        }
        self.onRegisterUser(nil)
        self.textFieldConfirm.resignFirstResponder()
        return true
    }
}

//MARK: - Add pop gesture
extension UserRegisterS2ViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
