//
//  ViewController.swift
//  VZONB
//
//  Created by PT on 8/18/17.
//  Copyright © 2017 Phuc. All rights reserved.
//

import UIKit

//MARK: - Black style

class BlackHomeViewController: UIViewController {
//    func configNavigation () {
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
}

class BlackChildViewController: BlackHomeViewController {
    func configNavigation() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_back_black"), style: UIBarButtonItemStyle.done, target: self, action: #selector(self.onDismiss))
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configNavigation()
    }
    @objc func onDismiss () {
        self.navigationController?.popViewController(animated: true)
    }
}
extension BlackChildViewController: UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

//MARK: - Default app style
class DefaultHomeViewController: UIViewController {
    func configNavigation () {
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationBar.barTintColor = UIColor(rgb: 0xF9F9F9) //UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: Font.header]
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configNavigation()
        UIApplication.shared.statusBarStyle = .default
    }
}
class DefaultChildViewController: DefaultHomeViewController {
    override func configNavigation() {
        super.configNavigation()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_back_black"), style: UIBarButtonItemStyle.done, target: self, action: #selector(self.onDismiss))
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    @objc func onDismiss () {
        self.navigationController?.popViewController(animated: true)
    }
}
extension DefaultChildViewController: UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

